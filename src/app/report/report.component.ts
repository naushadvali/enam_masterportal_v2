import { Component , ViewChild, ElementRef, AfterViewInit,OnInit} from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'report',
  templateUrl: 'report.component.html',
  styleUrls: ['report.component.css']
})
export class ReportComponent implements OnInit {
  data=[];
  bidReportData=[];
  reportFromDate:string;
  reportToDate:string;
  call0:string;
  call1:string;
  call2:string;
  dateInsert:any;
  dateChange:any;
  monthChnage:any;
  dateChange1:any;
  monthChnage1:any;
  fromDate:string;
  toDate:string;
  reportData=[];
  payOrgID:any;
  payAggrOprID:any;
  payTrdID:any;
  payInvoiceNo:any;
  payAggrNo:any;
  payInvoiceAmt:any;
  payMode:any;
  farmerName:any;
  lotCode:any;
  agentName:any;
  payOrdID:any;
  auchInvoiceDate:any;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) {}

  ngOnInit() {}

    getReport(){

        if(this.reportFromDate==undefined && this.reportToDate==undefined){
          this.flashMessagesService.show('Date is mandatory to search .', { cssClass: 'alert-danger', timeout: 3000 });

        }else{


          //for from date
          const reportFromDate={
            day:this.reportFromDate["date"]["day"],
            month:this.reportFromDate["date"]["month"],
            year:this.reportFromDate["date"]["year"]
          }


      	  this.dateChange=this.reportFromDate["date"]["day"];
      	  for(var i=1;i<=9;i++){
      		  if(this.dateChange==i){
      			  let zero=0;
      			  let x:any;
      			  let y:any;

      			  x=zero.toString()+(this.dateChange).toString();
              this.dateChange=x;
      		  }
      	  }
    	  this.monthChnage=this.reportFromDate["date"]["month"];

    	  for(var j=1;j<=9;j++){
    		  if(this.monthChnage==j){
    			  let zero1=0;
    			  let m:any;

    			  m=zero1.toString()+(this.monthChnage).toString();

    			  this.monthChnage=m;
    		  }
    	  }

          this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];


          //For to date
          const reportToDate={
            day:this.reportToDate["date"]["day"],
            month:this.reportToDate["date"]["month"],
            year:this.reportToDate["date"]["year"]
          }


      	  this.dateChange1=this.reportToDate["date"]["day"];
      	  for(var i=1;i<=9;i++){
      		  if(this.dateChange1==i){
      			  let zero=0;
      			  let x:any;
      			  let y:any;

      			  x=zero.toString()+(this.dateChange1).toString();
              this.dateChange1=x;
      		  }
      	  }
    	  this.monthChnage1=this.reportToDate["date"]["month"];

    	  for(var j=1;j<=9;j++){
    		  if(this.monthChnage1==j){
    			  let zero1=0;
    			  let m:any;


    			  m=zero1.toString()+(this.monthChnage1).toString();

    			  this.monthChnage1=m;
    		  }
    	  }

          this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportToDate["date"]["year"];

          this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];



          const dateJson={
            fromDate:this.fromDate,
            toDate:this.toDate
          }
          this.authservice.getReport(dateJson).subscribe
          (
            $data=>{
              //console.log("report data are",$data.listData);
              this.reportData=$data.listData;
              let payOrgID;
              let payAggrOprID;
              let payTrdID;
              let payInvoiceNo;
              let payAggrNo;
              let payInvoiceAmt;
              let payMode;
              let farmerName;
              let lotCode;
              let agentName;
              let payOrdID;
              let auchInvoiceDate;

              for(var i=0;i<this.reportData.length;i++){
                payOrgID=this.reportData[i].payOrgID;
                payAggrOprID=this.reportData[i].payAggrOprID;
                payTrdID=this.reportData[i].payTrdID;
                payInvoiceNo=this.reportData[i].payInvoiceNo;
                payAggrNo=this.reportData[i].payAggrNo;
                payInvoiceAmt=this.reportData[i].payInvoiceAmt;
                payMode=this.reportData[i].payMode;
                farmerName=this.reportData[i].farmerName;
                lotCode=this.reportData[i].lotCode;
                agentName=this.reportData[i].agentName;
                payOrdID=this.reportData[i].payOrdID;
                auchInvoiceDate=this.reportData[i].auchInvoiceDate;
              }
                this.payOrgID=payOrgID;
                this.payAggrOprID=payAggrOprID;
                this.payTrdID=payTrdID;
                this.payInvoiceNo=payInvoiceNo;
                this.payAggrNo=payAggrNo;
                this.payInvoiceAmt=payInvoiceAmt;
                this.payMode=payMode;
                this.farmerName=farmerName;
                this.lotCode=lotCode;
                this.agentName=agentName;
                this.payOrdID=payOrdID;
                this.auchInvoiceDate=auchInvoiceDate;

            }
          );
        }

    }

    //Online receipt button
    onReceipt(post:any){
      const receiptJson={
        payOrgID:post.payOrgID,
        payAggrOprID:post.payAggrOprID,
        payTrdID:post.payTrdID,
        payInvoiceNo:post.payInvoiceNo,
        payAggrNo:post.payAggrNo,
        payInvoiceAmt:post.payInvoiceAmt,
        payMode:post.payMode,
        farmerName:post.farmerName,
        lotCode:post.lotCode,
        agentName:post.agentName,
        payOrdID:post.payOrdID,
        auchInvoiceDate:post.auchInvoiceDate,
      }
      this.authservice.getOnlinereceipt(receiptJson).subscribe(res=>{
        //console.log("Online receipt data",res);
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
        })
      }


      //Settlement button
      onSettlement(post:any){
        const settlementJson={
          payOrgID:post.payOrgID,
          payAggrOprID:post.payAggrOprID,
          payTrdID:post.payTrdID,
          payInvoiceNo:post.payInvoiceNo,
          payAggrNo:post.payAggrNo,
          payInvoiceAmt:post.payInvoiceAmt,
          payMode:post.payMode,
          farmerName:post.farmerName,
          lotCode:post.lotCode,
          agentName:post.agentName,
          payOrdID:post.payOrdID,
          auchInvoiceDate:post.auchInvoiceDate,
        }
        this.authservice.getSettlementresult(settlementJson).subscribe(ret=>{
          //console.log("Settlement report data",ret);
          var fileURL = URL.createObjectURL(ret);
          window.open(fileURL);
          })
        }

    public myDatePickerOptions: IMyDpOptions = {
      // other options...
      dateFormat: 'yyyy-mm-dd',
    };


}
