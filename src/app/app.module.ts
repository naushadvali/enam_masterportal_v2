import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes} from '@angular/router';
import { DataTableModule } from "angular2-datatable";
import { MyDatePickerModule } from 'mydatepicker';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SelectModule } from 'ng2-select';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { DOCUMENT } from '@angular/platform-browser';
import { MdTooltipModule,MdButtonModule, MdCheckboxModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting
import { LoadingModule} from 'ngx-loading';
import { DragulaModule } from 'ng2-dragula';
import { environment } from '../environments/environment';
//services
import { AuthGuard } from './guards/auth.guards';
import { AuthService } from './services/auth.service';
import { ClockService } from './services/clock.service';
import { ValidateService } from './services/validate.service';
import { RoleAccessService } from './services/role-access.service';

//components

import { AppComponent } from './app.component';
import { BankdetailsComponent } from './bankdetails/bankdetails.component';
import { BidhistoryComponent } from './bidhistory/bidhistory.component';
import { TimerComponent } from './timer/timer.component';
import { ReportComponent } from './report/report.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent} from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NewbidlistingComponent } from './newbidlisting/newbidlisting.component';
import { CommoditybidlistingComponent } from './commoditybidlisting/commoditybidlisting.component';

import { OrganisationMasterComponent } from './administration/masters/organisation-master/organisation-master.component';
import { OperatingUnitComponent } from './administration/masters/operating-unit/operating-unit.component';
import { UserMasterApmcComponent } from './administration/masters/user-master-apmc/user-master-apmc.component';
import { UserMasterStateComponent } from './administration/masters/user-master-state/user-master-state.component';
import { UserMasterGlobalComponent } from './administration/masters/user-master-global/user-master-global.component';
import { ConfigureDocGenSchemesComponent } from './administration/masters/configure-doc-gen-schemes/configure-doc-gen-schemes.component';
import { GeneralMasterComponent } from './administration/masters/general-master/general-master.component';

import { SystemParameterLegalEntityComponent } from './administration/masters/system-parameter-legal-entity/system-parameter-legal-entity.component';
import { RoleMasterComponent } from './administration/masters/role-master/role-master.component';
import { SystemParameterOperatingUnitComponent } from './administration/masters/system-parameter-operating-unit/system-parameter-operating-unit.component';
import { MasterSetupReportComponent } from './administration/reports/master-setup-report/master-setup-report.component';
import { FeeComponentApmcApplicabilityComponent } from './entry_exit/masters/fee-component-apmc-applicability/fee-component-apmc-applicability.component';
import { CommodityMasterComponent } from './entry_exit/masters/commodity-master/commodity-master.component';
import { VehicleRateComponent } from './entry_exit/masters/vehicle-rate/vehicle-rate.component';
import { CommodityMasterApmcApplicabilityComponent } from './entry_exit/masters/commodity-master-apmc-applicability/commodity-master-apmc-applicability.component';
import { CommodityGroupComponent } from './entry_exit/masters/commodity-group/commodity-group.component';
import { FeeComponentComponent } from './entry_exit/masters/fee-component/fee-component.component';
import { FeeCategoryComponent } from './entry_exit/masters/fee-category/fee-category.component';
import { AgentRegistrationComponent } from './entry_exit/masters/agent-registration/agent-registration.component';
import { FarmerRegistrationComponent } from './entry_exit/masters/farmer-registration/farmer-registration.component';
import { BagTypeComponent } from './entry_exit/masters/bag-type/bag-type.component';
import { VehicleRegistrationComponent } from './entry_exit/transactions/vehicle-registration/vehicle-registration.component';
import { GateEntryComponent } from './entry_exit/transactions/gate-entry/gate-entry.component';
import { VehicleGateEntryComponent } from './entry_exit/transactions/vehicle-gate-entry/vehicle-gate-entry.component';
import { LotEntryComponent } from './entry_exit/transactions/lot-entry/lot-entry.component';
import { GateExitComponent } from './entry_exit/transactions/gate-exit/gate-exit.component';
import { GateEntryNewComponent } from './entry_exit/transactions/gate-entry-new/gate-entry-new.component';
import { GateEntryReceiptComponent } from './entry_exit/reports/gate-entry-receipt/gate-entry-receipt.component';
import { VehicleRegistrationReceiptComponent } from './entry_exit/reports/vehicle-registration-receipt/vehicle-registration-receipt.component';
import { GateEntryRegisterComponent } from './entry_exit/reports/gate-entry-register/gate-entry-register.component';
import { InGateCollectionComponent } from './entry_exit/reports/in-gate-collection/in-gate-collection.component';
import { InGateEntryReceiptComponent } from './entry_exit/reports/in-gate-entry-receipt/in-gate-entry-receipt.component';
import { WeighBridgeReportComponent } from './entry_exit/reports/weigh-bridge-report/weigh-bridge-report.component';
import { WeighBridgeReceiptComponent } from './entry_exit/reports/weigh-bridge-receipt/weigh-bridge-receipt.component';
import { ExitGateRegisterComponent } from './entry_exit/reports/exit-gate-register/exit-gate-register.component';
import { ExitGateReceiptComponent } from './entry_exit/reports/exit-gate-receipt/exit-gate-receipt.component';
import { CommodityListingReportComponent } from './entry_exit/reports/commodity-listing-report/commodity-listing-report.component';
import { FarmerReportsRegistrationComponent } from './entry_exit/reports/farmer-registration/farmer-registration.component';
import { AgentTraderRegistrationReportComponent } from './entry_exit/reports/agent-trader-registration-report/agent-trader-registration-report.component';
import { ArrivalSummaryReportComponent } from './entry_exit/reports/arrival-summary-report/arrival-summary-report.component';
import { TraderPurchaseReportComponent } from './entry_exit/reports/trader-purchase-report/trader-purchase-report.component';
import { FarmerDetailReportComponent } from './entry_exit/reports/farmer-detail-report/farmer-detail-report.component';
import { CommodityModelPriceReportComponent } from './entry_exit/reports/commodity-model-price-report/commodity-model-price-report.component';
import { NamTsReportComponent } from './entry_exit/reports/nam-ts-report/nam-ts-report.component';
import { WeexcelFormatReportComponent } from './entry_exit/reports/weexcel-format-report/weexcel-format-report.component';
import { CommissionAgentLotBfrBiddingReportsComponent } from './entry_exit/reports/commission-agent-lot-bfr-bidding-reports/commission-agent-lot-bfr-bidding-reports.component';
import { UpDailyReportComponent } from './entry_exit/reports/up-daily-report/up-daily-report.component';
import { WeighmentComponent } from './lot_operations/transactions/weighment/weighment.component';
import { LotManagementComponent } from './lot_operations/transactions/lot-management/lot-management.component';
import { SampleCreationComponent } from './lot_operations/transactions/sample-creation/sample-creation.component';
import { AssayingComponent } from './lot_operations/transactions/assaying/assaying.component';
import { ApproalForTradeComponent } from './lot_operations/transactions/approal-for-trade/approal-for-trade.component';
import { LotTrackerComponent } from './lot_operations/reports/lot-tracker/lot-tracker.component';
import { LotSampleDetailComponent } from './lot_operations/reports/lot-sample-detail/lot-sample-detail.component';
import { SampleAssayingResultComponent } from './lot_operations/reports/sample-assaying-result/sample-assaying-result.component';
import { BidCreationComponent } from './auction/transactions/bid-creation/bid-creation.component';
import { BidRejectionComponent } from './auction/transactions/bid-rejection/bid-rejection.component';
import { BidDeclarationComponent } from './auction/transactions/bid-declaration/bid-declaration.component';
import { SaleAgreementComponent } from './auction/transactions/sale-agreement/sale-agreement.component';
import { SaleBillComponent } from './auction/transactions/sale-bill/sale-bill.component';
import { OutcryAuctionComponent } from './auction/transactions/outcry-auction/outcry-auction.component';
import { OutsideApmcSaleAgrmntReceiptComponent } from './auction/reports/outside-apmc-sale-agrmnt-receipt/outside-apmc-sale-agrmnt-receipt.component';
import { OutsideApmcSaleReportComponent } from './auction/reports/outside-apmc-sale-report/outside-apmc-sale-report.component';
import { SaleAgreementReportPageComponent } from './auction/reports/sale-agreement-report-page/sale-agreement-report-page.component';
import { OutsideApmcSaleRegisterComponent } from './auction/reports/outside-apmc-sale-register/outside-apmc-sale-register.component';
import { DailyPerformanceReportComponent } from './auction/reports/daily-performance-report/daily-performance-report.component';
import { LotProgressDetailsReportComponent } from './auction/reports/lot-progress-details-report/lot-progress-details-report.component';
import { BidCreationReportComponent } from './auction/reports/bid-creation-report/bid-creation-report.component';
import { BidSubmissionSummaryComponent } from './auction/reports/bid-submission-summary/bid-submission-summary.component';
import { SaleAgreementCancellationComponent } from './auction/reports/sale-agreement-cancellation/sale-agreement-cancellation.component';
import { BidSubmissionReportComponent } from './auction/reports/bid-submission-report/bid-submission-report.component';
import { AgreementNotExitComponent } from './auction/reports/agreement-not-exit/agreement-not-exit.component';
import { StatewiseCommoditySaleReportComponent } from './auction/reports/statewise-commodity-sale-report/statewise-commodity-sale-report.component';
import { SaleAgreementTypewiseComponent } from './auction/reports/sale-agreement-typewise/sale-agreement-typewise.component';
import { BidOpeningReportComponent } from './auction/reports/bid-opening-report/bid-opening-report.component';
import { IngateNotAgreementComponent } from './auction/reports/ingate-not-agreement/ingate-not-agreement.component';
import { SettlementReportComponent } from './auction/reports/settlement-report/settlement-report.component';
import { SaleInvoiceComponent } from './auction/reports/sale-invoice/sale-invoice.component';
import { BidOpeningWinnerlistComponent } from './auction/reports/bid-opening-winnerlist/bid-opening-winnerlist.component';
import { SaleSummaryReportComponent } from './auction/reports/sale-summary-report/sale-summary-report.component';
import { TraderLotDetailsComponent } from './auction/reports/trader-lot-details/trader-lot-details.component';
import { TraderCommissionAgentBiddingReportComponent } from './auction/reports/trader-commission-agent-bidding-report/trader-commission-agent-bidding-report.component';
import { CommissionAgentLotdetailsAftrWeighmentComponent } from './auction/reports/commission-agent-lotdetails-aftr-weighment/commission-agent-lotdetails-aftr-weighment.component';
import { LotTrackerDetailReportComponent } from './auction/reports/lot-tracker-detail-report/lot-tracker-detail-report.component';
import { ShiftwiseBidOpeningWinnerlistComponent } from './auction/reports/shiftwise-bid-opening-winnerlist/shiftwise-bid-opening-winnerlist.component';
import { DatewiseFinalReportComponent } from './auction/reports/datewise-final-report/datewise-final-report.component';
import { NamStatewiseTradingComponent } from './auction/reports/nam-statewise-trading/nam-statewise-trading.component';
import { DaarpReportComponent } from './auction/reports/daarp-report/daarp-report.component';
import { PaymentSettlementDetailsComponent } from './auction/reports/payment-settlement-details/payment-settlement-details.component';
import { CommissionAgentwiseSoldunsoldLotReportComponent } from './auction/reports/commission-agentwise-soldunsold-lot-report/commission-agentwise-soldunsold-lot-report.component';
import { TradedCommoditywiseMandiReportComponent } from './auction/reports/traded-commoditywise-mandi-report/traded-commoditywise-mandi-report.component';
import { MandiwiseTradedCommodityReportComponent } from './auction/reports/mandiwise-traded-commodity-report/mandiwise-traded-commodity-report.component';
import { MandiwiseAgreementSummaryReportComponent } from './auction/reports/mandiwise-agreement-summary-report/mandiwise-agreement-summary-report.component';
import { ConsolidatedPaymentReportComponent } from './auction/reports/consolidated-payment-report/consolidated-payment-report.component';
import { WeeklyScReportComponent } from './auction/reports/weekly-sc-report/weekly-sc-report.component';
import { PaymentSettlementTrackingReportComponent } from './auction/reports/payment-settlement-tracking-report/payment-settlement-tracking-report.component';
import { EPaymentReportComponent } from './auction/reports/e-payment-report/e-payment-report.component';
import { SaleAgreementBillRegisterReportComponent } from './auction/reports/sale-agreement-bill-register-report/sale-agreement-bill-register-report.component';
import { StatewiseOnlinePaymentComponent } from './auction/reports/statewise-online-payment/statewise-online-payment.component';
import { CommodityWiseAssayingComponent } from './auction/reports/commodity-wise-assaying/commodity-wise-assaying.component';
import { InterMandiTradeDetailComponent } from './auction/reports/inter-mandi-trade-detail/inter-mandi-trade-detail.component';
import { WorkflowMasterComponent } from './administration/masters/workflow-master/workflow-master.component';
import { ApmcBankdetailsReportComponent } from './administration/reports/apmc-bankdetails-report/apmc-bankdetails-report.component';
import { HelpComponent } from './help/help.component';
import { GrievanceManagementSystemComponent } from './grievance-management-system/grievance-management-system.component';
import { ProformaComponent } from './auction/reports/proforma/proforma.component';
import { DigitalTradeComponent } from './auction/reports/digital-trade/digital-trade.component';
import { CommodityTradeInfoComponent } from './auction/reports/commodity-trade-info/commodity-trade-info.component';
import { AssayedTradedLotReportComponent } from './auction/reports/assayed-traded-lot-report/assayed-traded-lot-report.component';
import { CommodityRangeReportComponent } from './auction/reports/commodity-range-report/commodity-range-report.component';
import { PaymentAgeingComponent } from './auction/reports/payment-ageing/payment-ageing.component';
import { SaleagreementSalebillComponent } from './auction/reports/saleagreement-salebill/saleagreement-salebill.component';
import { MandiwiseTradeSummaryComponent } from './auction/reports/mandiwise-trade-summary/mandiwise-trade-summary.component';
import { UserPasswordChangeComponent } from './administration/masters/user-password-change/user-password-change.component';
import { CommodityGroupVarietyComponent } from './entry_exit/reports/commodity-group-variety/commodity-group-variety.component';
import { DailyReportComponent } from './entry_exit/reports/daily-report/daily-report.component';
import { LotAssayingSummaryComponent } from './lot_operations/reports/lot-assaying-summary/lot-assaying-summary.component';
import { AdminGrievanceComponent } from './grievance-management-system/admin-grievance/admin-grievance.component';
import { ApmcOthersGrievanceComponent } from './grievance-management-system/apmc-others-grievance/apmc-others-grievance.component';


import { GrievanceEntryComponent } from './trader-portal/grievance-management/grievance-entry/grievance-entry.component';
import { StatusOfGrievanceComponent } from './trader-portal/grievance-management/status-of-grievance/status-of-grievance.component';
import { SubmitCommodityRequirmentComponent } from './trader-portal/reverse-auction/submit-commodity-requirment/submit-commodity-requirment.component';
import { ViewRequiredCommodityComponent } from './trader-portal/reverse-auction/view-required-commodity/view-required-commodity.component';
import { AgreementSaleBillCancellationComponent } from './entry_exit/transactions/agreement-sale-bill-cancellation/agreement-sale-bill-cancellation.component';
import { RegistrationNotificationComponent } from './entry_exit/transactions/registration-notification/registration-notification.component';
// import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';



//Routing

// export const appRoutes: Routes =[
//   {path:'login',component:LoginComponent},
//   {path:'',component:LoginComponent},
//   {path:'dashboard',component:DashboardComponent,canActivate: [AuthGuard]},
//   //Master portal Routing
//   {path:'app-agent-resgistration',component:AgentResgistrationComponent,canActivate: [AuthGuard]},
//   {path:'app-organisation-master',component:OrganisationMasterComponent,canActivate: [AuthGuard]},
//   {path:'app-operating-unit',component:OperatingUnitComponent,canActivate: [AuthGuard]},
//   {path:'app-user-master-apmc',component:UserMasterApmcComponent,canActivate: [AuthGuard]},
// ]
// ;
export const appRoutes: Routes =[
  {path:'login',component:LoginComponent},
  {path:'',component:LoginComponent},
  {path:'dashboard',component:DashboardComponent,canActivate: [AuthGuard]},
  //Master portal Routing
  //Administration>Masters
  {path:'app-organisation-master',component:OrganisationMasterComponent,canActivate: [AuthGuard]},
  {path:'app-operating-unit',component:OperatingUnitComponent,canActivate: [AuthGuard]},
  {path:'app-user-master-apmc',component:UserMasterApmcComponent,canActivate: [AuthGuard]},
  {path:'app-user-master-state',component:UserMasterStateComponent,canActivate: [AuthGuard]},
  {path:'app-user-master-global',component:UserMasterGlobalComponent,canActivate: [AuthGuard]},
  {path:'app-configure-doc-gen-schemes',component:ConfigureDocGenSchemesComponent,canActivate: [AuthGuard]},
  {path:'app-general-master',component:GeneralMasterComponent,canActivate: [AuthGuard]},
  {path:'app-system-parameter-legal-entity',component:SystemParameterLegalEntityComponent,canActivate: [AuthGuard]},
  {path:'app-role-master',component:RoleMasterComponent,canActivate: [AuthGuard]},
  {path:'app-system-parameter-operating-unit',component:SystemParameterOperatingUnitComponent,canActivate: [AuthGuard]},
  {path:'app-workflow-master',component:WorkflowMasterComponent,canActivate: [AuthGuard]},
  {path:'app-user-password-change',component:UserPasswordChangeComponent,canActivate: [AuthGuard]},
  //Administration>Reports
  {path:'app-master-setup-report',component:MasterSetupReportComponent,canActivate: [AuthGuard]},
  {path:'app-apmc-bankdetails-report',component:ApmcBankdetailsReportComponent,canActivate: [AuthGuard]},
  //Entry exit>Masters
  {path:'app-fee-component-apmc-applicability',component:FeeComponentApmcApplicabilityComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-master',component:CommodityMasterComponent,canActivate: [AuthGuard]},
  {path:'app-vehicle-rate',component:VehicleRateComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-master-apmc-applicability',component:CommodityMasterApmcApplicabilityComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-group',component:CommodityGroupComponent,canActivate: [AuthGuard]},
  {path:'app-fee-component',component:FeeComponentComponent,canActivate: [AuthGuard]},
  {path:'app-fee-category',component:FeeCategoryComponent,canActivate: [AuthGuard]},
  {path:'app-agent-registration',component:AgentRegistrationComponent,canActivate: [AuthGuard]},
  {path:'app-farmer-registration',component:FarmerRegistrationComponent,canActivate: [AuthGuard]},
  {path:'app-bag-type',component:BagTypeComponent,canActivate: [AuthGuard]},
  //Entry>transactions
  {path:'app-vehicle-registration',component:VehicleRegistrationComponent,canActivate: [AuthGuard]},
  {path:'app-gate-entry',component:GateEntryComponent,canActivate: [AuthGuard]},
  {path:'app-vehicle-gate-entry',component:VehicleGateEntryComponent,canActivate: [AuthGuard]},
  {path:'app-lot-entry',component:LotEntryComponent,canActivate: [AuthGuard]},
  {path:'app-gate-exit',component:GateExitComponent,canActivate: [AuthGuard]},
  {path:'app-gate-entry-new',component:GateEntryNewComponent,canActivate: [AuthGuard]},
  {path:'app-agreement-sale-bill-cancellation',component:AgreementSaleBillCancellationComponent,canActivate: [AuthGuard]},
  {path:'app-registration-notification',component:RegistrationNotificationComponent,canActivate: [AuthGuard]},
  //Entry>reports
  {path:'app-gate-entry-receipt',component:GateEntryReceiptComponent,canActivate: [AuthGuard]},
  {path:'app-vehicle-registration-receipt',component:VehicleRegistrationReceiptComponent,canActivate: [AuthGuard]},
  {path:'app-gate-entry-register',component:GateEntryRegisterComponent,canActivate: [AuthGuard]},
  {path:'app-in-gate-collection',component:InGateCollectionComponent,canActivate: [AuthGuard]},
  {path:'app-in-gate-entry-receipt',component:InGateEntryReceiptComponent,canActivate: [AuthGuard]},
  {path:'app-weigh-bridge-report',component:WeighBridgeReportComponent,canActivate: [AuthGuard]},
  {path:'app-weigh-bridge-receipt',component:WeighBridgeReceiptComponent,canActivate: [AuthGuard]},
  {path:'app-exit-gate-register',component:ExitGateRegisterComponent,canActivate: [AuthGuard]},
  {path:'app-exit-gate-receipt',component:ExitGateReceiptComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-listing-report',component:CommodityListingReportComponent,canActivate: [AuthGuard]},
  {path:'app-reports-farmer-registration',component:FarmerReportsRegistrationComponent,canActivate: [AuthGuard]},
  {path:'app-agent-trader-registration-report',component:AgentTraderRegistrationReportComponent,canActivate: [AuthGuard]},
  {path:'app-arrival-summary-report',component:ArrivalSummaryReportComponent,canActivate: [AuthGuard]},
  {path:'app-trader-purchase-report',component:TraderPurchaseReportComponent,canActivate: [AuthGuard]},
  {path:'app-farmer-detail-report',component:FarmerDetailReportComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-model-price-report',component:CommodityModelPriceReportComponent,canActivate: [AuthGuard]},
  {path:'app-nam-ts-report',component:NamTsReportComponent,canActivate: [AuthGuard]},
  {path:'app-weexcel-format-report',component:WeexcelFormatReportComponent,canActivate: [AuthGuard]},
  {path:'app-commission-agent-lot-bfr-bidding-reports',component:CommissionAgentLotBfrBiddingReportsComponent,canActivate: [AuthGuard]},
  {path:'app-up-daily-report',component:UpDailyReportComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-group-variety',component:CommodityGroupVarietyComponent,canActivate: [AuthGuard]},
  {path:'app-daily-report',component:DailyReportComponent,canActivate: [AuthGuard]},
  //Lot Operations>Transactions
  {path:'app-weighment',component:WeighmentComponent,canActivate: [AuthGuard]},
  {path:'app-lot-management',component:LotManagementComponent,canActivate: [AuthGuard]},
  {path:'app-sample-creation',component:SampleCreationComponent,canActivate: [AuthGuard]},
  {path:'app-assaying',component:AssayingComponent,canActivate: [AuthGuard]},
  {path:'app-approal-for-trade',component:ApproalForTradeComponent,canActivate: [AuthGuard]},
  //Lot Operations>Reports
  {path:'app-lot-tracker',component:LotTrackerComponent,canActivate: [AuthGuard]},
  {path:'app-lot-sample-detail',component:LotSampleDetailComponent,canActivate: [AuthGuard]},
  {path:'app-sample-assaying-result',component:SampleAssayingResultComponent,canActivate: [AuthGuard]},
  {path:'app-lot-assaying-summary',component:LotAssayingSummaryComponent,canActivate: [AuthGuard]},
  //Auction>Transaction
  {path:'app-bid-creation',component:BidCreationComponent,canActivate: [AuthGuard]},
  {path:'app-bid-rejection',component:BidRejectionComponent,canActivate: [AuthGuard]},
  {path:'app-bid-declaration',component:BidDeclarationComponent,canActivate: [AuthGuard]},
  {path:'app-sale-agreement',component:SaleAgreementComponent,canActivate: [AuthGuard]},
  {path:'app-sale-bill',component:SaleBillComponent,canActivate: [AuthGuard]},
  {path:'app-outcry-auction',component:OutcryAuctionComponent,canActivate: [AuthGuard]},
  //Auction>Reports
  {path:'app-outside-apmc-sale-agrmnt-receipt',component:OutsideApmcSaleAgrmntReceiptComponent,canActivate: [AuthGuard]},
  {path:'app-outside-apmc-sale-report',component:OutsideApmcSaleReportComponent,canActivate: [AuthGuard]},
  {path:'app-sale-agreement-report-page',component:SaleAgreementReportPageComponent,canActivate: [AuthGuard]},
  {path:'app-outside-apmc-sale-register',component:OutsideApmcSaleRegisterComponent,canActivate: [AuthGuard]},
  {path:'app-daily-performance-report',component:DailyPerformanceReportComponent,canActivate: [AuthGuard]},
  {path:'app-lot-progress-details-report',component:LotProgressDetailsReportComponent,canActivate: [AuthGuard]},
  {path:'app-bid-creation-report',component:BidCreationReportComponent,canActivate: [AuthGuard]},
  {path:'app-bid-submission-summary',component:BidSubmissionSummaryComponent,canActivate: [AuthGuard]},
  {path:'app-sale-agreement-cancellation',component:SaleAgreementCancellationComponent,canActivate: [AuthGuard]},
  {path:'app-bid-submission-report',component:BidSubmissionReportComponent,canActivate: [AuthGuard]},
  {path:'app-agreement-not-exit',component:AgreementNotExitComponent,canActivate: [AuthGuard]},
  {path:'app-statewise-commodity-sale-report',component:StatewiseCommoditySaleReportComponent,canActivate: [AuthGuard]},
  {path:'app-sale-agreement-typewise',component:SaleAgreementTypewiseComponent,canActivate: [AuthGuard]},
  {path:'app-bid-opening-report',component:BidOpeningReportComponent,canActivate: [AuthGuard]},
  {path:'app-ingate-not-agreement',component:IngateNotAgreementComponent,canActivate: [AuthGuard]},
  {path:'app-settlement-report',component:SettlementReportComponent,canActivate: [AuthGuard]},
  {path:'app-sale-invoice',component:SaleInvoiceComponent,canActivate: [AuthGuard]},
  {path:'app-bid-opening-winnerlist',component:BidOpeningWinnerlistComponent,canActivate: [AuthGuard]},
  {path:'app-sale-summary-report',component:SaleSummaryReportComponent,canActivate: [AuthGuard]},
  {path:'app-trader-lot-details',component:TraderLotDetailsComponent,canActivate: [AuthGuard]},
  {path:'app-trader-commission-agent-bidding-report',component:TraderCommissionAgentBiddingReportComponent,canActivate: [AuthGuard]},
  {path:'app-commission-agent-lotdetails-aftr-weighment',component:CommissionAgentLotdetailsAftrWeighmentComponent,canActivate: [AuthGuard]},
  {path:'app-lot-tracker-detail-report',component:LotTrackerDetailReportComponent,canActivate: [AuthGuard]},
  {path:'app-shiftwise-bid-opening-winnerlist',component:ShiftwiseBidOpeningWinnerlistComponent,canActivate: [AuthGuard]},
  {path:'app-datewise-final-report',component:DatewiseFinalReportComponent,canActivate: [AuthGuard]},
  {path:'app-nam-statewise-trading',component:NamStatewiseTradingComponent,canActivate: [AuthGuard]},
  {path:'app-daarp-report',component:DaarpReportComponent,canActivate: [AuthGuard]},
  {path:'app-payment-settlement-details',component:PaymentSettlementDetailsComponent,canActivate: [AuthGuard]},
  {path:'app-commission-agentwise-soldunsold-lot-report',component:CommissionAgentwiseSoldunsoldLotReportComponent,canActivate: [AuthGuard]},
  {path:'app-traded-commoditywise-mandi-report',component:TradedCommoditywiseMandiReportComponent,canActivate: [AuthGuard]},
  {path:'app-mandiwise-traded-commodity-report',component:MandiwiseTradedCommodityReportComponent,canActivate: [AuthGuard]},
  {path:'app-mandiwise-agreement-summary-report',component:MandiwiseAgreementSummaryReportComponent,canActivate: [AuthGuard]},
  {path:'app-consolidated-payment-report',component:ConsolidatedPaymentReportComponent,canActivate: [AuthGuard]},
  {path:'app-weekly-sc-report',component:WeeklyScReportComponent,canActivate: [AuthGuard]},
  {path:'app-payment-settlement-tracking-report',component:PaymentSettlementTrackingReportComponent,canActivate: [AuthGuard]},
  {path:'app-e-payment-report',component:EPaymentReportComponent,canActivate: [AuthGuard]},
  {path:'app-sale-agreement-bill-register-report',component:SaleAgreementBillRegisterReportComponent,canActivate: [AuthGuard]},
  {path:'app-statewise-online-payment',component:StatewiseOnlinePaymentComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-wise-assaying',component:CommodityWiseAssayingComponent,canActivate: [AuthGuard]},
  {path:'app-inter-mandi-trade-detail',component:InterMandiTradeDetailComponent,canActivate: [AuthGuard]},
  {path:'app-proforma',component:ProformaComponent,canActivate: [AuthGuard]},
  {path:'app-digital-trade',component:DigitalTradeComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-trade-info',component:CommodityTradeInfoComponent,canActivate: [AuthGuard]},
  {path:'app-assayed-traded-lot-report',component:AssayedTradedLotReportComponent,canActivate: [AuthGuard]},
  {path:'app-commodity-range-report',component:CommodityRangeReportComponent,canActivate: [AuthGuard]},
  {path:'app-payment-ageing',component:PaymentAgeingComponent,canActivate: [AuthGuard]},
  {path:'app-saleagreement-salebill',component:SaleagreementSalebillComponent,canActivate: [AuthGuard]},
  {path:'app-mandiwise-trade-summary',component:MandiwiseTradeSummaryComponent,canActivate: [AuthGuard]},

  // Help
  {path:'app-help',component:HelpComponent,canActivate: [AuthGuard]},
  //grievance
  {path:'app-grievance-management-system',component:GrievanceManagementSystemComponent,canActivate: [AuthGuard]},
  {path:'app-admin-grievance',component:AdminGrievanceComponent,canActivate: [AuthGuard]},
  {path:'app-apmc-others-grievance',component:ApmcOthersGrievanceComponent,canActivate: [AuthGuard]},
  //trader portal
  {path:'app-grievance-entry',component:GrievanceEntryComponent,canActivate: [AuthGuard]},
  {path:'app-status-of-grievance',component:StatusOfGrievanceComponent,canActivate: [AuthGuard]},
  {path:'app-submit-commodity-requirment',component:SubmitCommodityRequirmentComponent,canActivate: [AuthGuard]},
  {path:'app-view-required-commodity',component:ViewRequiredCommodityComponent,canActivate: [AuthGuard]},

]
;

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    BankdetailsComponent,
    BidhistoryComponent,
    NewbidlistingComponent,
    CommoditybidlistingComponent,
    ReportComponent,
    LoginComponent,
    SidebarComponent,
    NewbidlistingComponent,
    CommoditybidlistingComponent,
    TimerComponent,
    AgentRegistrationComponent,
    OrganisationMasterComponent,
    OperatingUnitComponent,
    UserMasterApmcComponent,
    UserMasterStateComponent,
    UserMasterGlobalComponent,
    ConfigureDocGenSchemesComponent,
    GeneralMasterComponent,
    SystemParameterLegalEntityComponent,
    RoleMasterComponent,
    SystemParameterOperatingUnitComponent,
    MasterSetupReportComponent,
    FeeComponentApmcApplicabilityComponent,
    CommodityMasterComponent,
    VehicleRateComponent,
    CommodityMasterApmcApplicabilityComponent,
    CommodityGroupComponent,
    FeeComponentComponent,
    FeeCategoryComponent,
    AgentRegistrationComponent,
    FarmerRegistrationComponent,
    BagTypeComponent,
    VehicleRegistrationComponent,
    GateEntryComponent,
    VehicleGateEntryComponent,
    LotEntryComponent,
    GateExitComponent,
    GateEntryNewComponent,
    GateEntryReceiptComponent,
    VehicleRegistrationReceiptComponent,
    GateEntryRegisterComponent,
    InGateCollectionComponent,
    InGateEntryReceiptComponent,
    WeighBridgeReportComponent,
    WeighBridgeReceiptComponent,
    ExitGateRegisterComponent,
    ExitGateReceiptComponent,
    CommodityListingReportComponent,
    FarmerReportsRegistrationComponent,
    AgentTraderRegistrationReportComponent,
    ArrivalSummaryReportComponent,
    TraderPurchaseReportComponent,
    FarmerDetailReportComponent,
    CommodityModelPriceReportComponent,
    NamTsReportComponent,
    WeexcelFormatReportComponent,
    CommissionAgentLotBfrBiddingReportsComponent,
    UpDailyReportComponent,
    WeighmentComponent,
    LotManagementComponent,
    SampleCreationComponent,
    AssayingComponent,
    ApproalForTradeComponent,
    LotTrackerComponent,
    LotSampleDetailComponent,
    SampleAssayingResultComponent,
    BidCreationComponent,
    BidRejectionComponent,
    BidDeclarationComponent,
    SaleAgreementComponent,
    SaleBillComponent,
    OutcryAuctionComponent,
    OutsideApmcSaleAgrmntReceiptComponent,
    OutsideApmcSaleReportComponent,
    SaleAgreementReportPageComponent,
    OutsideApmcSaleRegisterComponent,
    DailyPerformanceReportComponent,
    LotProgressDetailsReportComponent,
    BidCreationReportComponent,
    BidSubmissionSummaryComponent,
    SaleAgreementCancellationComponent,
    BidSubmissionReportComponent,
    AgreementNotExitComponent,
    StatewiseCommoditySaleReportComponent,
    SaleAgreementTypewiseComponent,
    BidOpeningReportComponent,
    IngateNotAgreementComponent,
    SettlementReportComponent,
    SaleInvoiceComponent,
    BidOpeningWinnerlistComponent,
    SaleSummaryReportComponent,
    TraderLotDetailsComponent,
    TraderCommissionAgentBiddingReportComponent,
    CommissionAgentLotdetailsAftrWeighmentComponent,
    LotTrackerDetailReportComponent,
    ShiftwiseBidOpeningWinnerlistComponent,
    DatewiseFinalReportComponent,
    NamStatewiseTradingComponent,
    DaarpReportComponent,
    PaymentSettlementDetailsComponent,
    CommissionAgentwiseSoldunsoldLotReportComponent,
    TradedCommoditywiseMandiReportComponent,
    MandiwiseTradedCommodityReportComponent,
    MandiwiseAgreementSummaryReportComponent,
    ConsolidatedPaymentReportComponent,
    WeeklyScReportComponent,
    PaymentSettlementTrackingReportComponent,
    EPaymentReportComponent,
    SaleAgreementBillRegisterReportComponent,
    StatewiseOnlinePaymentComponent,
    CommodityWiseAssayingComponent,
    InterMandiTradeDetailComponent,
    WorkflowMasterComponent,
    ApmcBankdetailsReportComponent,
    HelpComponent,
    GrievanceManagementSystemComponent,
    ProformaComponent,
    DigitalTradeComponent,
    CommodityTradeInfoComponent,
    AssayedTradedLotReportComponent,
    CommodityRangeReportComponent,
    PaymentAgeingComponent,
    SaleagreementSalebillComponent,
    MandiwiseTradeSummaryComponent,
    UserPasswordChangeComponent,
    CommodityGroupVarietyComponent,
    DailyReportComponent,
    LotAssayingSummaryComponent,
    AdminGrievanceComponent,
    ApmcOthersGrievanceComponent,
    GrievanceEntryComponent,
    StatusOfGrievanceComponent,
    SubmitCommodityRequirmentComponent,
    ViewRequiredCommodityComponent,
    AgreementSaleBillCancellationComponent,
    RegistrationNotificationComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    DataTableModule,
    MyDatePickerModule,
    ReCaptchaModule,
    LocalStorageModule,
    SelectModule,
    FlashMessagesModule,
    MdTooltipModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    LoadingModule,
    MomentModule,
    DragulaModule,
    NgIdleKeepaliveModule.forRoot(),
    // OwlDateTimeModule,
    // OwlNativeDateTimeModule
    AngularDateTimePickerModule

  ],
  providers: [AuthService,ValidateService,ClockService,AuthGuard,RoleAccessService],
  bootstrap: [AppComponent]
})
export class AppModule { }
