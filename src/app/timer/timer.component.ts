import { Component,Input,Output, OnInit,ElementRef, OnDestroy, EventEmitter} from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit, OnDestroy {

  private future: Date;
  private futureString: string;
  @Input() inputDate:string;
  @Input() inputTime:string;
  @Output() stopBid:EventEmitter<any> = new EventEmitter<any>();
  private diff: number;
  private $counter: Observable<number>;
  private subscription: Subscription;
  public message: string;
  td:string;

  constructor(elm: ElementRef) {
    this.futureString = elm.nativeElement.getAttribute('inputDate');
    //console.log("The futurestring from timer ",this.futureString);
  }

  dhms(t) {
    var days, hours, minutes, seconds;
    days = Math.floor(t / 86400);
    t -= days * 86400;
    hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    seconds = t % 60;

    return [
      days + 'd',
      hours + 'h',
      minutes + 'm',
      seconds + 's'
    ].join(' ');
  }

  ngOnInit() {
    // this.futureString = this.inputDate + " " + this.inputTime;
    this.futureString = this.inputDate;
    this.future = new Date(this.futureString);
    this.$counter = Observable.interval(1000).map((x) => {
      this.diff = Math.floor((this.future.getTime() - new Date().getTime()) / 1000);
      return x;
    });
    this.subscription = this.$counter.subscribe((x) => {

      this.message=this.dhms(this.diff);
      //console.log("The time difference",(this.diff));
      this.stopBid.emit(this.diff);

      this.td=this.diff.toString();
      //localStorage.setItem('timeDifference',this.td);
      if(this.diff<0){
        this.message="Timed out";
      }

    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
