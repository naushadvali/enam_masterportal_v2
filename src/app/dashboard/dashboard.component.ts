import { Component, ViewChild, ElementRef, AfterViewInit,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { AuthService } from '../services/auth.service';
import {DataTableModule} from "angular2-datatable";
import { DatePipe } from '@angular/common';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as _ from 'lodash';

@Component({
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],

})
export class DashboardComponent implements OnInit{
  userType:string;searchDate:any;today:any;
  public my_class1='overlay';
  public my_class2='overlay';
  public loading = false;
  constructor(public authservice:AuthService,
    private _dataTable:DataTableModule,
    private flashMessagesService: FlashMessagesService,
    private http:Http,
    private router:Router
  )
  {}

  ngOnInit(){
  this.userType =  localStorage.getItem("userType");
  console.log("userType",this.userType)

  // set default date
  this.today = new Date();
  var dd = this.today.getDate();
  var mm = this.today.getMonth()+1; //January is 0!
  var yyyy = this.today.getFullYear();
  this.searchDate =
  { date: { year: this.today.getFullYear(), month: mm, day: dd } };
  this.searchArrivalSummary();

  // set stateId & apmcId to All
  this.stateId = null;
  this.apmcId = null;
  // fetch states
  if(this.userType != 'M'){
    this.getStates();
  }
  // fetch commodity
  if(this.userType == 'S'){
    this.getCommodityListForStateAdmin();
    // search all commodity bid listing
    this.commApmcId = null;
    this.productId = null;
    this.commStateId = null;
    this.searchAllCommodity();
  }

  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  // Search arrival summary
  stateId:string;apmcId:string;arrivalSummaryList=[];apmcWiseWorkingAsNow=[];lotTrackerList =[];saleSummaryList =[];
  searchArrivalSummary(){
    console.log("this.searchDate",this.searchDate);
    var dd = this.searchDate.date.day;
    var mm = this.searchDate.date.month;
    if(dd<10){
      dd = "0"+dd;
    }
    if(mm<10){
      mm = "0"+mm;
    }
    var filtSearchDate = (dd+"/"+mm+"/"+this.searchDate.date.year);
    console.log("filtSearchDate",filtSearchDate);

    if(this.stateId == 'all'){
      this.stateId = null;
    }
    if(this.apmcId == 'all'){
      this.apmcId = null;
    }
    var postJsonArrival = {};
    var postJsonApmcWise = {};

    if(this.userType == 'M'){
      postJsonArrival ={ "lotdate": filtSearchDate,"stateId": null,"oprid": null};
    }
    else{
      postJsonArrival ={ "lotdate": filtSearchDate,"stateId": this.stateId,"oprid": this.apmcId};
      postJsonApmcWise ={ "stateId": this.stateId,"oprid": this.apmcId};
    }

    this.authservice.fetchArrivalSummary(postJsonArrival).subscribe($data=>{
      console.log("arrival summary search: ",$data);
      this.loading = true;
      this.arrivalSummaryList =[];
      if($data.listData != undefined){
        this.arrivalSummaryList = $data.listData;
      }
      this.loading = false;
    }, (err) => {
                if (err === 'Unauthorized'){
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class1 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
                }
                else{
                  this.my_class2 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class2 = "overlay";
                  },2000);
                }
          });

    if(this.userType != 'M'){
      this.authservice.fetchApmcWiseWorkingStatus(postJsonApmcWise).subscribe($data=>{
        console.log("apmcWiseWorkingAsNow search: ",$data);
        this.loading = true;
        this.apmcWiseWorkingAsNow =[];
        if($data.listData != undefined){
          this.apmcWiseWorkingAsNow = $data.listData;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized'){
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class1 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
                  }
                  else{
                    this.my_class2 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class2 = "overlay";
                    },2000);
                  }
            });
    }


    this.authservice.fetchLotTracker(postJsonApmcWise).subscribe($data=>{
      console.log("lot tracker search: ",$data);
      this.loading = true;
      this.lotTrackerList =[];
      if($data.listData != undefined){
        this.lotTrackerList = $data.listData;
      }
      this.loading = false;
    }, (err) => {
                if (err === 'Unauthorized'){
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class1 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
                }
                else{
                  this.my_class2 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class2 = "overlay";
                  },2000);
                }
          });

      this.authservice.fetchSaleSummary(postJsonArrival).subscribe($data=>{
        console.log("arrival summary search: ",$data);
        this.loading = true;
        this.saleSummaryList =[];
        if($data.listData != undefined){
          this.saleSummaryList = $data.listData;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized'){
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class1 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
                  }
                  else{
                    this.my_class2 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class2 = "overlay";
                    },2000);
                  }
            });
  }

  // Fetch all states
  allStates=[];
  getStates(){
    this.authservice.getAllStateName().subscribe($allStatename=>{
      console.log("All state name: ",$allStatename.listData);
      this.loading = true;
      this.allStates=$allStatename.listData;
      this.loading = false;
    }, (err) => {
              if (err === 'Unauthorized'){
                this.errorPopupShow();
                setTimeout(()=>{
                  this.my_class1 = 'overlay';
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
              }
              else{
                this.my_class2 = 'overlay1';
                setTimeout(()=>{
                  this.my_class2 = "overlay";
                },2000);
              }
        }
      );
  }
  // Fetch all apmcs
  allAPMCs=[];
  getAPMCList(stateId){
    var postJson={"stateId": stateId};
    this.authservice.fetchApmcList(postJson).subscribe($allAPMCs=>{
      console.log("All apmc name: ",$allAPMCs.listData);
      this.loading = true;
      this.allAPMCs = [];
      if($allAPMCs.listData != undefined){
        this.allAPMCs = $allAPMCs.listData;
      }
      this.loading = false;
    }, (err) => {
              if (err === 'Unauthorized'){
                this.errorPopupShow();
                setTimeout(()=>{
                  this.my_class1 = 'overlay';
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
              }
              else{
                this.my_class2 = 'overlay1';
                setTimeout(()=>{
                  this.my_class2 = "overlay";
                },2000);
              }
        }
      );
  }
  // Fetch all commodity
  allCommodity=[];
  getCommodityListForStateAdmin(){
    if(this.stateId == 'all'){
      this.stateId = null;
    }
    if(this.apmcId == 'all'){
      this.apmcId = null;
    }
    var postJson={"stateId": this.stateId,"oprid": this.apmcId};
    this.authservice.fetchCommodityListForStateAdmin(postJson).subscribe($allCommodity=>{
      console.log("All apmc name: ",$allCommodity.listData);
      this.loading = true;
      this.allCommodity = [];
      if($allCommodity.listData != undefined){
        this.allCommodity = $allCommodity.listData;
      }
      this.loading = false;
    }, (err) => {
              if (err === 'Unauthorized'){
                this.errorPopupShow();
                setTimeout(()=>{
                  this.my_class1 = 'overlay';
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
              }
              else{
                this.my_class2 = 'overlay1';
                setTimeout(()=>{
                  this.my_class2 = "overlay";
                },2000);
              }
        }
      );
  }

  // on select state
  onStateSelect(stateId){
    this.stateId = stateId;
    console.log("this.stateId",this.stateId)
    if(this.stateId != 'all'){
      this.getAPMCList(this.stateId);
    }
    else{
      this.allAPMCs =[];
    }
  }
  // on select apmc
  onAPMCSelect(apmcId){
    this.apmcId = apmcId;
    console.log("this.apmcId",this.apmcId)
  }
  // on select state for commodity
  commStateId:string;
  onCommodityStateSelect(stateId){
    this.commStateId = stateId;
    console.log("this.commStateId",this.commStateId)
    if(this.commStateId != 'all'){
      this.getAPMCList(this.commStateId);
    }
  }
  // on select apmc for commodity
  commApmcId:string;
  onCommodityAPMCSelect(apmcId){
    this.commApmcId = apmcId;
    console.log("this.commApmcId",this.commApmcId)
  }
  // on select commodity
  productId:string;
  onCommoditySelect(productId){
    this.productId = productId;
    console.log("this.productId",this.productId)
  }

  // search all commodity bid listing
  commSearchData =[];
  searchAllCommodity(){
    if(this.productId == 'all'){
      this.productId = null;
    }
    if(this.commApmcId == 'all'){
      this.commApmcId = null;
    }
    if(this.commStateId == 'all'){
      this.commStateId = null;
    }
    console.log("this.productId",this.productId)
    console.log("this.commStateId",this.commStateId)
    console.log("this.commApmcId",this.commApmcId)
    var postJson={"productId":this.productId,"stateId": this.commStateId,"oprid": this.commApmcId}
    this.authservice.searchAllCommodityBidListing(postJson).subscribe($commSearchData=>{
      this.loading = true;
      console.log("All commSearchData name: ",$commSearchData.listData);
      this.commSearchData = [];
      if($commSearchData.listData != undefined){
        this.commSearchData = $commSearchData.listData;
      }
      this.loading = false;
    }, (err) => {
              if (err === 'Unauthorized'){
                this.errorPopupShow();
                setTimeout(()=>{
                  this.my_class1 = 'overlay';
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
              }
              else{
                this.my_class2 = 'overlay1';
                setTimeout(()=>{
                  this.my_class2 = "overlay";
                },2000);
              }
        }
      );
  }

  errorPopupShow(){
    this.my_class1='overlay1';
  }
}
