import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotAssayingSummaryComponent } from './lot-assaying-summary.component';

describe('LotAssayingSummaryComponent', () => {
  let component: LotAssayingSummaryComponent;
  let fixture: ComponentFixture<LotAssayingSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotAssayingSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotAssayingSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
