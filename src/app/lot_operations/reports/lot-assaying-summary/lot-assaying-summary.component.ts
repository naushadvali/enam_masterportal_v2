import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';


@Component({
  selector: 'app-lot-assaying-summary',
  templateUrl: './lot-assaying-summary.component.html',
  styleUrls: ['./lot-assaying-summary.component.css']
})
export class LotAssayingSummaryComponent implements OnInit {
  public my_class1='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor() { }

  ngOnInit() {
  }
  div_show1(){
      this.my_class1='overlay1';
    }
div_hide1(){
      this.my_class1='overlay';
    }
}
