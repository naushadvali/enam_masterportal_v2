import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotSampleDetailComponent } from './lot-sample-detail.component';

describe('LotSampleDetailComponent', () => {
  let component: LotSampleDetailComponent;
  let fixture: ComponentFixture<LotSampleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotSampleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotSampleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
