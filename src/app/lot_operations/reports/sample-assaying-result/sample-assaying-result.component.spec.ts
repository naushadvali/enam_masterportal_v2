import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleAssayingResultComponent } from './sample-assaying-result.component';

describe('SampleAssayingResultComponent', () => {
  let component: SampleAssayingResultComponent;
  let fixture: ComponentFixture<SampleAssayingResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleAssayingResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleAssayingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
