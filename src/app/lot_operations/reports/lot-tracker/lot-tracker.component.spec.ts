import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotTrackerComponent } from './lot-tracker.component';

describe('LotTrackerComponent', () => {
  let component: LotTrackerComponent;
  let fixture: ComponentFixture<LotTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
