import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';


@Component({
  selector: 'app-lot-management',
  templateUrl: './lot-management.component.html',
  styleUrls: ['./lot-management.component.css']
})
export class LotManagementComponent implements OnInit {
  public loading = false;
  lotFromDate:any;
  lotToDate:any;
  dateChange1:any;
  dateChange2:any;
  monthChnage1:any;
  monthChnage2:any;
  fromDate:any;
  lotQuotient:any;
  lotRemainder:any;
  toDate:any;
  splitMergeData:any=[];
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';
  public my_class9='overlay';
  public my_class10='overlay';
  public my_class11='overlay';
  public my_class12='overlay';
  public my_class13='overlay';
  public my_class14='overlay';
  public my_class15='overlay';
  public my_class16='overlay';
  public my_class17='overlay';
  public my_class18='overlay';
  splitButton:boolean=true;
  nolot:number;
  public my_class50:any='overlay';

  ILEH_MULTI_FARMER:any;
  ILEH_ORG_ID:any;
  adgm_desc:any;
  adgm_gen_id:any;
  adoum_district_id:any;
  adoum_opr_id:any;
  adoum_org_id:any;
  adoum_state_id:any;
  createdby:any;
  createdon:any;
  frm_full_name:any;
  frm_opr_id:any;
  frm_orgid:any;
  frm_regi_id:any;
  gmpm_allow_partial:any;
  gmpm_auto_aprrove:any;
  gmpm_auto_assin_winer:any;
  gmpm_auto_create_bid:any;
  gmpm_bid_open_hrs:any;
  gmpm_bid_open_min:any;
  gmpm_bid_type:any;
  gmpm_end_hrs:any;
  gmpm_end_min:any;
  gmpm_open_bid_next_day:any;
  gmpm_oprid:any;
  gmpm_orgid:any;
  gmpm_prod_id:any;
  gmpm_start_hrs:any;
  gmpm_start_min:any;
  gmpm_sub_mult_bid:any;
  gmpm_tolerance_hrs:any;
  gmpm_tolerance_min:any;
  gmpmg_orgid:any;
  gmpmg_prod_code:any;
  gmpmg_prod_id:any;
  gmpmg_prod_name:any;
  gmpmg_qty_uom:any;
  gmptm_desc:any;
  gmptm_oprid:any;
  gmptm_orgid:any;
  gmptm_packid:any;
  gmptm_qty:any;
  gmptm_qty_uom:any;
  iled_bag_type:any;
  iled_comm_agent_id:any;
  iled_commodity_id:any;
  iled_district_id:any;
  iled_entry_type:any;
  iled_exit_qty:any;
  iled_farmer_id:any;
  iled_farmer_name:any;
  iled_gross_qty_quintal:any;
  iled_impo_type:any;
  iled_lot_code:any;
  iled_lot_date:any;
  iled_lot_id:any;
  iled_lot_status:any;
  iled_mobile_no:any;
  iled_noof_bag:any;
  iled_opr_id:any;
  iled_org_id:any;
  iled_qty:any;
  iled_relation_id:any;
  iled_relation_name:any;
  iled_srno:any;
  iled_state_id:any;
  iled_tehsil:any;
  iled_tran_id:any;
  iled_vech_no:any;
  iled_vech_type_id:any;
  iled_village_id:any;
  ileh_entry_type:any;
  ileh_opr_id:any;
  ileh_tran_id:any;
  lcam_agen_comp_name:any;
  lcam_full_name1:any;
  lcam_oprid1:any;
  lcam_orgid1:any;
  lcam_trn_id1:any;
  lot_date:any;

lotCode:any;seller:any;trader:any;agent:any;commodity:any;company:any;
lotType:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
  ) { }

  ngOnInit() {
    this.getQuantityInQuintal();
    this.lotType = 'N';
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  errorPopupShow(){
        this.my_class50='overlay1';
      }

  lotstype(typeData){
    this.lotType = typeData;
    console.log("selected type is : ",this.lotType);
  }

  filteredSplitMergeData =[];
  searchMerge(){
    console.log("search button clicked",this.lotType,this.lotFromDate,this.lotToDate);
      if(this.lotFromDate != undefined && this.lotToDate != undefined && this.lotFromDate != "" && this.lotToDate != ""){
        if(this.lotType != "" && this.lotType != undefined){
        const lotDates={
          fromDate:this.lotFromDate.formatted,
          toDate:this.lotToDate.formatted
        }
        console.log("lotDates",lotDates)
      this.loading = true;
      this.authservice.LMSplitMerge(lotDates,this.lotType).subscribe($splitMergeData=>{
        this.splitMergeData=$splitMergeData.listData;
        this.filteredSplitMergeData=$splitMergeData.listData;
        console.log("merge data: ",this.splitMergeData);
        this.loading = false;
      }
      , (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
              else{
                this.my_class4 = 'overlay1';
                setTimeout(()=>{
                  this.my_class4 = 'overlay';
                },2000);
              }
            }
      );
    }
    else{
      this.flashMessagesService.show('Please Select Lot Type First', { cssClass: 'alert-danger', timeout: 3000 });
    }
      }
      else{
        this.flashMessagesService.show('Dates Are Mandatory To Search', { cssClass: 'alert-danger', timeout: 3000 });
      }
  }

  onCheckBid(post,isChecked){
    // For Multiple
    if(isChecked == true){
      for(var i = 0; i < this.splitMergeData.length; i++) {
        if (this.splitMergeData[i].iledLotId == post.iledLotId) {
          this.splitMergeData[i].checked  = true;
          break;
        }
      }
    }
    else{
      for(var i = 0; i < this.splitMergeData.length; i++) {
        if (this.splitMergeData[i].iledLotId == post.iledLotId) {
          this.splitMergeData[i].checked  = false;
          break;
        }
      }
    }
    console.log(this.splitMergeData)
  }

  split(){
    if(this.iled_noof_bag>=this.nolot){
      this.lotQuotient=(this.iled_noof_bag)/(this.nolot);
      console.log("lotQuotient is:",this.lotQuotient);
      this.lotRemainder=(this.iled_noof_bag)%(this.nolot);
      console.log("Remainder is:",this.lotRemainder);
    }else{
      console.log("Value must be lesser then ",this.iled_noof_bag);
    }
  }

  selLotToSplit:any =[];
  openSplitPopup(post){
      var count = 0;
      for(var i = 0; i < this.splitMergeData.length; i++) {
        if (this.splitMergeData[i].checked == true) {
          count = count +1;
        }
      }
      if(count == 1){
        this.my_class1='overlay1';
        console.log("post",post);
        this.selLotToSplit = post;
      }
      else{
        if(count == 0){
            this.my_class16 = 'overlay1';
            setTimeout(()=>{
              this.my_class16 = 'overlay';
            },2000);
        }
        else{
          this.my_class7 = 'overlay1';
          setTimeout(()=>{
            this.my_class7 = 'overlay';
          },2000);
        }
      }
    }
  div_hide1(){
        this.my_class1='overlay';
      }

// Split lot by no
splitArr =[];
onSplitLot(){
  this.splitArr = [];
    // var iledQty = Number(this.selLotToSplit["iledQty"])/Number(this.nolot);
    // var noOfBag = Number(this.selLotToSplit["iledNoofBag"])/Number(this.nolot);
  var actNoOfBag = Number(this.selLotToSplit["iledNoofBag"]);
  console.log("splitArr",this.splitArr,Number(this.selLotToSplit["iledNoofBag"])%Number(this.nolot))
  var remainder = Number(this.selLotToSplit["iledNoofBag"])%Number(this.nolot);
  if(remainder != 0){
    var noOfBag = (Number(this.selLotToSplit["iledNoofBag"]) - remainder)/Number(this.nolot);
    for(var i= 0;i<Number(this.nolot)-1;i++){
      var quantity =  Number(this.selLotToSplit["gmptmQty"] )* noOfBag * this.quintalUOM;
      console.log("quantity",quantity )
      var rowObj = {"noOfBag": noOfBag,"bagType":this.selLotToSplit["gmptmDesc"],"approxQuantity":quantity};
      this.splitArr.push(rowObj);
      console.log("rowObj")
    }
    var lastNo = noOfBag + remainder;
    var quantity =  Number(this.selLotToSplit["gmptmQty"] )* lastNo * this.quintalUOM;
    var rowObj = {"noOfBag": lastNo,"bagType":this.selLotToSplit["gmptmDesc"],"approxQuantity":quantity};
    this.splitArr.push(rowObj);
  }
  else{
    var noOfBag = (Number(this.selLotToSplit["iledNoofBag"]))/Number(this.nolot);
    for(var i= 0;i<Number(this.nolot);i++){
      var quantity =  Number(this.selLotToSplit["gmptmQty"] )* noOfBag * this.quintalUOM;
      var rowObj = {"noOfBag": noOfBag,"bagType":this.selLotToSplit["gmptmDesc"],"approxQuantity":quantity};
      this.splitArr.push(rowObj);
    }
  }
  console.log("this.splitArr",this.splitArr)

}


// Get quantity in kg from quintal
conversionList = [];kgVal:string;unitList = [];quintalUOM:number;valInKg:string;
getQuantityInQuintal(){
  this.loading = true;
  this.authservice.getUOMList().subscribe(uomData=>{
    console.log("All uomData name: ",uomData.data);
    this.conversionList = uomData.data["uomlist"];
    console.log("this.conversionList",this.conversionList)
    for(var i=0;i<this.conversionList.length;i++){
      if(this.conversionList[i].gmucmAltUomId == "666" && this.conversionList[i].gmucmUomId == "444"){
        this.quintalUOM = this.conversionList[i].gmucmConvFact
        console.log("kgVal",this.quintalUOM)
      }
      if(this.conversionList[i].gmucmAltUomId == "444" && this.conversionList[i].gmucmUomId == "666"){
        this.valInKg = this.conversionList[i].gmucmConvFact
        console.log("kgVal",this.quintalUOM)
      }
    }
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {       this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);  }  });
                    return this.quintalUOM;
}

// on bag no change
onNoOfBagChange(bagNoVal,index){
    this.splitArr[index].noOfBag = Number(bagNoVal);
    var quantity =  Number(this.selLotToSplit["gmptmQty"] )* Number(bagNoVal) * this.quintalUOM;
    this.splitArr[index].approxQuantity = (quantity);
    console.log("modNoOfBag",bagNoVal,this.splitArr)
}



// save split
actualQuintal:string;calcQuintal:string;
onSplitSave(){
  console.log("selLotToSplit",this.selLotToSplit)
  console.log("splitArr",this.splitArr)
  var totalquintal = 0;
  for(var i=0;i<this.splitArr.length;i++){
    totalquintal = totalquintal + this.splitArr[i].approxQuantity;
  }
  console.log("calc quintal",totalquintal.toFixed(2))
  console.log("actual quintal",this.selLotToSplit["iledGrossQtyQuintal"])
  console.log("check",totalquintal.toFixed(4) == this.selLotToSplit["iledGrossQtyQuintal"] )
  // var totalquintal =
  this.actualQuintal = this.selLotToSplit["iledGrossQtyQuintal"];
  this.calcQuintal = totalquintal.toFixed(4);
  if(totalquintal.toFixed(4) == this.selLotToSplit["iledGrossQtyQuintal"]){
    // var qtyKgArr = [];
    // var qtyQuintalArr = [];
    // var noBag = [];
    var rowObj = {
    "noOfBag": "",
    "qtyKg": "",
    "qtyQn": ""
    }
    var splitObjArr = [];
    for(var i=0;i<this.splitArr.length;i++){
      splitObjArr.push(rowObj);
      splitObjArr[i]["noOfBag"] = this.splitArr[i].noOfBag.toString();
      splitObjArr[i]["qtyKg"] = (this.splitArr[i].approxQuantity * Number(this.valInKg)).toString();
      splitObjArr[i]["qtyQn"] = this.splitArr[i].approxQuantity.toString();
      // qtyQuintalArr.push(this.splitArr[i].approxQuantity);
      // qtyKgArr.push(this.splitArr[i].approxQuantity * Number(this.valInKg));
      // noBag.push(this.splitArr[i].noOfBag);
    }
    console.log("continue to save",splitObjArr)
    // console.log("continue to save qtyKgArr",qtyKgArr)
    // console.log("continue to save noBag",noBag)

    var obj = {
    "splitedRowList": splitObjArr,
    "splitMerge": this.selLotToSplit
    };
    console.log("obj",obj,this.splitArr)
    if(this.splitArr.length != 0){
      this.loading = true;
      this.authservice.saveSplitData(obj,this.splitArr.length).subscribe($data=>{
        console.log("$data",$data)
        this.loading = false;
        // if($data.status == '1'){
        //   this.my_class5 = 'overlay1';
        //   setTimeout(()=>{
        //     this.my_class5 = 'overlay';
        //     this.my_class1 = 'overlay';
        //   },2000);
        // }
        var fileURL = URL.createObjectURL($data);
        window.open(fileURL);
          this.my_class5 = 'overlay1';
          setTimeout(()=>{
            this.my_class5 = 'overlay';
            this.my_class1 = 'overlay';
          },2000);
        splitObjArr = [];
        this.onMargeSplitResetPage();
        this.searchMerge();
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class50 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
                  else{
                    this.my_class4 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                    },2000);
                  }
            });
    }
    else{
      this.my_class6 = 'overlay1';
      setTimeout(()=>{
        this.my_class6 = 'overlay';
      },2000);
    }
  }
  else{
    this.my_class3 = 'overlay1';
    setTimeout(()=>{
      this.my_class3 = 'overlay';
    },2000);
  }

}

// checkLotType:boolean;
onResetPage(){
  this.splitArr = this.selLotToSplit = [];
  this.actualQuintal = this.calcQuintal  = "";
  this.nolot = 0;
  this.selBagTypeObj = this.toMergeArr = [];
  this.showCommodity = this.bagDesc = this.modNoOfBag = this.showQtyKg = this.showQtyQuintal = "";
  this.lotFromDate = this.lotToDate  = "";
  // this.checkLotType  = true;
  this.lotType = 'N';
  this.filteredSplitMergeData =[];
  this.splitMergeData = [];
  this.lotCode ="";this.seller ="";this.trader =""; this.agent ="";this.commodity ="";this.company ="";
}
onMargeSplitResetPage(){
  this.splitArr = this.selLotToSplit = [];
  this.actualQuintal = this.calcQuintal  = "";
  this.nolot = 0;
  this.selBagTypeObj = this.toMergeArr = [];
  this.showCommodity = this.bagDesc = this.modNoOfBag = this.showQtyKg = this.showQtyQuintal = "";
  // this.checkLotType  = true;
  this.lotType = 'N';
  this.filteredSplitMergeData =[];
  this.splitMergeData = [];
  this.lotCode ="";this.seller ="";this.trader =""; this.agent ="";this.commodity ="";this.company ="";
}

selLotForAgent:any=[];
onAgentName(lotData){
  this.my_class8 = 'overlay1';
  this.selLotForAgent = lotData;
  this.agentName = this.selLotForAgent["lcamFullName1"];
  console.log("onAgentName",  this.selLotForAgent)
}
onAgentCompanyName(lotData){
  console.log("onAgentName")
  this.my_class8 = 'overlay1';
  this.selLotForAgent = lotData;
  this.agentName = this.selLotForAgent["lcamFullName1"];
  console.log("onAgentName",  this.selLotForAgent)
}
// Hide agent popup
hideAgentPopup(){
  this.my_class8 = 'overlay';
}

// open merge popup
sameLot:boolean;showCommodity:string;showQtyKg:string;showQtyQuintal:string;toMergeArr =[];sameSellerType:boolean;
openMergePopup(){
  this.sameSellerType = false;
  if(this.lotType == 'N'){
    console.log(this.splitMergeData);
    console.log("this.toMergeArr",this.toMergeArr);
    this.sameLot = false;
    this.toMergeArr =[];
    for(var i=0;i<this.splitMergeData.length;i++){
      if(this.splitMergeData[i].checked == true){
        this.toMergeArr.push(this.splitMergeData[i]);
      }
    }
    if(this.toMergeArr.length > 1){
      for(var i=0;i<this.toMergeArr.length;i++){
        var toCheck = this.toMergeArr[i];
        for(var j=0;j<this.toMergeArr.length;j++){
          if(toCheck.iledImpoType == this.toMergeArr[j].iledImpoType){
            this.sameSellerType = true;
          }
          else{
            this.sameSellerType = false;
            break;
          }
        }
        if(this.sameSellerType == true){
          for(var j=0;j<this.toMergeArr.length;j++){
              console.log("same type",toCheck.iledImpoType ,this.toMergeArr[j].iledImpoType)
              if(toCheck.iledImpoType == 'T'){
                if((toCheck.gmpmgProdId != this.toMergeArr[j].gmpmgProdId) || (toCheck.iledTraderId != this.toMergeArr[j].iledTraderId)){
                  this.sameLot = false;
                  break;
                }
                else{
                  this.sameLot = true;
                }
              }
              else{
                if((toCheck.gmpmgProdId != this.toMergeArr[j].gmpmgProdId) || (toCheck.iledFarmerId != this.toMergeArr[j].iledFarmerId)){
                  this.sameLot = false;
                  break;
                }
                else{
                  this.sameLot = true;
                }
              }
          }
        }

      }
      console.log("obj",this.sameLot)
      if(this.sameLot == true){
        this.my_class11 = 'overlay1';
        this.showCommodity = this.toMergeArr[0].gmpmgProdName;
        var showTotalKg =0;
        var showTotalQn =0;
        for(var i=0;i<this.toMergeArr.length;i++){
          showTotalKg = showTotalKg + Number(this.toMergeArr[i].iledQty);
          showTotalQn = showTotalQn + Number(this.toMergeArr[i].iledGrossQtyQuintal);
        }
        this.showQtyKg = showTotalKg.toString();
        this.showQtyQuintal = showTotalQn.toString();

      }
      else{
        this.my_class9 = 'overlay1';
        setTimeout(()=>{
          this.my_class9 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class10 = 'overlay1';
      setTimeout(()=>{
        this.my_class10 = 'overlay';
      },2000);
    }
  }
  else{
    this.my_class15 = 'overlay1';
    setTimeout(()=>{
      this.my_class15 = 'overlay';
    },2000);
  }
}

// hide popup for merge
hideMergePopup(){
  this.my_class11 = 'overlay';
}
// open bag type popup
allBagTypedata=[];filteredBagData=[];
onSelectBagType(){
  this.my_class12 = 'overlay1';
  this.loading = true;
    this.authservice.getBagType().subscribe($allBagData=>{
      this.allBagTypedata=$allBagData.listData;
      this.filteredBagData=this.allBagTypedata;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  );
}

bagTypeVal=[];selectedBag:number;
selectedBagData(post5:any,index5){
  this.selectedBag=index5;
  this.bagTypeVal = post5;
}
// hide bag type popup
hideBagTypePopup(){
  this.my_class12 = 'overlay';
}
selBagTypeObj =[];bagDesc:string;modNoOfBag:string;bagQuantityKG:number;bagQuantityQn:number;
selBagTypeOK(){
    this.my_class12='overlay';
    this.selBagTypeObj = this.bagTypeVal;
    this.bagDesc = this.selBagTypeObj["gmptm_desc"];
    this.bagQuantityKG = this.selBagTypeObj["gmptm_qty"];
    this.modNoOfBag =( Number(this.showQtyKg)/this.bagQuantityKG).toFixed(2).toString();
    console.log("this.showQtyKg",this.showQtyKg)
    console.log("this.bagQuantityKG",this.bagQuantityKG)
    console.log("this.modNoOfBag",this.modNoOfBag)
  }

onMergeSave(){
  if(this.selBagTypeObj != undefined && this.showQtyKg != undefined && this.showQtyQuintal != undefined){
    var postJson={
      "lotList": this.toMergeArr,
      "noOfBag" :this.modNoOfBag,
      "qtyKg": this.showQtyKg.toString(),
      "qtyQn": this.showQtyQuintal.toString(),
      "bagTypes": this.selBagTypeObj
    }
    console.log("postJson",postJson,this.selBagTypeObj)
    this.loading = true;
    this.authservice.saveMergeData(postJson).subscribe($data=>{
      console.log("$data",$data)
      this.loading = false;
      var fileURL = URL.createObjectURL($data);
      window.open(fileURL);
        this.my_class11 = 'overlay1';
        setTimeout(()=>{
          this.my_class13 = 'overlay';
          this.my_class11 = 'overlay';
        },2000);
      // if($data.status == '1'){
      //   this.my_class11 = 'overlay1';
      //   setTimeout(()=>{
      //     this.my_class13 = 'overlay';
      //     this.my_class11 = 'overlay';
      //   },2000);
      // }
      this.onMargeSplitResetPage();
      this.searchMerge();
    }, (err) => {
                if (err === 'Unauthorized')
                {
                  this.errorPopupShow();
                        setTimeout(()=>{
                          this.my_class50 = 'overlay';
                          localStorage.clear();
                          this.router.navigateByUrl('/login');
                        },3000);
                }
                else{
                  this.my_class4 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class4 = 'overlay';
                  },2000);
                }
          });
  }
  else{
    this.my_class14 = 'overlay1';
    setTimeout(()=>{
     this.my_class14 = 'overlay';
    },2000);
  }

}

// Filter By lotcode
lotCodeFilter(){
  if(this.splitMergeData != undefined && this.splitMergeData.length != 0){
    var filteredData = _.filter(this.splitMergeData, (a)=>a.iledLotCode.indexOf(this.lotCode)>=0);
    this.filteredSplitMergeData = filteredData;
  }
}
// Filter By seller
sellerNameFilter(){
  var sellerNames=[];
  if(this.splitMergeData != undefined && this.splitMergeData.length != 0){
    for(var i=0;i<this.splitMergeData.length;i++){
      if(this.splitMergeData[i].iledFarmerName != undefined){
        sellerNames.push(this.splitMergeData[i]);
      }
    }
    if(this.seller){
      var filteredData = _.filter(sellerNames, (a)=>a.iledFarmerName.toLowerCase().indexOf(this.seller.toLowerCase())>=0);
      this.filteredSplitMergeData = filteredData;
    }
    else{
      this.filteredSplitMergeData = this.splitMergeData;
    }
  }
}
// Filter By trader
traderNameFilter(){
  var traderNames=[];
  if(this.splitMergeData != undefined && this.splitMergeData.length != 0){
    for(var i=0;i<this.splitMergeData.length;i++){
      if(this.splitMergeData[i].iledFarmerName != undefined){
        traderNames.push(this.splitMergeData[i]);
      }
    }
    if(this.trader){
      var filteredData = _.filter(traderNames, (a)=>a.iledFarmerName.toLowerCase().indexOf(this.trader.toLowerCase())>=0);
      this.filteredSplitMergeData = filteredData;
    }
    else{
      this.filteredSplitMergeData = this.splitMergeData;
    }
  }
}

// Filter By company
companyNameFilter(){
  var compNames=[];
  if(this.splitMergeData != undefined && this.splitMergeData.length != 0){
    for(var i=0;i<this.splitMergeData.length;i++){
      if(this.splitMergeData[i].lcamAgenCompName != undefined){
        compNames.push(this.splitMergeData[i]);
      }
    }
    if(this.company){
      var filteredData = _.filter(compNames, (a)=>a.lcamAgenCompName.toLowerCase().indexOf(this.company.toLowerCase())>=0);
      this.filteredSplitMergeData = filteredData;
    }
    else{
      this.filteredSplitMergeData = this.splitMergeData;
    }
  }
}
// Filter By commodity
commmodityNameFilter(){
  var commodityNames=[];
  if(this.splitMergeData != undefined && this.splitMergeData.length != 0){
    for(var i=0;i<this.splitMergeData.length;i++){
      if(this.splitMergeData[i].gmpmgProdName != undefined){
        commodityNames.push(this.splitMergeData[i]);
      }
    }
    if(this.commodity){
      var filteredData = _.filter(commodityNames, (a)=>a.gmpmgProdName.toLowerCase().indexOf(this.commodity.toLowerCase())>=0);
      this.filteredSplitMergeData = filteredData;
    }
    else{
      this.filteredSplitMergeData = this.splitMergeData;
    }
  }
}

// Filter By agent
agentNameFilter(){
  var agentNames=[];
  if(this.splitMergeData != undefined && this.splitMergeData.length != 0){
    for(var i=0;i<this.splitMergeData.length;i++){
      if(this.splitMergeData[i].lcamFullName1 != undefined){
        agentNames.push(this.splitMergeData[i]);
      }
    }
    if(this.agent){
      var filteredData = _.filter(agentNames, (a)=>a.lcamFullName1.toLowerCase().indexOf(this.agent.toLowerCase())>=0);
      this.filteredSplitMergeData = filteredData;
    }
    else{
      this.filteredSplitMergeData = this.splitMergeData;
    }
  }
}
// on agent name
selAgentName(){
  this.my_class17 ='overlay1';
  this.selectTraderName();
}
//select Trader Name
sellerNameData =[];caListData=[];
selectTraderName(){
  this.loading = true;
  console.log("Trader type selected");
  this.authservice.getCAList().subscribe($allTraderData=>{
    console.log("Trader List data: ",$allTraderData.listData);
    this.sellerNameData=$allTraderData.listData;
    this.caListData=this.sellerNameData;
    this.loading = false;
  }, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class50 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);

      }
    }
);
}
selTraderVal:any;selectedRow:number;
onTraderSelect(post,index){
  this.selTraderVal = post;
  this.selectedRow=index;
  console.log("selected row is1 : ",this.selTraderVal);
  console.log("selected row is2 : ",post);

}

traderObjVal=[];agentName:string;
selTraderOK(){
this.traderObjVal = this.selTraderVal;
this.my_class17 ='overlay';
console.log("this.traderObjVal",this.traderObjVal)
this.agentName = this.traderObjVal["lcamFullName"];
}
hideTraderPoup(){
  this.my_class17 ='overlay';
}
onUpdateAgent(){
  var postJson={"split": this.selLotForAgent ,"ca" : this.traderObjVal}
  console.log("post",postJson)
  this.loading = true;
  this.authservice.updateAgent(postJson).subscribe($allTraderData=>{
    this.loading = false;
    this.my_class18 = 'overlay1';
    setTimeout(()=>{
     this.my_class18 = 'overlay';
     this.my_class8 = 'overlay';
    },2000);
    this.searchMerge();
    this.selLotForAgent= [];
    this.traderObjVal =[];
    this.agentName ="";
  }, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class50 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);

      }
    })
}
}
