import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssayingComponent } from './assaying.component';

describe('AssayingComponent', () => {
  let component: AssayingComponent;
  let fixture: ComponentFixture<AssayingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssayingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssayingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
