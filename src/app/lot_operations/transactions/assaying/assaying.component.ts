import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';



@Component({
  selector: 'app-assaying',
  templateUrl: './assaying.component.html',
  styleUrls: ['./assaying.component.css']
})
export class AssayingComponent implements OnInit {
  public loading = false;

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';
  public my_class9='overlay';public my_class10='overlay';public my_class11='overlay';
  public shown1:any=true;
  public shown2:any=false;
  fromDateInsert:any;
  toDateInsert:any;
  assayingLotData=[];
  lotCodeFilter:any;
  sampleCodeFilter:any;
  selectedSampleRow : Number;

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false
  };
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router
  ) { }

  ngOnInit() {
  }
  onAdvance1(){
    this.shown1=false;
    this.shown2=true;
  }
  onBasic1(){
    this.shown1=true;
    this.shown2=false;
  }

  div_show(){
      this.my_class1='overlay1';
    }
  div_hide(){
      this.fromDateInsert = "";
      this.toDateInsert = "";
      this.lotCodeFilter = "";
      this.assayingLotData = [];
      this.filteredData = [];
      this.selectedRow = -1;
      this.my_class1='overlay';
    }
    onLotCodeReset(){
      this.fromDateInsert = "";
      this.toDateInsert = "";
      this.lotCodeFilter = "";
      this.assayingLotData = [];
      this.filteredData = [];
      this.lotCodeFilter = "";
      this.selectedRow = -1;
    }
    div_hide_sample(){
      console.log("here")
      this.selectedSampleRow = -1;
      this.sampleIdData = [];
      this.sampledFilteredData = [];
      this.my_class2='overlay';
      this.sampleCodeFilter = "";
    }
    onSampleIdReset(){
      this.sampleIdData = [];
      this.sampledFilteredData = [];
      this.sampleCodeFilter = "";
      this.selectedSampleRow = -1;
    }
    errorPopupShow(){
      this.my_class4='overlay1';
    }
//By Najma


assayingLotDataObj = [];
filteredData = [];
onSearchLotCode(){
  console.log("1",this.toDateInsert);
  console.log("2",this.fromDateInsert);
  var flashMessagesService = this.flashMessagesService;
  console.log("3",this.lotCodeFilter);

  if(this.toDateInsert == undefined || this.fromDateInsert == undefined || this.toDateInsert == "" || this.fromDateInsert == ""){
    flashMessagesService.show('Dates are  mandatory to search. LotCode is optional .', { cssClass: 'alert-danger', timeout: 3000 });
  }
    else{
      // this.toDateInsert = this.toDateInsert.formatted;
      // this.fromDateInsert = this.fromDateInsert.formatted;

      let postJson = {"fromDate": this.fromDateInsert.formatted, "toDate": this.toDateInsert.formatted};
      this.loading = true;
      this.authservice.lotCodeSearchForAssaying(postJson).subscribe(
        (res) => {
              let $data=res;
              if($data.listData != undefined){
                this.assayingLotData = $data.listData;
                this.assayingLotDataObj = $data.listData;
                this.filteredData = this.assayingLotData;
                console.log("data",this.assayingLotData);
              }
              else{
                this.assayingLotData = [];
                this.assayingLotDataObj = [];
                this.filteredData = [];
              }
              this.loading = false;
          }
          , (err) => {
                      if (err === 'Unauthorized')
                      {
                        this.errorPopupShow();
                              setTimeout(()=>{
                                this.my_class4 = 'overlay';
                                localStorage.clear();
                                this.router.navigateByUrl('/login');
                              },3000);
                      }
                })
    }
}

selectedRow : Number;
selectedLotCode:any;
selectedLot(post,index){
   this.selectedRow = index;
   this.selectedLotCode = post;
}


assayingParamterData=[];
listParamterData=[];
selLot:string;
commodityName:string;
selectedLotRow(){
  //this.fromDateInsert = "";
//  this.toDateInsert = "";
  //this.lotCodeFilter = "";
  this.assayingLotData = [];
  this.filteredData = [];
  this.selectedRow = -1;

  console.log("ok",this.selectedLotCode);
  if(this.selectedLotCode != undefined){
    this.my_class1='overlay';
    this.selLot = this.selectedLotCode.iledlotcode;
    this.selectedSampleCode = [];
    this.sampleIdData =[];
    this.sampledFilteredData =[];
    this.sampleIdDataObj =[];
    this.selectedSampleObj =[];
    console.log("ok",this.selectedLotCode,this.selLot);
    this.loading = true;
    this.authservice.commodityForAssaying(this.selectedLotCode).subscribe(
      (res) => {
          this.loading = false;
          let $data=res;
          this.commodityName = $data.data.gmpmgprodname;
        }
        , (err) => {
                    if (err === 'Unauthorized')
                    {
                      this.errorPopupShow();
                            setTimeout(()=>{
                              this.my_class4 = 'overlay';
                              localStorage.clear();
                              this.router.navigateByUrl('/login');
                            },3000);
                    }
              })

    this.loading = true;
    this.authservice.parameterListForAssaying(this.selectedLotCode).subscribe(
      (res) => {
          this.loading = false;
          let $data=res;
          this.assayingParamterData = $data;
          this.listParamterData =$data.listData;
          console.log("param",this.assayingParamterData);
        }
        , (err) => {
                    if (err === 'Unauthorized')
                    {
                      this.errorPopupShow();
                            setTimeout(()=>{
                              this.my_class4 = 'overlay';
                              localStorage.clear();
                              this.router.navigateByUrl('/login');
                            },3000);
                    }
              })
  }
  else{
    console.log("here");
  this.flashMessagesService.show('Please Select Lot Row Below', { cssClass: 'alert-danger', timeout: 3000 });
  }
}

sampleIdData = [];
sampleIdDataObj = [];
openSamplePopUp(){
  if(this.selLot != undefined && this.selLot != ""){
    this.my_class2='overlay1';
    this.loading = true;
    this.authservice.sampleIdForAssaying(this.selectedLotCode).subscribe(
      (res) => {
          let $data=res;
          this.sampleIdData = $data.listData;
          this.sampledFilteredData = $data.listData;
          this.sampleIdDataObj = $data.listData;
          console.log("sample",this.sampleIdDataObj);
          this.loading = false;
        }
        , (err) => {
                    if (err === 'Unauthorized')
                    {
                      this.errorPopupShow();
                            setTimeout(()=>{
                              this.my_class4 = 'overlay';
                              localStorage.clear();
                              this.router.navigateByUrl('/login');
                            },3000);
                    }
              })
  }

}


selectedSampleCode:any;
selectedSampleObj=[];
selectedSample(post,index){
   this.selectedSampleRow = index;
   this.selectedSampleCode = post.iledsampleid;
   this.selectedSampleObj = post;

}

selectedAssayingSample(){
 console.log("selectedSampleObj",this.selectedSampleCode);
 if(this.selectedSampleCode == undefined || this.selectedSampleCode == ""){
   this.flashMessagesService.show('Please Choose Sample Id', { cssClass: 'alert-danger', timeout: 3000 });
 }
 else{
   this.my_class2='overlay';
   this.selectedSampleRow = -1;
   this.sampleCodeFilter ="";
 }

}

onMeasureValue(index,measureValue){
  console.log("k",index,measureValue);
  if(this.listParamterData[index].gmpamtype == 'N'){
    console.log("here",isNaN(measureValue) );
    if(isNaN(measureValue)){
      (<HTMLInputElement>document.getElementById("measureAmount_"+index)).value = "";
      this.my_class10 = 'overlay1';
      setTimeout(()=>{
        this.my_class10 = 'overlay';
      },2000);
    }
    else{
      if(this.listParamterData[index].gmpammaxval != undefined && this.listParamterData[index].gmpammaxval != "" && this.listParamterData[index].gmpamminval != undefined && this.listParamterData[index].gmpamminval != ""){
        var minRange = this.listParamterData[index].gmpamminval;
        var maxRange = this.listParamterData[index].gmpammaxval;
        if((Number(measureValue) < Number(maxRange)) && (Number(measureValue) > Number(minRange))){
          this.listParamterData[index].mesuredValue = measureValue;
        }
        else{
              (<HTMLInputElement>document.getElementById("measureAmount_"+index)).value = "";
              this.my_class9 = 'overlay1';
              setTimeout(()=>{
                this.my_class9 = 'overlay';
              },2000);
        }
      }
      else{
        this.listParamterData[index].mesuredValue = measureValue;
      }
    }
  }
  else{
    this.listParamterData[index].mesuredValue = measureValue;
  }
  console.log("measureValue",this.listParamterData);

}

checkMeasuredValue:boolean;
savedRes:any;
onSaveAssaying(){
  this.checkMeasuredValue = true;
  var postJson  = {
    "assayingList": this.selectedLotCode,
    "assyaingSampleId": this.selectedSampleObj,
    "assayingParameters": this.listParamterData
  };
  console.log("json in making",postJson)
  if(this.listParamterData.length == 0){
    this.my_class6 = 'overlay1';
    setTimeout(()=>{
      this.my_class6 = 'overlay';
    },2000);
  }
  else{
    if(this.selectedSampleObj.length != 0){
      console.log("listParamterData",this.listParamterData.length)

      for(let i=0;i<this.listParamterData.length;i++){
        console.log("MeasuredValue in",this.listParamterData[i].mesuredValue)
        if(this.listParamterData[i].gmpamtype == 'F' && this.listParamterData[i].mesuredValue == undefined){
          this.listParamterData[i].mesuredValue  = this.listParamterData[i].gmpamrange1;
        }
        if(this.listParamterData[i].mesuredValue == undefined || this.listParamterData[i].mesuredValue == ""){
          this.checkMeasuredValue = false;
          break;
        }
      }
       console.log("json in making",postJson)
       console.log("checkMeasuredValue",this.checkMeasuredValue)
       if(this.checkMeasuredValue == true){
         this.loading = true;
         this.authservice.saveForAssaying(postJson).subscribe(
           (res) => {
               this.loading = false;
               let $data=res;
               console.log("save res",$data);
               this.savedRes = $data;
               this.my_class8 = 'overlay1';
               setTimeout(()=>{
                 this.my_class8 = 'overlay';
               },2000);
               this.resetAssaying();
             }
             , (err) => {
                         if (err === 'Unauthorized')
                         {
                           this.errorPopupShow();
                                 setTimeout(()=>{
                                   this.my_class4 = 'overlay';
                                   localStorage.clear();
                                   this.router.navigateByUrl('/login');
                                 },3000);
                         }
                         else{
                           this.my_class11 = 'overlay1';
                           setTimeout(()=>{
                             this.my_class11 = 'overlay';
                           },2000);
                         }
                   })
       }
       else{
         this.my_class7 = 'overlay1';
         setTimeout(()=>{
           this.my_class7 = 'overlay';
         },2000);
       }
    }
    else{
      console.log("no length")
      this.my_class5 = 'overlay1';
      setTimeout(()=>{
        this.my_class5 = 'overlay';
      },2000);
    }
  }
}

onLotCodeFilter(){
  if(this.lotCodeFilter != undefined && this.lotCodeFilter != ""){
    this.filteredData = _.filter(this.assayingLotData , (a)=>a.iledlotcode.indexOf(this.lotCodeFilter)>=0);
  }
  else{
  this.filteredData  = this.assayingLotData;
  }
}


sampledFilteredData = [];
onSampleCodeFilter(){
  console.log(this.sampleCodeFilter);
  console.log(this.sampleIdData);
  if(this.sampleCodeFilter != undefined && this.sampleCodeFilter != ""){
    this.sampledFilteredData = _.filter(this.sampleIdData, (a)=>a.iledsampleid.indexOf(this.sampleCodeFilter)>=0);
  }
  else{
    this.sampledFilteredData = this.sampleIdData ;
  }
}

resetAssaying(){
  this.filteredData = this.listParamterData = this.selectedSampleObj = [];
  this.sampledFilteredData = [];
  this.assayingParamterData = [];
  this.selectedLotCode = [];
  this.selLot = "";
  this.selectedSampleCode = "";
  this.commodityName = "";

}

}
