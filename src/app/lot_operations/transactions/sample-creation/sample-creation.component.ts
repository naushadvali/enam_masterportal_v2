import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { FormsModule, FormGroup }   from '@angular/forms';
import * as _ from 'lodash';



@Component({
  selector: 'app-sample-creation',
  templateUrl: './sample-creation.component.html',
  styleUrls: ['./sample-creation.component.css'],
  providers: [DatePipe]
})
export class SampleCreationComponent implements OnInit {
  public loading = false;
  lotFromDate:any;
  lotToDate:any;
  dateChange1:any;
  dateChange2:any;
  monthChnage1:any;
  monthChnage2:any;
  fromDate:any;
  toDate:any;
  fromDateInsert=[];
  quantityValue=[];
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4 = "overlay";
  public my_class5 = "overlay";

  selectedSampleRow : Number;
  matchSellerName:any;
  matchTraderName:any;
  matchAgentName:any;
  matchCommodityName:any;
  matchCompanyName:any;

  defaultMode:boolean;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
    private datepipe:DatePipe
  ) { }

  ngOnInit() {
    this.defaultMode = true;
    this.modifyMode = false;
  }

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  errorPopupShow(){
   this.my_class4='overlay1';
  }
  div_hide_sample(){
    this.my_class1='overlay';
    this.fromDateInsert = [];
    this.quantityValue = [];
    this.rowArr = [];
    this.selectedSampleRow = -1;
  }

  sampleDataList = [];
  filteredSampleData = [];
  searchSample(){
    console.log("aa",this.lotFromDate)

    if(this.lotFromDate != undefined && this.lotFromDate != ""){
      const lotDates={
        "fromDate":this.lotFromDate.formatted
      };
      this.sampleDataList =[];
      this.filteredSampleData =[];
      this.loading = true;
      this.authservice.sampleCreationSearch(lotDates).subscribe($sampleData=>{
        console.log("Sample Data after search: ",$sampleData);
        if($sampleData.listData != undefined){
          this.sampleDataList = $sampleData.listData;
          this.filteredSampleData = $sampleData.listData;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
            });
    }
    else{
      this.my_class3 = 'overlay1';
      setTimeout(()=>{
        this.my_class3 = 'overlay';
      },2000);    }

  }

  sampleDetailsData : any;
  commodityName : string;
  lotCode : string; previousSampleDtls =[];
  onSampleDetails(post){
    console.log("post",post);
    this.sampleDetailsData = post;
    this.lotCode = this.sampleDetailsData.iled_lot_code;
    this.commodityName = this.sampleDetailsData.gmpmg_prod_name;
    this.my_class1='overlay1';
  }

  uomVal = [];
  onUOM(uomVal,index){
    this.uomVal[index] = uomVal;
    console.log("uomVal",this.uomVal);
  }

  checkAllRowValue:boolean;
  saveSampleData(){
    console.log("uomVal",this.uomVal);
    console.log("quantityValue",this.quantityValue);
    console.log("fromDateInsert",this.fromDateInsert);
    //var date = this.datepipe.transform(new Date(), 'yyyy-MM-dd') + ' 00:00:00';
// this.fromDateInsert.jsdate
    this.checkAllRowValue = false;
    if(this.rowArr.length != 0){
      for(var i=0;i<this.rowArr.length;i++){
        console.log("quantityValuei",Number(this.quantityValue[i]));

        if((this.quantityValue[i] != "" && Number(this.quantityValue[i]) > 0)){
          console.log("this.fromDateInsert[i]",this.fromDateInsert[i]);

          if(this.fromDateInsert[i].jsdate == undefined){
            if(this.fromDateInsert[i].date == undefined){
              this.rowArr[i].qty = this.quantityValue[i];
              this.rowArr[i].UOM = this.uomVal[i];
              this.rowArr[i].rcdDateTime = this.fromDateInsert[i];
            }
            else{
              var dd = this.fromDateInsert[i].date.day;
              var mm = this.fromDateInsert[i].date.month;
              if(dd<10){
                dd = "0"+dd;
              }
              if(mm<10){
                mm = "0"+mm;
              }
              var date = (this.fromDateInsert[i].date.year+'-'+mm+'-'+dd) + ' 00:00:00';
              this.rowArr[i].qty = this.quantityValue[i];
              this.rowArr[i].UOM = this.uomVal[i];
              this.rowArr[i].rcdDateTime = date;
            }
          }
          else{
            var date = this.datepipe.transform(new Date(this.fromDateInsert[i].jsdate), 'yyyy-MM-dd') + ' 00:00:00';
            this.rowArr[i].qty = this.quantityValue[i];
            this.rowArr[i].UOM = this.uomVal[i];
            this.rowArr[i].rcdDateTime = date;
          }
        }
        else{
        this.checkAllRowValue = true;
        break;
        }
      }
      if(this.checkAllRowValue == false){
        var postJson = {
          "smpl": this.sampleDetailsData,
          "sampleDataList": this.rowArr
        }
        console.log("postJson",postJson)
        console.log("row",this.rowArr)
        console.log("checkAllRowValue",this.checkAllRowValue)
        this.loading = true;
        this.authservice.saveForSample(postJson).subscribe($sampleData=>{
          console.log("$sampleData",$sampleData)
          this.loading = false;
          if($sampleData.status == '1'){
            this.searchSample();
            this.my_class2 = 'overlay1';
            setTimeout(()=>{
              this.my_class2 = 'overlay';
              this.my_class1 = 'overlay';
              this.fromDateInsert = [];
              this.quantityValue = [];
              this.rowArr = [];
              this.selectedSampleRow = -1;
            },2000);
          }
        }, (err) => {
                    if (err === 'Unauthorized')
                    {
                      this.errorPopupShow();
                            setTimeout(()=>{
                              this.my_class1 = 'overlay';
                              localStorage.clear();
                              this.router.navigateByUrl('/login');
                            },3000);
                    }
                    else{
                      this.my_class5 = 'overlay1';
                      setTimeout(()=>{
                        this.my_class5 = 'overlay';
                      },2000);
                    }
              });
      }
      else{
        this.flashMessagesService.show('Quantity Can Not Be Null Or Less Than Zero.', { cssClass: 'alert-danger', timeout: 3000 });
      }

    }
    else{
      this.flashMessagesService.show('Please Add Row First', { cssClass: 'alert-danger', timeout: 3000 });
    }


  }

  rowArr=[];
  selSample=[];
  valuedate:any;
  addRow(){
    this.defaultMode = true;
    this.modifyMode = false;
    console.log("row before",this.rowArr.length)
    console.log("row before", new Date())
    this.valuedate = new Date();
    var dd = this.valuedate.getDate();
    var mm = this.valuedate.getMonth()+1; //January is 0!
    var yyyy = this.valuedate.getFullYear();
    this.fromDateInsert[this.rowArr.length] =
    { date: { year: this.valuedate.getFullYear(), month: mm, day: dd } };
    this.uomVal[this.rowArr.length] = "444"
      var rowObj =  {
          "qty": "",
          "UOM": this.uomVal[this.rowArr.length],
          "rcdDateTime": this.datepipe.transform(new Date(), 'yyyy-MM-dd') + ' 00:00:00'
        };
        this.rowArr.push(rowObj);
        console.log("row after",this.rowArr)


  }
onSelAddedRow(index,post,rows){
  console.log(index,post,rows);
  this.selSample = post;
  this.selectedSampleRow = index;
}

deleteRow(index){
  this.defaultMode = true;
  this.modifyMode = false;
  this.selectedSampleRow = index;
 //this.rowArr = this.rowArr.splice(Number(index), 1);
 if(this.selectedSampleRow != -1){
   for(var i =0;i< this.rowArr.length; i++){
   if (i === this.selectedSampleRow){
      this.rowArr.splice(i, 1);
      this.fromDateInsert.splice(i, 1);
      this.quantityValue.splice(i, 1);
      this.uomVal.splice(i, 1);
      break;
    }
  }
 }
 else{
   this.flashMessagesService.show('Please Select Row To Delete', { cssClass: 'alert-danger', timeout: 3000 });
 }

this.selectedSampleRow = -1;
console.log(this.rowArr);
console.log("this.selectedSampleRow",this.selectedSampleRow);
}

onResetSample(){
  this.sampleDataList = [];
  this.filteredSampleData = this.rowArr = [];
  this.lotFromDate = this.matchSellerName = this.matchAgentName = this.matchTraderName = this.matchCompanyName = this.matchCommodityName = "";

}

onSeller(){
console.log("sellerName",this.matchSellerName);
var filteredData = _.filter(this.sampleDataList, (a)=>a.frm_full_name.indexOf(this.matchSellerName)>=0);
this.filteredSampleData = filteredData;
}

onTrader(){
var filteredData = _.filter(this.sampleDataList, (a)=>a.frm_full_name.indexOf(this.matchTraderName)>=0);
this.filteredSampleData = filteredData;
}

onCommodity(){
  var filteredData = _.filter(this.sampleDataList, (a)=>a.gmpmg_prod_name.indexOf(this.matchCommodityName)>=0);
  this.filteredSampleData = filteredData;
}

onAgentName(){
  if(this.matchAgentName != ""){
    var filteredData = _.filter(
        this.sampleDataList, (a)=>{
          if(a.lcam_full_name != undefined){
            a.lcam_full_name.indexOf(this.matchAgentName)>=0
          }
        });
    this.filteredSampleData = filteredData;
  }
  else{
    this.filteredSampleData = this.sampleDataList;
  }
}

onCompanyName(){
  if(this.matchCompanyName != ""){
    var filteredData = _.filter(
        this.sampleDataList, (a)=>{
          if(a.lcam_agen_comp_name != undefined){
            a.lcam_agen_comp_name.indexOf(this.matchCompanyName)>=0
          }
        });
    this.filteredSampleData = filteredData;
  }
  else{
    this.filteredSampleData = this.sampleDataList;
  }
}


// Modify row
modifyMode:boolean;fromDateModify =[];quantityModValue =[];uomModVal= [];actualSampleDetails=[];
modifyRow(){
  this.modifyMode = true;
  this.defaultMode = false;
  this.loading = true;
  this.authservice.getPreviousSampleDetails(this.sampleDetailsData).subscribe($sampleData=>{
    console.log("Sample Data after search: ",$sampleData);
    if($sampleData.listData != undefined){
      this.previousSampleDtls = $sampleData.listData;
      // for(var i=0;i<this.previousSampleDtls.length;i++){
      //   this.quantityModValue[i] = this.previousSampleDtls[i]["iledQty"];
      //   this.uomModVal[i] = this.previousSampleDtls[i]["gmumUomId"];
      //   this.fromDateModify[i] = this.previousSampleDtls[i]["iledRecdDtTime"];
      //   var rowObj =  {
      //       "qty": this.previousSampleDtls[i]["iledQty"],
      //       "UOM": this.previousSampleDtls[i]["gmumUomId"],
      //       "rcdDateTime": this.previousSampleDtls[i]["iledRecdDtTime"]
      //     };
      //     this.modRowArr.push(rowObj);
      //     var currentDate = new Date(this.fromDateInsert[i]);
      //     var date = currentDate.getDate();
      //     var month = currentDate.getMonth()+1;
      //     var year = currentDate.getFullYear();
      //     this.fromDateModify[i] =
      //     { date: { year: year, month: month, day: date } };
      // }
      // console.log("uomVal",this.uomVal);
      // console.log("quantityValue",this.quantityValue);
      // console.log("fromDateInsert",this.fromDateInsert);
      this.actualSampleDetails = this.previousSampleDtls;
      for(var i=0;i<this.previousSampleDtls.length;i++){
        this.quantityModValue[i] = this.previousSampleDtls[i]["iledQty"];
        this.uomModVal[i] = this.previousSampleDtls[i]["gmumUomId"];
        this.fromDateModify[i] = this.previousSampleDtls[i]["iledRecdDtTime"];
        this.previousSampleDtls[i].checked = false;
        // var rowObj =  {
        //     "qty": this.previousSampleDtls[i]["iledQty"],
        //     "UOM": this.previousSampleDtls[i]["gmumUomId"],
        //     "rcdDateTime": this.previousSampleDtls[i]["iledRecdDtTime"],
        //     "checked" : false
        //   };
        //   this.modRowArr.push(rowObj);
          var currentDate = new Date(this.fromDateModify[i]);
          var date = currentDate.getDate();
          var month = currentDate.getMonth()+1;
          var year = currentDate.getFullYear();
          this.fromDateModify[i] =
          { date: { year: year, month: month, day: date } };
    }
  }
  this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
              else{
                this.my_class5 = 'overlay1';
                setTimeout(()=>{
                  this.my_class5 = 'overlay';
                },2000);
              }
        });


}

// on sel modifyRow
modifySample =[];selectedModifyRow:number;
onSelModifyRow(index,post,rows){
  console.log(index,post,rows);
  this.modifySample = post;
  this.selectedModifyRow = index;
}

// modify uom
modifyUOM(uomVal,index){
  this.previousSampleDtls[index].gmumUomId = uomVal;
  console.log("uomVal",this.previousSampleDtls);
}

// check modify sample
onModifyCheck(index,checked){
  console.log("checked",checked)
  if(checked == true){
    this.previousSampleDtls[index].checked = true;
  }
  else{
    // this.selectedModifyRow = -1;
    this.previousSampleDtls[index].checked = false;
    // var currentDate = new Date(this.previousSampleDtls[index].iledRecdDtTime);
    // var date = currentDate.getDate();
    // var month = currentDate.getMonth()+1;
    // var year = currentDate.getFullYear();
    // this.fromDateModify[index] =
    // { date: { year: year, month: month, day: date } };
    // this.quantityModValue[index] = this.previousSampleDtls[index].iledQty;
    // this.uomModVal[index] = this.previousSampleDtls[index].gmumUomId;
    // this.previousSampleDtls[index].gmumUomId = this.actualSampleDetails[index].gmumUomId;
    // this.fromDateModify[index] = this.previousSampleDtls[index]["iledRecdDtTime"];
    // this.quantityModValue[index] = this.previousSampleDtls[index]["iledQty"];
    // this.modRowArr[index].UOM = this.previousSampleDtls[index]["gmumUomId"];
  }
  // console.log("this.modRowArr",this.modRowArr)
  console.log("this.previousSampleDtls",this.previousSampleDtls,this.previousSampleDtls)
}


saveModifySampleData(){
  console.log("this.previousSampleDtls",this.previousSampleDtls)
  console.log("this.fromDateModify[i]",this.fromDateModify)
  var modArr =[];
  for(var i=0;i<this.previousSampleDtls.length;i++){
    if(this.previousSampleDtls[i].checked == true){
      modArr.push(this.previousSampleDtls[i]);
      // modArr[i]["iledRecdDtTime"] = this.fromDateModify[i];
      modArr[i]["iledQty"] = this.quantityModValue[i].toString();
      modArr[i]["gmumUomId"] = this.previousSampleDtls[i].gmumUomId;
    }

  }
  if(modArr.length != 0){
    for(var i=0;i<modArr.length;i++){
      if(this.fromDateModify[i].jsdate == undefined){
        if(this.fromDateModify[i].date == undefined){
          modArr[i]["iledRecdDtTime"] = this.fromDateModify[i];
        }
        else{
          var dd = this.fromDateModify[i].date.day;
          var mm = this.fromDateModify[i].date.month;
          if(dd<10){
            dd = "0"+dd;
          }
          if(mm<10){
            mm = "0"+mm;
          }
          var date = (this.fromDateModify[i].date.year+'-'+mm+'-'+dd) + ' 00:00:00';
          modArr[i]["iledRecdDtTime"]  = date;
        }
      }
      else{
        var date = this.datepipe.transform(new Date(this.fromDateModify[i].jsdate), 'yyyy-MM-dd') + ' 00:00:00';
        modArr[i]["iledRecdDtTime"]  = date;
      }
    }
  }

  console.log("modArr",modArr)
  this.loading = true;
  this.authservice.updatePreviousSampleDetails(modArr).subscribe($sampleData=>{
    console.log("$sampleData",$sampleData)
    this.loading = false;
    if($sampleData.status == '1'){
      this.searchSample();
      this.my_class2 = 'overlay1';
      setTimeout(()=>{
        this.my_class2 = 'overlay';
        this.my_class1 = 'overlay';
        this.fromDateModify = [];
        this.quantityModValue = [];
        this.selectedModifyRow = -1;
        this.uomModVal =[];
        modArr =[];
        this.defaultMode = true;
        this.modifyMode = false;
      },2000);
    }

  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
              else{
                this.my_class5 = 'overlay1';
                setTimeout(()=>{
                  this.my_class5 = 'overlay';
                },2000);
              }
        });
}
}
