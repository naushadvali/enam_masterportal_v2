import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleCreationComponent } from './sample-creation.component';

describe('SampleCreationComponent', () => {
  let component: SampleCreationComponent;
  let fixture: ComponentFixture<SampleCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
