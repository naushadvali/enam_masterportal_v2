import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import * as _ from 'lodash';


@Component({
  selector: 'app-approal-for-trade',
  templateUrl: './approal-for-trade.component.html',
  styleUrls: ['./approal-for-trade.component.css'],
  providers: [DatePipe]
})
export class ApproalForTradeComponent implements OnInit {
  public loading = false;

  fromDate:any;
  toDate:any;
  public my_class1 ='overlay';
  public my_class2 ='overlay';
  matchLotCode:string;
  matchCommodityName:string;
  matchAgentName:string;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private datepipe:DatePipe,
    private router:Router,
  ) { }

  activeCreate:boolean;
  activeCreate1:boolean;
  ngOnInit() {
    this.activeCreate = false;
    this.activeCreate1 = false;
  }

  errorPopupShow(){
  //  this.my_class4='overlay1';
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

pendingLotList = [];
filteredPendingLot = [];
onPendiingLotSearch(){
  console.log("from",this.fromDate);
  console.log("to",this.toDate);
  this.pendingLotList = [];
  this.filteredPendingLot = [];
  if(this.fromDate != undefined && this.toDate != undefined && this.fromDate != "" && this.toDate != "" ){
    var currentDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd') + ' 00:00:00'
    var toDate = this.datepipe.transform(new Date(this.toDate.jsdate), 'yyyy-MM-dd') + ' 00:00:00'
    var fromDate = this.datepipe.transform(new Date(this.fromDate.jsdate), 'yyyy-MM-dd') + ' 00:00:00'
    if(fromDate<=toDate){
      if(toDate<=currentDate ){
            var timeDiff = Math.abs(this.fromDate.jsdate.getTime() - this.toDate.jsdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
           if(diffDays>3){
             this.flashMessagesService.show('Maximum Date Difference Of 3 Days Is Allowed Between From And To Dates', { cssClass: 'alert-danger', timeout: 3000 });
           }
           else{
             let postJson= {
               "fromDate":this.fromDate.formatted,
               "toDate": this.toDate.formatted
             }
             this.loading = true;
             this.authservice.searchPendingLots(postJson).subscribe($data=>{
               console.log("Pending Lots after search: ",$data);
               if($data.listData != undefined){
                 this.pendingLotList = $data.listData;
                 this.filteredPendingLot = $data.listData;
                 this.activeCreate = true;
               }
               else {
                 this.activeCreate = false;
                 this.activeCreate1 = false;
               }
               this.loading = false;
             }, (err) => {
                         if (err === 'Unauthorized')
                         {
                           this.errorPopupShow();
                                 setTimeout(()=>{
                                   this.my_class1 = 'overlay';
                                   localStorage.clear();
                                   this.router.navigateByUrl('/login');
                                 },3000);
                         }
                   });
           }
      } else {
        this.flashMessagesService.show('To-Date Should Be On Or Before Current Date', { cssClass: 'alert-danger', timeout: 3000 });
      }
    } else {
      this.flashMessagesService.show('From-Date Should Be On Or Before To-Date', { cssClass: 'alert-danger', timeout: 3000 });
    }
  } else {
    this.flashMessagesService.show('Dates Are Mandatory To Search Pending Lots', { cssClass: 'alert-danger', timeout: 3000 });
  }
}

selectedApproval = [];isPresent:boolean;
onApprovedLot(post,isChecked,index){
  this.isPresent = false;
  // For Multiple
  if(isChecked == true){
    for(var i = 0; i < this.selectedApproval.length; i++) {
      if (this.selectedApproval[i].iledlotid == post.iledlotid) {
        this.isPresent = true;
        break;
      }
    }
    if(this.isPresent == false){
      console.log("here")
     this.selectedApproval.push(post);
    }
    this.activeCreate1 = true;
  }
  else{
    this.activeCreate1 = false;
    for(var i = 0; i < this.selectedApproval.length; i++) {
      if (this.selectedApproval[i].iledlotid == post.iledlotid) {
        this.selectedApproval.splice(i,1);
        break;
      }
    }
  }
  console.log("this.selectedApproval",this.selectedApproval)
}

savedRes:any;
onSaveApproval(){
  console.log("selectedApproval",this.selectedApproval)
  if(this.selectedApproval.length != 0){
    this.loading = true;
    this.authservice.saveApprovalTrades(this.selectedApproval).subscribe($data=>{
      console.log("Pending Lots after search: ",$data);
        this.loading = false;
        this.savedRes = $data;
        this.my_class2 = 'overlay1';
        setTimeout(()=>{
          this.my_class2 = 'overlay';
        },2000);
        this.filteredPendingLot = [];
        // this.fromDate = "";
        // this.toDate = "";
        this.onPendiingLotSearch();
    }, (err) => {
                if (err === 'Unauthorized')
                {
                  this.errorPopupShow();
                        setTimeout(()=>{
                          this.my_class1 = 'overlay';
                          localStorage.clear();
                          this.router.navigateByUrl('/login');
                        },3000);
                }
          });
  }
  else{
    this.flashMessagesService.show('Please Select Atleast One Pending Lot To Approve', { cssClass: 'alert-danger', timeout: 3000 });
  }
}

onLotCode(lotVal){
  var filteredData = _.filter(this.pendingLotList, (a)=>a.iledlotcode.indexOf(this.matchLotCode)>=0);
  this.filteredPendingLot = filteredData;
}

onCommodity(comm){
  this.matchCommodityName = this.matchCommodityName.toUpperCase();
  var filteredData = _.filter(this.pendingLotList, (a)=>a.gmpmgprodname.indexOf(this.matchCommodityName)>=0);
  this.filteredPendingLot = filteredData;
}

onAgentname(agentName){
  // var filteredData = _.filter(this.pendingLotList, (a)=>
  // a.lcamfullname1.indexOf(this.matchAgentName)>=0);
  this.matchAgentName = this.matchAgentName.toUpperCase();
  var filteredData = _.filter(
      this.pendingLotList, (a)=>{
        if(a.lcamfullname1 != undefined){
          if(a.lcamfullname1.indexOf(this.matchAgentName)>=0){
            this.filteredPendingLot = filteredData;
          }
        }
      });
}

onReset(){
  this.filteredPendingLot = [];
  this.fromDate = "";
  this.toDate = "";
  this.matchLotCode = "";
  this.matchCommodityName = "";
  this.matchAgentName = "";
  this.selectedApproval =[];
  this.activeCreate = false;
  this.activeCreate1 = false;
}

// Select All Lots
onSelectAll(){
  for(var i=0;i<this.pendingLotList.length;i++){
    (<HTMLInputElement>document.getElementById("pending_"+i)).checked = true;
    this.selectedApproval = this.pendingLotList;
  }
  this.activeCreate1 = true;
}

// De select all lots
onDeSelectAll(){
  for(var i=0;i<this.pendingLotList.length;i++){
    (<HTMLInputElement>document.getElementById("pending_"+i)).checked = false;
    this.selectedApproval = [];
  }
  this.activeCreate1 = false;
}
}
