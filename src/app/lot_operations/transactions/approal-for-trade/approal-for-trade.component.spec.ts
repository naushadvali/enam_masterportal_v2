import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproalForTradeComponent } from './approal-for-trade.component';

describe('ApproalForTradeComponent', () => {
  let component: ApproalForTradeComponent;
  let fixture: ComponentFixture<ApproalForTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproalForTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproalForTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
