import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-weighment',
  templateUrl: './weighment.component.html',
  styleUrls: ['./weighment.component.css']
})
export class WeighmentComponent implements OnInit {

  public my_class1='overlay';  public my_class2='overlay';  public my_class3='overlay';public my_class4='overlay';
  public my_class5='overlay';public my_class6='overlay';public my_class7='overlay';public my_class8='overlay';public my_class9='overlay';public my_class10='overlay';public my_class11='overlay';public my_class12='overlay';public my_class13='overlay';public my_class14='overlay';
  public loading = false;
  lotFromDate:any;
  lotToDate:any;
  dateChange1:any;
  dateChange2:any;
  monthChnage1:any;
  monthChnage2:any;
  fromDate:any;
  toDate:any;
  allLotCodeData=[];
  filteredData1:any;
  weighCharges:any;
  remarks:any;


  gmpmg_prod_code:any;
  gmpmg_prod_name:any;
  gmptm_desc:any;
  gmptm_packid:any;
  iledQtyQuintals:any;
  iled_commodity_id:any;
  iled_entry_type:any;
  iled_lot_code:any;
  iled_lot_date:any;
  iled_lot_id:any;
  iled_lot_status:any;
  iled_noof_bags:any;
  iled_opr_id:any;
  iled_org_id:any;
  iled_srno:any;
  iled_tran_id:any;
  importer_id:any;
  in_importer_name:any;
  in_importer_type:any;
  ingateqtykgs:any;
  lot_date:any;
  public shown1:any=false;
  public shown2:any=true;
  selectedLot:any;
  isDisabled:boolean=true;
  public my_class50:any='overlay';
  entryDate:any;


  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
  ) { }

  ngOnInit() {
    this.getQuantityInQuintal();
  }

  errorPopupShow(){
        this.my_class50='overlay1';
      }
      public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd/mm/yyyy',
        editableDateField:false,
        openSelectorOnInputClick:true,
        inline:false,
      };

  onSearchLotCode(){
    if(this.lotToDate != "" && this.lotToDate != undefined && this.lotFromDate != "" && this.lotFromDate != undefined ){
          const lotDates={
            fromDate:this.lotFromDate.formatted,
            toDate:this.lotToDate.formatted
          }
          this.loading = true;
          this.authservice.searchWlotCode(lotDates).subscribe($allLotCodeData=>{
            this.allLotCodeData=$allLotCodeData.listData;
            this.filteredData1=this.allLotCodeData;
            console.log("all lot code data are: ",this.filteredData1);
            this.loading = false;
          }
          , (err) => {
                      if (err === 'Unauthorized')
                      {
                              this.errorPopupShow();
                              setTimeout(()=>{
                                this.my_class50 = 'overlay';
                                localStorage.clear();
                                this.router.navigateByUrl('/login');
                              },3000);
                  }
                  else{
                    this.my_class13 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class13 = 'overlay';
                    },2000);
                  }
                }
        );
    }
    else{
      this.flashMessagesService.show('Dates are  mandatory to search', { cssClass: 'alert-danger', timeout: 3000 });
    }
  }

  setRadio(e:string){
    console.log("Radio clicked is: ",e);
    if(e=="lot"){
      this.shown2=true;
      this.shown1=false;
    }
    else if(e=="vehicle"){
      this.shown2=false;
      this.shown1=true;
    }
  }

  selectedLotVal =[];
  CheckLotCode(post:any,i:any){
    this.selectedLot=i;
    console.log("selected field is: ",post);
    this.selectedLotVal = post;
  }

  finalLotVal =[];
  selectedLotRow(){
    this.finalLotVal = this.selectedLotVal;
    this.gmpmg_prod_code=this.finalLotVal["gmpmg_prod_code"];
    this.gmpmg_prod_name=this.finalLotVal["gmpmg_prod_name"];
    this.gmptm_desc=this.finalLotVal["gmptm_desc"];
    this.gmptm_packid=this.finalLotVal["gmptm_packid"];
    this.iledQtyQuintals=this.finalLotVal["iledQtyQuintals"];
    this.iled_commodity_id=this.finalLotVal["iled_commodity_id"];
    this.iled_entry_type=this.finalLotVal["iled_entry_type"];
    this.iled_lot_code=this.finalLotVal["iled_lot_code"];
    this.iled_lot_date=this.finalLotVal["iled_lot_date"];
    this.iled_lot_id=this.finalLotVal["iled_lot_id"];
    this.iled_lot_status=this.finalLotVal["iled_lot_status"];
    this.iled_noof_bags=this.finalLotVal["iled_noof_bags"];
    this.iled_opr_id=this.finalLotVal["iled_opr_id"];
    this.iled_org_id=this.finalLotVal["iled_org_id"];
    this.iled_srno=this.finalLotVal["iled_srno"];
    this.iled_tran_id=this.finalLotVal["iled_tran_id"];
    this.importer_id=this.finalLotVal["importer_id"];
    this.in_importer_name=this.finalLotVal["in_importer_name"];
    this.in_importer_type=this.finalLotVal["in_importer_type"];
    this.ingateqtykgs=this.finalLotVal["ingateqtykgs"];
    this.lot_date=this.finalLotVal["lot_date"];
    this.my_class1='overlay';
  }

  editBags(bagChanged){
    console.log("editbags is: ",bagChanged);
    console.log("this.finalLotVal: ",this.finalLotVal);
    this.iled_noof_bags=bagChanged;
    this.ingateqtykgs = Number(this.finalLotVal["gmptm_qty"]) * bagChanged;
    this.iledQtyQuintals = Number(this.ingateqtykgs) * Number(this.quintalUOM);
    this.finalLotVal["iledQtyQuintals"] = this.iledQtyQuintals.toString();

  //   this.authservice.getApprxQty(this.iled_noof_bags).subscribe($apprxBagsqty=>{
  //     console.log("Approx quality of bag is: ",$apprxBagsqty);
  //     //this.approxQty=$apprxBagsqty.approxQty;
  //   }
  //   , (err) => {
  //           if (err === 'Unauthorized')
  //           {
  //                   this.errorPopupShow();
  //                   setTimeout(()=>{
  //                     this.my_class50 = 'overlay';
  //                     localStorage.clear();
  //                     this.router.navigateByUrl('/login');
  //                   },3000);
  //
  //       }
  //     }
  // );
  }
  convVal:number;
  onKgValChange(){
    // this.convVal = this.getQuantityInQuintal();
    this.iledQtyQuintals = Number(this.ingateqtykgs) * Number(this.quintalUOM);
    console.log("this.iledQtyQuintals",this.iledQtyQuintals,this.convVal)
    this.finalLotVal["iledQtyQuintals"] = this.iledQtyQuintals.toString();
  }
  saveMsg :string;
  onSaveWeighment(){
    console.log("finalLotVal",this.finalLotVal,this.finalLotVal.length)
    if(this.finalLotVal.length != 0){
      console.log("iled_noof_bags",this.iled_noof_bags,(this.finalLotVal["iled_noof_bags"]).toString() == '0',Number(this.iled_noof_bags))

      if(Number(this.iled_noof_bags)>0){
        if(Number(this.ingateqtykgs)>0){
          this.finalLotVal["iled_noof_bags"] = this.iled_noof_bags.toString();
          this.finalLotVal["ingateqtykgs"] = this.ingateqtykgs.toString();
          if(this.remarks != ""){
            this.finalLotVal["remarks"] = this.remarks;
          }
          this.finalLotVal["wbwtWbAmount"] = "0";

          console.log("final data  : ",this.finalLotVal);
          this.loading = true;
          this.authservice.saveWeighment(this.finalLotVal).subscribe($weighMentReply=>{
            console.log("weighment saved data: ",$weighMentReply);
            this.loading = false;
                if($weighMentReply.status == '1'){
                  this.saveMsg = $weighMentReply.message;
                  this.my_class2 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class2 = 'overlay';
                  },2000);
                  this.onReset();
                }
                else{
                  this.my_class13 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class13 = 'overlay';
                  },2000);
                }
          }
          , (err) => {
                  if (err === 'Unauthorized')
                  {
                          this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class50 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
              }
              else{
                this.my_class13 = 'overlay1';
                setTimeout(()=>{
                  this.my_class13 = 'overlay';
                },2000);
              }
            }  );
        }
        else{
          this.my_class5 = 'overlay1';
          setTimeout(()=>{
            this.my_class5 = 'overlay';
          },2000);
        }
      }
      else{
        this.my_class4 = 'overlay1';
        setTimeout(()=>{
          this.my_class4 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class3 = 'overlay1';
      setTimeout(()=>{
        this.my_class3 = 'overlay';
      },2000);
    }

  }

  //popup show hide controllers
  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
      this.my_class1='overlay';
    //  this.onReset();
    }
// Page Reset
    onReset(){
      this.filteredData1 = [];
      this.allLotCodeData = [];
      this.finalLotVal = [];
      this.lotFromDate = "";
      this.lotToDate = "";
      this.lotCodeFilter = "";
      this.iled_lot_code = "";
      this.gmpmg_prod_name = "";
      this.in_importer_type = "";
      this.in_importer_name = "";
      this.in_importer_name = this.iled_noof_bags = this.gmptm_desc = this.ingateqtykgs= this. iledQtyQuintals = this. weighCharges = this.remarks="";
      this.selectedLot = -1;
    //  this.commisionAgent = "";
    }

// Lot Code reset
onLotCodeReset(){
  this.filteredData1 = [];
  this.allLotCodeData = [];
  this.lotFromDate = "";
  this.lotToDate = "";
  this.lotCodeFilter = "";
}

// Filter Lot code
lotCodeFilter:string;
onLotCodeFilter(){
  if(this.lotCodeFilter != undefined && this.lotCodeFilter != ""){
    this.filteredData1 = _.filter(this.allLotCodeData , (a)=>a.iled_lot_code.indexOf(this.lotCodeFilter)>=0);
  }
  else{
  this.filteredData1  = this.allLotCodeData;
  }
}

// Vehicel wise
// Search lot by date
lotVehList=[];filteredLotVehList=[];
getLotVehWise(){
  console.log("entryDate",this.entryDate);
  if(this.entryDate != undefined && this.entryDate != ""){
    var dateObj= {"fromDate":this.entryDate.formatted};
    this.loading = true;
    this.authservice.getLotVehicleWise(dateObj).subscribe(vehList=>{
      console.log("vehList fetched data: ",vehList);
          if(vehList.status == '1'){
            this.lotVehList = vehList.listData;
            this.filteredLotVehList = vehList.listData;
            this.my_class6 = 'overlay1';
          }
          this.onReset();
          this.loading = false;
    }
    , (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
        else{
          this.my_class13 = 'overlay1';
          setTimeout(()=>{
            this.my_class13 = 'overlay';
          },2000);
        }
      }  );
  }
}

// Hide Veh wise popup
hidePopUpVehWise(){
  this.my_class6 = 'overlay';
  this.matchIngateNo = this.matchOwnerName = this.matchVehicleNo = "";
}
// Select one vehicle
selVehicleVal=[];selVehIndex:number;
onVehicleSelect(post,index){
  this.selVehicleVal = post;
  this.selVehIndex = index;
  console.log("selected row this.selVehicleVal : ",this.selVehicleVal);
}

// On vehicle Select OK
finalVehicleVal=[];vehDesc:string;vehNo:string;entryNo:string;grossWeight:string;vehWeight:string;netWeight:string;
onSelVehicleOK(){
  this.finalVehicleVal = this.selVehicleVal;
  this.my_class6 = "overlay";
  console.log("finalVehicleVal",this.finalVehicleVal)
  this.selVehIndex = -1;
  this.vehDesc = this.finalVehicleVal["gvrmVehDesc"];
  this.vehNo = this.finalVehicleVal["ilehVechNo"];
  this.entryNo = this.finalVehicleVal["ilehTranId"];
  this.matchIngateNo = this.matchOwnerName = this.matchVehicleNo = "";
}

//Change in gross weight
onGrossWeight(){
  console.log("vehWeight",this.vehWeight)
  console.log("grossWeight",this.grossWeight)
  if(this.grossWeight != null && this.grossWeight != undefined && this.vehWeight != null && this.vehWeight != undefined){
    if(this.grossWeight > this.vehWeight){
      var a = Number(this.grossWeight) - Number(this.vehWeight);
      this.netWeight = a.toString();
      console.log("net weight here",this.netWeight)
    }
    else{
      this.grossWeight = null;
      this.my_class7 = 'overlay1';
      setTimeout(()=>{
        this.my_class7 = 'overlay';
      },2000);
    }
  }
}
//Change in Vehicle weight
onVehWeight(){
  console.log("vehWeight",this.vehWeight)
  console.log("grossWeight",this.grossWeight)
  if(this.grossWeight != null && this.grossWeight != undefined && this.vehWeight != null && this.vehWeight != undefined){
    if(this.vehWeight < this.grossWeight ){
      var a = Number(this.grossWeight) - Number(this.vehWeight);
      this.netWeight = a.toString();
      console.log("net weight here",this.netWeight)
    }
    else{
      var a = Number(this.grossWeight) - 0;
      this.netWeight = a.toString();
      this.vehWeight = null;
      this.my_class8 = 'overlay1';
      setTimeout(()=>{
        this.my_class8 = 'overlay';
      },2000);
    }
  }
}

// Save Weighment vehcile wise
saveWeighmentVehicleWise(){
  if(this.entryDate != undefined && this.entryDate != ""){
    if(this.finalVehicleVal.length != 0){
      if(this.grossWeight != null && this.grossWeight !=undefined && this.grossWeight != "0"){
        if(this.vehWeight != null && this.vehWeight !=undefined && this.vehWeight != "0"){
          this.finalVehicleVal["wbwtGrossWt"] = this.grossWeight.toString();
          this.finalVehicleVal["wbwtGrossTare"] = this.vehWeight.toString();
          this.finalVehicleVal["wbwtNetWt"] = this.netWeight.toString();
          this.finalVehicleVal["wbwtWbAmount"] = "0";
          this.finalVehicleVal["wbwtWbRemark"] = this.remarks;
          this.loading = true;
          this.authservice.saveWeighmentVehicleWise(this.finalVehicleVal).subscribe(vehList=>{
            this.loading = false;
            this.my_class14 = 'overlay1';
            setTimeout(()=>{
              this.my_class14 = 'overlay';
            },2000);
            this.onResetVehicleWise();
          }
          , (err) => {
                  if (err === 'Unauthorized')
                  {
                          this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class50 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
              }
              else{
                this.my_class13 = 'overlay1';
                setTimeout(()=>{
                  this.my_class13 = 'overlay';
                },2000);
              }
            }  );
        }
        else{
          this.my_class10 = 'overlay1';
          setTimeout(()=>{
            this.my_class10 = 'overlay';
          },2000);
        }
      }
      else{
        this.my_class9 = 'overlay1';
        setTimeout(()=>{
          this.my_class9 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class12 = 'overlay1';
      setTimeout(()=>{
        this.my_class12 = 'overlay';
      },2000);
    }
  }
  else{
    this.my_class11 = 'overlay1';
    setTimeout(()=>{
      this.my_class11 = 'overlay';
    },2000);
  }
  console.log("finalVehicleVal",this.finalVehicleVal)
}


// Reset weighment vehicle wise
onResetVehicleWise(){
this.remarks = this.vehWeight = this.grossWeight = this.netWeight = this.entryDate = this.entryNo = this.vehDesc = this.vehNo = "";
this.finalVehicleVal = [];
this.matchIngateNo = this.matchOwnerName = this.matchVehicleNo = "";
}

// Filter by vehcile no
matchVehicleNo:string;
searchVehicleNo(){
  this.matchVehicleNo = this.matchVehicleNo.toUpperCase();
  if(this.matchVehicleNo != undefined && this.matchVehicleNo != ""){
    this.filteredLotVehList = _.filter(this.lotVehList , (a)=>a.ilehVechNo.indexOf(this.matchVehicleNo)>=0);
  }
  else{
  this.filteredLotVehList  = this.lotVehList;
  }
}
// Filter by Ingate no
matchIngateNo:string;
searchIngateNo(){
  if(this.matchIngateNo != undefined && this.matchIngateNo != ""){
    this.filteredLotVehList = _.filter(this.lotVehList , (a)=>a.ilehTranId.indexOf(this.matchIngateNo)>=0);
  }
  else{
  this.filteredLotVehList  = this.lotVehList;
  }
}
// Filter by Owner Name
matchOwnerName:string;
searchOwnerName(){
  this.matchOwnerName = this.matchOwnerName.toUpperCase();
  var filtList=[];
  if(this.lotVehList != undefined){
    for(var i=0;i<this.lotVehList.length;i++){
      if(this.lotVehList[i].ilehOwnerFullName !=undefined){
        filtList.push(this.lotVehList[i]);
      }
    }
  }
  if(this.matchOwnerName != undefined && this.matchOwnerName != ""){
    this.filteredLotVehList = _.filter(filtList , (a)=>a.ilehOwnerFullName.indexOf(this.matchOwnerName)>=0);
  }
  else{
  this.filteredLotVehList  = this.lotVehList;
  }
}

// Get quantity in quintal from kg
conversionList = [];kgVal:string;unitList = [];quintalUOM:number;valInKg:string;
getQuantityInQuintal(){
  this.loading = true;
  this.authservice.getUOMList().subscribe(uomData=>{
    console.log("All uomData name: ",uomData.data);
    this.conversionList = uomData.data["uomlist"];
    console.log("this.conversionList",this.conversionList)
    for(var i=0;i<this.conversionList.length;i++){
      if(this.conversionList[i].gmucmAltUomId == "666" && this.conversionList[i].gmucmUomId == "444"){
        this.quintalUOM = this.conversionList[i].gmucmConvFact
        console.log("kgVal",this.quintalUOM)
      }
    }
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {       this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);  }  });
}

}
