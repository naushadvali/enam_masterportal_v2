import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';



@Component({
  selector: 'app-agent-registration',
  templateUrl: './agent-registration.component.html',
  styleUrls: ['./agent-registration.component.css']
})
export class AgentRegistrationComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public shown1:any=true;
  public shown2:any=false;
  public shown3:any=true;
  public shown4:any=false;
  public shown5:any=true;
  public shown6:any=false;



  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  onAdvance1(){
    this.shown1=false;
    this.shown2=true;
  }
  onBasic1(){
    this.shown1=true;
    this.shown2=false;
  }
  onAdvance2(){
    this.shown3=false;
    this.shown4=true;
  }
  onBasic2(){
    this.shown3=true;
    this.shown4=false;
  }
  onAdvance3(){
    this.shown5=false;
    this.shown6=true;
  }
  onBasic3(){
    this.shown5=true;
    this.shown6=false;
  }

  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
      this.my_class1='overlay';
    }
  div_show2(){
      this.my_class2='overlay1';
    }
  div_hide2(){
      this.my_class2='overlay';
    }
  div_show3(){
      this.my_class3='overlay1';
    }
  div_hide3(){
      this.my_class3='overlay';
    }

}
