import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeComponentApmcApplicabilityComponent } from './fee-component-apmc-applicability.component';

describe('FeeComponentApmcApplicabilityComponent', () => {
  let component: FeeComponentApmcApplicabilityComponent;
  let fixture: ComponentFixture<FeeComponentApmcApplicabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeComponentApmcApplicabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeComponentApmcApplicabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
