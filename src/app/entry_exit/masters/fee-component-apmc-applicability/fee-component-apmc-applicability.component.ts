import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-fee-component-apmc-applicability',
  templateUrl: './fee-component-apmc-applicability.component.html',
  styleUrls: ['./fee-component-apmc-applicability.component.css']
})
export class FeeComponentApmcApplicabilityComponent implements OnInit {

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,	
  ) { }

  ngOnInit() {
  }

}
