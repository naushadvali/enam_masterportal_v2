import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';



@Component({
  selector: 'app-commodity-master',
  templateUrl: './commodity-master.component.html',
  styleUrls: ['./commodity-master.component.css']
})
export class CommodityMasterComponent implements OnInit {
  addC:any;
  childData=[];
  x:any=[];
  uniqChildData=[];
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  shown1:any=true;
  shown2:any=false;
  shown3:any=true;
  shown4:any=false;
  shown5:any=true;
  shown6:any=false;
  shown7:any=true;
  shown8:any=false;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  addChild(){
    //console.log("You are added a cild named: ",this.addC);

        this.childData.push(this.addC.toLowerCase());
        this.uniqChildData=this.childData.filter((x, i, a) => x && a.indexOf(x) === i);
        //  console.log("New array child is",this.childData);
        //  console.log("Uniq child data",this.uniqChildData);
  }
  deleteChild(child:any){
    console.log("selected child to delete",child);
    for(var i=0;i<this.uniqChildData.length;i++){
      if(this.uniqChildData[i]==child){
        this.uniqChildData.splice(i,1);
        // console.log("after deletion",this.uniqChildData);
      }
    }

  }

  grpAdv(){
    this.shown1=false;
    this.shown2=true;
  }

  grpBas(){
    this.shown1=true;
    this.shown2=false;
  }

  alcAdv(){
    this.shown3=false;
    this.shown4=true;
  }

  alcBas(){
    this.shown3=true;
    this.shown4=false;
  }
  
  yardAdv(){
    this.shown5=false;
    this.shown6=true;
  }

  yardBas(){
    this.shown5=true;
    this.shown6=false;
  }

  UnitAdv(){
    this.shown7=false;
    this.shown8=true;
  }

  UnitBas(){
    this.shown7=true;
    this.shown8=false;
  }

div_show1(){
      this.my_class1='overlay1';
    }
div_hide1(){
      this.my_class1='overlay';
    }
div_show2(){
      this.my_class2='overlay1';
    }
div_hide2(){
      this.my_class2='overlay';
    }
div_show3(){
      this.my_class3='overlay1';
    }
div_hide3(){
      this.my_class3='overlay';
    }
div_show4(){
      this.my_class4='overlay1';
    }
div_hide4(){
      this.my_class4='overlay';
    }

}
