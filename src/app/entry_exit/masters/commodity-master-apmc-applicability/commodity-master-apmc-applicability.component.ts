import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-commodity-master-apmc-applicability',
  templateUrl: './commodity-master-apmc-applicability.component.html',
  styleUrls: ['./commodity-master-apmc-applicability.component.css']
})
export class CommodityMasterApmcApplicabilityComponent implements OnInit {

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

}
