import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityMasterApmcApplicabilityComponent } from './commodity-master-apmc-applicability.component';

describe('CommodityMasterApmcApplicabilityComponent', () => {
  let component: CommodityMasterApmcApplicabilityComponent;
  let fixture: ComponentFixture<CommodityMasterApmcApplicabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityMasterApmcApplicabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityMasterApmcApplicabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
