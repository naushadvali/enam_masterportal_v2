import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-commodity-group',
  templateUrl: './commodity-group.component.html',
  styleUrls: ['./commodity-group.component.css']
})
export class CommodityGroupComponent implements OnInit {

  public my_class1='overlay';
  public shown1:any=false;
  public shown2:any=false;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
        this.my_class1='overlay';
      }
}
