import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-farmer-registration',
  templateUrl: './farmer-registration.component.html',
  styleUrls: ['./farmer-registration.component.css']
})
export class FarmerRegistrationComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';

public shown1:any=true;
public shown2:any=false;
public shown3:any=true;
public shown4:any=false;
public shown5:any=true;
public shown6:any=false;
public shown7:any=true;
public shown8:any=false;
dateInsert:any;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,

  ) { }

  ngOnInit() {
  }

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  }; 
  onAdvance(){
    this.shown1=false;
    this.shown2=true;
  }
  onBasic(){
    this.shown1=true;
    this.shown2=false;
  }

  onAdvance2(){
    this.shown3=false;
    this.shown4=true;
  }
  onBasic2(){
    this.shown3=true;
    this.shown4=false;
  }

  onAdvance3(){
    this.shown5=false;
    this.shown6=true;
  }
  onBasic3(){
    this.shown5=true;
    this.shown6=false;
  }

  onAdvance4(){
    this.shown7=false;
    this.shown8=true;
  }
  onBasic4(){
    this.shown7=true;
    this.shown8=false;
  }


  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
      this.my_class1='overlay';
    }
  div_show2(){
        this.my_class2='overlay1';
      }
  div_hide2(){
        this.my_class2='overlay';
      }
  div_show3(){
        this.my_class3='overlay1';
      }
  div_hide3(){
        this.my_class3='overlay';
          }
  div_show4(){
        this.my_class4='overlay1';
      }
  div_hide4(){
        this.my_class4='overlay';
      }


}
