import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleRateComponent } from './vehicle-rate.component';

describe('VehicleRateComponent', () => {
  let component: VehicleRateComponent;
  let fixture: ComponentFixture<VehicleRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
