import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-vehicle-rate',
  templateUrl: './vehicle-rate.component.html',
  styleUrls: ['./vehicle-rate.component.css']
})
export class VehicleRateComponent implements OnInit {

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,

  ) { }

  ngOnInit() {
  }

}
