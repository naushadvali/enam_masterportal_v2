import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BagTypeComponent } from './bag-type.component';

describe('BagTypeComponent', () => {
  let component: BagTypeComponent;
  let fixture: ComponentFixture<BagTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BagTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BagTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
