import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {IMyDpOptions} from 'mydatepicker';
import { WeexcelFormatReportComponent } from './weexcel-format-report.component';

describe('WeexcelFormatReportComponent', () => {
  let component: WeexcelFormatReportComponent;
  let fixture: ComponentFixture<WeexcelFormatReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeexcelFormatReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeexcelFormatReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

});
