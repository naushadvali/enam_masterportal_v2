import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionAgentLotBfrBiddingReportsComponent } from './commission-agent-lot-bfr-bidding-reports.component';

describe('CommissionAgentLotBfrBiddingReportsComponent', () => {
  let component: CommissionAgentLotBfrBiddingReportsComponent;
  let fixture: ComponentFixture<CommissionAgentLotBfrBiddingReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionAgentLotBfrBiddingReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionAgentLotBfrBiddingReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
