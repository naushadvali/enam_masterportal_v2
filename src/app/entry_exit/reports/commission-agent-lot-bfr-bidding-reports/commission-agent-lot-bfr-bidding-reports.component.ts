import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-commission-agent-lot-bfr-bidding-reports',
  templateUrl: './commission-agent-lot-bfr-bidding-reports.component.html',
  styleUrls: ['./commission-agent-lot-bfr-bidding-reports.component.css']
})
export class CommissionAgentLotBfrBiddingReportsComponent implements OnInit {
  public my_class1='overlay';
  dateInsert:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  div_show1(){
      this.my_class1='overlay1';
    }
div_hide1(){
      this.my_class1='overlay';
    }
}
