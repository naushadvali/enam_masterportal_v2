import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';


@Component({
  selector: 'app-daily-report',
  templateUrl: './daily-report.component.html',
  styleUrls: ['./daily-report.component.css']
})
export class DailyReportComponent implements OnInit {
  public my_class1='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor() { }

  ngOnInit() {
  }
  div_show1(){
      this.my_class1='overlay1';
    }
div_hide1(){
      this.my_class1='overlay';
    }
}
