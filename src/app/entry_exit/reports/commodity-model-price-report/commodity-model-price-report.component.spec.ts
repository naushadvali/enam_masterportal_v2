import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityModelPriceReportComponent } from './commodity-model-price-report.component';

describe('CommodityModelPriceReportComponent', () => {
  let component: CommodityModelPriceReportComponent;
  let fixture: ComponentFixture<CommodityModelPriceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityModelPriceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityModelPriceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
