import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitGateRegisterComponent } from './exit-gate-register.component';

describe('ExitGateRegisterComponent', () => {
  let component: ExitGateRegisterComponent;
  let fixture: ComponentFixture<ExitGateRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitGateRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitGateRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
