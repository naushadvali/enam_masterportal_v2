import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmerDetailReportComponent } from './farmer-detail-report.component';

describe('FarmerDetailReportComponent', () => {
  let component: FarmerDetailReportComponent;
  let fixture: ComponentFixture<FarmerDetailReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmerDetailReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmerDetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
