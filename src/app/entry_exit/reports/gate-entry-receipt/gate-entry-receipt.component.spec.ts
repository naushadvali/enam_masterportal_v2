import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GateEntryReceiptComponent } from './gate-entry-receipt.component';

describe('GateEntryReceiptComponent', () => {
  let component: GateEntryReceiptComponent;
  let fixture: ComponentFixture<GateEntryReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GateEntryReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GateEntryReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
