import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GateEntryRegisterComponent } from './gate-entry-register.component';

describe('GateEntryRegisterComponent', () => {
  let component: GateEntryRegisterComponent;
  let fixture: ComponentFixture<GateEntryRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GateEntryRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GateEntryRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
