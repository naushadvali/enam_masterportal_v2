import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-exit-gate-receipt',
  templateUrl: './exit-gate-receipt.component.html',
  styleUrls: ['./exit-gate-receipt.component.css']
})
export class ExitGateReceiptComponent implements OnInit {

  public my_class1='overlay';
  public shown1:any=true;
  public shown2:any=false;
  dateInsert:any;
  dateInsert1:any;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
        this.my_class1='overlay';
      }
}
