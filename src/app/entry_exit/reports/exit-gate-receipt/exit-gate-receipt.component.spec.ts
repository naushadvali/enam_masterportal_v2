import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitGateReceiptComponent } from './exit-gate-receipt.component';

describe('ExitGateReceiptComponent', () => {
  let component: ExitGateReceiptComponent;
  let fixture: ComponentFixture<ExitGateReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitGateReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitGateReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
