import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityGroupVarietyComponent } from './commodity-group-variety.component';

describe('CommodityGroupVarietyComponent', () => {
  let component: CommodityGroupVarietyComponent;
  let fixture: ComponentFixture<CommodityGroupVarietyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityGroupVarietyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityGroupVarietyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
