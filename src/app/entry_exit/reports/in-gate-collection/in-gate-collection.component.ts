import { Component, OnInit } from '@angular/core';

import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-in-gate-collection',
  templateUrl: './in-gate-collection.component.html',
  styleUrls: ['./in-gate-collection.component.css']
})
export class InGateCollectionComponent implements OnInit {

  dateInsert:any;
  dateInsert1:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
}
