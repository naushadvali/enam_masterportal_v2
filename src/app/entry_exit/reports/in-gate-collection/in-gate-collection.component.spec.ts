import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InGateCollectionComponent } from './in-gate-collection.component';

describe('InGateCollectionComponent', () => {
  let component: InGateCollectionComponent;
  let fixture: ComponentFixture<InGateCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InGateCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InGateCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
