import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderPurchaseReportComponent } from './trader-purchase-report.component';

describe('TraderPurchaseReportComponent', () => {
  let component: TraderPurchaseReportComponent;
  let fixture: ComponentFixture<TraderPurchaseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraderPurchaseReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderPurchaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
