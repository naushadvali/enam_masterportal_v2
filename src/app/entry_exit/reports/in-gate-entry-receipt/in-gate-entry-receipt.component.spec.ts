import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InGateEntryReceiptComponent } from './in-gate-entry-receipt.component';

describe('InGateEntryReceiptComponent', () => {
  let component: InGateEntryReceiptComponent;
  let fixture: ComponentFixture<InGateEntryReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InGateEntryReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InGateEntryReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
