import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityListingReportComponent } from './commodity-listing-report.component';

describe('CommodityListingReportComponent', () => {
  let component: CommodityListingReportComponent;
  let fixture: ComponentFixture<CommodityListingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityListingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityListingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
