import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-commodity-listing-report',
  templateUrl: './commodity-listing-report.component.html',
  styleUrls: ['./commodity-listing-report.component.css']
})
export class CommodityListingReportComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public shown1:any=true;
  public shown2:any=false;
  public shown3:any=true;
  public shown4:any=false;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
      this.my_class1='overlay';
    }
  div_show2(){
    this.my_class2='overlay1';
  }
  div_hide2(){
      this.my_class2='overlay';
    }

}
