import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeighBridgeReportComponent } from './weigh-bridge-report.component';

describe('WeighBridgeReportComponent', () => {
  let component: WeighBridgeReportComponent;
  let fixture: ComponentFixture<WeighBridgeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeighBridgeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeighBridgeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
