import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamTsReportComponent } from './nam-ts-report.component';

describe('NamTsReportComponent', () => {
  let component: NamTsReportComponent;
  let fixture: ComponentFixture<NamTsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamTsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamTsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
