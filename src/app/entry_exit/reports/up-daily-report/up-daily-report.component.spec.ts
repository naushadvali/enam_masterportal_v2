import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpDailyReportComponent } from './up-daily-report.component';

describe('UpDailyReportComponent', () => {
  let component: UpDailyReportComponent;
  let fixture: ComponentFixture<UpDailyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpDailyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpDailyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
