import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleRegistrationReceiptComponent } from './vehicle-registration-receipt.component';

describe('VehicleRegistrationReceiptComponent', () => {
  let component: VehicleRegistrationReceiptComponent;
  let fixture: ComponentFixture<VehicleRegistrationReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleRegistrationReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleRegistrationReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
