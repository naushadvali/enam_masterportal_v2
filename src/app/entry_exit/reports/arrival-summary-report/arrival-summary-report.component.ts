import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-arrival-summary-report',
  templateUrl: './arrival-summary-report.component.html',
  styleUrls: ['./arrival-summary-report.component.css']
})
export class ArrivalSummaryReportComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public shown1:any=true;
  public shown2:any=false;
  public shown3:any=true;
  public shown4:any=false;
  dateInsert:any;
  dateInsert1:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
div_show1(){
    this.my_class1='overlay1';
  }
div_hide1(){
      this.my_class1='overlay';
    }
div_show2(){
  this.my_class2='overlay1';
}
div_hide2(){
      this.my_class2='overlay';
    }

}
