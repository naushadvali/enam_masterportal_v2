import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrivalSummaryReportComponent } from './arrival-summary-report.component';

describe('ArrivalSummaryReportComponent', () => {
  let component: ArrivalSummaryReportComponent;
  let fixture: ComponentFixture<ArrivalSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrivalSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrivalSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
