import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeighBridgeReceiptComponent } from './weigh-bridge-receipt.component';

describe('WeighBridgeReceiptComponent', () => {
  let component: WeighBridgeReceiptComponent;
  let fixture: ComponentFixture<WeighBridgeReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeighBridgeReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeighBridgeReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
