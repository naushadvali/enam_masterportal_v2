import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentTraderRegistrationReportComponent } from './agent-trader-registration-report.component';

describe('AgentTraderRegistrationReportComponent', () => {
  let component: AgentTraderRegistrationReportComponent;
  let fixture: ComponentFixture<AgentTraderRegistrationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentTraderRegistrationReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentTraderRegistrationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
