import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-vehicle-gate-entry',
  templateUrl: './vehicle-gate-entry.component.html',
  styleUrls: ['./vehicle-gate-entry.component.css'],
  providers: [DatePipe]
})
export class VehicleGateEntryComponent implements OnInit {

  public my_class1='overlay';  public my_class2='overlay';public my_class3='overlay';  public my_class4='overlay';public my_class5 ='overlay';public my_class6 ='overlay';
  public loading = false;
  public shown1:any=true;
  public shown2:any=false;
  public shown3:any=true;
  public shown4:any=false;
  public shown5:any=true;
  public shown6:any=false;
  public shown7:any=false;
  radioChk:boolean=true;
  nameDisable:boolean=false;
  regDate:any;
  today:any;regType:string;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
    private datepipe:DatePipe
  ) { }

  ngOnInit() {
    this.today = new Date();
    var dd = this.today.getDate();
    var mm = this.today.getMonth()+1; //January is 0!
    var yyyy = this.today.getFullYear();
    this.regDate =
    { date: { year: this.today.getFullYear(), month: mm, day: dd } };
    this.regType="U";
    this.nameDisable=false;
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  checkedradio(radioCheck){
    console.log("checking true: ",radioCheck);
    if(radioCheck==false){
      this.shown7=true;
      this.nameDisable=true;
      this.regType="R";
      this.onReset();
    }
    else{
      this.shown7=false;
      this.nameDisable=false;
      this.regType="U";
      this.onReset();
    }
    console.log("reg type: ",this.regType);

  }

  errorPopupShow(){
        this.my_class4='overlay1';
      }

  onAdvance1(){
    this.shown1=false;
    this.shown2=true;
  }
  onBasic1(){
    this.shown1=true;
    this.shown2=false;
  }
  onAdvance2(){
    this.shown3=false;
    this.shown4=true;
  }
  onBasic2(){
    this.shown3=true;
    this.shown4=false;
  }
  onAdvance3(){
    this.shown5=false;
    this.shown6=true;
  }
  onBasic3(){
    this.shown5=true;
    this.shown6=false;
  }

  // Open Yard Popup
  div_show1(){
      this.my_class1='overlay1';
      this.getInfoForYard();
    }
  // Hide Yard Popup
  div_hide1(){
        this.my_class1='overlay';
        this.yardCodeFilter ="";
        this.yardDescFilter = "";
        this.selectedYard = -1;
      }

  div_show2(){
        this.my_class2='overlay1';
        this.getInfoForVehicleType();
      }
  div_hide2(){
        this.my_class2='overlay';
         this.vehicleDescFilter ="";
         this.vehicleCodeFilter = "";
         this.selectedVehicle = -1;
      }
  // Open Gate No Popup
  div_show3(){
        this.my_class3='overlay1';
        this.getInfoForGate();
      }
  div_hide3(){
        this.my_class3='overlay';
        this.gateNoFilter ="";
        this.selectedGate = -1;
      }

//  Get Yard Info
yardInfo = [];
filterdYardInfo = [];
getInfoForYard(){
  this.loading = true;
  this.authservice.getYardInfo().subscribe($data=>{
    console.log("Yard Info: ",$data);
    if($data.listData != undefined){
      this.yardInfo = $data.listData;
      this.filterdYardInfo = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

// select Yard here
selYard=[];yardDesc:string;valYard=[];selectedYard:number;
onYardSelect(yardVal,index){
  this.valYard = yardVal;
  this.selectedYard = index;
}

// save selected yard
selYardVal=[];
onSelectYardOK(){
  this.selYardVal = this.valYard;
  this.yardDesc = this.selYardVal["gmtmMktDesc"];
  this.my_class1='overlay';
  this.yardCodeFilter ="";
  this.yardDescFilter = "";
  this.selectedYard = -1;
}

// Filter on yard code
yardCodeFilter:string;
onYardCodeFilter(){
  this.yardCodeFilter=this.yardCodeFilter.toUpperCase();
  if(this.yardCodeFilter != ""){
    var info = _.filter(this.yardInfo, (a)=>a.gmtmMktCode.indexOf(this.yardCodeFilter)>=0);
    this.filterdYardInfo = info;
  }
  else{
  this.filterdYardInfo = this.yardInfo;
  }
}
// Filter on yard desc
yardDescFilter:string;
onYardDescFilter(){
  this.yardDescFilter=this.yardDescFilter.toUpperCase();
  if(this.yardDescFilter != ""){
    var info = _.filter(this.yardInfo, (a)=>a.gmtmMktDesc.indexOf(this.yardDescFilter)>=0);
    this.filterdYardInfo = info;
  }
  else{
  this.filterdYardInfo = this.yardInfo;
  }
}

// Get Gate No info
gateInfo =[];
filteredGateInfo =[];
getInfoForGate(){
  this.loading = true;
  this.authservice.getGateInfo().subscribe($data=>{
    console.log("Gate Info: ",$data);
    if($data.listData != undefined){
      this.gateInfo = $data.listData;
      this.filteredGateInfo = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}


gateNo:string;selGate=[];valGate=[];selectedGate:number;
onGateSelect(gateVal,index){
  this.valGate = gateVal;
  this.selectedGate = index;
}

// save selected gate
selGateVal=[];
onSelectGateOK(){
  this.selGateVal = this.valGate;
  this.gateNo = this.selGateVal["ggmGateNo"];
  this.my_class3='overlay';
  this.gateNoFilter ="";
  this.selectedGate = -1;
}

// Filter on gate no
gateNoFilter:string;
onGateNoFilter(){
  this.gateNoFilter = this.gateNoFilter.toUpperCase();
  if(this.gateNoFilter != ""){
    var info = _.filter(this.gateInfo, (a)=>a.ggmGateNo.indexOf(this.gateNoFilter)>=0);
    this.filteredGateInfo = info;
  }
  else{
  this.filteredGateInfo = this.gateInfo;
  }
}


// Get Vehicle Type List
VehicleTypeInfo = [];
filterdVehicleTypeInfo = [];
getInfoForVehicleType(){
  this.loading = true;
  this.authservice.getVehicleTypeList().subscribe($data=>{
    console.log("vehicle type Info: ",$data);
    if($data.listData != undefined){
      this.VehicleTypeInfo = $data.listData;
      this.filterdVehicleTypeInfo = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}


// Select Vehicle Type here
valVehicleType=[];selectedVehicle:number;
onVehicleTypeSelect(vehicleTypeVal,index){
  this.valVehicleType = vehicleTypeVal;
  this.selectedVehicle = index;
}

// save selected vehicle type
selVehicleTypeVal=[];vehDesc:string;vehAmount:string;
onSelectVehicleTypeOK(){
  this.selVehicleTypeVal = this.valVehicleType;
  this.vehDesc = this.selVehicleTypeVal["gvrmVehDesc"];
  this.vehAmount = this.selVehicleTypeVal["gvrmVehRateDaily"];
  this.my_class2 = 'overlay';
  this.selectedVehicle = -1;
  this.vehicleDescFilter ="";
  this.vehicleCodeFilter = "";
}


// Save vehicle entry
ownerName:string;mobileNo:string;vehNo:string;successMsg:string;saveRes:any
onSaveVehicleEntry(){
  console.log("selYardVal",this.selYardVal)
  if(this.selYardVal.length != 0){
    if(this.selGateVal.length != 0){
      if(this.vehDesc !=  "" && this.vehDesc !=  undefined){
        if(this.vehNo != "" && this.vehNo != undefined){
          if(this.ownerName != "" && this.ownerName != undefined){
            if(this.mobileNo != "" && this.mobileNo != undefined){
              var postJson={
                "ilehGateNo": this.gateNo,
                "ilehMarketTypeId": this.selYardVal["gmtmMktId"],
                "ilehVechRegType" :this.regType,
                "ilehVechNo" : this.vehNo,
                "ilehVechTypeId":"5664",
                "ilehOwnerFulName": this.ownerName,
                "ilehGateEntryFee": this.vehAmount,
                "ilehMobileNo":  this.mobileNo
              }
              if(this.regType == "R"){
                postJson["ivrtTrnId"] =  this.selVehicleTypeVal["gvrmTrnId"]
                postJson["ivrtVehTypeId"] =  this.selVehicleTypeVal["gvrmTrnId"]
              }
              if(this.regType == "U"){
                postJson["ilehVechTypeId"] =  this.selVehicleTypeVal["gvrmTrnId"]
              }
              console.log("postJson",postJson)
              this.loading = true;
              this.authservice.saveVehicleEntry(postJson).subscribe($data=>{
                console.log("after vehicle entry save: ",$data);
                this.loading = false;
                this.saveRes = $data;
                this.successMsg = $data.message
                this.my_class6 = 'overlay1';
                setTimeout(()=>{
                  this.my_class6 = 'overlay';
                  this.onReset();
                },2000);
              }, (err) => {
                          if (err === 'Unauthorized')
                          {
                            this.errorPopupShow();
                                  setTimeout(()=>{
                                    this.my_class1 = 'overlay';
                                    localStorage.clear();
                                    this.router.navigateByUrl('/login');
                                  },3000);
                          }
                    });
            }
            else{
              this.flashMessagesService.show('Please Enter Mobile No', { cssClass: 'alert-danger', timeout: 3000 });
            }
          }
          else{
            this.flashMessagesService.show('Please Enter Full Name', { cssClass: 'alert-danger', timeout: 3000 });
          }
        }
        else{
          this.flashMessagesService.show('Please Enter Vehicle No', { cssClass: 'alert-danger', timeout: 3000 });
        }
      }
      else{
        this.flashMessagesService.show('Please Choose Vehicle ', { cssClass: 'alert-danger', timeout: 3000 });
      }
    }
    else{
      this.flashMessagesService.show('Please Choose Gate No', { cssClass: 'alert-danger', timeout: 3000 });
    }
  }
  else{
    this.flashMessagesService.show('Please Choose Yard Type', { cssClass: 'alert-danger', timeout: 3000 });
  }
}


onReset(){
  this.yardDesc = this.gateNo = this.vehNo = this.vehDesc = this.ownerName = this.vehAmount = this.mobileNo
  =this.vehDocNo = this.fromDate = this.toDate = "";
  this.selGateVal = this.filteredGateInfo = [];
  this.selYardVal = this.filterdYardInfo =[];
  this.selVehicleTypeVal = this.filterdVehicleTypeInfo=[];
  this.regVehicles =this.filRteredegVehicles =[]
  this.regNoFilter ="";
  this.vehicleNoFilter = "";
  this.selVehicle = -1;
  this.yardCodeFilter ="";
  this.yardDescFilter = "";
  this.selectedYard = -1;
  this.gateNoFilter ="";
  this.selectedGate = -1;
  this.vehicleDescFilter ="";
  this.vehicleCodeFilter = "";
  this.selectedVehicle = -1;
}

// Open Registered Vehicle Popup
openRegVehiclePopUp(){
  this.my_class5='overlay1';
  this.getRegVehicles();
    }
// Hide Registered Vehicle Popup
hideRegVehiclePopUp(){
      this.my_class5='overlay';
      this.regNoFilter ="";
      this.vehicleNoFilter = "";
      this.selVehicle = -1;
    }

//get all registered vehicles
regVehicles =[];filRteredegVehicles =[];
getRegVehicles(){
  this.loading = true;
  this.authservice.getRegVehicles().subscribe($data=>{
    console.log("Vehicles : ",$data);
    if($data.listData != undefined){
      this.regVehicles = $data.listData;
      this.filRteredegVehicles = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

// select registered vehicles
regVehVal:any;selVehicle:number;
onVehicleSelect(vehVal,index){
  this.selVehicle = index;
  this.regVehVal = vehVal;
}

// oon sel vehicle ok
finalRegVehVal:any;vehDocNo:string;fromDate:any;toDate:any;
selRegVehicle(){
  this.my_class5='overlay';
  this.finalRegVehVal = this.regVehVal;
  this.vehDocNo = this.finalRegVehVal["ivrtDocno"];
  this.mobileNo = this.finalRegVehVal["ivrtMobileNo"];
  this.vehAmount = this.finalRegVehVal["ivrtVehFee"];
  this.ownerName = this.finalRegVehVal["ivrttOwnerName"];
  this.vehNo = this.finalRegVehVal["ivrtVehRegNo"];
  // var fromDate = this.datepipe.transform(new Date(this.finalRegVehVal["ivrtValidFromDt"]), 'yyyy-MM-dd')
  this.fromDate = this.finalRegVehVal["ivrtValidFromDt"] ;
  this.toDate = this.finalRegVehVal["ivrtValidToDt"];
  this.vehDesc  = this.finalRegVehVal["gvrmVehDesc"];
  this.regNoFilter ="";
  this.vehicleNoFilter = "";
  this.selVehicle = -1;
  console.log("post",this.finalRegVehVal)
}

// Filter on reg no
regNoFilter:string;
onRegNoFilter(){
  this.regNoFilter=this.regNoFilter.toUpperCase();
  if(this.regNoFilter != ""){
    var info = _.filter(this.regVehicles, (a)=>a.ivrtDocno.indexOf(this.regNoFilter)>=0);
    this.filRteredegVehicles = info;
  }
  else{
  this.filRteredegVehicles = this.regVehicles;
  }
}
// Filter on vehicle no
vehicleNoFilter:string;
onVehicleNoFilter(){
  this.vehicleNoFilter=this.vehicleNoFilter.toUpperCase();
  if(this.vehicleNoFilter != ""){
    var info = _.filter(this.regVehicles, (a)=>a.ivrtVehRegNo.indexOf(this.vehicleNoFilter)>=0);
    this.filRteredegVehicles = info;
  }
  else{
  this.filRteredegVehicles = this.regVehicles;
  }
}
// Filter on vehicle type code
vehicleCodeFilter:string;
onVehicleCodeFilter(){
  this.vehicleCodeFilter=this.vehicleCodeFilter.toUpperCase();
  if(this.vehicleCodeFilter != ""){
    var info = _.filter(this.VehicleTypeInfo, (a)=>a.gvrmVehTypeCd.indexOf(this.vehicleCodeFilter)>=0);
    this.filterdVehicleTypeInfo = info;
  }
  else{
  this.filterdVehicleTypeInfo = this.VehicleTypeInfo;
  }
}
// Filter on vehicle type desc
vehicleDescFilter:string;
onVehicleDescFilter(){
  this.vehicleDescFilter=this.vehicleDescFilter.toUpperCase();
  if(this.vehicleDescFilter != ""){
    var info = _.filter(this.VehicleTypeInfo, (a)=>a.gvrmVehDesc.indexOf(this.vehicleDescFilter)>=0);
    this.filterdVehicleTypeInfo = info;
  }
  else{
  this.filterdVehicleTypeInfo = this.VehicleTypeInfo;
  }
}

}
