import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleGateEntryComponent } from './vehicle-gate-entry.component';

describe('VehicleGateEntryComponent', () => {
  let component: VehicleGateEntryComponent;
  let fixture: ComponentFixture<VehicleGateEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleGateEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleGateEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
