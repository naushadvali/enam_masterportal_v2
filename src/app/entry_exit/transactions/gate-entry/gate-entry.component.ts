import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as _ from 'lodash';
import { Router } from '@angular/router';


@Component({
  selector: 'app-gate-entry',
  templateUrl: './gate-entry.component.html',
  styleUrls: ['./gate-entry.component.css']
})
export class GateEntryComponent implements OnInit {
  public loading = false;
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';
  public my_class9='overlay';
  public my_class10='overlay';
  public my_class11='overlay';
  public my_class12='overlay';
  public my_class13='overlay';
  public my_class14='overlay';
  public my_class15='overlay';
  public my_class16='overlay';
  public my_class17='overlay';
  public my_class18='overlay';
  public my_class50:any='overlay';

  public my_class20= 'overlay';
  public my_class21= 'overlay';
  public my_class22= 'overlay';
  public my_class23= 'overlay';
  public my_class24= 'overlay';
  public my_class25= 'overlay';
  public my_class26= 'overlay';
  public my_class27= 'overlay';
  public my_class28= 'overlay';
  public my_class29= 'overlay';
  public my_class30= 'overlay';
  public my_class31= 'overlay';
  public my_class32= 'overlay';
  public my_class33= 'overlay';
  public my_class34= 'overlay';
  public my_class35= 'overlay';
  public my_class36= 'overlay';
  public my_class37= 'overlay';
  public my_class38= 'overlay';
  public my_class39= 'overlay';
  public my_class40= 'overlay';
  public my_class41= 'overlay';

  public shown=true;
  public shown1=false;
  public shown2=false;
  public shown3=true;
  public shown4=false;
  public shown5=false;
  public shown6=true;
  public shown7=false;
  public shown8=false;
  public shown9=true;
  public shown10=false;
  public shown11=true;
  public shown12=false;
  public shown13=true;
  public shown14=false;
  public shown15=true;
  public shown16=false;
  public shown17=true;
  public shown18=false;
  public shown19=true;
  public shown20=false;
  public shown21=false;
  public shown22=true;
  public shown23=false;
  public shown24=false;
  public shown25=false;
  public shown26=true;
  public shown27=false;
  public shown28=false;

  public shown29:any=true;
  public shown30:any=false;
  public shown31:any=true;
  public shown32:any=false;
  public shown33:any=true;
  public shown34:any=false;

  sellerNameData=[];
  selectedRow:any;
  selectedDistrict:any;
  farmOrgId:any;
  farmRegIId:any;
  frmCityId:any;
  frmContactNo =[];
  frmDistId:any;
  frmFullName =[];
  frmOprId:any;
  frmOprType:any;
  frmRegidt:any;
  frmRelationId:any;
  frmStateId:any;
  frmTahsilId:any;
  frmdefunct:any;
  frmsonfo = [];
  gmcmCity =[];
  gmcmCityId:any;
  flag:any;
  statePerticularName:any;
  distPerticularName =[];
  tehsilPerticularName =[];
  allDIstData=[];
  allTehsilData=[];
  allVillageData=[];
  allCommodityData=[];
  allBagTypedata=[];
  vehicleData=[];
  allCAData=[];
  allStateName=[];
  filteredData1=[];
  filteredData2=[];
  filteredData3=[];
  filteredData4=[];
  filteredData5=[];
  filteredData6=[];
  filteredData7=[];
  filteredData8=[];
  filteredData9=[];
  choosenDist:any;
  choosenDistId:any;
  selectedTehsil:any;
  gmtmTahsilId:any;
  gmtmTashilDesc:any;
  selectedVillage:any;
  selectedCommodity:any;
  gmpmProdId:any;
  gmpmgProdName =[];
  gmpmDefunct:any;
  gmpmMarketId:any;
  gmpmOprId:any;
  gmpmOrgId:any;
  gmpmQtyUom:any;
  gmpmgProdCode:any;
  gmpmgQtyUom:any;
  selectedBag:any;
  gmptm_desc =[];
  gmptm_defunct:any;
  gmptm_oprid:any;
  gmptm_orgid:any;
  gmptm_packid:any;
  gmptm_qty:any;
  gmptm_qty_uom:any;
  noOfBags =[];
  sellerType=[];
  approxQty = [];
  gvrmDefunct:any;
  gvrmOprId:any;
  gvrmOrgId:any;
  gvrmTrnId:any;
  gvrmVehRateAnnual:any;
  gvrmVehRateDaily:any;
  gvrmVehRateMonthly:any;
  gvrmVehTypeCd =[];
  selectedVehicle:any;
  vehicleno =[];
  //CA list
  lcamAadharNo:any;
  lcamAgenAdd1:any;
  lcamAgenAdd2:any;
  lcamAgenCatgCd:any;
  lcamAgenCatgDesc:any;
  lcamAgenCompName =[];
  lcamAgenCompRegino:any;
  lcamAgenContno:any;
  lcamAgenDistId:any;
  lcamAgenEmailid:any;
  lcamAgenFaxno:any;
  lcamAgenGendStat:any;
  lcamAgenLicenNo:any;
  lcamAgenPincd:any;
  lcamAgenProdCd:any;
  lcamAgenProdDesc:any;
  lcamAgenStateId:any;
  lcamAgenThasilId:any;
  lcamAgenTradLicenno:any;
  lcamAgenType:any;
  lcamBankAccount:any;
  lcamBankName:any;
  lcamDefunct:any;
  lcamFactorBidLimit:any;
  lcamFullName =[];
  lcamIfscCode:any;
  lcamOprid:any;
  lcamOrgid:any;
  lcamPanNo:any;
  lcamRemark:any;
  lcamSecurityDeposit:any;
  lcamTrnId:any;
  message:any;
  selectedCA:any;
  farmerNameUpper:any;
  selectedState:any;
  gmsmDefunct:any;
  gmsmOrgid:any;
  gmsmStateCd:any;
  gmsmStateDesc = [];
  gmsmStateId:any;
  //TraderList
  lcam_agen_add1:any;
  lcam_agen_comp_name:any;
  lcam_agen_contno:any;
  lcam_agen_dist_id:any;
  lcam_agen_gend_stat:any;
  lcam_agen_licen_no:any;
  lcam_agen_pincd:any;
  lcam_agen_state_id:any;
  lcam_agen_thasil_id:any;
  lcam_agen_trad_licenno:any;
  lcam_agen_type:any;
  lcam_defunct:any;
  lcam_full_name:any;
  lcam_oprid:any;
  lcam_orgid:any;
  lcam_trn_id:any;
  selectedFarmerState:any;
  FStateName:any;
  FStateId:any;
  selectedFarmerDistrict:any;
  FDistName:any;
  FDistId:any;
  selectedFarmerTehsil:any;
  FTehsilId:any;
  FTehsilName:any;
  selectedFarmerVillage:any;
  FVillageName:any;
  FVillageId:any;
  FFullName:any;
  FAddrs1:any;
  FAddrs2:any;
  proofValue:any;
  relationFValue:any;
  FMobileNo:any;
  FIdProofNo:any;
  frmsonOf:any;

  locationName:string;
  selectedGate:number;
  matchGateNo:any;
  matchGateName:any;


  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
  ) { }

  gateInfo =[];createLotDetailList = [];
  yardInfo =[];
  regType:string;
  lotType:string;

  ngOnInit() {
  this.locationName = localStorage.getItem("location");

  this.regType = "U";
  this.lotType = "N";
  this.sellerType[0] = "F";
  this.shown23=true;
  this.shown26=false;
  this.shown27=true;
  this.shown28=false;
  this.shown25=false;
  this.shown22=false;
  this.shown24=false;
  this.tableLength=[0];
  this.createLotDetailList.push({
  "framerType": "F",
  "farmerObj": "",
  "traderList" :"",
  "commodityObj": "",
  "bagTypeObj": "",
  "noBag": "",
  "approxQty" :"",
  "vechileNo":"",
  "vechRegType": "" ,
  "vechileObj" :"",
  "lotType": "",
  "caObj":"",
  "gateObj":"",
  "regiVechObj":""
})
this.showFarmer = true;
this.showTrader = false;
// this.onSelectState();
  }

  errorPopupShow(){
        this.my_class50='overlay1';
      }

  showFarmer:boolean;showTrader:boolean;
  onSelectSellertype(index){
    this.createLotDetailList[index]["framerType"]  = this.sellerType[index];
    console.log("this.createLotDetailList type is: ",this.createLotDetailList);
    // this.frmFullName = "";
    // this.frmContactNo = "";
    // this.gmsmStateDesc = "";
    // this.choosenDist = "";
    // this.gmtmTashilDesc = "";
    // this.gmcmCity = "";
    // this.gmpmgProdName = "";
    // this.gmptm_desc = "";
    // this.noOfBags = "";
    // this.gvrmVehTypeCd = "";
    // this.vehicleno = "";
    // this.lcamFullName =this.distPerticularName = this.choosenDist = this.tehsilPerticularName = this.approxQty =this.lcamAgenCompName ="";
    if(this.sellerType[index]=='F'){
      // this.onReset();
      this.onResetLotData(index);
      this.showFarmer = true;
      this.showTrader = false;
      // this.shown23=true;
      // this.shown26=false;
      // this.shown27=true;
      // this.shown28=false;
      // this.shown25=false;
      // this.shown22=false;
      // this.shown24=false;
    }
    else if(this.sellerType[index] =='T'){
      // this.onReset();
      this.onResetLotData(index);

      this.showFarmer = false;
      this.showTrader = true;
      // this.shown26=false;
      // this.shown27=false;
      // this.shown28=true;
      // this.shown25=true;
      // this.shown23=false;
      // this.shown22=false;
    }
  }

  // Select seller name
  selectSellerName(){
    this.loading = true;
    this.authservice.selectSellerName().subscribe($data=>{
      console.log("Seller names table: ",$data);
      this.sellerNameData=$data.listData;
      this.filteredData1=this.sellerNameData;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
        }
      }
  )
  }

  //select Trader Name
  selectTraderName(){
    this.loading = true;
    console.log("Trader type selected");
    this.authservice.getTraderList().subscribe($allTraderData=>{
      console.log("Trader List data: ",$allTraderData.listData);
      this.sellerNameData=$allTraderData.listData;
      this.filteredData1=this.sellerNameData;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  );
  }

selTraderVal:any;
onTraderORFarmerSelect(post,index){
  this.selTraderVal = post;
  this.selectedRow=index;
  console.log("selected row is1 : ",this.selTraderVal);
  console.log("selected row is2 : ",post);
}

  setSellerName(){
    this.selectedRow = -1;
    this.matchFarmer = "";
    this.matchTrader = "";

      this.createLotDetailList[this.selectedLotRow].farmerObj = this.selTraderVal;
      this.createLotDetailList[this.selectedLotRow].traderList = undefined;
      console.log("selected row is : ",this.selTraderVal);
      console.log("this.createLotDetailList: ",this.createLotDetailList);


      this.frmFullName[this.selectedLotRow]=this.selTraderVal.frmFullName;
      if(this.selTraderVal.frmsonfo == "null"){
        this.frmsonfo[this.selectedLotRow]="";
      }
      else{
        this.frmsonfo[this.selectedLotRow]=this.selTraderVal.frmsonfo;
      }
      this.frmContactNo[this.selectedLotRow]=this.selTraderVal.frmContactNo;
      this.relationVal[this.selectedLotRow]=this.selTraderVal.frmRelationId;
      this.loading = true;
      this.authservice.getAllStateName().subscribe($allStatename=>{
      console.log("All state name: ",$allStatename.listData);
      this.allStateName=$allStatename.listData;
      this.filteredData2=this.allStateName;
      for(var i=0;i<this.allStateName.length;i++){
        if(this.allStateName[i].gmsmStateId == this.selTraderVal.frmStateId){
          this.gmsmStateDesc[this.selectedLotRow] = this.allStateName[i].gmsmStateDesc;
          this.stateId[this.selectedLotRow] = this.allStateName[i]["gmsmStateId"];
          break;
        }
      }
      this.loading = false;
    }, (err) => {
              if (err === 'Unauthorized')
              {
                      this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class50 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);

          }
        }
  );

      // this.getDistrictList(this.selTraderVal.frmStateId);
      this.loading = true;
      this.authservice.getAllDistrictName(this.selTraderVal.frmStateId).subscribe($allDist=>{
        console.log("All district name: ",$allDist);
        this.allDIstData=$allDist.listData;
        this.filteredData3=this.allDIstData;
        for(var i=0;i<this.allDIstData.length;i++){
          if(this.allDIstData[i].gmdmDistId == this.selTraderVal.frmDistId){
            this.distPerticularName[this.selectedLotRow] = this.allDIstData[i].gmdmDistDesc;
            this.distId[this.selectedLotRow] = this.allDIstData[i]["gmdmDistId"];
            break;
          }
        }
        this.loading = false;
      }, (err) => {
              if (err === 'Unauthorized')
              {
                      this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class50 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);

          }
        }
    )
    if(this.selTraderVal.frmTahsilId != undefined){
      this.loading = true;
      this.authservice.getAllTehsilName(this.selTraderVal.frmDistId).subscribe($alltehsil=>{
          console.log("all tehsil name: ",$alltehsil);
          this.allTehsilData=$alltehsil.listData;
          this.filteredData4=this.allTehsilData;
          for(var i=0;i<this.allTehsilData.length;i++){
            if(this.allTehsilData[i].gmtmTahsilId == this.selTraderVal.frmTahsilId){
              this.tehsilPerticularName[this.selectedLotRow] = this.allTehsilData[i].gmtmTashilDesc;
              this.tehsilId[this.selectedLotRow] = this.allTehsilData[i].gmtmTahsilId;
              break;
            }
          }
          this.loading = false;
         }, (err) => {
              if (err === 'Unauthorized')
              {
                      this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class50 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);

          }
        }
    );
    }

    if(this.selTraderVal.gmcmCity != undefined){
      this.gmcmCity[this.selectedLotRow] = this.selTraderVal.gmcmCity;
      this.villId[this.selectedLotRow] = this.selTraderVal.gmcmCityId;
    }

      console.log("this.allDIstData",this.allDIstData["listData"],this.allDIstData)

      this.my_class9='overlay';
      this.my_class1='overlay';
      this.filteredData1 =[];
      this.matchFarmer ="";
      this.selectedRow = -1;

  }

//State Parameters
onSelectState(){
  this.my_class10='overlay1';
  this.loading = true;
  this.authservice.getAllStateName().subscribe($allStatename=>{
    console.log("All state name: ",$allStatename.listData);
    this.allStateName=$allStatename.listData;
    this.filteredData2=this.allStateName;
     this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
);

}

selStateVal =[];
stateName(post8:any,index8){
  // this.shown19=false;
  // this.shown20=false;
  // this.shown21=true;
  this.selectedState=index8;
  this.selStateVal = post8;
  // this.gmsmDefunct=post8.gmsmDefunct;
  // this.gmsmOrgid=post8.gmsmOrgid;
  // this.gmsmStateCd=post8.gmsmStateCd;
  // this.gmsmStateDesc=post8.gmsmStateDesc;
  // this.gmsmStateId=post8.gmsmStateId;
  // this.frmStateId=this.gmsmStateId;
}

// Select state OK
selSatateValOK =[];stateId=[];
onSelStateOK(){
  console.log()
  this.distId[this.selectedLotRow] = "";
  this.distPerticularName[this.selectedLotRow] = "";
  this.tehsilId[this.selectedLotRow] = "";
  this.tehsilPerticularName[this.selectedLotRow] = "";
  this.villId[this.selectedLotRow] = "";
  this.gmcmCity[this.selectedLotRow] = "";
  this.selSatateValOK = this.selStateVal;
  if(this.createLotDetailList[this.selectedLotRow].framerType == 'F'){
    if(this.createLotDetailList[this.selectedLotRow].farmerObj != ""){
      this.createLotDetailList[this.selectedLotRow].farmerObj.frmStateId = this.selSatateValOK["gmsmStateId"];
    }
  }
  else{
    if(this.createLotDetailList[this.selectedLotRow].traderList != ""){
      this.createLotDetailList[this.selectedLotRow].traderList.lcam_agen_state_id = this.selSatateValOK["gmsmStateId"];
    }
  }

  this.gmsmStateDesc[this.selectedLotRow] = this.selSatateValOK["gmsmStateDesc"];
  this.stateId[this.selectedLotRow] = this.selSatateValOK["gmsmStateId"];

}

//selected state in create farmer
selFarmerStateVal = [];
selectedFState(post9:any,index9){
  this.selFarmerStateVal = post9;
  this.selectedFarmerState=index9;
}

selFarmerStateValOK = []
createFarmerStateOK(){
  this.selFarmerStateValOK = this.selFarmerStateVal;
  console.log("this.selFarmerStateValOK",this.selFarmerStateValOK)
  this.FStateName=this.selFarmerStateValOK["gmsmStateDesc"];
  this.FStateId=this.selFarmerStateValOK["gmsmStateId"];
  this.gmsmStateId=this.FStateId;
}


//District Parameters
onSelectDistrict(){
  console.log("this.gmsmStateId",this.stateId[this.selectedLotRow],this.stateId)
        if(this.stateId[this.selectedLotRow] != undefined){
          this.my_class2='overlay1';
          this.getDistrictList(this.stateId[this.selectedLotRow]);
        }
        else{
          this.my_class30 = 'overlay1';
          setTimeout(()=>{
            this.my_class30 = 'overlay';
            //this.loading = true;
          },2000);
        }
  }

 getDistrictList(stateId){
   this.loading = true;
   this.authservice.getAllDistrictName(stateId).subscribe($allDist=>{
     console.log("All district name: ",$allDist);
     this.allDIstData=$allDist.listData;
     this.filteredData3=this.allDIstData;
     this.loading = false;
   }, (err) => {
           if (err === 'Unauthorized')
           {
                   this.errorPopupShow();
                   setTimeout(()=>{
                     this.my_class50 = 'overlay';
                     localStorage.clear();
                     this.router.navigateByUrl('/login');
                   },3000);

       }
     }
 )
 }
  selDistrictVal =[];
  DistrictName(post:any,index){
    this.shown1=false;
    this.shown2=true;
    //console.log("selected post of district name is: ",post);
    this.selectedDistrict=index;
    this.selDistrictVal = post;
    // this.choosenDist=post.gmdmDistDesc;
    // this.choosenDistId=post.gmdmDistId;
    // this.frmDistId=this.choosenDistId;
    //console.log("choosen district id: ",this.choosenDistId);
  }

finalDistrictVal =[];distId=[];
onDistrictNameOK(){
this.tehsilId[this.selectedLotRow] = "";
this.tehsilPerticularName[this.selectedLotRow] = "";
this.gmcmCity[this.selectedLotRow] = "";
this.villId[this.selectedLotRow] = "";
this.finalDistrictVal = this.selDistrictVal;
this.distId[this.selectedLotRow] = this.finalDistrictVal["gmdmDistId"];
this.distPerticularName[this.selectedLotRow] = this.finalDistrictVal["gmdmDistDesc"];

}

 //Selected district in create farmer
  selectedFDistrict(post10:any,index10){
    this.selectedFarmerDistrict=index10;
    this.FDistName=post10.gmdmDistDesc;
    this.FDistId=post10.gmdmDistId;
    this.choosenDistId=this.FDistId;
  }




//Tehsil parametetrs
  onSelectTehsil(){
    //this.flag=5;
    if(this.distId[this.selectedLotRow] != undefined){
      this.my_class3='overlay1';
      this.loading = true;
      this.authservice.getAllTehsilName(this.distId[this.selectedLotRow]).subscribe($alltehsil=>{
          console.log("all tehsil name: ",$alltehsil);
          this.allTehsilData=$alltehsil.listData;
          this.filteredData4=this.allTehsilData;
          this.loading = false;
      }, (err) => {
              if (err === 'Unauthorized')
              {this.errorPopupShow();
                setTimeout(()=>{
                  this.my_class50 = 'overlay';
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
              }
            }
          );
        }
    else{
      this.my_class32 = 'overlay1';
      setTimeout(()=>{
        this.my_class32 = 'overlay';
      },2000);
    }

  }

  selTehsilVal =[];
  TehsilName(post2:any,index2){
    this.shown4=false;
    this.shown5=true;
    this.selectedTehsil=index2;
    this.selTehsilVal = post2;
    //console.log("selected tehsil is: ",post2);
    // this.gmtmTahsilId=post2.gmtmTahsilId;
    // this.gmtmTashilDesc=post2.gmtmTashilDesc;
    // this.frmTahsilId=this.gmtmTahsilId;

  }


  tehsilId=[];finalTehsilVal= [];
  onTehsilOK(){
    this.gmcmCity[this.selectedLotRow] = "";
    this.villId[this.selectedLotRow] = "";
    this.finalTehsilVal = this.selTehsilVal;
    this.tehsilPerticularName[this.selectedLotRow] = this.finalTehsilVal["gmtmTashilDesc"];
    this.tehsilId[this.selectedLotRow] = this.finalTehsilVal["gmtmTahsilId"];
  }

  //Selected Tehsil Name in create farmer
  selectedFTehsil(post11:any,index11){
    this.selectedFarmerTehsil=index11;
    //console.log("selected tehsil is: ",post2);
    this.FTehsilId=post11.gmtmTahsilId;
    this.gmtmTahsilId=this.FTehsilId;
    this.FTehsilName=post11.gmtmTashilDesc;

  }

  //Village parametetrs
  onSelectVillage(){
    if(this.distId[this.selectedLotRow]!= undefined && this.distId[this.selectedLotRow] != "" && this.tehsilId[this.selectedLotRow] != undefined && this.tehsilId[this.selectedLotRow] != ""){
      this.my_class4='overlay1';
      this.loading = true;
      this.authservice.getAllVillageName(this.distId[this.selectedLotRow],this.tehsilId[this.selectedLotRow]).subscribe($allVillName=>{
        console.log("All village name",$allVillName);
        this.allVillageData=$allVillName.listData;
        this.filteredData5=this.allVillageData;
        this.loading = false;
      }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                setTimeout(()=>{
                  this.my_class50 = 'overlay';
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
              }
        }
    )
    }
    else{
      this.my_class28 = 'overlay1';
      setTimeout(()=>{
        this.my_class28 = 'overlay';
      },2000);
    }

  }

  selVillVal =[];
  VillageName(post3:any,index3){
    this.shown7=false;
    this.shown8=true;
    this.selectedVillage=index3;
    this.selVillVal = post3;
    //console.log("Selected village is : ",post3);
    // this.gmcmCityId=post3.gmcmCityId;
    // this.gmcmCity=post3.gmcmCity;
    // console.log("Selected village is : ",this.gmcmCity,post3);

  }

  finalVillageVal =[];villId=[];
  onSelVillageOK(){
    this.finalVillageVal = this.selVillVal;
    this.gmcmCity[this.selectedLotRow] = this.finalVillageVal["gmcmCity"]
    this.villId[this.selectedLotRow] = this.finalVillageVal["gmcmCityId"]
  }
  //Selected village name in Farmer create
  selectedFVillage(post12:any,index12){
    this.selectedFarmerVillage=index12;
    //console.log("Selected village is : ",post3);
    this.FVillageId=post12.gmcmCityId;
    this.FVillageName=post12.gmcmCity;
  }


  //commodity parameters
  getAllCommodity(){
    this.loading = true;
    this.authservice.getAllCommodityList().subscribe($allCommodityList=>{
      //console.log("All commodity data: ",$allCommodityList);
      this.allCommodityData=$allCommodityList.listData;
      this.filteredData6=this.allCommodityData;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
  )
  }

commodityObjVal =[];
  CommodityName(post4:any,index4){
    this.shown9=false;
    this.shown10=true;
    this.selectedCommodity=index4;
    this.commodityObjVal = post4;
    //console.log("Selected commodity is: ",post4);
    this.gmpmProdId=post4.gmpmProdId;
    // this.gmpmgProdName=post4.gmpmgProdName;
    this.gmpmDefunct=post4.gmpmDefunct;
    this.gmpmMarketId=post4.gmpmMarketId;
    this.gmpmOprId=post4.gmpmOprId;
    this.gmpmOrgId=post4.gmpmOrgId;
    this.gmpmQtyUom=post4.gmpmQtyUom;
    this.gmpmgProdCode=post4.gmpmgProdCode;
    this.gmpmgQtyUom=post4.gmpmgQtyUom;
  }

  onSelectBagType(){
    this.loading = true;
    this.authservice.getBagType().subscribe($allBagData=>{
      //console.log("All bag types are: ",$allBagData);
      this.allBagTypedata=$allBagData.listData;
      this.filteredData7=this.allBagTypedata;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  );
  }

  bagTypeVal=[];
  selectedBagData(post5:any,index5){
    this.shown11=false;
    this.shown12=true;
    this.selectedBag=index5;
    this.bagTypeVal = post5;

    // this.gmptm_desc=post5.gmptm_desc;
    // this.gmptm_defunct=post5.gmptm_defunct;
    // this.gmptm_oprid=post5.gmptm_oprid;
    // this.gmptm_orgid=post5.gmptm_orgid;
    // this.gmptm_packid=post5.gmptm_packid;
    // this.gmptm_qty=post5.gmptm_qty;
    // this.gmptm_qty_uom=post5.gmptm_qty_uom;
  }

  getBagQuality(index){
    console.log("you typed no of bags:",this.noOfBags);
    if(this.createLotDetailList[index].commodityObj != "" ){
      if(this.createLotDetailList[index].bagTypeObj != ""){
        const apprxQtl={commodityObj: this.createLotDetailList[index].commodityObj,
                        bagObject: this.createLotDetailList[index].bagTypeObj,
                        noBag:this.noOfBags[index]}
        //console.log("apprxQtl : ",apprxQtl);
        this.loading = true;
        this.authservice.getApprxQty(apprxQtl).subscribe($apprxBagsqty=>{
          //console.log("Approx quality of bag is: ",$apprxBagsqty);
          this.approxQty[index]=$apprxBagsqty.approxQty;
          this.createLotDetailList[index].approxQty = $apprxBagsqty.approxQty;
          this.loading = false;
        }, (err) => {
                if (err === 'Unauthorized')
                {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class50 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
                }
              }
            );
      }
      else{
        this.my_class26 = 'overlay1';
        setTimeout(()=>{
          this.my_class26 = 'overlay';
          this.noOfBags[index] = "";
          this.approxQty[index] = "";
        },2000);
      }
    }
    else{
      this.my_class21 = 'overlay1';
      setTimeout(()=>{
        this.my_class21 = 'overlay';
        this.noOfBags[index] = "";
        this.approxQty[index] = "";
      },2000);
    }
    console.log("you typed no of bags:",this.noOfBags,index,this.selectedRow);

  }

  onClickVehicleType(){
    this.loading = true;
    this.authservice.getVehicleTypeList().subscribe($vehicleData=>{
      console.log("vehicle data type: ",$vehicleData);
      this.vehicleData=$vehicleData.listData;
      this.filteredData8=this.vehicleData;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  );
  }

  vehicleVal=[];
  selectedVehicleData(post6:any,index6){
    this.shown13=false;
    this.shown14=true;
    this.selectedVehicle=index6;
    this.vehicleVal = post6;
    console.log("vehicleVal",this.vehicleVal)

    this.gvrmDefunct=post6.gvrmDefunct;
    this.gvrmOprId=post6.gvrmOprId;
    this.gvrmOrgId=post6.gvrmOrgId;
    this.gvrmTrnId=post6.gvrmTrnId;
    this.gvrmVehRateAnnual=post6.gvrmVehRateAnnual;
    this.gvrmVehRateDaily=post6.gvrmVehRateDaily;
    this.gvrmVehRateMonthly=post6.gvrmVehRateMonthly;
    // this.gvrmVehTypeCd=post6.gvrmVehTypeCd;
  }

  getCaFirm(){
    this.loading = true;
    this.authservice.getCAList().subscribe($caData=>{
      console.log("All CA firm Data: ",$caData);
      this.allCAData=$caData.listData;
      this.filteredData9=this.allCAData;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  );
  }
  CAFirmVal:any;
  selectedCAFirm(post7:any,index7){
    this.shown15=false;
    this.shown16=true;
    this.shown17=false;
    this.shown18=true;
    this.selectedCA=index7;

    this.CAFirmVal = post7;

  //  this.lcamAgenCompName=post7.lcamAgenCompName;
    // this.lcamFullName=post7.lcamFullName;
    // this.lcamAadharNo=post7.lcamAadharNo;
    // this.lcamAgenAdd1=post7.lcamAgenAdd1;
    // this.lcamAgenAdd2=post7.lcamAgenAdd2;
    // this.lcamAgenCatgCd=post7.lcamAgenCatgCd;
    // this.lcamAgenCatgDesc=post7.lcamAgenCatgDesc;
    // this.lcamAgenCompRegino=post7.lcamAgenCompRegino;
    // this.lcamAgenContno=post7.lcamAgenContno;
    // this.lcamAgenDistId=post7.lcamAgenDistId;
    // this.lcamAgenEmailid=post7.lcamAgenEmailid;
    // this.lcamAgenFaxno=post7.lcamAgenFaxno;
    // this.lcamAgenGendStat=post7.lcamAgenGendStat;
    // this.lcamAgenLicenNo=post7.lcamAgenLicenNo;
    // this.lcamAgenPincd=post7.lcamAgenPincd;
    // this.lcamAgenProdCd=post7.lcamAgenProdCd;
    // this.lcamAgenProdDesc=post7.lcamAgenProdDesc;
    // this.lcamAgenStateId=post7.lcamAgenStateId;
    // this.lcamAgenThasilId=post7.lcamAgenThasilId;
    // this.lcamAgenTradLicenno=post7.lcamAgenTradLicenno;
    // this.lcamAgenType=post7.lcamAgenType;
    // this.lcamBankAccount=post7.lcamBankAccount;
    // this.lcamBankName=post7.lcamBankName;
    // this.lcamDefunct=post7.lcamDefunct;
    // this.lcamFactorBidLimit=post7.lcamFactorBidLimit;
    // this.lcamIfscCode=post7.lcamFactorBidLimit;
    // this.lcamOprid=post7.lcamOprid;
    // this.lcamOrgid=post7.lcamOrgid;
    // this.lcamPanNo=post7.lcamPanNo;
    // this.lcamRemark=post7.lcamRemark;
    // this.lcamSecurityDeposit=post7.lcamSecurityDeposit;
    // this.lcamTrnId=post7.lcamTrnId;
  }

  farmerPresent:boolean;commodityPresent:boolean;vechileObjPresent:boolean;bagTypePresent:boolean;noOfBagPresent:boolean;
  vehicleNoPresent:boolean;
  apprQtyPresent:boolean;traderPresent:boolean;
  onClickSave(){
    console.log("frmFullName",this.frmFullName)
    console.log("selTraderVal",this.selTraderVal);
    console.log("selCommodityObj",this.selCommodityObj);
    console.log("selBagTypeObj",this.selBagTypeObj);
    console.log("approxQty",this.approxQty);
    console.log("this.frmDistId",this.frmDistId);
    console.log("this.regVehVal",this.finalRegVehVal);
    console.log("sel",this.frmContactNo,this.frmDistId,this.frmStateId,this.frmTahsilId, this.frmCityId);
    // if(this.frmContactNo == undefined){
    //   this.frmContactNo = "";
    // }
    this.farmerPresent = true;this.commodityPresent = true;this.vechileObjPresent = true;this.bagTypePresent = true;
    this.vehicleNoPresent = true;
    this.vechileObjPresent = true;
    this.noOfBagPresent = true;
    this.vehicleNoPresent = true;
    this.apprQtyPresent = true;
    this.traderPresent = true;

    for(var i=0;i<this.createLotDetailList.length;i++){
      if(this.createLotDetailList[i].framerType == 'F'){
        if(this.createLotDetailList[i].farmerObj == "" || this.createLotDetailList[i].farmerObj == undefined){
          this.farmerPresent = false;
        }
      }
      if(this.createLotDetailList[i].framerType == 'T'){
        if(this.createLotDetailList[i].traderList == "" || this.createLotDetailList[i].traderList == undefined){
          this.traderPresent = false;
        }
      }

      if(this.createLotDetailList[i].commodityObj == "" || this.createLotDetailList[i].commodityObj == undefined){
        this.commodityPresent = false;
      }
      if(this.createLotDetailList[i].bagTypeObj == "" || this.createLotDetailList[i].bagTypeObj == undefined){
        this.bagTypePresent = false;
      }
      if(this.selGate.length != 0){
        this.createLotDetailList[i].gateObj = this.selGate;
      }
      if(this.selGate.length == 0){
        this.createLotDetailList[i].gateObj = undefined ;
      }
      // this.createLotDetailList[this.selectedLotRow].gateObj = this.selGate;

      if(this.createLotDetailList[i].caObj == ""){
        this.createLotDetailList[i].caObj = undefined;
      }
      if(this.regType == 'U'){
        if(this.createLotDetailList[i].vechileObj == "" || this.createLotDetailList[i].vechileObj == undefined){
          this.vechileObjPresent = false;
        }
      }
      if(this.regType == 'R'){
        if(this.createLotDetailList[i].regiVechObj == "" || this.createLotDetailList[i].regiVechObj == undefined){
          this.vechileObjPresent = false;
        }
      }
      if(this.noOfBags[i] == "" || this.noOfBags[i] == undefined){
        this.noOfBagPresent = false;
      }
      if(this.vehicleno[i] == "" || this.vehicleno[i] == undefined){
        this.vehicleNoPresent = false;
      }
      if(this.createLotDetailList[i].approxQty == "" || this.createLotDetailList[i].approxQty == undefined){
        this.apprQtyPresent = false;
      }
    }
    if(this.farmerPresent == true && this.traderPresent == true ){
      if(this.commodityPresent == true){
        if(this.bagTypePresent == true){
          if(this.noOfBagPresent == true){
            if(this.vechileObjPresent ==true){
              if(this.vehicleNoPresent == true){
                if(this.apprQtyPresent == true){
                  for(var i=0;i<this.createLotDetailList.length;i++){
                    if(this.createLotDetailList[i].framerType == 'F'){
                      if(this.createLotDetailList[i].farmerObj.frmContactNo != undefined){
                        this.createLotDetailList[i].farmerObj.frmContactNo = this.frmContactNo[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmContactNo == undefined){
                        this.createLotDetailList[i].farmerObj.frmContactNo = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.frmRelationId != undefined){
                        this.createLotDetailList[i].farmerObj.frmRelationId = this.relationVal[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmRelationId == undefined){
                        this.createLotDetailList[i].farmerObj.frmRelationId = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.frmsonfo != undefined){
                        this.createLotDetailList[i].farmerObj.frmsonfo = this.frmsonfo[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmsonfo == undefined){
                        this.createLotDetailList[i].farmerObj.frmsonfo = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.frmStateId != undefined){
                        this.createLotDetailList[i].farmerObj.frmStateId = this.stateId[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmStateId == undefined){
                        this.createLotDetailList[i].farmerObj.frmStateId = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.frmDistId != undefined){
                        this.createLotDetailList[i].farmerObj.frmDistId = this.distId[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmDistId == undefined){
                        this.createLotDetailList[i].farmerObj.frmDistId = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.frmCityId != undefined){
                        this.createLotDetailList[i].farmerObj.frmCityId = this.villId[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmCityId == undefined){
                        this.createLotDetailList[i].farmerObj.frmCityId = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.gmcmCity != undefined){
                        this.createLotDetailList[i].farmerObj.gmcmCity = this.gmcmCity[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.gmcmCity == undefined){
                        this.createLotDetailList[i].farmerObj.gmcmCity = "";
                      }
                      if(this.createLotDetailList[i].farmerObj.frmTahsilId != undefined){
                        this.createLotDetailList[i].farmerObj.frmTahsilId = this.tehsilId[i];
                      }
                      if(this.createLotDetailList[i].farmerObj.frmTahsilId == undefined){
                        this.createLotDetailList[i].farmerObj.frmTahsilId = "";
                      }
                    }
                    else{
                      if(this.createLotDetailList[i].traderList.lcam_agen_contno != undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_contno = this.frmContactNo[i];
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_contno == undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_contno = "";
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_state_id != undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_state_id = this.stateId[i];
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_state_id == undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_state_id = "";
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_dist_id != undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_dist_id = this.distId[i];
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_dist_id == undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_dist_id = "";
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_thasil_id  != undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_thasil_id = this.tehsilId[i];
                      }
                      if(this.createLotDetailList[i].traderList.lcam_agen_thasil_id  == undefined){
                        this.createLotDetailList[i].traderList.lcam_agen_thasil_id = "";
                      }
                      // this.createLotDetailList[i].farmerObj.frmRelationId = this.relationVal[i];
                      // this.createLotDetailList[i].farmerObj.frmsonfo = this.frmsonfo[i];
                      // this.createLotDetailList[i].farmerObj.frmCityId = this.villId[i];
                      // this.createLotDetailList[i].farmerObj.gmcmCity = this.gmcmCity[i];
                    }

                    this.createLotDetailList[i].noBag = this.noOfBags[i].toString();
                    this.createLotDetailList[i].lotType = this.lotType;
                    this.createLotDetailList[i].vechRegType = this.regType;
                    this.createLotDetailList[i].vechileNo = this.vehicleno[i];
                    if(this.regType == 'R'){
                      this.createLotDetailList[i].vechileObj = undefined;
                    }
                    if(this.regType == 'U'){
                      this.createLotDetailList[i].regiVechObj = undefined;
                    }
                  }
                  console.log("createLotDetailList u",this.createLotDetailList)
                  this.loading = true;
                  this.authservice.getSaveResult(this.createLotDetailList).subscribe($RsSavedData=>{
                    this.loading = false;
                    console.log("Response from saved data: ",$RsSavedData);
                    if($RsSavedData.status==1){
                      this.div_show8();
                      var msg = "Gate Entry New "+$RsSavedData.dataList+" Created Successfully!"
                      this.message= msg;
                      this.resetTableData();
                      this.createLotDetailList.push({
                      "framerType": "F",
                      "farmerObj": "",
                      "traderList" :"",
                      "commodityObj": "",
                      "bagTypeObj": "",
                      "noBag": "",
                      "approxQty" :"",
                      "vechileNo":"",
                      "vechRegType": "" ,
                      "vechileObj" :"",
                      "lotType": "",
                      "caObj":"",
                      "gateObj":"",
                      "regiVechObj":""
                    })
                    this.showFarmer = true;
                    this.gateNo =this.yardDesc = "";
                    this.regType = "U";this.lotType = "N";
                    this.vehRegNo = this.vehContact = this.vehOwnerName = this.vehAmount ="";
                    }else
                    {
                      this.div_show8();
                      this.message="Not successfully saved !!";
                    }
                  }, (err) => {
                          if (err === 'Unauthorized')
                          {
                                  this.errorPopupShow();
                                  setTimeout(()=>{
                                    this.my_class50 = 'overlay';
                                    localStorage.clear();
                                    this.router.navigateByUrl('/login');
                                  },3000);

                      }
                    }
                );

                }
                else{
                  this.my_class23 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class23 = 'overlay';
                  },2000);
                }
              }
              else{
                this.my_class25 = 'overlay1';
                setTimeout(()=>{
                  this.my_class25 = 'overlay';
                },2000);
              }


            }
            else{
              this.my_class24 = 'overlay1';
              setTimeout(()=>{
                this.my_class24 = 'overlay';
              },2000);
            }
          }
          else{
            this.my_class22 = 'overlay1';
            setTimeout(()=>{
              this.my_class22 = 'overlay';
            },2000);
          }


        }
        else{
          this.my_class26 = 'overlay1';
          setTimeout(()=>{
            this.my_class26 = 'overlay';
          },2000);
        }



      }
      else{
        this.my_class21 = 'overlay1';
        setTimeout(()=>{
          this.my_class21 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class20 = 'overlay1';
      setTimeout(()=>{
        this.my_class20 = 'overlay';
      },2000);
    }
    // if(this.frmDistId == undefined){
    //   this.frmDistId = "";
    // }
    // if(this.frmStateId == undefined){
    //   this.frmStateId = "";
    // }
    // if(this.frmTahsilId == undefined){
    //   this.frmTahsilId = "";
    // }
    // if(this.frmCityId == undefined){
    //   this.frmCityId = "";
    // }
    // var postJson={};
    // if(this.selTraderVal != undefined){
    //   if(this.selCommodityObj.length != 0){
    //     if(this.selBagTypeObj.length != 0){
    //       if(this.noOfBags != []){
    //         if(this.approxQty != undefined && this.approxQty != []){
    //           if(this.vehicleTypeObj.length != 0){
    //             if(this.vehicleno != []){
    //               this.selTraderVal.frmCityId = this.frmCityId;
    //               this.selTraderVal.frmContactNo = this.frmContactNo;
    //               this.selTraderVal.frmDistId = this.frmDistId;
    //               this.selTraderVal.frmStateId = this.frmStateId;
    //               this.selTraderVal.frmTahsilId = this.frmTahsilId;
    //               postJson =[{
    //               "framerType": this.sellerType,
    //               "farmerObj": this.selTraderVal,
    //               "commodityObj": this.selCommodityObj,
    //               "bagTypeObj": this.selBagTypeObj,
    //               "noBag": this.noOfBags,
    //               "approxQty" :this.approxQty,
    //               "vechileNo":this.vehicleno,
    //               "vechTypeId": this.gvrmTrnId,
    //               "vechRegType": this.regType ,
    //               "lotType": this.lotType
    //
    //             }];
    //
    //               if(this.caFirmObj != undefined){
    //                 postJson[0]["caObj"] =  this.caFirmObj;
    //               }
    //               if(this.regType == 'R'){
    //                 if(this.finalRegVehVal != undefined){
    //                   postJson[0]["regiVechObj"] =  this.finalRegVehVal;
    //                 }
    //               }
    //               if(this.regType == 'U'){
    //                 postJson[0]["vechileObj"] =  this.vehicleTypeObj;
    //               }
    //
    //               if(this.valGate.length != 0){
    //                 postJson[0]["gateObj"] =  this.valGate;
    //               }
    //               console.log("postJson",postJson)
    //               console.log("regType",this.regType)
    //               //   this.authservice.getSaveResult(postJson).subscribe($RsSavedData=>{
    //               //     console.log("Response from saved data: ",$RsSavedData);
    //               //     if($RsSavedData.status==1){
    //               //       this.div_show8();
    //               //       this.message=$RsSavedData.message;
    //               //       this.frmFullName = [];
    //               //       this.frmContactNo = [];
    //               //       this.gmsmStateDesc = [];
    //               //       this.choosenDist = "";
    //               //       this.gmtmTashilDesc = "";
    //               //       this.gmcmCity = [];
    //               //       this.gmpmgProdName = [];
    //               //       this.gmptm_desc = [];
    //               //       this.noOfBags = [];
    //               //       this.gvrmVehTypeCd = [];
    //               //       this.vehicleno = [];
    //               //       this.frmsonOf = "";
    //               //       this.choosenDist  = "";
    //               //       this.gateNo = this.yardDesc = this.vehRegNo = this.vehContact = this.vehOwnerName = this.vehAmount = "";
    //               //       this.regType = "U";
    //               //       this.distPerticularName =this.tehsilPerticularName = this.approxQty = this.lcamAgenCompName = this.lcamFullName  = [];
    //               //     }else
    //               //     {
    //               //       this.div_show8();
    //               //       this.message="Not successfully saved !!";
    //               //     }
    //               //   }, (err) => {
    //               //           if (err === 'Unauthorized')
    //               //           {
    //               //                   this.errorPopupShow();
    //               //                   setTimeout(()=>{
    //               //                     this.my_class50 = 'overlay';
    //               //                     localStorage.clear();
    //               //                     this.router.navigateByUrl('/login');
    //               //                   },3000);
    //               //
    //               //       }
    //               //     }
    //               // );
    //             }
    //             else{
    //               this.my_class25 = 'overlay1';
    //               setTimeout(()=>{
    //                 this.my_class25 = 'overlay';
    //               },2000);
    //             }
    //
    //           }
    //           else{
    //             this.my_class24 = 'overlay1';
    //             setTimeout(()=>{
    //               this.my_class24 = 'overlay';
    //             },2000);
    //           }
    //
    //         }
    //         else{
    //           this.my_class23 = 'overlay1';
    //           setTimeout(()=>{
    //             this.my_class23 = 'overlay';
    //           },2000);
    //         }
    //       }
    //       else{
    //         this.my_class22 = 'overlay1';
    //         setTimeout(()=>{
    //           this.my_class22 = 'overlay';
    //         },2000);
    //       }
    //     }
    //     else{
    //       this.my_class26 = 'overlay1';
    //       setTimeout(()=>{
    //         this.my_class26 = 'overlay';
    //       },2000);
    //     }
    //
    //
    //   }
    //   else{
    //     this.my_class21 = 'overlay1';
    //     setTimeout(()=>{
    //       this.my_class21 = 'overlay';
    //     },2000);
    //   }
    //
    // }
    // else{
    //   this.my_class20 = 'overlay1';
    //   setTimeout(()=>{
    //     this.my_class20 = 'overlay';
    //   },2000);
    // }
    //
    //
    // console.log("postJson",postJson)

  }

  onClickSaveT(){
    console.log("sel",  this.lcam_agen_state_id,this.lcam_agen_thasil_id,this.lcam_agen_dist_id,this.lcam_agen_contno,this.lcam_full_name)
    if(this.lcam_agen_state_id == undefined){
      this.lcam_agen_state_id = "";
    }
    if(this.lcam_agen_thasil_id == undefined){
      this.lcam_agen_thasil_id = "";
    }
    if(this.lcam_agen_dist_id == undefined){
      this.lcam_agen_dist_id = "";
    }
    if(this.lcam_agen_contno == undefined){
      this.lcam_agen_contno = "";
    }

    var postJson={};
    if(this.selTraderVal != undefined){
      if(this.selCommodityObj.length != 0){
        if(this.selBagTypeObj.length != 0){
          if(this.noOfBags != []){
            if(this.approxQty != undefined && this.approxQty != []){
              if(this.vehicleTypeObj.length != 0){
                if(this.vehicleno != []){
                  this.selTraderVal.lcam_agen_contno = this.lcam_agen_contno;
                  this.selTraderVal.lcam_agen_state_id = this.lcam_agen_state_id;
                  this.selTraderVal.lcam_agen_dist_id = this.lcam_agen_dist_id;
                  this.selTraderVal.lcam_agen_thasil_id = this.lcam_agen_thasil_id;
                  postJson =[{
                  "framerType": this.sellerType,
                  "traderList": this.selTraderVal,
                  "commodityObj": this.selCommodityObj,
                  "bagTypeObj": this.selBagTypeObj,
                  "noBag": this.noOfBags,
                  "approxQty" :this.approxQty,
                  // "vechileObj": this.vehicleTypeObj,
                  "vechileNo":this.vehicleno,
                  "vechTypeId": this.gvrmTrnId,
                  "vechRegType": this.regType,
                  "lotType": this.lotType,
                }];
                  if(this.caFirmObj != undefined){
                    postJson[0]["caObj"] =  this.caFirmObj;
                  }
                  // if(this.finalRegVehVal != undefined){
                  //   postJson["regiVechObj"] =  this.finalRegVehVal;
                  // }
                  if(this.valGate.length != 0){
                    postJson[0]["gateObj"] =  this.valGate;
                  }
                  if(this.regType == 'R'){
                    if(this.finalRegVehVal != undefined){
                      postJson[0]["regiVechObj"] =  this.finalRegVehVal;
                    }
                  }
                  if(this.regType == 'U'){
                    postJson[0]["vechileObj"] =  this.vehicleTypeObj;
                  }
                  console.log("post json",postJson)
                  this.loading = true;
                  this.authservice.getSaveResult(postJson).subscribe($RsSavedData=>{
                    console.log("Response from saved data: ",$RsSavedData);
                    this.loading = false;
                    if($RsSavedData.status==1){
                      this.div_show8();
                      this.message=$RsSavedData.message;
                      this.frmFullName = [];
                      this.frmContactNo = [];
                      this.gmsmStateDesc = [];
                      this.choosenDist = "";
                      this.gmtmTashilDesc = "";
                      this.gmcmCity = [];
                      this.gmpmgProdName = [];
                      this.gmptm_desc = [];
                      this.noOfBags = [];
                      this.gvrmVehTypeCd = [];
                      this.vehicleno = [];
                      this.choosenDist  = "";
                      this.gateNo = this.yardDesc = this.vehRegNo = this.vehContact = this.vehOwnerName = this.vehAmount = "";
                      this.regType = "U";
                      this.distPerticularName = this.tehsilPerticularName = this.approxQty = this.lcamAgenCompName =this.lcamFullName  = [];
                    }else
                    {
                      this.div_show8();
                      this.message="Not successfully saved !!";
                    }
                  }
                  , (err) => {
                          if (err === 'Unauthorized')
                          {
                                  this.errorPopupShow();
                                  setTimeout(()=>{
                                    this.my_class50 = 'overlay';
                                    localStorage.clear();
                                    this.router.navigateByUrl('/login');
                                  },3000);

                      }
                    }
                );



                }
                else{
                  this.my_class25 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class25 = 'overlay';
                  },2000);
                }

              }
              else{
                this.my_class24 = 'overlay1';
                setTimeout(()=>{
                  this.my_class24 = 'overlay';
                },2000);
              }

            }
            else{
              this.my_class23 = 'overlay1';
              setTimeout(()=>{
                this.my_class23 = 'overlay';
              },2000);
            }
          }
          else{
            this.my_class22 = 'overlay1';
            setTimeout(()=>{
              this.my_class22 = 'overlay';
            },2000);
          }
        }
        else{
          this.my_class26 = 'overlay1';
          setTimeout(()=>{
            this.my_class26 = 'overlay';
          },2000);
        }


      }
      else{
        this.my_class21 = 'overlay1';
        setTimeout(()=>{
          this.my_class21 = 'overlay';
        },2000);
      }

    }
    else{
      this.my_class27 = 'overlay1';
      setTimeout(()=>{
        this.my_class27 = 'overlay';
      },2000);
    }

  }


  //On select ID proof type
  onSelectIdProof(proofValue){
    this.proofValue=proofValue;
    this.createFarmerObj.farmerIdProofId = this.proofValue;
    console.log("Id proof is: ",this.proofValue);
  }

  //on Select relation value
  relationValue(relationValue){
    this.relationFValue=relationValue;
    this.createFarmerObj.relationId = this.relationFValue;
    console.log("selected relation is: ",this.relationFValue);
  }
  //Create Farmer save
  defaultChecked:boolean;
  saveCreateFarmer(){
    if(this.FFullName !="" && this.FFullName != undefined){
      this.createFarmerObj.farmerFullName = this.FFullName;
    }
    if(this.FAddrs1 !="" && this.FAddrs1 != undefined ){
      this.createFarmerObj.farmerAdd1 = this.FAddrs1;
    }
    if(this.FAddrs2 !="" && this.FAddrs2 != undefined){
      this.createFarmerObj.farmerAdd2 = this.FAddrs2;
    }
    if(this.FMobileNo !="" && this.FMobileNo != undefined){
      this.createFarmerObj.farmerContactNo = this.FMobileNo;
    }
    if(this.FIdProofNo !="" && this.FIdProofNo != undefined){
      this.createFarmerObj.farmerKycIdProofType = this.FIdProofNo;
    }
    if(this.frmsonOf !="" && this.frmsonOf != undefined){
      this.createFarmerObj.farmerSonOf = this.frmsonOf;
    }

    if(this.createFarmerObj.farmerFullName !="" && this.createFarmerObj.farmerFullName != undefined  && this.createFarmerObj.farmerAdd1 !="" && this.createFarmerObj.farmerAdd1 != undefined && this.createFarmerObj.farmerStateId != "" && this.createFarmerObj.farmerStateId != undefined && this.createFarmerObj.farmerDistId != "" && this.createFarmerObj.farmerDistId != undefined && this.createFarmerObj.farmerTehsilId != "" && this.createFarmerObj.farmerTehsilId != undefined &&  this.createFarmerObj.cityId != "" && this.createFarmerObj.cityId != undefined && this.createFarmerObj.farmerContactNo != "" && this.createFarmerObj.farmerContactNo != undefined)
    {
        if(this.createFarmerObj.farmerAdd2 == ""){
          this.createFarmerObj.farmerAdd2 = undefined;
        }
        if(this.createFarmerObj.farmerKycIdProofType == ""){
          this.createFarmerObj.farmerKycIdProofType = undefined;
        }
        if(this.createFarmerObj.farmerSonOf == ""){
          this.createFarmerObj.farmerSonOf = undefined;
        }
        if(this.createFarmerObj.farmerIdProofId == ""){
          this.createFarmerObj.farmerIdProofId = undefined;
        }
        if(this.createFarmerObj.relationId == ""){
          this.createFarmerObj.relationId = undefined;
        }
        console.log("saveCreateFarmer is: ",this.createFarmerObj);
        this.loading = true;
        this.authservice.getCreateFarmer(this.createFarmerObj).subscribe($createFarmerData=>{
          this.loading = false;
          console.log("After submitting create farmer Data: ",$createFarmerData);
          if($createFarmerData.status==1){
            this.message=$createFarmerData.message;
            this.div_show8();
            this.div_hide10();
            this.resetCreateFarmer();
          }
          else
          {
            this.message=$createFarmerData.message;
            this.div_show8();
            this.div_hide10();
          }
        }, (err) => {
              if (err === 'Unauthorized')
              {
                      this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class50 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);

          }
        }
      );
    }
    else
    {    this.my_class41 = 'overlay1';
        setTimeout(()=>{
          this.my_class41 = 'overlay';
        },2000);
          // this.message="Mandatory fields(*) can't be blank ";
          // this.div_show8();
    }


  }


  //Searching fields
  matchFarmer:string;
  searchFarmerName(){
    this.farmerNameUpper= this.matchFarmer.toUpperCase();
    if(this.farmerNameUpper) {
      this.filteredData1 = _.filter(this.sellerNameData, (a)=>a.frmFullName.indexOf(this.farmerNameUpper)>=0);
    } else {
      this.filteredData1 = this.sellerNameData  ;
    }
  }

  matchTrader:string;
  searchTraderName(){
    console.log("1",this.matchTrader)
    console.log("1",this.sellerNameData)
    this.farmerNameUpper= this.matchTrader.toUpperCase();
    if(this.farmerNameUpper) {
      this.filteredData1 = _.filter(this.sellerNameData, (a)=>a.lcam_full_name.indexOf(this.farmerNameUpper)>=0);
    } else {
      this.filteredData1 = this.sellerNameData  ;
    }
  }

  matchCompany:string;
  searchCompanyName(){
    console.log("2",this.matchCompany)
    console.log("2",this.sellerNameData)

    this.farmerNameUpper= this.matchCompany.toUpperCase();
    console.log("farmerNameUpper",this.farmerNameUpper)

    if(this.farmerNameUpper) {
      var filteredData = _.filter(
          this.sellerNameData, (a)=>{
            if(a.lcam_agen_comp_name != undefined){
              a.lcam_agen_comp_name.indexOf(this.farmerNameUpper)>=0
            }
          });
        this.filteredData1 = filteredData;
        console.log("filteredData",filteredData)
    } else {
      this.filteredData1 = this.sellerNameData  ;
    }
  }

  searchStateName(query:string){
    if(query) {
      this.filteredData2 = _.filter(this.allStateName, (a)=>a.gmsmStateDesc.indexOf(query)>=0);
    } else {
      this.filteredData2 = this.allStateName;
    }
  }

  searchDistName(query:string){
    if(query) {
      this.filteredData3 = _.filter(this.allDIstData, (a)=>a.gmdmDistDesc.indexOf(query)>=0);
    } else {
      this.filteredData3 = this.allDIstData;
    }
  }

  searchTehsilName(query:string){
    if(query) {
      this.filteredData4 = _.filter(this.allTehsilData, (a)=>a.gmtmTashilDesc.indexOf(query)>=0);
    } else {
      this.filteredData4 = this.allTehsilData;
    }
  }

  searchVillageName(query:string){
    if(query) {
      this.filteredData5 = _.filter(this.allVillageData, (a)=>a.gmcmCity.indexOf(query)>=0);
    } else {
      this.filteredData5 = this.allVillageData;
    }
  }

  searchCommodityName(query:string){
    if(query) {
      this.filteredData6 = _.filter(this.allCommodityData, (a)=>a.gmpmgProdName.indexOf(query)>=0);
    } else {
      this.filteredData6 = this.allCommodityData;
    }
  }

  searchBagTypeName(query:string){
    if(query) {
      this.filteredData7 = _.filter(this.allBagTypedata, (a)=>a.gmptm_desc.indexOf(query)>=0);
    } else {
      this.filteredData7 = this.allBagTypedata;
    }
  }

  searchVehicleTypeName(query:string){
    if(query) {
      this.filteredData8 = _.filter(this.vehicleData, (a)=>a.gvrmVehTypeCd.indexOf(query)>=0);
    } else {
      this.filteredData8 = this.vehicleData;
    }
  }

  searchCAName(query:string){
    if(query) {
      this.filteredData9 = _.filter(this.allCAData, (a)=>a.lcamAgenCompName.indexOf(query)>=0);
    } else {
      this.filteredData9 = this.allCAData;
    }
  }

filteredGateInfo=[];
getInfoForGate(){
  this.loading = true;
  this.authservice.getGateInfo().subscribe($data=>{
    console.log("Gate Info: ",$data);
    if($data.listData != undefined){
      this.gateInfo = $data.listData;
      this.filteredGateInfo = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

filteredYardInfo =[];
getInfoForYard(){
  this.loading = true;
  this.authservice.getYardInfo().subscribe($data=>{
    console.log("Yard Info: ",$data);
    if($data.listData != undefined){
      this.yardInfo = $data.listData;
      this.filteredYardInfo = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

gateNo:string;
selGate=[];
valGate=[];
onGateSelect(gateVal,index){
  this.valGate = gateVal;
  this.selectedGate = index;
}
yardDesc:string;
selYard=[];
valYard=[];
selectedYard:number;
onYardSelect(yardVal,index){
  this.yardDesc = yardVal.gmtmMktDesc;
  this.valYard = yardVal;
  this.selectedYard = index;
}

  onAdvance1(){
    this.shown29=false;
    this.shown30=true;
  }
  onBasic1(){
    this.shown29=true;
    this.shown30=false;
  }
  onAdvance2(){
    this.shown31=false;
    this.shown32=true;
  }
  onBasic2(){
    this.shown31=true;
    this.shown32=false;
  }
  onAdvance3(){
    this.shown33=false;
    this.shown34=true;
  }
  onBasic3(){
    this.shown33=true;
    this.shown34=false;
  }



  // Open & closes of popups
  openFarmerTraderPopup(index){
    console.log("kk",this.createLotDetailList,index)
      if(this.createLotDetailList[index].framerType == 'F'){
        this.selectSellerName();
        this.my_class1='overlay1';
      }
      if(this.createLotDetailList[index].framerType == 'T'){
        this.selectTraderName();
        this.my_class33='overlay1';
      }
    }
  div_hide(){
      this.my_class1='overlay';
      this.my_class33='overlay';
      this.selectedRow = -1;
      this.matchFarmer = "";
      this.matchTrader = "";
      this.selTraderVal = [];
    }
  div_show1(index){
      this.selectedLotRow = index;
      this.my_class2='overlay1';
    }
  div_hide1(){
      this.my_class2='overlay';
    }
  div_show2(){
      this.my_class3='overlay1';
    }
  div_hide2(){
      this.my_class3='overlay';
    }
  div_show3(){
      this.my_class4='overlay1';
    }
  div_hide3(){
      this.my_class4='overlay';
    }
  div_show4(){
      this.my_class5='overlay1';
    }
  div_hide4(){
      this.my_class5='overlay';
    }

  selCommodityObj =[];
  selCommodityOK(){
      this.my_class5='overlay';
      this.selCommodityObj = this.commodityObjVal;
      this.createLotDetailList[this.selectedLotRow].commodityObj = this.selCommodityObj;
      this.gmpmgProdName[this.selectedLotRow] = this.selCommodityObj["gmpmgProdName"];
      console.log("this.selCommodityObj",this.createLotDetailList)
      this.noOfBags[this.selectedLotRow] = "";
      this.approxQty[this.selectedLotRow] = "";
    }
  div_show5(){
      this.my_class6='overlay1';
    }
  div_hide5(){
      this.my_class6='overlay';
    }
  selBagTypeObj =[];
  selBagTypeOK(){
      this.my_class6='overlay';
      this.selBagTypeObj = this.bagTypeVal;
      this.createLotDetailList[this.selectedLotRow].bagTypeObj = this.selBagTypeObj;
      this.gmptm_desc[this.selectedLotRow] = this.selBagTypeObj["gmptm_desc"];
      console.log("this.selBagTypeObj",this.createLotDetailList)
      this.noOfBags[this.selectedLotRow] = "";
      this.approxQty[this.selectedLotRow] = "";
    }
  div_show6(){
      this.my_class7='overlay1';
    }
  div_hide6(){
      this.my_class7='overlay';
    }

    vehicleTypeObj =[];
    vehicleTypeOK(){
    this.vehicleTypeObj = this.vehicleVal;
    console.log("vehicleTypeObj",this.vehicleTypeObj)
    this.my_class7='overlay';
    this.createLotDetailList[this.selectedLotRow].vechileObj = this.vehicleTypeObj;
    this.gvrmVehTypeCd[this.selectedLotRow] = this.vehicleTypeObj["gvrmVehDesc"];
    console.log("this.vehicleTypeObj ss",this.createLotDetailList)
    }

  div_show7(){
      this.my_class8='overlay1';
    }
  div_hide7(){
      this.my_class8='overlay';
    }

  caFirmObj:any;
  selCAFirmOK(){
      this.my_class8='overlay';
      this.lcamAgenCompName[this.selectedLotRow] = this.CAFirmVal.lcamAgenCompName;
      this.lcamFullName[this.selectedLotRow] = this.CAFirmVal.lcamFullName;
      this.caFirmObj = this.CAFirmVal;
      this.createLotDetailList[this.selectedLotRow].caObj = this.caFirmObj;
      this.gvrmVehTypeCd[this.selectedLotRow] = this.vehicleTypeObj["gvrmVehDesc"];
      console.log("this.vehicleTypeObj ss",this.createLotDetailList)
    }
  div_show8(){
      this.my_class9='overlay1';
    }
  div_hide8(){
      this.my_class9='overlay';
    }
  div_show9(){
      this.my_class10='overlay1';
    }
  div_hide9(){
      this.my_class10='overlay';
    }
  createFarmerObj:any;
  div_show10(){
      this.my_class11='overlay1';
      this.createFarmerObj = {
      "farmerFullName":"",
      "farmerAdd1":"",
      "farmerAdd2":"",
      "farmerStateId":"",
      "farmerDistId":"",
      "farmerTehsilId":"",
      "farmerContactNo":"",
      "farmerKycIdProofType":"",
      "farmerIdProofId":"",
      "farmerSonOf":"",
      "cityId":"",
      "relationId":""
      }
      console.log("this.createFarmerObj",this.createFarmerObj)
    }
  div_hide10(){
      this.my_class11='overlay';
      this.defaultChecked = true;
    }
  div_show12(){
      this.my_class12='overlay1';
    }
  div_hide12(){
      this.my_class12='overlay';
    }
  div_show13(){
      this.my_class13='overlay1';
    }
  div_hide13(){
      this.my_class13='overlay';
    }
  div_show14(){
      this.my_class14='overlay1';
    }
  div_hide14(){
      this.my_class14='overlay';
    }
  div_show15(){
      this.my_class15='overlay1';
    }
  div_hide15(){
      this.my_class15='overlay';
    }
  div_show16(){
      this.my_class16='overlay1';
    }
  div_hide16(){
      this.my_class16='overlay';
    }
  div_show17(){
      this.my_class17='overlay1';
      this.getInfoForGate();
    }

  selGateOK(){
      this.my_class17='overlay';
      this.selectedGate = -1;
      this.selGate =   this.valGate;
      this.gateNo = this.selGate["ggmGateNo"];
      console.log("sel gate",this.selGate)
      this.matchGateNo = "";
    }

  div_show18(){
      this.my_class18='overlay1';
      this.getInfoForYard();
    }
  selYardOK(){
      this.my_class18='overlay';
      this.selectedYard = -1;
      this.selYard =   this.valYard;
      console.log("sel yard",this.selYard)
      this.matchYardCode = this.matchYardDesc = "";
    }
    div_hide18(){
      this.my_class18='overlay';
      this.selectedYard = -1;
      this.matchYardCode = this.matchYardDesc = "";
    }
    div_hide17(){
      this.my_class17='overlay';
      this.selectedGate = -1;
      this.matchGateNo = "";
    }

regVehicles=[];
filRteredegVehicles=[];
onRegistered(){
  this.regType = "R";
  this.my_class29='overlay1';
  this.loading = true;
  this.authservice.getRegVehicles().subscribe($data=>{
    console.log("Vehicles : ",$data);
    if($data.listData != undefined){
      this.regVehicles = $data.listData;
      this.filRteredegVehicles = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });

}
onUnRegistered(){
  this.regType = "U";
  for(var i=0;i<this.createLotDetailList.length;i++){
    this.createLotDetailList[i].regiVechObj = "" ;
    this.createLotDetailList[i].vechRegType = this.regType ;
    this.vehicleno[i] = "";
    this.gvrmVehTypeCd[i] = "";
  }
  this.vehRegNo =   "";
  this.vehContact =   "";
  this.vehOwnerName =   "";
  this.vehAmount =   "";
  this.finalRegVehVal =[];
  this.traderDis = false;
}
onNormal(){
  this.lotType = "N";
}
onTemporary(){
  this.lotType = "Y";
}
onReset(){
  this.frmFullName = [];
  this.frmContactNo = [];
  this.gmsmStateDesc = [];
  this.choosenDist = "";
  this.gmtmTashilDesc = "";
  this.gmcmCity = [];
  this.gmpmgProdName = [];
  this.gmptm_desc = [];
  this.noOfBags = [];
  this.gvrmVehTypeCd = [];
  this.vehicleno = [];
  this.choosenDist  = "";
  this.frmsonfo =[];
  this.distPerticularName =[];
  this.tehsilPerticularName=[];
  this.approxQty = [];
  this.lcamAgenCompName = [];
  this.lcamFullName  = [];
}
onResetLotData(index){
  this.frmFullName[index] = "";
  this.frmContactNo[index] = "";
  this.gmsmStateDesc[index] = "";
  this.choosenDist = "";
  this.gmtmTashilDesc = "";
  this.gmcmCity[index] = "";
  this.gmpmgProdName[index] = "";
  this.gmptm_desc[index] = "";
  this.noOfBags[index] = "";
  if(this.finalRegVehVal != undefined && this.finalRegVehVal.length != 0){
       this.vehicleno[index] = this.finalRegVehVal.ivrtVehRegNo;
       this.gvrmVehTypeCd[index] = this.finalRegVehVal.gvrmVehTypeCd;
  }
  else{
    this.gvrmVehTypeCd[index] = "";
    this.vehicleno[index] = "";
  }
  this.choosenDist  = "";
  this.frmsonfo[index] ="";
  this.distPerticularName[index] ="";
  this.tehsilPerticularName[index]="";
  this.approxQty[index] = "";
  this.lcamAgenCompName[index] = "";
  this.lcamFullName[index]  = "";
}
selVehicle:number;
vehRegNo:string;
vehContact:string;
vehOwnerName:string;
vehAmount:string;
regVehVal:any;
onVehicleSelect(vehVal,index){
  this.selVehicle = index;
  console.log("post",vehVal)
  this.regVehVal = vehVal;
}
traderDis:boolean;
finalRegVehVal:any;
selRegVehicle(){
  this.my_class29='overlay';
  this.finalRegVehVal = this.regVehVal;
  for(var i=0;i<this.createLotDetailList.length;i++){
    this.createLotDetailList[i].regiVechObj = this.finalRegVehVal ;
    this.createLotDetailList[i].vechRegType = this.regType ;
    this.vehicleno[i] = this.finalRegVehVal.ivrtVehRegNo;
    this.gvrmVehTypeCd[i] = this.finalRegVehVal.gvrmVehTypeCd;
  }

  this.vehRegNo =   this.regVehVal.ivrtDocno;
  this.vehContact =   this.regVehVal.ivrtMobileNo;
  this.vehOwnerName =   this.regVehVal.ivrttOwnerName;
  this.vehAmount =   this.regVehVal.ivrtVehFee;

  console.log("this.gvrmVehTypeCd",this.gvrmVehTypeCd,this.regVehVal)
  console.log("this.createLotDetailList",this.createLotDetailList)
  this.shown13=false;
  this.shown14=true;

  this.traderDis = true;
}
div_hide29(){
  this.my_class29='overlay';
  this.regType = 'U';
  this.filtRegNo = null;
  this.filtVehNo = "";
}

// Add Lot Here
tableLength=[];
onAddLot(){
      var rowObj =  {
        "framerType": "F",
        "farmerObj": "",
        "traderList" :"",
        "commodityObj": "",
        "bagTypeObj": "",
        "noBag": "",
        "approxQty" :"",
        "vechileNo":"",
        "vechRegType": "" ,
        "vechileObj" :"",
        "lotType": "",
        "caObj":"",
        "gateObj":"",
        "regiVechObj":""
    };
    this.createLotDetailList.push(rowObj);
    this.sellerType[this.createLotDetailList.length - 1] = 'F';
     console.log("tableLength",this.tableLength)
     console.log("this.finalRegVehVal",this.finalRegVehVal)
     if(this.finalRegVehVal != undefined && this.finalRegVehVal.length != 0){
          this.createLotDetailList[this.createLotDetailList.length - 1].regiVechObj = this.finalRegVehVal ;
          this.createLotDetailList[this.createLotDetailList.length - 1].vechRegType = this.regType ;
          this.vehicleno[this.createLotDetailList.length - 1] = this.finalRegVehVal.ivrtVehRegNo;
          this.gvrmVehTypeCd[this.createLotDetailList.length - 1] = this.finalRegVehVal.gvrmVehTypeCd;
     }

    //  this.finalRegVehVal = this.regVehVal;
    //  for(var i=0;i<this.createLotDetailList.length;i++){
    //    this.createLotDetailList[i].regiVechObj = this.finalRegVehVal ;
    //    this.createLotDetailList[i].vechRegType = this.regType ;
    //    this.vehicleno[i] = this.finalRegVehVal.ivrtVehRegNo;
    //    this.gvrmVehTypeCd[i] = this.finalRegVehVal.gvrmVehTypeCd;
    //  }
     console.log("createLotDetailList",this.createLotDetailList)
   }

   // on select row
   selectedLotRow:number;
   onSelectLotRow(index){
     console.log("index",index)
     this.selectedLotRow = index;
   }

   // on delete row
   onDeleteLot(){
    console.log("vv",this.selectedLotRow.toString())
    for(var i =0;i< this.createLotDetailList.length; i++){
   	if (i === this.selectedLotRow){
       this.createLotDetailList.splice(i, 1);
       break;
     }
   }
   this.selectedLotRow = -1;
   console.log("createLotDetailList",this.createLotDetailList)

   }


// Relation Change
relationVal =[];
onRelationChange(post,index,value){
  console.log("dd",post,index,value)
  if(this.createLotDetailList[index].framerType == 'F'){
    if(this.createLotDetailList[index].farmerObj != ""){
      this.createLotDetailList[index].farmerObj["frmRelationId"] = value;
    }
  }
  else{

  }

}

firmName=[];
selTraderOK(){

    this.createLotDetailList[this.selectedLotRow].traderList = this.selTraderVal;
    this.createLotDetailList[this.selectedLotRow].farmerObj = undefined;
    console.log("selected row is : ",this.selTraderVal);
    console.log("this.createLotDetailList: ",this.createLotDetailList);


    this.frmFullName[this.selectedLotRow]=this.selTraderVal.lcam_full_name;
    this.frmContactNo[this.selectedLotRow]=this.selTraderVal.lcam_agen_contno;
    this.firmName[this.selectedLotRow]=this.selTraderVal.lcam_agen_comp_name;
    this.loading = true;
    this.authservice.getAllStateName().subscribe($allStatename=>{
    console.log("All state name: ",$allStatename.listData);
    this.allStateName=$allStatename.listData;
    this.filteredData2=this.allStateName;
    for(var i=0;i<this.allStateName.length;i++){
      if(this.allStateName[i].gmsmStateId == this.selTraderVal.lcam_agen_state_id){
        this.gmsmStateDesc[this.selectedLotRow] = this.allStateName[i].gmsmStateDesc;
        this.stateId[this.selectedLotRow] = this.allStateName[i]["gmsmStateId"];
        break;
      }
    }
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
);

    // this.getDistrictList(this.selTraderVal.frmStateId);
    this.loading = true;
    this.authservice.getAllDistrictName(this.selTraderVal.lcam_agen_state_id).subscribe($allDist=>{
      console.log("All district name: ",$allDist);
      this.allDIstData=$allDist.listData;
      this.filteredData3=this.allDIstData;
      for(var i=0;i<this.allDIstData.length;i++){
        if(this.allDIstData[i].gmdmDistId == this.selTraderVal.lcam_agen_dist_id){
          this.distPerticularName[this.selectedLotRow] = this.allDIstData[i].gmdmDistDesc;
          this.distId[this.selectedLotRow] = this.allDIstData[i]["gmdmDistId"];
          break;
        }
      }
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  )
  if(this.selTraderVal.lcam_agen_thasil_id != undefined){
    this.loading = true;
    this.authservice.getAllTehsilName(this.selTraderVal.lcam_agen_dist_id).subscribe($alltehsil=>{
        console.log("all tehsil name: ",$alltehsil);
        this.allTehsilData=$alltehsil.listData;
        this.filteredData4=this.allTehsilData;
        for(var i=0;i<this.allTehsilData.length;i++){
          if(this.allTehsilData[i].gmtmTahsilId == this.selTraderVal.lcam_agen_thasil_id){
            this.tehsilPerticularName[this.selectedLotRow] = this.allTehsilData[i].gmtmTashilDesc;
            this.tehsilId[this.selectedLotRow] = this.allTehsilData[i].gmtmTahsilId;
            break;
          }
        }
        this.loading = false;
       }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
  );
  }

  if(this.selTraderVal.gmcmCity != undefined){
    this.gmcmCity[this.selectedLotRow] = this.selTraderVal.gmcmCity;
    this.villId[this.selectedLotRow] = this.selTraderVal.gmcmCityId;
  }
  else{
    this.gmcmCity[this.selectedLotRow] = "";
    this.villId[this.selectedLotRow] = "";
  }

    console.log("this.allDIstData",this.allDIstData["listData"],this.allDIstData)
    console.log("think city",this.gmcmCity)
    console.log("think approxQty",this.approxQty)

    this.my_class33='overlay';
    this.filteredData1 =[];
    this.matchTrader ="";
    this.selectedRow = -1;
}

// Create Farmer Section Starts Here
// State for farmer creation
faremerStateList =[];filteredFarmerStateList =[];
showFarmerStatePopUp(){
  this.my_class34 = "overlay1";
  this.loading = true;
  this.authservice.getAllStateName().subscribe($allStatename=>{
    this.faremerStateList=$allStatename.listData;
    this.filteredFarmerStateList=this.faremerStateList;
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
);
}
hideFarmerStatePopUp(){
  this.my_class34 = "overlay";
}
selFStateVal =[];selFStateIndex:number;
selFState(post,index){
  this.selFStateVal = post;
  this.selFStateIndex = index;
}

selFStateValOK =[];
onSelFStateOK(){
  this.selFStateValOK = this.selFStateVal;
  this.FStateName = this.selFStateValOK["gmsmStateDesc"];
  this.createFarmerObj.farmerStateId = this.selFStateValOK["gmsmStateId"]
  console.log("this.createFarmerObj",this.createFarmerObj)

  this.selFDistValOK = [];
  this.FDistName = "";
  this.createFarmerObj.farmerDistId = "";

  this.selFTehsilValOK = [];
  this.FTehsilName = "";
  this.createFarmerObj.farmerTehsilId = "";

  this.selFVillageValOK = [];
  this.FVillageName = "";
  this.createFarmerObj.cityId = ""

}
// District for farmer creation
faremerDistList =[];filteredFarmerDistList =[];
showFarmerDistPopUp(){
  console.log("this.gmsmStateId",this.selFStateValOK["gmsmStateId"])
        if(this.selFStateValOK["gmsmStateId"] != undefined){
          this.my_class35='overlay1';
          this.loading = true;
          this.authservice.getAllDistrictName(this.selFStateValOK["gmsmStateId"]).subscribe($allDist=>{
            this.faremerDistList=$allDist.listData;
            this.filteredFarmerDistList=this.faremerDistList;
            this.loading = false;
          }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                    setTimeout(()=>{
                    this.my_class50 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
                  }
            }  )
        }
        else{
          this.my_class36 = 'overlay1';
          setTimeout(()=>{
            this.my_class36 = 'overlay';
          },2000);
        }
}
hideFarmerDistPopUp(){
  this.my_class35 = "overlay";
}
selFDistVal =[];selFDistIndex:number;
selFDist(post,index){
  this.selFDistVal = post;
  this.selFDistIndex = index;
}

selFDistValOK =[];
onSelFDistOK(){
  this.selFDistValOK = this.selFDistVal;
  this.FDistName = this.selFDistValOK["gmdmDistDesc"];
  this.createFarmerObj.farmerDistId = this.selFDistValOK["gmdmDistId"]
  console.log("this.createFarmerObj",this.createFarmerObj)

  this.selFTehsilValOK = [];
  this.FTehsilName = "";
  this.createFarmerObj.farmerTehsilId = "";

  this.selFVillageValOK = [];
  this.FVillageName = "";
  this.createFarmerObj.cityId = ""
}
// Tehsil for farmer creation
faremerTehsilList =[];filteredFarmerTehsilList =[];
showFarmerTehsilPopUp(){
  var  distId = this.selFDistValOK["gmdmDistId"]
  if(distId != undefined){
    this.my_class37='overlay1';
    this.loading = true;
    this.authservice.getAllTehsilName(distId).subscribe($alltehsil=>{
        console.log("all tehsil name: ",$alltehsil);
        this.faremerTehsilList=$alltehsil.listData;
        this.filteredFarmerTehsilList=this.faremerTehsilList;
        this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class50 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
            }
      }
  );
  }
  else{
    this.my_class38 = 'overlay1';
    setTimeout(()=>{
      this.my_class38 = 'overlay';
    },2000);
  }
}
hideFarmerTehsilPopUp(){
  this.my_class37 = "overlay";
}
selFTehsilVal =[];selFTehsilIndex:number;
selFTehsil(post,index){
  this.selFTehsilVal = post;
  this.selFTehsilIndex = index;
}

selFTehsilValOK =[];
onSelFTehsilOK(){
  this.selFTehsilValOK = this.selFTehsilVal;
  this.FTehsilName = this.selFTehsilValOK["gmtmTashilDesc"];
  this.createFarmerObj.farmerTehsilId = this.selFTehsilValOK["gmtmTahsilId"]
  console.log("this.createFarmerObj",this.createFarmerObj)

  this.selFVillageValOK = [];
  this.FVillageName = "";
  this.createFarmerObj.cityId = ""
}
// Village for farmer creation
farmerVillageList =[];filteredFarmerVillageList =[];
showFarmerVillagePopUp(){
  var  distId = this.selFDistValOK["gmdmDistId"]
  var  tehsilId = this.selFTehsilValOK["gmtmTahsilId"]

  if(distId!= undefined && distId != "" && tehsilId != undefined && tehsilId != ""){
    this.my_class39='overlay1';
    this.loading = true;
    this.authservice.getAllVillageName(distId,tehsilId).subscribe($allVillName=>{
      console.log("All village name",$allVillName);
      this.farmerVillageList=$allVillName.listData;
      this.filteredFarmerVillageList=this.farmerVillageList;
      this.loading = false;
    }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
      }
  )
  }
  else{
    this.my_class40 = 'overlay1';
    setTimeout(()=>{
      this.my_class40 = 'overlay';
    },2000);
  }

}
hideFarmerVillagePopUp(){
  this.my_class39 = "overlay";
}
selFVillageVal =[];selFVillageIndex:number;
selFVillage(post,index){
  this.selFVillageVal = post;
  this.selFVillageIndex = index;
}

selFVillageValOK =[];
onSelFVillageOK(){
  this.selFVillageValOK = this.selFVillageVal;
  this.FVillageName = this.selFVillageValOK["gmcmCity"];
  this.createFarmerObj.cityId = this.selFVillageValOK["gmcmCityId"]
  console.log("this.createFarmerObj",this.createFarmerObj)
}

resetCreateFarmer(){
  this.createFarmerObj = this.selFVillageValOK = this.selFTehsilValOK  = this.selFDistValOK  = this.selFStateValOK = [];
  this.FFullName = this.FAddrs1 = this.FAddrs2 = this.FStateName = this.FDistName = this.FTehsilName  = this.FVillageName
  = this.FMobileNo = this.FIdProofNo = this.frmsonOf = "";
  this.selFDistIndex = this.selFStateIndex = this.selFTehsilIndex = this.selFVillageIndex = -1;
  this.defaultChecked = true;
}

onGateNoFilter(){
  if(this.matchGateNo != ""){
    var info = _.filter(this.gateInfo, (a)=>a.ggmGateNo.indexOf(this.matchGateNo)>=0);
    this.filteredGateInfo = info;
  }
  else{
  this.filteredGateInfo = this.gateInfo;
  }
}
onGateNameFilter(){
  if(this.matchGateName != ""){
    var info = _.filter(this.gateInfo, (a)=>a.ggmName.indexOf(this.matchGateName)>=0);
    this.filteredGateInfo = info;
  }
  else{
  this.filteredGateInfo = this.gateInfo;
  }
}
matchYardCode:string;
onYardCodeFilter(){
  this.matchYardCode = this.matchYardCode.toUpperCase();
  if(this.matchYardCode != ""){
    var info = _.filter(this.yardInfo, (a)=>a.gmtmMktCode.indexOf(this.matchYardCode)>=0);
    this.filteredYardInfo = info;
  }
  else{
  this.filteredYardInfo = this.yardInfo;
  }
}
matchYardDesc:string;
onYardDescFilter(){
  this.matchYardDesc = this.matchYardDesc.toUpperCase();
  if(this.matchYardDesc != ""){
    var info = _.filter(this.yardInfo, (a)=>a.gmtmMktDesc.indexOf(this.matchYardDesc)>=0);
    this.filteredYardInfo = info;
  }
  else{
  this.filteredYardInfo = this.yardInfo;
  }
}

// FIlter reg no
filtRegNo:string;
onFilterRegNo(){
  if(this.filtRegNo != null){
    var info = _.filter(this.regVehicles, (a)=>a.ivrtDocno.indexOf(this.filtRegNo)>=0);
    this.filRteredegVehicles = info;
  }
  else{
  this.filRteredegVehicles = this.regVehicles;
  }
}
// filter vehicle no
filtVehNo:string;
onFilterVehNo(){
  if(this.filtVehNo != ""){
    var info = _.filter(this.regVehicles, (a)=>a.ivrtVehRegNo.toUpperCase().indexOf(this.filtVehNo.toUpperCase())>=0);
    this.filRteredegVehicles = info;
  }
  else{
  this.filRteredegVehicles = this.regVehicles;
  }
}

// reset table data
resetTableData(){
  this.createLotDetailList =[];
  this.sellerType =[];
  this.frmFullName =[];
  this.firmName=[];
  this.relationVal=[];
  this.frmsonfo=[];
  this.frmContactNo=[];
  this.gmsmStateDesc=[];
  this.distPerticularName=[];
  this.tehsilPerticularName=[];
  this.gmcmCity=[];
  this.gmpmgProdName=[];
  this.gmptm_desc=[];
  this.noOfBags=[];
  this.approxQty=[];
  this.gvrmVehTypeCd=[];
  this.vehicleno=[];
  this.lcamAgenCompName=[];
  this.lcamFullName=[];
  this.sellerType[0] = "F";
}
}
