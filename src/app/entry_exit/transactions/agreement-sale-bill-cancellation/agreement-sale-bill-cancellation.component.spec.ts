import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementSaleBillCancellationComponent } from './agreement-sale-bill-cancellation.component';

describe('AgreementSaleBillCancellationComponent', () => {
  let component: AgreementSaleBillCancellationComponent;
  let fixture: ComponentFixture<AgreementSaleBillCancellationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreementSaleBillCancellationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementSaleBillCancellationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
