import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { FormsModule, FormGroup }   from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'app-agreement-sale-bill-cancellation',
  templateUrl: './agreement-sale-bill-cancellation.component.html',
  styleUrls: ['./agreement-sale-bill-cancellation.component.css'],
  providers :[DatePipe]
})
export class AgreementSaleBillCancellationComponent implements OnInit {
  public shown1:any=true;
  public shown2:any=false;
  saleBillFrmDate:any;saleBillToDate:any;saleAgrFrmDate:any;saleAgrToDate:any;
  public my_class1='overlay';public my_class2='overlay';
  public loading = false;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
    private datepipe:DatePipe  ) { }

  ngOnInit() {
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  errorPopupShow(){
    this.my_class1 = 'overlay1';
  }
  onSelecttypeCancellation(search){
    console.log("type cancellation: ",search);
    if(search=="AC")
    {
      this.onResetTab1();
      this.shown1=false;
      this.shown2=true;
    }
    else if(search=="BC"){
      this.onResetTab2();
      this.shown1=true;
      this.shown2=false;
    }
  }

  // On Sale Bill Search
  saleBillList=[];modSaleBillList=[];
  onSaleBillSearch(){
    if((this.saleBillFrmDate != undefined && this.saleBillToDate != undefined) && this.saleBillFrmDate != "" && this.saleBillToDate != ""){
      var dateObj = {"fromDate": this.saleBillFrmDate.formatted, "toDate": this.saleBillToDate.formatted}
      this.loading = true;
      this.authservice.getSaleBillList(dateObj).subscribe(saleBill=>{
        console.log("$saleBill",saleBill)
        if(saleBill.listData != undefined){
          this.saleBillList = saleBill.listData;
          this.modSaleBillList = saleBill.listData;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
            });
    }
    else{
      this.onResetTab1();
      this.flashMessagesService.show('Both Dates Are Mandatory To Search', { cssClass: 'alert-danger', timeout: 3000 });
    }
  }
  // On Sale Agreement Search
  saleAgrList=[];
  onSaleAgrSearch(){
    if((this.saleAgrFrmDate != undefined && this.saleAgrToDate != undefined) && this.saleAgrFrmDate != "" && this.saleAgrToDate != ""){
      var dateObj = {"fromDate": this.saleAgrFrmDate.formatted, "toDate": this.saleAgrToDate.formatted}
      this.loading = true;
      this.authservice.getSaleAgrList(dateObj).subscribe(saleAgr=>{
        console.log("$saleAgr",saleAgr)
        if(saleAgr.listData != undefined){
          this.saleAgrList = saleAgr.listData;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
            });
    }
    else{
      this.onResetTab2();
      this.flashMessagesService.show('Both Dates Are Mandatory To Search', { cssClass: 'alert-danger', timeout: 3000 });
    }
  }

// Check sale Bill List To cancel
cancelSaleBillList=[];
isPresent:boolean;
onCheckSaleBillList(post,index,isChecked){
  console.log("list",post,index)
  this.isPresent = false;
  if(isChecked == true){
    for(var i = 0; i < this.cancelSaleBillList.length; i++) {
      if (this.cancelSaleBillList[i].auchLotId == post.auchLotId) {
        this.isPresent = true;
        break;
      }
    }
    if(this.isPresent == false){
     this.cancelSaleBillList.push(post);
    }
  }
  else{
    for(var i = 0; i < this.cancelSaleBillList.length; i++) {
      if (this.cancelSaleBillList[i].auchLotId == post.auchLotId) {
        this.cancelSaleBillList.splice(i,1);
        break;
      }
    }
  }
  console.log("final Lists",this.cancelSaleBillList)
}

// On Remarks
onRemarks(post,remarks){
  if(remarks != undefined && remarks !=""){
    for(var i = 0; i < this.modSaleBillList.length; i++) {
      if (this.modSaleBillList[i].auchLotId == post.auchLotId) {
        this.modSaleBillList[i]["auchRemarks"] = remarks;
      }
    }
  }
  console.log("final Lists after remark",this.modSaleBillList)
}

// Save Sale Bill cancellation
saleBillSavedRes:any;
onSaveSaleBillCancellation(){
  for(var i = 0; i < this.cancelSaleBillList.length; i++) {
    for(var j = 0; j < this.modSaleBillList.length; j++) {
      if (this.cancelSaleBillList[i].auchLotId == this.modSaleBillList[j].auchLotId) {
        if(this.modSaleBillList[j].auchRemarks != undefined){
          this.cancelSaleBillList[i].auchRemarks = this.modSaleBillList[j].auchRemarks
        }
      }
    }
  }
  console.log("save json",this.cancelSaleBillList)
  this.loading = true;
  this.authservice.saveSaleBillCancellation(this.cancelSaleBillList).subscribe(cancelSaleBill=>{
    this.loading = false;
    console.log("$saleBill",cancelSaleBill)
    this.saleBillSavedRes = cancelSaleBill;
    this.my_class2 = 'overlay1';
    setTimeout(()=>{
      this.my_class2 = 'overlay';
    },2000);
    this.onSaleBillSearch();
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

// Reset For Tab 1
onResetTab1(){
  this.saleBillFrmDate = this.saleBillToDate = "";
  this.saleBillList = this.cancelSaleBillList = this.modSaleBillList = this.saleBillSavedRes = [];
}
// Reset For Tab 2
onResetTab2(){
  this.saleAgrToDate = this.saleAgrFrmDate = "";
  this.saleAgrList = this.cancelSaleAgrList =this.saleBillSavedRes = [];
}


// Check sale Bill List To cancel
cancelSaleAgrList=[];
isPresentTab2:boolean;
onCheckSaleAgrList(post,index,isChecked){
  console.log("list",post,index)
  this.isPresentTab2 = false;
  if(isChecked == true){
    for(var i = 0; i < this.cancelSaleAgrList.length; i++) {
      if (this.cancelSaleAgrList[i].auchLotId == post.auchLotId) {
        this.isPresentTab2 = true;
        break;
      }
    }
    if(this.isPresentTab2 == false){
     this.cancelSaleAgrList.push(post);
    }
  }
  else{
    for(var i = 0; i < this.cancelSaleAgrList.length; i++) {
      if (this.cancelSaleAgrList[i].auchLotId == post.auchLotId) {
        this.cancelSaleAgrList.splice(i,1);
        break;
      }
    }
  }
  console.log("final Lists agr",this.cancelSaleAgrList)
}


// Save Sale Agreement cancellation
saleAgrSavedRes:any;
onSaveSaleAgrCancellation(){
  console.log("save json",this.cancelSaleAgrList)
  this.loading = true;
  this.authservice.saveSaleAgrCancellation(this.cancelSaleAgrList).subscribe(cancelSaleAgr=>{
    this.loading = false;
    console.log("$saleAgr",cancelSaleAgr)
    this.saleAgrSavedRes = cancelSaleAgr;
    this.my_class2 = 'overlay1';
    setTimeout(()=>{
      this.my_class2 = 'overlay';
    },2000);
    this.onSaleAgrSearch();
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}
}
