import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as _ from 'lodash';

@Component({
  selector: 'app-gate-entry-new',
  templateUrl: './gate-entry-new.component.html',
  styleUrls: ['./gate-entry-new.component.css']
})
export class GateEntryNewComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';
  public my_class9='overlay';
  public my_class10='overlay';
  public my_class11='overlay';
  public my_class12='overlay';
  public my_class13='overlay';
  public my_class14='overlay';
  public my_class15='overlay';
  public my_class16='overlay';
  public my_class17='overlay';
  public my_class18='overlay';

  public shown=true;
  public shown1=false;
  public shown2=false;
  public shown3=true;
  public shown4=false;
  public shown5=false;
  public shown6=true;
  public shown7=false;
  public shown8=false;
  public shown9=true;
  public shown10=false;
  public shown11=true;
  public shown12=false;
  public shown13=true;
  public shown14=false;
  public shown15=true;
  public shown16=false;
  public shown17=true;
  public shown18=false;
  public shown19=true;
  public shown20=false;
  public shown21=false;
  public shown22=true;
  public shown23=false;
  public shown24=false;
  public shown25=false;
  public shown26=true;
  public shown27=false;
  public shown28=false;

  public shown29:any=true;
  public shown30:any=false;
  public shown31:any=true;
  public shown32:any=false;
  public shown33:any=true;
  public shown34:any=false;

  sellerNameData=[];
  selectedRow:any;
  selectedDistrict:any;
  farmOrgId:any;
  farmRegIId:any;
  frmCityId:any;
  frmContactNo:any;
  frmDistId:any;
  frmFullName:any;
  frmOprId:any;
  frmOprType:any;
  frmRegidt:any;
  frmRelationId:any;
  frmStateId:any;
  frmTahsilId:any;
  frmdefunct:any;
  frmsonfo:any;
  gmcmCity:any;
  gmcmCityId:any;
  flag:any;
  statePerticularName:any;
  distPerticularName:any;
  tehsilPerticularName:any;
  allDIstData=[];
  allTehsilData=[];
  allVillageData=[];
  allCommodityData=[];
  allBagTypedata=[];
  vehicleData=[];
  allCAData=[];
  allStateName=[];
  filteredData1=[];
  filteredData2=[];
  filteredData3=[];
  filteredData4=[];
  filteredData5=[];
  filteredData6=[];
  filteredData7=[];
  filteredData8=[];
  filteredData9=[];
  choosenDist:any;
  choosenDistId:any;
  selectedTehsil:any;
  gmtmTahsilId:any;
  gmtmTashilDesc:any;
  selectedVillage:any;
  selectedCommodity:any;
  gmpmProdId:any;
  gmpmgProdName:any;
  gmpmDefunct:any;
  gmpmMarketId:any;
  gmpmOprId:any;
  gmpmOrgId:any;
  gmpmQtyUom:any;
  gmpmgProdCode:any;
  gmpmgQtyUom:any;
  selectedBag:any;
  gmptm_desc:any;
  gmptm_defunct:any;
  gmptm_oprid:any;
  gmptm_orgid:any;
  gmptm_packid:any;
  gmptm_qty:any;
  gmptm_qty_uom:any;
  noOfBags:any;
  sellerType:any;
  approxQty:any;
  gvrmDefunct:any;
  gvrmOprId:any;
  gvrmOrgId:any;
  gvrmTrnId:any;
  gvrmVehRateAnnual:any;
  gvrmVehRateDaily:any;
  gvrmVehRateMonthly:any;
  gvrmVehTypeCd:any;
  selectedVehicle:any;
  vehicleno:any;
  //CA list
  lcamAadharNo:any;
  lcamAgenAdd1:any;
  lcamAgenAdd2:any;
  lcamAgenCatgCd:any;
  lcamAgenCatgDesc:any;
  lcamAgenCompName:any;
  lcamAgenCompRegino:any;
  lcamAgenContno:any;
  lcamAgenDistId:any;
  lcamAgenEmailid:any;
  lcamAgenFaxno:any;
  lcamAgenGendStat:any;
  lcamAgenLicenNo:any;
  lcamAgenPincd:any;
  lcamAgenProdCd:any;
  lcamAgenProdDesc:any;
  lcamAgenStateId:any;
  lcamAgenThasilId:any;
  lcamAgenTradLicenno:any;
  lcamAgenType:any;
  lcamBankAccount:any;
  lcamBankName:any;
  lcamDefunct:any;
  lcamFactorBidLimit:any;
  lcamFullName:any;
  lcamIfscCode:any;
  lcamOprid:any;
  lcamOrgid:any;
  lcamPanNo:any;
  lcamRemark:any;
  lcamSecurityDeposit:any;
  lcamTrnId:any;
  message:any;
  selectedCA:any;
  farmerNameUpper:any;
  selectedState:any;
  gmsmDefunct:any;
  gmsmOrgid:any;
  gmsmStateCd:any;
  gmsmStateDesc:any;
  gmsmStateId:any;
  //TraderList
  lcam_agen_add1:any;
  lcam_agen_comp_name:any;
  lcam_agen_contno:any;
  lcam_agen_dist_id:any;
  lcam_agen_gend_stat:any;
  lcam_agen_licen_no:any;
  lcam_agen_pincd:any;
  lcam_agen_state_id:any;
  lcam_agen_thasil_id:any;
  lcam_agen_trad_licenno:any;
  lcam_agen_type:any;
  lcam_defunct:any;
  lcam_full_name:any;
  lcam_oprid:any;
  lcam_orgid:any;
  lcam_trn_id:any;
  selectedFarmerState:any;
  FStateName:any;
  FStateId:any;
  selectedFarmerDistrict:any;
  FDistName:any;
  FDistId:any;
  selectedFarmerTehsil:any;
  FTehsilId:any;
  FTehsilName:any;
  selectedFarmerVillage:any;
  FVillageName:any;
  FVillageId:any;
  FFullName:any;
  FAddrs1:any;
  FAddrs2:any;
  proofValue:any;
  relationFValue:any;
  FMobileNo:any;
  FIdProofNo:any;
  frmsonOf:any;



  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {


  }

  onSelectSellertype(sellerType){
    console.log("Seller type is: ",sellerType);
    this.sellerType=sellerType;
    if(this.sellerType=='F'){
      this.shown23=true;
      this.shown26=false;
      this.shown27=true;
      this.shown28=false;
      this.shown25=false;
      this.shown22=false;
      this.shown24=false;
    }
    else if(this.sellerType=='T'){
      this.shown24=true;
      this.shown26=false;
      this.shown27=false;
      this.shown28=true;
      this.shown25=true;
      this.shown23=false;
      this.shown22=false;
    }
    else if(this.sellerType==''){
      this.shown22=true;
      this.shown26=true;
      this.shown25=false;
      this.shown23=false;
      this.shown24=false;
    }
  }

  // Select seller name
  selectSellerName(){
    this.authservice.selectSellerName().subscribe($data=>{
      console.log("Seller names table: ",$data);
      this.sellerNameData=$data.listData;
      this.filteredData1=this.sellerNameData;
    })
  }

  //select Trader Name
  selectTraderName(){
    console.log("Trader type selected");
    this.authservice.getTraderList().subscribe($allTraderData=>{
      console.log("Trader List data: ",$allTraderData.listData);
      this.sellerNameData=$allTraderData.listData;
      this.filteredData1=this.sellerNameData;
    });
  }

  setSellerName(post,index){
      this.shown=false;
      this.shown1=true;
      this.shown3=false;
      this.shown4=true;
      this.shown6=false;
      this.shown7=true;


      console.log("selected row is : ",post);

      this.selectedRow=index;

      this.farmOrgId=post.farmOrgId;
      this.farmRegIId=post.farmRegIId;
      this.frmCityId=post.frmCityId;
      this.frmContactNo=post.frmContactNo;
      this.frmDistId=post.frmDistId;
      this.frmFullName=post.frmFullName;
      this.frmOprId=post.frmOprId;
      this.frmOprType=post.frmOprType;
      this.frmRegidt=post.frmRegidt;
      this.frmRelationId=post.frmRelationId;
      this.frmStateId=post.frmStateId;
      this.frmTahsilId=post.frmTahsilId;
      this.frmdefunct=post.frmdefunct;
      this.frmsonfo=post.frmsonfo;
      this.gmcmCity=post.gmcmCity;

      if(this.frmRelationId=="FREL"){
        (<HTMLInputElement>document.getElementById('selectedSellerRel')).value = 'FO';
      }
      if(this.frmRelationId=="WREL"){
        (<HTMLInputElement>document.getElementById('selectedSellerRel')).value = 'WO';
      }
      if(this.frmRelationId=="DREL"){
        (<HTMLInputElement>document.getElementById('selectedSellerRel')).value = 'DO';
      }
      if(this.frmRelationId=="SREL"){
        (<HTMLInputElement>document.getElementById('selectedSellerRel')).value = 'SO';
      }


      // Getting the stateName
      if(this.sellerType=='T'){

        //Trader objects
        this.lcam_agen_add1=post.lcam_agen_add1;
        this.lcam_agen_comp_name=post.lcam_agen_comp_name;
        this.lcam_agen_contno=post.lcam_agen_contno;
        this.lcam_agen_dist_id=post.lcam_agen_dist_id;
        this.lcam_agen_gend_stat=post.lcam_agen_gend_stat;
        this.lcam_agen_licen_no=post.lcam_agen_licen_no;
        this.lcam_agen_pincd=post.lcam_agen_pincd;
        this.lcam_agen_state_id=post.lcam_agen_state_id;
        this.lcam_agen_thasil_id=post.lcam_agen_thasil_id;
        this.lcam_agen_trad_licenno=post.lcam_agen_trad_licenno;
        this.lcam_agen_type=post.lcam_agen_type;
        this.lcam_defunct=post.lcam_defunct;
        this.lcam_full_name=post.lcam_full_name;
        this.lcam_oprid=post.lcam_oprid;
        this.lcam_orgid=post.lcam_orgid;
        this.lcam_trn_id=post.lcam_trn_id;


        this.frmFullName=this.lcam_full_name;
        this.frmContactNo=this.lcam_agen_contno;


            const stid=this.lcam_agen_state_id;
            this.authservice.getStateName(stid).subscribe($stataData=>{
              console.log("State Name is: ",$stataData);
              this.gmsmStateDesc=$stataData.listData[0].gmsmStateDesc;
            });
            const did=this.lcam_agen_dist_id;
            this.authservice.getDistrictName(stid,did).subscribe($districtName=>{

              console.log("District Name is: ",$districtName);
              this.distPerticularName=$districtName.listData[0].gmdmDistDesc;
            });
            const tid=this.lcam_agen_thasil_id;
            this.authservice.getTehsilName(did,tid).subscribe($tehsilname=>{
              console.log("Tehsil Name is: ",$tehsilname);
              this.tehsilPerticularName=$tehsilname.listData[0].gmtmTashilDesc;
            });
      }
      else{
              const stid=this.frmStateId;
              this.authservice.getStateName(stid).subscribe($stataData=>{
                console.log("State Name is: ",$stataData);
                this.gmsmStateDesc=$stataData.listData[0].gmsmStateDesc;
              });
              const did=this.frmDistId;
              this.authservice.getDistrictName(stid,did).subscribe($districtName=>{

                console.log("District Name is: ",$districtName);
                this.distPerticularName=$districtName.listData[0].gmdmDistDesc;
              });
              const tid=this.frmTahsilId;
              this.authservice.getTehsilName(did,tid).subscribe($tehsilname=>{
                console.log("Tehsil Name is: ",$tehsilname);
                this.tehsilPerticularName=$tehsilname.listData[0].gmtmTashilDesc;
              });

          }
      //this.flag=4;

  }

//State Parameters
onSelectState(){
  this.authservice.getAllStateName().subscribe($allStatename=>{
    console.log("All state name: ",$allStatename.listData);
    this.allStateName=$allStatename.listData;
    this.filteredData2=this.allStateName;
  });
}

stateName(post8:any,index8){
  this.shown19=false;
  this.shown20=false;
  this.shown21=true;
  this.selectedState=index8;
  this.gmsmDefunct=post8.gmsmDefunct;
  this.gmsmOrgid=post8.gmsmOrgid;
  this.gmsmStateCd=post8.gmsmStateCd;
  this.gmsmStateDesc=post8.gmsmStateDesc;
  this.gmsmStateId=post8.gmsmStateId;
  this.frmStateId=this.gmsmStateId;
}

//selected state in create farmer
selectedFState(post9:any,index9){

  this.selectedFarmerState=index9;
  this.FStateName=post9.gmsmStateDesc;
  this.FStateId=post9.gmsmStateId;
  this.gmsmStateId=this.FStateId;
}



//District Parameters
  onSelectDistrict(){

    this.authservice.getAllDistrictName(this.gmsmStateId).subscribe($allDist=>{
      console.log("All district name: ",$allDist);
      this.allDIstData=$allDist.listData;
      this.filteredData3=this.allDIstData;

    })
  }
  DistrictName(post:any,index){
    this.shown1=false;
    this.shown2=true;
    //console.log("selected post of district name is: ",post);
    this.selectedDistrict=index;
    this.choosenDist=post.gmdmDistDesc;
    this.choosenDistId=post.gmdmDistId;
    this.frmDistId=this.choosenDistId;
    //console.log("choosen district id: ",this.choosenDistId);
  }


 //Selected district in create farmer
  selectedFDistrict(post10:any,index10){
    this.selectedFarmerDistrict=index10;
    this.FDistName=post10.gmdmDistDesc;
    this.FDistId=post10.gmdmDistId;
    this.choosenDistId=this.FDistId;
  }




//Tehsil parametetrs
  onSelectTehsil(){
    //this.flag=5;
    this.authservice.getAllTehsilName(this.choosenDistId).subscribe($alltehsil=>{
        console.log("all tehsil name: ",$alltehsil);
        this.allTehsilData=$alltehsil.listData;
        this.filteredData4=this.allTehsilData;
    });
  }

  TehsilName(post2:any,index2){
    this.shown4=false;
    this.shown5=true;
    this.selectedTehsil=index2;
    //console.log("selected tehsil is: ",post2);
    this.gmtmTahsilId=post2.gmtmTahsilId;
    this.gmtmTashilDesc=post2.gmtmTashilDesc;
    this.frmTahsilId=this.gmtmTahsilId;

  }

  //Selected Tehsil Name in create farmer
  selectedFTehsil(post11:any,index11){

    this.selectedFarmerTehsil=index11;
    //console.log("selected tehsil is: ",post2);
    this.FTehsilId=post11.gmtmTahsilId;
    this.gmtmTahsilId=this.FTehsilId;
    this.FTehsilName=post11.gmtmTashilDesc;

  }


  //Village parametetrs
  onSelectVillage(){
    this.authservice.getAllVillageName(this.choosenDistId,this.gmtmTahsilId).subscribe($allVillName=>{
      console.log("All village name",$allVillName);
      this.allVillageData=$allVillName.listData;
      this.filteredData5=this.allVillageData;
    })
  }

  VillageName(post3:any,index3){
    this.shown7=false;
    this.shown8=true;
    this.selectedVillage=index3;
    //console.log("Selected village is : ",post3);
    this.gmcmCityId=post3.gmcmCityId;
    this.gmcmCity=post3.gmcmCity;
  }

  //Selected village name in Farmer create
  selectedFVillage(post12:any,index12){

    this.selectedFarmerVillage=index12;
    //console.log("Selected village is : ",post3);
    this.FVillageId=post12.gmcmCityId;
    this.FVillageName=post12.gmcmCity;
  }


  //commodity parameters
  getAllCommodity(){
    this.authservice.getAllCommodityList().subscribe($allCommodityList=>{
      //console.log("All commodity data: ",$allCommodityList);
      this.allCommodityData=$allCommodityList.listData;
      this.filteredData6=this.allCommodityData;
    })
  }

  CommodityName(post4:any,index4){
    this.shown9=false;
    this.shown10=true;
    this.selectedCommodity=index4;
    //console.log("Selected commodity is: ",post4);
    this.gmpmProdId=post4.gmpmProdId;
    this.gmpmgProdName=post4.gmpmgProdName;
    this.gmpmDefunct=post4.gmpmDefunct;
    this.gmpmMarketId=post4.gmpmMarketId;
    this.gmpmOprId=post4.gmpmOprId;
    this.gmpmOrgId=post4.gmpmOrgId;
    this.gmpmQtyUom=post4.gmpmQtyUom;
    this.gmpmgProdCode=post4.gmpmgProdCode;
    this.gmpmgQtyUom=post4.gmpmgQtyUom;
  }

  onSelectBagType(){
    this.authservice.getBagType().subscribe($allBagData=>{
      //console.log("All bag types are: ",$allBagData);
      this.allBagTypedata=$allBagData.listData;
      this.filteredData7=this.allBagTypedata;
    });
  }

  selectedBagData(post5:any,index5){
    this.shown11=false;
    this.shown12=true;
    this.selectedBag=index5;
    this.gmptm_desc=post5.gmptm_desc;
    this.gmptm_defunct=post5.gmptm_defunct;
    this.gmptm_oprid=post5.gmptm_oprid;
    this.gmptm_orgid=post5.gmptm_orgid;
    this.gmptm_packid=post5.gmptm_packid;
    this.gmptm_qty=post5.gmptm_qty;
    this.gmptm_qty_uom=post5.gmptm_qty_uom;
  }

  getBagQuality(){
    //console.log("you typed no of bags:",this.noOfBags);
    const apprxQtl={
                        commodityObj: {
                            gmpmDefunct:this.gmpmDefunct ,
                            gmpmMarketId:this. gmpmMarketId,
                            gmpmOprId:this.gmpmOprId ,
                            gmpmOrgId:this.gmpmOrgId ,
                            gmpmProdId:this.gmpmProdId ,
                            gmpmQtyUom:this.gmpmQtyUom ,
                            gmpmgProdCode:this.gmpmgProdCode ,
                            gmpmgProdName:this.gmpmgProdName ,
                            gmpmgQtyUom:this.gmpmgQtyUom
                        },
                        bagObject: {
                            gmptm_defunct:this.gmptm_defunct ,
                            gmptm_desc:this.gmptm_desc ,
                            gmptm_oprid:this.gmptm_oprid ,
                            gmptm_orgid:this.gmptm_orgid ,
                            gmptm_packid:this.gmptm_packid ,
                            gmptm_qty:this.gmptm_qty ,
                            gmptm_qty_uom:this.gmptm_qty_uom
                        },
                        noBag:this.noOfBags
                      }
    //console.log("apprxQtl : ",apprxQtl);
    this.authservice.getApprxQty(apprxQtl).subscribe($apprxBagsqty=>{
      //console.log("Approx quality of bag is: ",$apprxBagsqty);
      this.approxQty=$apprxBagsqty.approxQty;
    });

  }

  onClickVehicleType(){
    this.authservice.getVehicleTypeList().subscribe($vehicleData=>{
      console.log("vehicle data type: ",$vehicleData);
      this.vehicleData=$vehicleData.listData;
      this.filteredData8=this.vehicleData;
    });
  }

  selectedVehicleData(post6:any,index6){
    this.shown13=false;
    this.shown14=true;
    this.selectedVehicle=index6;
    this.gvrmDefunct=post6.gvrmDefunct;
    this.gvrmOprId=post6.gvrmOprId;
    this.gvrmOrgId=post6.gvrmOrgId;
    this.gvrmTrnId=post6.gvrmTrnId;
    this.gvrmVehRateAnnual=post6.gvrmVehRateAnnual;
    this.gvrmVehRateDaily=post6.gvrmVehRateDaily;
    this.gvrmVehRateMonthly=post6.gvrmVehRateMonthly;
    this.gvrmVehTypeCd=post6.gvrmVehTypeCd;
  }

  getCaFirm(){
    this.authservice.getCAList().subscribe($caData=>{
      console.log("All CA firm Data: ",$caData);
      this.allCAData=$caData.listData;
      this.filteredData9=this.allCAData;
    });
  }

  selectedCAFirm(post7:any,index7){
    this.shown15=false;
    this.shown16=true;
    this.shown17=false;
    this.shown18=true;
    this.selectedCA=index7;
    this.lcamAgenCompName=post7.lcamAgenCompName;
    this.lcamFullName=post7.lcamFullName;
    this.lcamAadharNo=post7.lcamAadharNo;
    this.lcamAgenAdd1=post7.lcamAgenAdd1;
    this.lcamAgenAdd2=post7.lcamAgenAdd2;
    this.lcamAgenCatgCd=post7.lcamAgenCatgCd;
    this.lcamAgenCatgDesc=post7.lcamAgenCatgDesc;
    this.lcamAgenCompRegino=post7.lcamAgenCompRegino;
    this.lcamAgenContno=post7.lcamAgenContno;
    this.lcamAgenDistId=post7.lcamAgenDistId;
    this.lcamAgenEmailid=post7.lcamAgenEmailid;
    this.lcamAgenFaxno=post7.lcamAgenFaxno;
    this.lcamAgenGendStat=post7.lcamAgenGendStat;
    this.lcamAgenLicenNo=post7.lcamAgenLicenNo;
    this.lcamAgenPincd=post7.lcamAgenPincd;
    this.lcamAgenProdCd=post7.lcamAgenProdCd;
    this.lcamAgenProdDesc=post7.lcamAgenProdDesc;
    this.lcamAgenStateId=post7.lcamAgenStateId;
    this.lcamAgenThasilId=post7.lcamAgenThasilId;
    this.lcamAgenTradLicenno=post7.lcamAgenTradLicenno;
    this.lcamAgenType=post7.lcamAgenType;
    this.lcamBankAccount=post7.lcamBankAccount;
    this.lcamBankName=post7.lcamBankName;
    this.lcamDefunct=post7.lcamDefunct;
    this.lcamFactorBidLimit=post7.lcamFactorBidLimit;
    this.lcamIfscCode=post7.lcamFactorBidLimit;
    this.lcamOprid=post7.lcamOprid;
    this.lcamOrgid=post7.lcamOrgid;
    this.lcamPanNo=post7.lcamPanNo;
    this.lcamRemark=post7.lcamRemark;
    this.lcamSecurityDeposit=post7.lcamSecurityDeposit;
    this.lcamTrnId=post7.lcamTrnId;
  }

  onClickSave(){
    const savedData={
      framerType:this.sellerType,
        farmerObj:{
            farmOrgId: this.farmOrgId,
            farmRegIId:this.farmRegIId ,
            frmCityId:this.frmCityId ,
            frmContactNo:this.frmContactNo ,
            frmDistId:this.frmDistId ,
            frmFullName:this.frmFullName ,
            frmOprId:this.frmOprId ,
            frmOprType: this.frmOprType,
            frmRegidt:this.frmRegidt ,
            frmRelationId:this.frmRelationId ,
            frmStateId: this.frmStateId,
            frmTahsilId: this.frmTahsilId,
            frmdefunct:this.frmdefunct ,
            frmsonfo:this.frmsonfo ,
            gmcmCity:this.gmcmCity
        },
        commodityObj: {
            gmpmDefunct: this.gmpmDefunct,
            gmpmMarketId: this.gmpmMarketId,
            gmpmOprId: this.gmpmOprId,
            gmpmOrgId: this.gmpmOrgId,
            gmpmProdId:this.gmpmProdId ,
            gmpmQtyUom: this.gmpmQtyUom,
            gmpmgProdCode: this.gmpmgProdCode,
            gmpmgProdName: this.gmpmgProdName,
            gmpmgQtyUom:this.gmpmgQtyUom
        },
        bagTypeObj: {
            gmptm_defunct:this.gmptm_defunct ,
            gmptm_desc:this.gmptm_desc ,
            gmptm_oprid:this.gmptm_oprid ,
            gmptm_orgid:this.gmptm_orgid ,
            gmptm_packid:this.gmptm_packid ,
            gmptm_qty:this.gmptm_qty ,
            gmptm_qty_uom:this.gmptm_qty_uom
        },
        noBag:this.noOfBags,
        approxQty:this.approxQty,
        vechileObj:{
            gvrmDefunct:this.gvrmDefunct ,
            gvrmOprId:this.gvrmOprId,
            gvrmOrgId:this.gvrmOrgId ,
            gvrmTrnId: this.gvrmTrnId,
            gvrmVehRateAnnual: this.gvrmVehRateAnnual,
            gvrmVehRateDaily:this.gvrmVehRateDaily ,
            gvrmVehRateMonthly: this.gvrmVehRateMonthly,
            gvrmVehTypeCd:this.gvrmVehTypeCd
        },
        vechileNo:this.vehicleno,
        caObj:{
            lcamAadharNo:this.lcamAadharNo ,
            lcamAgenAdd1:this. lcamAgenAdd1,
            lcamAgenAdd2:this.lcamAgenAdd2 ,
            lcamAgenCatgCd: this.lcamAgenCatgCd,
            lcamAgenCatgDesc:this.lcamAgenCatgDesc ,
            lcamAgenCompName:this. lcamAgenCompName,
            lcamAgenCompRegino:this.lcamAgenCompRegino ,
            lcamAgenContno: this.lcamAgenContno,
            lcamAgenDistId: this.lcamAgenDistId,
            lcamAgenEmailid:this.lcamAgenEmailid ,
            lcamAgenFaxno:this.lcamAgenFaxno ,
            lcamAgenGendStat:this.lcamAgenGendStat ,
            lcamAgenLicenNo:this.lcamAgenLicenNo ,
            lcamAgenPincd: this.lcamAgenPincd,
            lcamAgenProdCd: this.lcamAgenProdCd,
            lcamAgenProdDesc:this.lcamAgenProdDesc ,
            lcamAgenStateId:this.lcamAgenStateId ,
            lcamAgenThasilId: this.lcamAgenThasilId,
            lcamAgenTradLicenno: this.lcamAgenTradLicenno,
            lcamAgenType:this.lcamAgenType ,
            lcamBankAccount: this.lcamBankAccount,
            lcamBankName:this.lcamBankName ,
            lcamDefunct: this.lcamDefunct,
            lcamFactorBidLimit:this.lcamFactorBidLimit ,
            lcamFullName: this.lcamFullName,
            lcamIfscCode: this.lcamIfscCode,
            lcamOprid: this.lcamOprid,
            lcamOrgid:this.lcamOrgid ,
            lcamPanNo: this.lcamPanNo,
            lcamRemark: this.lcamRemark,
            lcamSecurityDeposit:this.lcamSecurityDeposit ,
            lcamTrnId:this.lcamTrnId
        }

    }


    //console.log("Saved data is:",savedData);
    this.authservice.getSaveResult(savedData).subscribe($RsSavedData=>{
      console.log("Response from saved data: ",$RsSavedData);
      if($RsSavedData.status==1){
        this.div_show8();
        this.message=$RsSavedData.message;
      }else
      {
        this.message="Not successfully saved !!";
      }
    });
  }

  onClickSaveT(){
    const savedData={
      framerType:this.sellerType,
      traderList:{
          lcam_agen_add1: this.lcam_agen_add1,
          lcam_agen_comp_name:this.lcam_agen_comp_name ,
          lcam_agen_contno: this.lcam_agen_contno,
          lcam_agen_dist_id: this.lcam_agen_dist_id,
          lcam_agen_gend_stat:this.lcam_agen_gend_stat ,
          lcam_agen_licen_no:this.lcam_agen_licen_no ,
          lcam_agen_pincd:this.lcam_agen_pincd ,
          lcam_agen_state_id:this.lcam_agen_state_id ,
          lcam_agen_thasil_id:this.lcam_agen_thasil_id ,
          lcam_agen_trad_licenno: this.lcam_agen_trad_licenno,
          lcam_agen_type:this.lcam_agen_type ,
          lcam_defunct:this.lcam_defunct ,
          lcam_full_name: this.lcam_full_name,
          lcam_oprid:this.lcam_oprid ,
          lcam_orgid:this.lcam_orgid ,
          lcam_trn_id:this.lcam_trn_id
      },
        commodityObj: {
            gmpmDefunct: this.gmpmDefunct,
            gmpmMarketId: this.gmpmMarketId,
            gmpmOprId: this.gmpmOprId,
            gmpmOrgId: this.gmpmOrgId,
            gmpmProdId:this.gmpmProdId ,
            gmpmQtyUom: this.gmpmQtyUom,
            gmpmgProdCode: this.gmpmgProdCode,
            gmpmgProdName: this.gmpmgProdName,
            gmpmgQtyUom:this.gmpmgQtyUom
        },
        bagTypeObj: {
            gmptm_defunct:this.gmptm_defunct ,
            gmptm_desc:this.gmptm_desc ,
            gmptm_oprid:this.gmptm_oprid ,
            gmptm_orgid:this.gmptm_orgid ,
            gmptm_packid:this.gmptm_packid ,
            gmptm_qty:this.gmptm_qty ,
            gmptm_qty_uom:this.gmptm_qty_uom
        },
        noBag:this.noOfBags,
        approxQty:this.approxQty,
        vechileObj:{
            gvrmDefunct:this.gvrmDefunct ,
            gvrmOprId:this.gvrmOprId,
            gvrmOrgId:this.gvrmOrgId ,
            gvrmTrnId: this.gvrmTrnId,
            gvrmVehRateAnnual: this.gvrmVehRateAnnual,
            gvrmVehRateDaily:this.gvrmVehRateDaily ,
            gvrmVehRateMonthly: this.gvrmVehRateMonthly,
            gvrmVehTypeCd:this.gvrmVehTypeCd
        },
        vechileNo:this.vehicleno,
        caObj:{
            lcamAadharNo:this.lcamAadharNo ,
            lcamAgenAdd1:this. lcamAgenAdd1,
            lcamAgenAdd2:this.lcamAgenAdd2 ,
            lcamAgenCatgCd: this.lcamAgenCatgCd,
            lcamAgenCatgDesc:this.lcamAgenCatgDesc ,
            lcamAgenCompName:this. lcamAgenCompName,
            lcamAgenCompRegino:this.lcamAgenCompRegino ,
            lcamAgenContno: this.lcamAgenContno,
            lcamAgenDistId: this.lcamAgenDistId,
            lcamAgenEmailid:this.lcamAgenEmailid ,
            lcamAgenFaxno:this.lcamAgenFaxno ,
            lcamAgenGendStat:this.lcamAgenGendStat ,
            lcamAgenLicenNo:this.lcamAgenLicenNo ,
            lcamAgenPincd: this.lcamAgenPincd,
            lcamAgenProdCd: this.lcamAgenProdCd,
            lcamAgenProdDesc:this.lcamAgenProdDesc ,
            lcamAgenStateId:this.lcamAgenStateId ,
            lcamAgenThasilId: this.lcamAgenThasilId,
            lcamAgenTradLicenno: this.lcamAgenTradLicenno,
            lcamAgenType:this.lcamAgenType ,
            lcamBankAccount: this.lcamBankAccount,
            lcamBankName:this.lcamBankName ,
            lcamDefunct: this.lcamDefunct,
            lcamFactorBidLimit:this.lcamFactorBidLimit ,
            lcamFullName: this.lcamFullName,
            lcamIfscCode: this.lcamIfscCode,
            lcamOprid: this.lcamOprid,
            lcamOrgid:this.lcamOrgid ,
            lcamPanNo: this.lcamPanNo,
            lcamRemark: this.lcamRemark,
            lcamSecurityDeposit:this.lcamSecurityDeposit ,
            lcamTrnId:this.lcamTrnId
        }

    }


    //console.log("Saved data is:",savedData);
    this.authservice.getSaveResult(savedData).subscribe($RsSavedData=>{
      console.log("Response from saved data: ",$RsSavedData);
      if($RsSavedData.status==1){
        this.div_show8();
        this.message=$RsSavedData.message;
      }else
      {
        this.message="Not successfully saved !!";
      }
    });

  }


  //On select ID proof type
  onSelectIdProof(proofValue){
    this.proofValue=proofValue;
    console.log("Id proof is: ",this.proofValue);
  }

  //on Select relation value
  relationValue(relationValue){
    this.relationFValue=relationValue;
    console.log("selected relation is: ",this.relationFValue);
  }
  //Create Farmer save
  saveCreateFarmer(){
    if(this.FFullName==undefined || this.FFullName==null || this.FFullName==""){
      this.FFullName=null;
    }
    if(this.FAddrs1==undefined || this.FAddrs1==null || this.FAddrs1==""){
      this.FAddrs1=null;
    }
    if(this.FAddrs2==undefined || this.FAddrs2==null || this.FAddrs2==""){
      this.FAddrs2=null;
    }
    if(this.FStateId==undefined || this.FStateId==null || this.FStateId==""){
      this.FStateId=null;
    }
    if(this.FDistId==undefined || this.FDistId==null || this.FDistId==""){
      this.FDistId=null;
    }
    if(this.FTehsilId==undefined || this.FTehsilId==null || this.FTehsilId==""){
      this.FTehsilId=null;
    }
    if(this.FMobileNo==undefined || this.FMobileNo==null || this.FMobileNo==""){
      this.FMobileNo=null;
    }
    if(this.proofValue==undefined || this.proofValue==null || this.proofValue==""){
      this.proofValue=null;
    }
    if(this.FIdProofNo==undefined || this.FIdProofNo==null || this.FIdProofNo==""){
      this.FIdProofNo=null;
    }
    if(this.frmsonOf==undefined || this.frmsonOf==null || this.frmsonOf==""){
      this.frmsonOf=null;
    }
    if(this.FVillageId==undefined || this.FVillageId==null || this.FVillageId==""){
      this.FVillageId=null;
    }
    if(this.relationFValue==undefined || this.relationFValue==null || this.relationFValue==""){
      this.relationFValue=null;
    }

    const farmerData={

         farmerFullName:this.FFullName,
         farmerAdd1:this.FAddrs1,
         farmerAdd2:this.FAddrs2,
         farmerStateId:this.FStateId,
         farmerDistId:this.FDistId,
         farmerTehsilId:this.FTehsilId,
         farmerContactNo:this.FMobileNo,
         farmerKycIdProofType:this.proofValue,
         farmerIdProofId:this.FIdProofNo,
         farmerSonOf:this.frmsonOf,
         cityId:this.FVillageId,
         relationId:this.relationFValue

    }
    console.log("saveCreateFarmer is: ",farmerData);

    if(this.FFullName=="" || this.FFullName==null  && this.FAddrs1=="" || this.FAddrs1==null && this.FStateId=="" || this.FStateId==null && this.FDistId=="" || this.FDistId==null && this.FTehsilId=="" || this.FTehsilId==null &&
      this.FVillageId=="" || this.FVillageId==null && this.FMobileNo=="" || this.FMobileNo==null)

    {
        this.message="Mandatory fields(*) can't be blank ";
        this.div_show8();
    }

    else if(this.FFullName!="" || this.FFullName!=null  && this.FAddrs1!="" || this.FAddrs1!=null && this.FStateId!="" || this.FStateId!=null && this.FDistId!="" || this.FDistId!=null && this.FTehsilId!="" || this.FTehsilId!=null &&
      this.FVillageId!="" || this.FVillageId!=null && this.FMobileNo!="" || this.FMobileNo!=null)

    {
      this.authservice.getCreateFarmer(farmerData).subscribe($createFarmerData=>{
        console.log("After submitting create farmer Data: ",$createFarmerData);
        if($createFarmerData.status==1){
          this.message=$createFarmerData.message;
          this.div_show8();
          this.div_hide10();
        }
        else
        {
          this.message=$createFarmerData.message;
          this.div_show8();
          this.div_hide10();
        }
      });
    }


  }


  //Searching fields
  searchFarmerName(query:string){
    this.farmerNameUpper=query.toUpperCase();
    if(this.farmerNameUpper) {
      this.filteredData1 = _.filter(this.sellerNameData, (a)=>a.frmFullName.indexOf(this.farmerNameUpper)>=0);
    } else {
      this.filteredData1 = this.sellerNameData  ;
    }
  }

  searchStateName(query:string){
    if(query) {
      this.filteredData2 = _.filter(this.allStateName, (a)=>a.gmsmStateDesc.indexOf(query)>=0);
    } else {
      this.filteredData2 = this.allStateName;
    }
  }

  searchDistName(query:string){
    if(query) {
      this.filteredData3 = _.filter(this.allDIstData, (a)=>a.gmdmDistDesc.indexOf(query)>=0);
    } else {
      this.filteredData3 = this.allDIstData;
    }
  }

  searchTehsilName(query:string){
    if(query) {
      this.filteredData4 = _.filter(this.allTehsilData, (a)=>a.gmtmTashilDesc.indexOf(query)>=0);
    } else {
      this.filteredData4 = this.allTehsilData;
    }
  }

  searchVillageName(query:string){
    if(query) {
      this.filteredData5 = _.filter(this.allVillageData, (a)=>a.gmcmCity.indexOf(query)>=0);
    } else {
      this.filteredData5 = this.allVillageData;
    }
  }

  searchCommodityName(query:string){
    if(query) {
      this.filteredData6 = _.filter(this.allCommodityData, (a)=>a.gmpmgProdName.indexOf(query)>=0);
    } else {
      this.filteredData6 = this.allCommodityData;
    }
  }

  searchBagTypeName(query:string){
    if(query) {
      this.filteredData7 = _.filter(this.allBagTypedata, (a)=>a.gmptm_desc.indexOf(query)>=0);
    } else {
      this.filteredData7 = this.allBagTypedata;
    }
  }

  searchVehicleTypeName(query:string){
    if(query) {
      this.filteredData8 = _.filter(this.vehicleData, (a)=>a.gvrmVehTypeCd.indexOf(query)>=0);
    } else {
      this.filteredData8 = this.vehicleData;
    }
  }

  searchCAName(query:string){
    if(query) {
      this.filteredData9 = _.filter(this.allCAData, (a)=>a.lcamAgenCompName.indexOf(query)>=0);
    } else {
      this.filteredData9 = this.allCAData;
    }
  }

  onAdvance1(){
    this.shown29=false;
    this.shown30=true;
  }
  onBasic1(){
    this.shown29=true;
    this.shown30=false;
  }
  onAdvance2(){
    this.shown31=false;
    this.shown32=true;
  }
  onBasic2(){
    this.shown31=true;
    this.shown32=false;
  }
  onAdvance3(){
    this.shown33=false;
    this.shown34=true;
  }
  onBasic3(){
    this.shown33=true;
    this.shown34=false;
  }





  // Open & closes of popups
  div_show(){
      this.my_class1='overlay1';
    }
  div_hide(){
      this.my_class1='overlay';
    }
  div_show1(){
      this.my_class2='overlay1';
    }
  div_hide1(){
      this.my_class2='overlay';
    }
  div_show2(){
      this.my_class3='overlay1';
    }
  div_hide2(){
      this.my_class3='overlay';
    }
  div_show3(){
      this.my_class4='overlay1';
    }
  div_hide3(){
      this.my_class4='overlay';
    }
  div_show4(){
      this.my_class5='overlay1';
    }
  div_hide4(){
      this.my_class5='overlay';
    }
  div_show5(){
      this.my_class6='overlay1';
    }
  div_hide5(){
      this.my_class6='overlay';
    }
  div_show6(){
      this.my_class7='overlay1';
    }
  div_hide6(){
      this.my_class7='overlay';
    }
  div_show7(){
      this.my_class8='overlay1';
    }
  div_hide7(){
      this.my_class8='overlay';
    }
  div_show8(){
      this.my_class9='overlay1';
    }
  div_hide8(){
      this.my_class9='overlay';
    }
  div_show9(){
      this.my_class10='overlay1';
    }
  div_hide9(){
      this.my_class10='overlay';
    }
  div_show10(){
      this.my_class11='overlay1';
    }
  div_hide10(){
      this.my_class11='overlay';
    }
  div_show12(){
      this.my_class12='overlay1';
    }
  div_hide12(){
      this.my_class12='overlay';
    }
  div_show13(){
      this.my_class13='overlay1';
    }
  div_hide13(){
      this.my_class13='overlay';
    }
  div_show14(){
      this.my_class14='overlay1';
    }
  div_hide14(){
      this.my_class14='overlay';
    }
  div_show15(){
      this.my_class15='overlay1';
    }
  div_hide15(){
      this.my_class15='overlay';
    }
  div_show16(){
      this.my_class16='overlay1';
    }
  div_hide16(){
      this.my_class16='overlay';
    }
  div_show17(){
      this.my_class17='overlay1';
    }
  div_hide17(){
      this.my_class17='overlay';
    }
  div_show18(){
      this.my_class18='overlay1';
    }
  div_hide18(){
      this.my_class18='overlay';
    }


}
