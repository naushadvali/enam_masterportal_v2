import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GateEntryNewComponent } from './gate-entry-new.component';

describe('GateEntryNewComponent', () => {
  let component: GateEntryNewComponent;
  let fixture: ComponentFixture<GateEntryNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GateEntryNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GateEntryNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
