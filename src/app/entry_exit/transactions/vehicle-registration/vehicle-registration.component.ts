import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import * as _ from 'lodash';


@Component({
  selector: 'app-vehicle-registration',
  templateUrl: './vehicle-registration.component.html',
  styleUrls: ['./vehicle-registration.component.css'],
  providers :[DatePipe]
})
export class VehicleRegistrationComponent implements OnInit {
  public loading = false;
  public shown1:any=true;
  public shown2:any=false;
  public shown3:any=true;
  public shown4:any=false;
  public my_class1 ='overlay';public my_class2 ='overlay';public my_class3 ='overlay';public my_class4='overlay';public my_class5='overlay';public my_class6='overlay';
  dateInsert:any;today:any;fromDate:any;toDate:any;years:any;months:any;
  distSearchDisabled:boolean;tahsilSearchDisabled:boolean;
  feeType:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private datepipe :DatePipe,
    private router:Router
  ) { }

  // // Date time picker settings
  // settings = {
  //     bigBanner: true,
  //     timePicker: true,
  //     format: 'dd/mm/yyyy HH:mm:ss',
  //     defaultOpen: false,
  // }
  ngOnInit() {
    this.feeType = "M"
    this.today = new Date();
    var dd = this.today.getDate();
    var mm = this.today.getMonth()+1; //January is 0!
    var yyyy = this.today.getFullYear();
    this.fromDate =
    { date: { year: this.today.getFullYear(), month: mm, day: dd } };
    this.toDate =
    { date: { year: this.today.getFullYear(), month: mm+1, day: dd } };
    this.distSearchDisabled = true;
    this.tahsilSearchDisabled = true;
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  errorPopupShow(){
        this.my_class4='overlay1';
      }
  onAdvance1(){
    this.shown1=false;
    this.shown2=true;
  }
  onBasic1(){
    this.shown1=true;
    this.shown2=false;
  }

  onAdvance2(){
    this.shown3=false;
    this.shown4=true;
  }
  onBasic2(){
    this.shown3=true;
    this.shown4=false;
  }
// Open Vehicle Type popup
div_show1(){
  this.my_class1='overlay1';
  this.getInfoForVehicleType();
}
// Hide vehicle type popup
div_hide1(){
  this.my_class1='overlay';
  this.vehicleDescFilter ="";
  this.vehicleCodeFilter = "";
  this.selectedVehicle = -1;
}
// Open state Popup
div_show2(){
  this.my_class2='overlay1';
  this.getStates();
}
// Hide State Popup
div_hide2(){
  this.my_class2='overlay';
  this.selectedState= -1;
}
// Open disrict Popup
openDistPopup(){
  this.my_class3='overlay1';
  this.getDistricts();
}
// Hide district Popup
hideDistPopup(){
  this.my_class3='overlay';
  this.selectedDist = -1;
}
// Open Tehsil Popup
openTehsilPopup(){
  this.my_class5='overlay1';
  this.getTehsils();
}
// Hide Tehsil Popup
hideTehsilPopup(){
  this.my_class5='overlay';
  this.selectedTehsil = -1;
}
// on monthly
checkedMonthly(){
  this.feeType = "M"
  console.log("feeType",this.feeType)
  this.fromDate =
  { date: { year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate() } };
  this.toDate =
  { date: { year: this.today.getFullYear(), month: this.today.getMonth()+2, day: this.today.getDate() } };
  this.onReset();
}
// on annually
checkedAnnually(){
  this.feeType = "A"
  console.log("feeType",this.feeType)
  this.fromDate =
  { date: { year: this.today.getFullYear(), month: this.today.getMonth()+1, day: this.today.getDate() } };
  this.toDate =
  { date: { year: this.today.getFullYear()+1, month: this.today.getMonth()+1, day: this.today.getDate() } };
  this.onReset();
}


// Get Vehicle Type List
VehicleTypeInfo = [];
filterdVehicleTypeInfo = [];
getInfoForVehicleType(){
  this.loading = true;
  this.authservice.getVehicleTypeList().subscribe($data=>{
    console.log("vehicle type Info: ",$data);
    if($data.listData != undefined){
      this.VehicleTypeInfo = $data.listData;
      this.filterdVehicleTypeInfo = $data.listData;
      this.loading = false;
    }
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class4 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}
// Select Vehicle Type here
valVehicleType=[];selectedVehicle:number;
onVehicleTypeSelect(vehicleTypeVal,index){
  this.valVehicleType = vehicleTypeVal;
  this.selectedVehicle = index;
}

// save selected vehicle type
selVehicleTypeVal=[];vehDesc:string;vehAmount:string;vehTypeId:string;
onSelectVehicleTypeOK(){
  this.selVehicleTypeVal = this.valVehicleType;
  console.log("this.selVehicleTypeVal",this.selVehicleTypeVal)
  this.vehDesc = this.selVehicleTypeVal["gvrmVehDesc"];
  if(this.feeType == 'M'){
    this.vehAmount = this.selVehicleTypeVal["gvrmVehRateMonthly"];
  }
  else{
    this.vehAmount = this.selVehicleTypeVal["gvrmVehRateAnnual"];
  }
  this.my_class1 = 'overlay';
  this.selectedVehicle = -1;
  this.vehicleDescFilter ="";
  this.vehicleCodeFilter = "";
  this.vehTypeId = this.selVehicleTypeVal["gvrmTrnId"];
}

// Filter on vehicle type code
vehicleCodeFilter:string;
onVehicleCodeFilter(){
  this.vehicleCodeFilter=this.vehicleCodeFilter.toUpperCase();
  if(this.vehicleCodeFilter != ""){
    var info = _.filter(this.VehicleTypeInfo, (a)=>a.gvrmVehTypeCd.indexOf(this.vehicleCodeFilter)>=0);
    this.filterdVehicleTypeInfo = info;
  }
  else{
  this.filterdVehicleTypeInfo = this.VehicleTypeInfo;
  }
}
// Filter on vehicle type desc
vehicleDescFilter:string;
onVehicleDescFilter(){
  this.vehicleDescFilter=this.vehicleDescFilter.toUpperCase();
  if(this.vehicleDescFilter != ""){
    var info = _.filter(this.VehicleTypeInfo, (a)=>a.gvrmVehDesc.indexOf(this.vehicleDescFilter)>=0);
    this.filterdVehicleTypeInfo = info;
  }
  else{
  this.filterdVehicleTypeInfo = this.VehicleTypeInfo;
  }
}

// Reset Page
altMobNo:string
pinCode:string
remarks:string
onReset(){
  this.selVehicleTypeVal = this.selDistVal = this.selStateVal = this.selTehsilVal = [];
  this.vehDesc = this.vehAmount = this.stateDesc = this.vehNo = this.ownerName = this.mobNo = this.emailId = this.add1 = this.add2 = this.altMobNo = this.distDesc = this.tehsilDesc = this.city = this.pinCode = this.remarks = undefined;
  this.vehTypeId ="";this.vehAmount = "";
}

// Get All selectedFarmerTehsil//State Parameters
allStates=[];
getStates(){
  this.loading = true;
  this.authservice.getAllStateName().subscribe($allStatename=>{
    console.log("All state name: ",$allStatename.listData);
    this.allStates=$allStatename.listData;
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
);
}

// Select One State here
selectedState:number;stateVal =[];
onSelState(post,index){
  this.selectedState=index;
  this.stateVal = post;
}

// On Select State Ok
selStateVal =[];stateDesc:string;stateId:string;
onSelectStateOK(){
this.selStateVal = this.stateVal;
console.log("selStateVal",this.selStateVal)
this.my_class2='overlay';
this.selectedState= -1;
this.stateDesc = this.selStateVal["gmsmStateDesc"];
this.stateId = this.selStateVal["gmsmStateId"];
this.distSearchDisabled = false;
this.selTehsilVal =this.selDistVal = [];
this.tehsilDesc =this.distDesc = "";
}

// Get all dist by state id
allDists =[];selectedDist:number;
filteredDists =[];
getDistricts(){
  this.loading = true;
  this.authservice.getAllDistrictName(this.stateId).subscribe($allDist=>{
    console.log("All district name: ",$allDist);
    if($allDist.listData != undefined){
      this.allDists=$allDist.listData;
      this.filteredDists=$allDist.listData;
    }
    this.loading = false;
  }, (err) => {
          if (err === 'Unauthorized')
          {       this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class4 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
      }
    });
}
// Filter by Dist Name
filterDistName(query:string){
  if(query) {
    this.filteredDists = _.filter(this.allDists, (a)=>a.gmdmDistDesc.indexOf(query)>=0);
  } else {
    this.filteredDists = this.allDists;
  }
}

// select one dist
distVal =[];
onSelDist(post,index){
  this.selectedDist=index;
  this.distVal = post;
}

// on select district Ok
selDistVal =[];distDesc:string;distId:string;
onSelectDistrictOK(){
this.selDistVal = this.distVal;
console.log("selStateVal",this.selDistVal)
this.my_class3='overlay';
this.selectedDist= -1;
this.distDesc = this.selDistVal["gmdmDistDesc"];
this.distId = this.selDistVal["gmdmDistId"];
this.tahsilSearchDisabled = false;
this.selTehsilVal = [];
this.tehsilDesc = "";
}

// Get Tehsils by dist id
allTehsils=[];filteredTehsils=[];selectedTehsil:number;
getTehsils(){
  this.loading = true;
  this.authservice.getAllTehsilName(this.distId).subscribe($alltehsil=>{
      console.log("all tehsil name: ",$alltehsil);
      this.allTehsils=$alltehsil.listData;
      this.filteredTehsils=this.allTehsils;
      this.loading = false;
  }, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class4 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);

      }
    }
);
}

// select one tehsil
tehsilVal =[];
onSelTehsil(post,index){
  this.selectedTehsil=index;
  this.tehsilVal = post;
}

// on select tehsil Ok
selTehsilVal =[];tehsilDesc:string;tehsilId:string;
onSelectTehsilOK(){
  this.selTehsilVal = this.tehsilVal;
  this.my_class5='overlay';
  this.selectedTehsil= -1;
  this.tehsilDesc = this.selTehsilVal["gmtmTashilDesc"];
  this.tehsilId = this.selTehsilVal["gmtmTahsilId"];
}


// Filter by tehsil Name
filterTehsilName(query:string){
  if(query) {
    this.filteredTehsils = _.filter(this.allTehsils, (a)=>a.gmtmTashilDesc.indexOf(query)>=0);
  } else {
    this.filteredTehsils = this.allTehsils;
  }
}

// Format Date mm/dd/yyyy
convertDate(myDate){
  var dd = myDate.date.day;
  var mm = myDate.date.month;
  if(dd<10){
    dd = "0"+dd;
  }
  if(mm<10){
    mm = "0"+mm;
  }
  var convDate = (dd+"/"+mm+"/"+myDate.date.year);
  return convDate;
}

// Save Vehicle Registration
ownerName:string;add1:string;add2:string;city:string;mobNo:string;emailId:string;vehNo:string;successMsg:string;saveRes:any;
saveVehicleRegistration(){
  if(this.feeType == 'M'){
    var duration = 1;
  }
  else{
    var duration = 12;
  }
  var fromDate = this.convertDate(this.fromDate);
  var toDate = this.convertDate(this.toDate);
  if(this.vehTypeId != "" && this.vehTypeId != undefined){
    if(this.vehNo != "" && this.vehNo != undefined){
      if(this.ownerName != "" && this.ownerName != undefined){
        if(this.mobNo != "" && this.mobNo != undefined){
          var postJson =  {"vechTypeId": this.vehTypeId,"vechRegNho": this.vehNo,"vechFeeType": this.feeType,"vechFee": this.vehAmount,"validFrmDate": fromDate,"validToDate": toDate,	"validDuration": duration,"ownerName": this.ownerName,"add1": this.add1,"add2": this.add2,"stateId": this.stateId,"distId": this.distId,"teshilId": this.tehsilId,"city": this.city,"mobileNo": this.mobNo,	"emailId": this.emailId
         }
         this.loading = true;
         this.authservice.saveVehicleRegistration(postJson).subscribe($data=>{
            this.loading = false;
            this.saveRes = $data;
            this.successMsg = $data.message;
            this.my_class6 = 'overlay1';
            setTimeout(()=>{
              this.my_class6 = 'overlay';
              this.onReset();
            },2000);
         }, (err) => {
                     if (err === 'Unauthorized')
                     {
                       this.errorPopupShow();
                             setTimeout(()=>{
                               this.my_class1 = 'overlay';
                               localStorage.clear();
                               this.router.navigateByUrl('/login');
                             },3000);
                     }
               });
        }
        else{
          this.flashMessagesService.show('Please Enter Mobile No', { cssClass: 'alert-danger', timeout: 3000 });
        }
      }
      else{
        this.flashMessagesService.show('Please Enter Owner Name', { cssClass: 'alert-danger', timeout: 3000 });
      }
    }
    else{
      this.flashMessagesService.show('Please Enter Vehicle Number', { cssClass: 'alert-danger', timeout: 3000 });
    }
  }
  else{
    this.flashMessagesService.show('Please Select Vehicle', { cssClass: 'alert-danger', timeout: 3000 });
  }


}

}
