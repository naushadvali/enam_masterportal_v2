import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotEntryComponent } from './lot-entry.component';

describe('LotEntryComponent', () => {
  let component: LotEntryComponent;
  let fixture: ComponentFixture<LotEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
