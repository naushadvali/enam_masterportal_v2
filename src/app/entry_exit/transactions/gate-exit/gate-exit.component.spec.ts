import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GateExitComponent } from './gate-exit.component';

describe('GateExitComponent', () => {
  let component: GateExitComponent;
  let fixture: ComponentFixture<GateExitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GateExitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GateExitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
