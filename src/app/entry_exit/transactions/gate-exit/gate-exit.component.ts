import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';


@Component({
  selector: 'app-gate-exit',
  templateUrl: './gate-exit.component.html',
  styleUrls: ['./gate-exit.component.css']
})
export class GateExitComponent implements OnInit {

  public shown1:any=false;
  public my_class1='overlay';
  public my_class2='overlay';  public my_class3='overlay'; public my_class4 ='overlay';public my_class5 ='overlay';public my_class6 ='overlay';public my_class7 ='overlay';public my_class8 ='overlay';public my_class9 ='overlay';public my_class10 ='overlay';public my_class11 ='overlay';public my_class12 ='overlay';public my_class13 ='overlay';public my_class14 ='overlay';public my_class15 ='overlay';
  exitType:string;
  shown2:any=true;
  shown3:any=false;
  shown4:any=true;
  shown5:any=false;
  postTradeDisabled:boolean;
  public loading = false;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router
  ) { }

  ngOnInit() {
    this.sellerType = "F";
    this.getQuantityInKg();
    this.exitType = "F";
    this.postTradeDisabled = false;
  }

  errorPopupShow(){
        this.my_class3 = 'overlay1';
      }

  onExitType(e:string){
    console.log("exit type is: ",e);
    if(e=="postTrade"){
      this.onResetPage();
      this.shown1=true;
      this.exitType = "T";
      this.postTradeDisabled = true;
    }
    else if(e=="goodsReturn"){
      this.onResetPage();
      this.shown1=false
      this.exitType = "F"
      this.postTradeDisabled = false;
    }
  }

  onAdvance1(){
    this.shown2=false;
    this.shown3=true;
  }
  onBasic1(){
    this.shown2=true;
    this.shown3=false;
  }
  onAdvance2(){
    this.shown4=false;
    this.shown5=true;
  }
  onBasic2(){
    this.shown4=true;
    this.shown5=false;
  }

  div_show(){
      this.my_class1='overlay1';
      this.getInfoForGate();
    }
  div_hide(){
        this.my_class1='overlay';
        this.selectedGate = -1;  this.matchGateNo = this.matchGateName = "";
      }
  div_show2(){
      if(this.sellerType == 'F'){
        this.my_class2='overlay1';
      }
      if(this.sellerType == 'T'){
        this.my_class6='overlay1';
      }
      if(this.sellerType == 'L'){
        this.my_class7='overlay1';
      }
      this.openSellerNamePopup();
    }
  div_hide2(){
        this.my_class2='overlay';
        this.filterFarmerName = "";
        this.selFarmerIndex  = -1;
      }
// Hide Trader Popup
hideTraderPopup(){
  this.selTraderIndex = -1;
  this.my_class6 ='overlay';
  this.matchSellerName = this.matchCompanyName = "";
}
// Hide Post Trader Popup
hidePostTraderPopup(){
  this.selTraderPostIndex = -1;
  this.my_class15 ='overlay';
  this.matchTraderName = this.matchTraderCompanyName = this.matchLicenceNo = "";
}

// Choose seller type
sellerType:string;
chooseSellerType(seller){
  if(seller == 'F'){
    this.onResetPage();
    this.sellerType = "F";
  }
  if(seller == 'T'){
    this.onResetPage();
    this.sellerType = "T";
  }
  if(seller == 'L'){
    this.onResetPage();
    this.sellerType = "L";
  }
  console.log("sellerType",this.sellerType)
}

// Open Seller Name Popup
openSellerNamePopup(){
if(this.sellerType == 'F'){
  this.getSellerForFarmer();
}
if(this.sellerType == 'T'){
  this.getSellerForTrader();
}
if(this.sellerType == 'L'){
  this.getLotCodeForLotWiseFarmer();
}
}

// Reset popup
vehNo:string;traderFirmName:string;
onResetPage(){
  this.lotDetails = [];
  this.sellerDesc = this.vehNo = this.traderFirmName = this.gateNo= this.lotcode = this.traderDesc = "";
  this.filterFarmerName =  this.matchSellerName = this.matchCompanyName = this.matchGateNo = this.matchGateName = this.matchLotCode =   this.matchTraderName = this.matchTraderCompanyName = this.matchLicenceNo = this.matchBagType = "";

}

//populate seller name for farmer
sellerForFarmer=[];farmerId:string;filteredSellerForFarmer=[];
getSellerForFarmer(){
  this.loading = true;
  this.authservice.getSellerForFarmer().subscribe(sellerForFarmer=>{
    console.log("All seller name: ",sellerForFarmer.listData);
    this.sellerForFarmer = sellerForFarmer.listData;
    this.filteredSellerForFarmer = sellerForFarmer.listData;
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class3 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);  }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
}


// Select one farmer
selFarmerVal=[];selFarmerIndex:number;
onSelFarmer(post,index){
  this.selFarmerVal = post;
  this.selFarmerIndex = index;
}

// On select farmer OK
finalFarmerVal = [];lotDetails=[];sellerDesc:string;lotDetailJson:any;
onSelFarmerOK(){
  this.filterFarmerName = "";
  this.selFarmerIndex  = -1;
  this.finalFarmerVal = this.selFarmerVal;
  this.uomPresent = true;
  console.log("this.finalFarmerVal ",this.finalFarmerVal )
  this.my_class2='overlay';
  this.farmerId = this.finalFarmerVal["frmRegiId"];
  this.sellerDesc = this.finalFarmerVal["frmFullName"];
  var postJson ={"impType":"F","farmerId": this.farmerId}
  this.lotDetailJson = postJson;
  this.loading = true;
  this.authservice.fetchLotDetailsForFarmer(postJson).subscribe(lotDetails=>{
    console.log("All lotDetails : ",lotDetails.listData);
    this.loading = false;
    if(lotDetails.listData != undefined){
      this.lotDetails = lotDetails.listData;
      for(var i=0;i<this.lotDetails.length;i++){
      this.lotDetails[i].modifiedQtl = this.lotDetails[i].inqty;
        for(var j=0;j<this.conversionList.length;j++){
          console.log("this.conversionList[j].gmucmAltUomId",this.conversionList[j].gmucmAltUomId)

          if(this.conversionList[j].gmucmAltUomId == this.lotDetails[i].bagUom && this.conversionList[j].gmucmUomId ==this.quintalUOM){
            console.log("here",this.lotDetails[i].inqty)
            this.lotDetails[i]["qtyInKg"] =( Number(this.lotDetails[i].inqty) * Number(this.conversionList[j].gmucmConvFact)).toString()
          }
        }
        if(this.lotDetails[i]["bagUom"] == undefined){
          this.uomPresent = false;
          break;
        }
      }
      console.log("this.lotDetails",this.lotDetails)

      if(this.uomPresent == false){
        this.my_class8 = "overlay1";
      }
    }
    this.filterFarmerName = "";
    this.selFarmerIndex  = -1;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class3 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);  }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
}

// Fetch Gate Info
gateInfo =[];filteredGateInfo = [];
getInfoForGate(){
  this.loading = true;
  this.authservice.getGateInfo().subscribe($data=>{
    console.log("Gate Info: ",$data);
    if($data.listData != undefined){
      this.gateInfo = $data.listData;
      this.filteredGateInfo = $data.listData;
    }
    this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class3 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

//sel one gate
selGateVal=[];selectedGate:number;
onGateSelect(gateVal,index){
  this.selGateVal = gateVal;
  this.selectedGate = index;
}

// on gate select OK
finalGateVal=[];gateNo:string;
onSelGateOK(){
  this.finalGateVal = this.selGateVal;
  this.my_class1 = 'overlay';
  this.gateNo = this.finalGateVal["ggmGateNo"];
  this.selectedGate = -1;
  this.matchGateNo = this.matchGateName = "";
}

// Open Bag types
bagTypes = [];filteredBagTypes = [];selectedLotRow:number;
openBagDetailsPopup(rowIndex){
  this.selectedLotRow = rowIndex;
  this.loading = true;
  this.authservice.getBagType().subscribe($allBagData=>{
    this.loading = false;
    this.my_class5 = "overlay1";
    this.bagTypes=$allBagData.listData;
    this.filteredBagTypes=$allBagData.listData;
  });
}

// on sel bag type
selBagTypeVal=[];selectedBagType:number;
onSelBagType(bagVal,index){
  this.selBagTypeVal = bagVal;
  this.selectedBagType = index;
}

//on select bag type OK
finalBagType=[];qtyInKg=[];
onSelBagTypeOK(){
  this.selectedBagType = -1;this.matchBagType ="";
  this.finalBagType = this.selBagTypeVal;
  this.my_class5 = "overlay";
  if(this.exitType == 'F'){
    this.lotDetails[this.selectedLotRow]["bagDesc"] = this.finalBagType["gmptm_desc"];
    this.qtyInKg[this.selectedLotRow] = Number(this.lotDetails[this.selectedLotRow]["inqty"]) *1000;
    console.log("this.lotDetails",this.lotDetails,this.qtyInKg)
  }
else{
  this.lotDetails[this.selectedLotRow]["gmptmDesc"] = this.finalBagType["gmptm_desc"];
  this.qtyInKg[this.selectedLotRow] = Number(this.lotDetails[this.selectedLotRow]["egtGrossQtyQunital"]) *1000;
  console.log("this.lotDetails",this.lotDetails,this.qtyInKg)
}
}

// Hide Bag Type popup
hideBagTypePopup(){
  this.my_class5 = "overlay";
  this.selectedBagType = -1; this.matchBagType = "";
}

//populate seller name for trader
sellerForTrader=[];traderId:string;filteredSellerForTrader=[];
getSellerForTrader(){
  this.loading = true;
  this.authservice.getSellerForTrader().subscribe(sellerForTrader=>{
    console.log("All seller name: ",sellerForTrader.listData);
    this.sellerForTrader = sellerForTrader.listData;
    this.filteredSellerForTrader = sellerForTrader.listData;
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class3 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);  }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
}


// Select one trader
selTraderVal=[];selTraderIndex:number;
onSelTrader(post,index){
  this.selTraderVal = post;
  this.selTraderIndex = index;
}

// On select farmer OK
finalTraderVal = [];
onSelTraderOK(){
  this.uomPresent = true;
  this.selTraderIndex = -1;
  this.matchSellerName = this.matchCompanyName = "";
  this.finalTraderVal = this.selTraderVal;
  console.log("this.finalTraderVal ",this.finalTraderVal )
  this.my_class6='overlay';
  this.traderId = this.finalTraderVal["lcamTrnId"];
  this.traderFirmName = this.finalTraderVal["lcamAgenCompName"];
  this.sellerDesc = this.finalTraderVal["lcamFullName"];
  var postJson ={"impType":"T","traderId": this.traderId}
  this.lotDetailJson = postJson;
  this.loading = true;
  this.authservice.fetchLotDetailsForFarmer(postJson).subscribe(lotDetails=>{
    console.log("All lotDetails : ",lotDetails.listData);
    this.loading = false;
    if(lotDetails.listData != undefined){
      this.lotDetails = lotDetails.listData;
      for(var i=0;i<this.lotDetails.length;i++){
      this.lotDetails[i].modifiedQtl = this.lotDetails[i].inqty;
        for(var j=0;j<this.conversionList.length;j++){
          console.log("this.conversionList[j].gmucmAltUomId",this.conversionList[j].gmucmAltUomId)

          if(this.conversionList[j].gmucmAltUomId == this.lotDetails[i].bagUom && this.conversionList[j].gmucmUomId ==this.quintalUOM){
            console.log("here",this.lotDetails[i].inqty)
            this.lotDetails[i]["qtyInKg"] =( Number(this.lotDetails[i].inqty) * Number(this.conversionList[j].gmucmConvFact)).toString()
          }
        }
        if(this.lotDetails[i]["bagUom"] == undefined){
          this.uomPresent = false;
          break;
        }
      }
      console.log("this.lotDetails",this.lotDetails)

      if(this.uomPresent == false){
        this.my_class8 = "overlay1";
      }
    }
  }, (err) => {
            if (err === 'Unauthorized')
            { this.errorPopupShow();
              setTimeout(()=>{
              this.my_class3 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);  }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
}


//populate seller name for trader
lotCodeForFarmer=[];filteredLotCodeForFarmer = [];
getLotCodeForLotWiseFarmer(){
  this.loading = true;
  this.authservice.getLotForFarmer().subscribe(lotForFarmer=>{
    console.log("All seller name: ",lotForFarmer.listData);
    this.lotCodeForFarmer = lotForFarmer.listData;
    this.filteredLotCodeForFarmer = lotForFarmer.listData;
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class3 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);  }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
}

// Hide Popup for lot Code
hideLotCodePopUp(){
  this.my_class7 = "overlay";
  this.selLotIndex = -1;this.matchLotCode="";
}

// Select one lot for farmer
selLotVal=[];selLotIndex:number;
onSelLotCode(post,index){
  this.selLotVal = post;
  this.selLotIndex = index;
}

// On select lot OK
finalLotVal = [];lotId:string;lotcode:string;uomPresent:boolean;
onSelLotOK(){
  this.selLotIndex = -1;this.matchLotCode="";
  this.uomPresent = true;
  this.finalLotVal = this.selLotVal;
  console.log("this.finalLotVal ",this.finalLotVal )
  this.my_class7='overlay';
  this.sellerDesc = this.finalLotVal["frmFullName"];
  this.farmerId = this.finalLotVal["iledFarmerId"];
  this.lotcode = this.finalLotVal["iledLotCode"];
  this.lotId = this.finalLotVal["iledLotId"];
  var postJson ={"impType":"F","farmerId": this.farmerId,"lotId": this.lotId}
  this.lotDetailJson = postJson;
  this.loading = true;
  this.authservice.fetchLotDetailsForFarmer(postJson).subscribe(lotDetails=>{
    console.log("All lotDetails : ",lotDetails.listData);
    console.log("All conversionList : ",this.conversionList);
    this.loading = false;
    if(lotDetails.listData != undefined){
      this.lotDetails = lotDetails.listData;
      for(var i=0;i<this.lotDetails.length;i++){
      this.lotDetails[i].modifiedQtl = this.lotDetails[i].inqty;
        for(var j=0;j<this.conversionList.length;j++){
          console.log("this.conversionList[j].gmucmAltUomId",this.conversionList[j].gmucmAltUomId)

          if(this.conversionList[j].gmucmAltUomId == this.lotDetails[i].bagUom && this.conversionList[j].gmucmUomId ==this.quintalUOM){
            console.log("here",this.lotDetails[i].inqty)
            this.lotDetails[i]["qtyInKg"] =( Number(this.lotDetails[i].inqty) * Number(this.conversionList[j].gmucmConvFact)).toString()
          }
        }
        if(this.lotDetails[i]["bagUom"] == undefined){
          this.uomPresent = false;
          break;
        }
      }
      console.log("this.lotDetails",this.lotDetails)

      if(this.uomPresent == false){
        this.my_class8 = "overlay1";
      }
    }
  }, (err) => {
            if (err === 'Unauthorized')
            {this.errorPopupShow();
            setTimeout(()=>{
              this.my_class3 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);  }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);

}


// Search By Farmer Name
filterFarmerName:string;
searchByFarmerName(){
  console.log("here",this.filterFarmerName)
  this.filterFarmerName=this.filterFarmerName.toUpperCase();
  if(this.filterFarmerName) {
    this.filteredSellerForFarmer = _.filter(this.sellerForFarmer, (a)=>a.frmFullName.indexOf(this.filterFarmerName)>=0);
  } else {
    this.filteredSellerForFarmer = this.sellerForFarmer  ;
  }
}

// Hide Message popup
hideMessagePopUp(){
  this.my_class8 = "overlay";
}

// Get quantity in kg from quintal
conversionList = [];kgVal:string;unitList = [];quintalUOM:string;
getQuantityInKg(){
  this.loading = true;
  this.authservice.getUOMList().subscribe(uomData=>{
    console.log("All uomData name: ",uomData.data);
    this.conversionList = uomData.data["uomlist"];
    this.unitList = uomData.data["unitList"];
    console.log("this.conversionList",this.conversionList)
    for(var i=0;i<this.unitList.length;i++){
      if(this.unitList[i].gmumUnitCode == "QUI"){
        this.quintalUOM = this.unitList[i].gmumUomId
        console.log("kgVal",this.kgVal)
      }
    }
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            { this.errorPopupShow();
              setTimeout(()=>{
              this.my_class3 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
           }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
  return this.kgVal;
}

// No of bag CHANGE
finalQtl:string;
onBagNoChange(post,index,bagNo){
  console.log("val here",post,index,bagNo)
  this.lotDetails[index]["iledNoofBag"] = bagNo.toString();
  if(this.exitType == 'F'){
    this.lotDetails[index]["qtyInKg"] = bagNo * Number(this.lotDetails[index]["inqty"]);
    for(var j=0;j<this.conversionList.length;j++){
      if(this.conversionList[j].gmucmAltUomId ==  this.quintalUOM  && this.conversionList[j].gmucmUomId ==this.lotDetails[index].bagUom){
        console.log("here",this.lotDetails[index].inqty)
        this.lotDetails[index]["modifiedQtl"] =( Number(this.lotDetails[index].qtyInKg) * Number(this.conversionList[j].gmucmConvFact)).toString()
      }
    }
  if(Number(this.lotDetails[index]["modifiedQtl"]) > Number(this.lotDetails[index]["inqty"])){
    this.finalQtl = this.lotDetails[index]["inqty"];
    this.my_class9 = 'overlay1';
    setTimeout(()=>{
      this.my_class9 = 'overlay';
    },2000);
  }
  }
  else{
    this.lotDetails[index]["qtyInKg"] = bagNo * Number(this.lotDetails[index]["iledGrossQtyQuintal"]);
    for(var j=0;j<this.conversionList.length;j++){
      if(this.conversionList[j].gmucmAltUomId ==  this.quintalUOM  && this.conversionList[j].gmucmUomId ==this.lotDetails[index].gmptmQtyUom){
        console.log("here",this.lotDetails[index].egtGrossQtyQunital)
        this.lotDetails[index]["modifiedQtl"] =( Number(this.lotDetails[index].qtyInKg) * Number(this.conversionList[j].gmucmConvFact)).toString()
      }
    }
    console.log("modified quintal",this.lotDetails[index]["modifiedQtl"],this.lotDetails[index]["iledGrossQtyQuintal"])
  if(Number(this.lotDetails[index]["modifiedQtl"]) > Number(this.lotDetails[index]["iledGrossQtyQuintal"])){
    this.finalQtl = this.lotDetails[index]["iledGrossQtyQuintal"];
    this.my_class9 = 'overlay1';
    setTimeout(()=>{
      this.my_class9 = 'overlay';
    },2000);
  }
  }
}


createLotDetailList=[];
isPresent:boolean;
onCheckBid(post,isChecked,index){
console.log(post,isChecked,index,  (<HTMLInputElement>document.getElementById("checkId_"+index)).checked)

  this.isPresent = false;
  // For Multiple
  if(isChecked == true){
    for(var i = 0; i < this.lotDetails.length; i++) {
      if (this.lotDetails[i].lotId == post.lotId) {
        // this.isPresent = true;
        this.lotDetails[i].checked  = true;
        break;
      }
    }
    // if(this.isPresent == false){
    //   console.log("here",this.createLotDetailList,this.isPresent)
    //  this.createLotDetailList.push(post);
    // }
  }
  else{
    for(var i = 0; i < this.lotDetails.length; i++) {
      if (this.lotDetails[i].lotId == post.lotId) {
        this.lotDetails[i].checked  = false;
        // this.createLotDetailList.splice(i,1);
        break;
      }
    }
  }

  console.log(this.lotDetails)

}
// check lot for trade post
isPresentPost:boolean;
onCheckBidPost(post,isChecked,index){
  this.isPresentPost = false;
  // For Multiple
  if(isChecked == true){
    for(var i = 0; i < this.lotDetails.length; i++) {
      if (this.lotDetails[i].lot == post.lot) {
        // this.isPresentPost = true;
        this.lotDetails[i].checked  = true;
        break;
      }
    }
    // if(this.isPresentPost == false){
    //  console.log("here",this.createLotDetailList,this.isPresentPost)
    //  this.createLotDetailList.push(post);
    // }
  }
  else{
    for(var i = 0; i < this.lotDetails.length; i++) {
      if (this.lotDetails[i].lot == post.lot) {
        this.lotDetails[i].checked  = false;
        break;
      }
    }
  }

  console.log(this.lotDetails)

}


// On Save Gate exit
onSaveGateExit(){
  console.log("this.lotDetails",this.lotDetails)
  if(this.lotDetails.length != 0){
    if(this.vehNo != "" && this.vehNo != undefined){
      if(this.finalGateVal != undefined && this.finalGateVal.length != 0){
        for(var i=0;i<this.lotDetails.length;i++){
            if(this.lotDetails[i].checked == true){
              this.createLotDetailList.push(this.lotDetails[i]);
            }
          }
        if(this.createLotDetailList.length != 0){
          for(var i=0;i<this.lotDetails.length;i++){
              if(this.lotDetails[i].modifiedQtl != undefined){
              if(this.exitType == 'F'){
              this.lotDetails[i].egtGrossQtyQunital = this.lotDetails[i].modifiedQtl;
              this.lotDetails[i].egtQty = this.lotDetails[i].qtyInKg.toString();
              }
              else{
                this.lotDetails[i].egtGrossQtyQunital = this.lotDetails[i].modifiedQtl;
                this.lotDetails[i].egtQty = this.lotDetails[i].qtyInKg.toString();
              }
            }
          }
          var postJson ={
            "exitType": this.exitType,
            "vechileNo": this.vehNo,
            "impType": this.sellerType,
            "gate" : this.finalGateVal
          }
          if(this.exitType == 'F'){
            postJson["lotDetail"] = this.createLotDetailList;
            postJson["deatilEovo"] = this.lotDetailJson;
          }
          else{
            postJson["lotDetailPostTrade"] = this.createLotDetailList;
            postJson["deatilEovoPostTrade"] = this.lotDetailJson;
          }
          console.log("postJson",postJson)
          this.loading = true;
          this.authservice.saveGateExit(postJson).subscribe($data=>{
          this.loading = false;
          this.my_class14 = 'overlay1';
          setTimeout(()=>{
            this.my_class14 = 'overlay';
          },2000);
          this.lotDetailJson = [];
          this.onResetPage();
          }, (err) => {
                      if (err === 'Unauthorized')
                      { this.errorPopupShow();
                        setTimeout(()=>{
                          this.my_class3 = 'overlay';
                          localStorage.clear();
                          this.router.navigateByUrl('/login');
                        },3000);
                      }
                      else{
                        this.my_class4 = 'overlay1';
                        setTimeout(()=>{
                          this.my_class4 = 'overlay';
                        },2000);
                      }
                });
        }
        else{
          this.my_class13 = 'overlay1';
          setTimeout(()=>{
            this.my_class13 = 'overlay';
          },2000);
        }

      }
      else{
        this.my_class12 = 'overlay1';
        setTimeout(()=>{
          this.my_class12 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class11 = 'overlay1';
      setTimeout(()=>{
        this.my_class11 = 'overlay';
      },2000);
    }
  }
  else{
    this.my_class10 = 'overlay1';
    setTimeout(()=>{
      this.my_class10 = 'overlay';
    },2000);
  }
}


// Open Trader Name Popup
traderList =[];
filteredTraderList =[];
openTraderNamePopup(){
  this.my_class15 = 'overlay1';
  this.loading = true;
  this.authservice.getTraderForPostTrade().subscribe(traderList=>{
    console.log("All trader name: ",traderList.listData);
    this.traderList = traderList.listData;
    this.filteredTraderList = traderList.listData;
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
              this.my_class3 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
           }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);

}


// on select trader for post trade exit
selTraderPostVal =[];selTraderPostIndex:number;
onSelTraderForPostTrade(post, index){
  this.selTraderPostVal = post;
  this.selTraderPostIndex = index;
}

// On select trader post ok
finalTraderPostVal = [];traderDesc:string;
onSelTraderPostOK(){
  this.selTraderPostIndex = -1;  this.matchTraderName = this.matchTraderCompanyName = this.matchLicenceNo = "";

  this.finalTraderPostVal = this.selTraderPostVal;
  console.log("this.finalTraderPostVal ",this.finalTraderPostVal )
  this.my_class15='overlay';
  this.traderFirmName = this.finalTraderPostVal["lcamAgenCompName"];
  this.traderDesc = this.finalTraderPostVal["lcamFullName"];
  var postJson = this.finalTraderPostVal;
  this.lotDetailJson = postJson;
  this.loading = true;
  this.authservice.fetchLotDetailsForTraderPost(postJson).subscribe(lotDetails=>{
    console.log("All lotDetails : ",lotDetails.listData);
    this.loading = false;
    if(lotDetails.listData != undefined){
      this.lotDetails = lotDetails.listData;
      for(var i=0;i<this.lotDetails.length;i++){
      this.lotDetails[i].modifiedQtl = this.lotDetails[i].iledGrossQtyQuintal;
      // this.lotDetails[i].qtyInKg = this.lotDetails[i].egtQty;
        for(var j=0;j<this.conversionList.length;j++){
          console.log("this.conversionList[j].gmucmAltUomId",this.conversionList[j].gmucmAltUomId)
          if(this.conversionList[j].gmucmAltUomId == this.lotDetails[i].gmptmQtyUom && this.conversionList[j].gmucmUomId ==this.quintalUOM){
            console.log("here",this.lotDetails[i].iledGrossQtyQuintal)
            this.lotDetails[i]["qtyInKg"] =( Number(this.lotDetails[i].iledGrossQtyQuintal) * Number(this.conversionList[j].gmucmConvFact)).toString()
          }
        }

      }
      for(var i=0;i<this.lotDetails.length;i++){
        if(this.lotDetails[i]["gmptmQtyUom"] == undefined){
          this.uomPresent = false;
          break;
        }
      }
      console.log("this.lotDetails",this.lotDetails)

      if(this.uomPresent == false){
        this.my_class8 = "overlay1";
      }
    }
  }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class3 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
            else{
              this.my_class4 = 'overlay1';
              setTimeout(()=>{
                this.my_class4 = 'overlay';
              },2000);
            }
      }
);
}

// Filter gate no
matchGateNo:string;
filterGateNo(){
  if(this.matchGateNo) {
    this.filteredGateInfo = _.filter(this.gateInfo, (a)=>a.ggmGateNo.indexOf(this.matchGateNo)>=0);
  } else {
    this.filteredGateInfo = this.gateInfo  ;
  }
}

// Filter gate name
matchGateName:string;
filterGateName(){
  this.matchGateName = this.matchGateName.toUpperCase();
  if(this.matchGateName) {
    this.filteredGateInfo = _.filter(this.gateInfo, (a)=>a.ggmName.indexOf(this.matchGateName)>=0);
  } else {
    this.filteredGateInfo = this.gateInfo  ;
  }
}
// Filter seller name
matchSellerName:string;
filterSellerName(){
  this.matchSellerName = this.matchSellerName.toUpperCase();
  var filtList=[];
  for(var i=0;i<this.sellerForTrader.length;i++){
    if(this.sellerForTrader[i].lcamFullName !=undefined){
      filtList.push(this.sellerForTrader[i]);
    }
  }
  if(this.matchSellerName != undefined && this.matchSellerName != ""){
    this.filteredSellerForTrader = _.filter(filtList , (a)=>a.lcamFullName.indexOf(this.matchSellerName)>=0);
  }
  else{
  this.filteredSellerForTrader  = this.sellerForTrader;
  }
}
// Filter Comapnay name
matchCompanyName:string;
filterCompanyName(){
  this.matchCompanyName = this.matchCompanyName.toUpperCase();
  var filtList=[];
  for(var i=0;i<this.sellerForTrader.length;i++){
    if(this.sellerForTrader[i].lcamAgenCompName !=undefined){
      filtList.push(this.sellerForTrader[i]);
    }
  }
  if(this.matchCompanyName != undefined && this.matchCompanyName != ""){
    this.filteredSellerForTrader = _.filter(filtList , (a)=>a.lcamAgenCompName.indexOf(this.matchCompanyName)>=0);
  }
  else{
  this.filteredSellerForTrader  = this.sellerForTrader;
  }
}

// Filter Lot code
matchLotCode:string;
filterLotCode(){
  if(this.matchLotCode) {
    this.filteredLotCodeForFarmer = _.filter(this.lotCodeForFarmer, (a)=>a.iledLotCode.indexOf(this.matchLotCode)>=0);
  } else {
    this.filteredLotCodeForFarmer = this.lotCodeForFarmer  ;
  }
}


// Filter Trader name
matchTraderName:string;
filterTraderName(){
  this.matchTraderName = this.matchTraderName.toUpperCase();
  var filtList=[];
  for(var i=0;i<this.traderList.length;i++){
    if(this.traderList[i].lcamFullName !=undefined){
      filtList.push(this.traderList[i]);
    }
  }
  if(this.matchTraderName != undefined && this.matchTraderName != ""){
    this.filteredTraderList = _.filter(filtList , (a)=>a.lcamFullName.indexOf(this.matchTraderName)>=0);
  }
  else{
  this.filteredTraderList  = this.traderList;
  }
}

// Filter Company name
matchTraderCompanyName:string;
filterTraderCompanyName(){
  this.matchTraderCompanyName = this.matchTraderCompanyName.toUpperCase();
  var filtList=[];
  for(var i=0;i<this.traderList.length;i++){
    if(this.traderList[i].lcamAgenCompName !=undefined){
      filtList.push(this.traderList[i]);
    }
  }
  if(this.matchTraderCompanyName != undefined && this.matchTraderCompanyName != ""){
    this.filteredTraderList = _.filter(filtList , (a)=>a.lcamAgenCompName.indexOf(this.matchTraderCompanyName)>=0);
  }
  else{
  this.filteredTraderList  = this.traderList;
  }
}
// Filter Licence No
matchLicenceNo:string;
filterLicenceNo(){
  var filtList=[];
  for(var i=0;i<this.traderList.length;i++){
    if(this.traderList[i].lcamAgenTradLicenno !=undefined){
      filtList.push(this.traderList[i]);
    }
  }
  if(this.matchLicenceNo != undefined && this.matchLicenceNo != ""){
    this.filteredTraderList = _.filter(filtList , (a)=>a.lcamAgenTradLicenno.indexOf(this.matchLicenceNo)>=0);
  }
  else{
  this.filteredTraderList  = this.traderList;
  }
}


// Filter Lot code
matchBagType:string;
filterBagType(){
  if(this.matchBagType) {
    this.filteredBagTypes = _.filter(this.bagTypes, (a)=>a.gmptm_desc.indexOf(this.matchBagType)>=0);
  } else {
    this.filteredBagTypes = this.bagTypes  ;
  }
}

}
