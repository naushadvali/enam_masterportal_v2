import { Component,Input, OnInit, ElementRef, OnDestroy ,Pipe} from '@angular/core';
import { MdTooltipModule,MdButtonModule, MdCheckboxModule } from '@angular/material';

import { DataTableModule } from "angular2-datatable";
import { AuthService } from '../services/auth.service';
import { SelectModule } from 'ng2-select';
import { FormsModule, ReactiveFormsModule,FormBuilder, Validators ,NG_VALIDATORS} from '@angular/forms';
import { Observable, Subscription } from 'rxjs/Rx';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { IntervalObservable } from "rxjs/observable/IntervalObservable";
import { TimerComponent } from '../timer/timer.component';

import * as _ from 'lodash';
import 'hammerjs';

@Component({
  selector: 'app-newbidlisting',
  templateUrl: './newbidlisting.component.html',
  styleUrls: ['./newbidlisting.component.css'],
  providers: [DatePipe]
})
export class NewbidlistingComponent implements OnInit{

  data={};
  listData=[];
  apmclocation:string;
  location=[];
  agent:string;
  commodity:string;
  lotcodeText:string;
  sellerText:string;
  villageText:string;
  sellerName:string;
  village:string;
  disableConfirm:boolean;
  bidvalue:number;
  minbidvalue:number;
  abcStartDate:string;
  abcEndDate:string;
  abcExtendedEndTime:string;
  abcSubMultBid:string;
  gmpmProdMinValue:string;
  calculatedMaxDeviation:number;
  calculatedMinDeviation:number;
  mainContent=[];
  remaintime:string;
  adoumOprName:string;
  private alive: boolean;
  private timer: Observable<number>;
  private interval: number;
  imgsrc:string="assets/images/arrow-up";

  maxOpenBidValue:number;
  //@Input('diff') msg:number;
  oprID:string;
  userGlobal:string;
  showlocations=[];
  locationTypeM:string;
  userType:string;
  adoumOprID:string;
  agentDetails=[];
  agentNameTypeM=[];
  iledOprID:string;
  iledOrgID:string;
  totalBidders:string;
  villageName:string;
  gmpmgProdName:string;
  iledLotCode:string;
  abcOrgID:string;
  iledNoofBag:string;
  iledQTY:string;
  abcMspBidRate:number;
  remainingTime:string;
  CA:string;
  abcAllowPartial:string;
  abcAutoAssinWiner:string;
  abcBidID:string;
  abcBidOpenDate:string;
  abcBidOpenTime:string;
  abcBidStatus:string;
  abcBidType:string;
  abcLotID:string;
  abcMinBuyers:string;
  abcOpenBidNextDay:string;
  abcOprID:string;
  abcStartTime:string;
  abcTranID:string;
  abcTrnDate:string;
  createdBy:string;
  createdOn:string;
  gmpmgProdCode:string;
  gmpmgProdID:string;
  gmptmDESC:string;
  iledLotDate:string;
  lastBidValue:number;
  wbWeight:string;
  apmcSelection:string;
  agentSelection:string;
  commoditySelection:string;

  uniqadoumOprName:any=[];
  uniqResultadoumOprName:any=[];
  uniqCA:any=[];
  uniqResultCA:any=[];
  uniqgmpmgProdName:any=[];
  uniqResultgmpmgProdName:any=[];
  statusCode:any;
  successMsg:any="Bid successfull";

  failMsg:any="Bid unsuccessfull";
  disableBid:boolean;
  sellerUpper:any;
  villageUpper:any;
  idxVal1:any;
  idxVal:any;
  isValid:boolean;
  newbidlistingError:any;
  certificateData=[];
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';



  public loading = false;
  public filteredData;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private datepipe:DatePipe,

  ) {
      this.disableConfirm=true;
      this.alive = true;
      this.interval = 120000;
      this.timer = Observable.timer(0, this.interval);
    }
  //EXPORT TO EXEL
  tableToExcel(table, name){
    let uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }

  ngOnInit() {

    this.location.push(JSON.parse(localStorage.getItem('arrayLocation')));


    //console.log("Newbidlisting location: ",(this.location[0]));
    this.oprID=localStorage.getItem('oprID');
    this.userGlobal=localStorage.getItem('userGlobal');
    this.locationTypeM=localStorage.getItem('locationTypeM');
    this.userType=localStorage.getItem('userType');
    this.adoumOprID=localStorage.getItem('adoumOprID');

    // this.newbidlistingError=sessionStorage.getItem('newbidlistingError');
    // if(this.newbidlistingError==0){
    //    this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
    //  }

      this.loading = true;
      this.authservice.getNewBidListing(this.oprID).subscribe(
        data=>{
          this.loading=false;
          //console.log("the data from NEWBIDLISTING: ",data);
          let $data=data;
          this.mainContent=$data.listData;
          this.filteredData=this.mainContent;



//console.log("true",this.filteredData);
          let abcBidType:string;
          let remaintime:string;
          let mbidvalue:number;
          let abcStartDate:string;
          let abcEndDate:string;
          let abcExtendedEndTime:string;
          let abcSubMultBid:string;
          let gmpmProdMinValue:string;
          let calculatedMaxDeviation:number;
          let calculatedMinDeviation:number;
          let adoumOprName:string;
          let lastBidValue:number;
          let maxOpenBidValue:number;
          let CA:string;
          for(let j=0;j<this.mainContent.length;j++){
            //console.log("the main contents are",this.mainContent[j]);
            //console.log("this.is extended time",this.mainContent[i].abcExtendedEndTime);
             this.abcOprID=this.mainContent[j].abcOprID;
             this.abcLotID=this.mainContent[j].abcLotID;
            // console.log("abcOprID is",this.abcOprID);
            // console.log("abcLotID is",this.abcLotID);

            remaintime=this.mainContent[j].remainingTime;
            CA=this.mainContent[j].CA;
            mbidvalue=this.mainContent[j].abcMspBidRate;
            abcStartDate=this.mainContent[j].abcStartDate;
            abcEndDate=this.mainContent[j].abcEndDate;
            abcExtendedEndTime=this.mainContent[j].abcExtendedEndTime;
            abcSubMultBid=this.mainContent[j].abcSubMultBid;
            gmpmProdMinValue=this.mainContent[j].gmpmProdMinValue;
            calculatedMaxDeviation=this.mainContent[j].calculatedMaxDeviation;
            calculatedMinDeviation=this.mainContent[j].calculatedMinDeviation;
            adoumOprName=this.mainContent[j].adoumOprName;
            lastBidValue=this.mainContent[j].lastBidValue;
            maxOpenBidValue=this.mainContent[j].maxOpenBidValue;
            abcBidType=this.mainContent[j].abcBidType;

            this.uniqadoumOprName.push(this.mainContent[j].adoumOprName);
            this.uniqCA.push(this.mainContent[j].CA);
            this.uniqgmpmgProdName.push(this.mainContent[j].gmpmgProdName);
            this.abcMspBidRate=mbidvalue;
            this.abcBidType=abcBidType;
            this.lastBidValue=lastBidValue;
            this.lastBidValue.toString();
            this.abcSubMultBid=abcSubMultBid;
            this.maxOpenBidValue=maxOpenBidValue;
            if(this.maxOpenBidValue==undefined || this.maxOpenBidValue==null){
              this.maxOpenBidValue=0;
            }

            if(this.abcBidType=='C' && this.abcSubMultBid=='Y'){
              this.maxOpenBidValue=0.00;
            }
            if(this.abcBidType=='C' && this.abcSubMultBid=='N'){
              this.maxOpenBidValue=0.00;
            }
            if(this.mainContent[j].lastBidValue==this.mainContent[j].maxOpenBidValue){
              this.imgsrc="assets/images/arrow-up.png";

            }
            else{
              this.imgsrc="assets/images/arrow-down.png";
            }
          }



          this.uniqResultadoumOprName=this.uniqadoumOprName.filter((x, i, a) => x && a.indexOf(x) === i);
          this.uniqResultCA=this.uniqCA.filter((x, i, a) => x && a.indexOf(x) === i);
          this.uniqResultgmpmgProdName=this.uniqgmpmgProdName.filter((x, i, a) => x && a.indexOf(x) === i);

          let test=this.datepipe.transform(Date.now(),"MMMM dd yyyy");

          this.adoumOprName=adoumOprName;

          this.lastBidValue=lastBidValue;
          this.maxOpenBidValue=maxOpenBidValue;
          this.abcStartDate=abcStartDate;
          this.abcEndDate=abcEndDate;
          this.abcExtendedEndTime=abcExtendedEndTime;
          this.abcSubMultBid=abcSubMultBid;
          this.gmpmProdMinValue=gmpmProdMinValue;
          this.calculatedMaxDeviation=calculatedMaxDeviation;
          this.calculatedMinDeviation=calculatedMinDeviation;
          this.loading=false;

        })

    }



    test(value, index){

      if(value <= 0){
        this.mainContent[index].abcSubMultBid = "N"
      }

    }
    //Button BidNow Validation check
    chkBid(post:any,index){
      document.getElementById("focus1").focus();
      //console.log("chkbid called: ",post);
      this.bidvalue=null;
      this.CA=post.CA;
      this.abcAllowPartial=post.abcAllowPartial;
      this.abcAutoAssinWiner=post.abcAutoAssignWiner;
      this.abcBidID=post.abcBidID;
      this.abcBidOpenDate=post.abcBidOpenDate;
      this.abcBidOpenTime=post.abcBidOpenTime;
      this.abcBidStatus=post.abcBidStatus;
      this.abcBidType=post.abcBidType;
      this.createdBy=post.abcCreatedBy;
      this.createdOn=post.abcCreatedOnDate;
      this.abcEndDate=post.abcEndDate;
      this.abcLotID=post.abcLotID;
      this.abcMinBuyers=post.abcMinBuyers;
      this.abcMspBidRate=post.abcMspBidRate;
      this.abcOpenBidNextDay=post.abcOpenBidNextDay;
      this.abcOprID=post.abcOprID;
      this.abcOrgID=post.abcOrgID;
      this.abcStartDate=post.abcStartDate;
      this.abcStartTime=post.abcStartTime;
      this.abcSubMultBid=post.abcSubMultBid;
      this.abcTranID=post.abcTranID;
      this.abcTrnDate=post.abcTrnDate;
      this.adoumOprName=post.adoumOprName;
      this.gmpmgProdCode=post.gmpmgProdCode;
      this.gmpmgProdID=post.gmpmgProdID;
      this.gmpmgProdName=post.gmpmgProdName;
      this.gmptmDESC=post.gmptmDESC;
      this.iledLotCode=post.iledLotCode;
      this.iledLotDate=post.iledLotDate;
      this.iledNoofBag=post.iledNoofBag;
      this.iledOprID=post.iledOprID;
      this.iledOrgID=post.iledOrgID;
      this.iledQTY=post.iledQTY;
      this.lastBidValue=post.lastBidValue;
      this.remainingTime=post.remainingTime;
      this.sellerName=post.sellerName;
      this.totalBidders=post.totalBidders;
      this.villageName=post.villageName;
      this.wbWeight=post.wbWeight;
      this.maxOpenBidValue=post.maxOpenBidValue;
      //console.log("Your mspbidrate is: ",this.abcMspBidRate," last bid value is: ",this.maxOpenBidValue," your bid value is: ",this.lastBidValue);
      if(post.maxOpenBidValue==undefined || post.maxOpenBidValue==null){
        this.maxOpenBidValue=0;
        //console.log("undefined maxOpenBidValue is: ",this.maxOpenBidValue);
      }

    }
    bidNow(){

      if((this.abcSubMultBid=="Y")||(this.abcSubMultBid=='N')){
        this.getBid();
        //console.log("yehh!!! you are allowed");
        return true;
        }
      }


    finalBid(){
      const bidnow={
        CA:this.CA,
        abcAllowPartial:this.abcAllowPartial,
        abcAutoAssinWiner:this.abcAutoAssinWiner,
        abcBidID:this.abcBidID,
        abcBidOpenDate:this.abcBidOpenDate,
        abcBidOpenTime:this.abcBidOpenTime,
        abcBidStatus:this.abcBidStatus,
        abcBidType:this.abcBidType,
        createdBy:this.createdBy,
        createdOn:this.createdOn,
        abcEndDate:this.abcEndDate,
        abcLotID:this.abcLotID,
        abcMinBuyers:this.abcMinBuyers,
        abcMspBidRate:this.abcMspBidRate,
        abcOpenBidNextDay:this.abcOpenBidNextDay,
        abcOprID:this.abcOprID,
        abcOrgID:this.abcOrgID,
        abcStartDate:this.abcStartDate,
        abcStartTime:this.abcStartTime,
        abcSubMultBid:this.abcSubMultBid,
        abcTranID:this.abcTranID,
        abcTrnDate:this.abcTrnDate,
        adoumOprName:this.adoumOprName,
        gmpmgProdCode:this.gmpmgProdCode,
        gmpmgProdID:this.gmpmgProdID,
        gmpmgProdName:this.gmpmgProdName,
        gmptmDESC:this.gmptmDESC,
        iledLotCode:this.iledLotCode,
        iledLotDate:this.iledLotDate,
        iledNoofBag:this.iledNoofBag,
        iledOprID:this.iledOprID,
        iledOrgID:this.iledOrgID,
        iledQTY:this.iledQTY,
        lastBidValue:this.lastBidValue,
        remainingTime:this.remainingTime,
        sellerName:this.sellerName,
        totalBidders:this.totalBidders,
        villageName:this.villageName,
        wbWeight:this.wbWeight,
        newBidAmount:this.bidvalue,

      }

      //console.log("Your bid Amount is: ",this.bidvalue);
      this.authservice.bidNow(bidnow).subscribe($data=>{
        //console.log("after bidding Data ",$data);
        if($data.status==1){
            this.statusCode=$data.status;
            this.successMsg;
            this.resultShow1();
            this.closeButton1();
            this.closeButton2();
            //this.closeButton3();
            setTimeout(()=>{
              this.my_class3 = 'overlay';
            },1680);
            //this.my_class3='overlay';
            this.disableConfirm=false;
            this.ngOnInit();
            this.bidvalue=null;


        }else if($data.status==0){
          this.statusCode=$data.status;

          this.failMsg;
          this.my_class1='overlay';
          this.my_class2='overlay';
          setTimeout(()=>{
            this.my_class3 = 'overlay';
          },1680);
          //this.my_class3='overlay';
          this.ngOnInit();

        }

      }),err => console.log(err);

    }
    // Here Overlay1 is Showing the popup and Overlay is for hiding the popup

    viewCertificate(){
      const abcOprID=this.abcOprID;
      const abcLotID=this.abcLotID;
      // console.log("abcOprID is",this.abcOprID);
      // console.log("abcLotID is",this.abcLotID);
      this.authservice.getCertificate(abcOprID,abcLotID).subscribe($data=>{
        this.certificateData=$data.listData;
        // console.log("Certificate data comes here",this.certificateData);
      })
      this.my_class4='overlay1';
    }
    seeTradeResult(){
      const abcOprID=this.abcOprID;
      const abcLotID=this.abcLotID;
      this.authservice.getTradeResult(abcLotID,abcOprID).subscribe(
        (res) => {

          var fileURL = URL.createObjectURL(res);

          window.open(fileURL);
          
          }
      )
    }

    div_show(){
      this.my_class1='overlay1';
    }
    div1_show(){
      this.my_class2='overlay1';
    }
    resultShow1(){
      this.my_class3='overlay1';
    }





    div1_hide(){
      this.my_class2='overlay';
    }
    closeButton1(){
      this.my_class1='overlay';
    }
    closeButton2(){

      this.my_class2='overlay';
    }
    closeButton3(){
      this.my_class3='overlay';
    }
    closeButton4(){
      this.my_class4='overlay';
    }


    //  resultHide1() {
    //    document.getElementById('popup5').style.opacity = "0";
    //    document.getElementById('popup5').style.visibility = "hidden";
    //   }
      //
      // onCertificate(){
      //   console.log("clicked");
      // }


    getBid(){

       if(this.bidvalue < this.abcMspBidRate){
         //console.log("this.bidvalue < this.abcMspBidRate clicked");
         this.flashMessagesService.show('Bid value must be greater than minimum bid value', { cssClass: 'alert-danger', timeout: 3000 });
         this.disableConfirm=true;
      }
    else if(this.bidvalue==null || this.bidvalue==undefined || this.bidvalue==0){
        //console.log("bid value is null called");
        this.flashMessagesService.show('Bid value can not be null', { cssClass: 'alert-danger', timeout: 3000 });
        this.disableConfirm=true;
      }

      else if((this.bidvalue<this.maxOpenBidValue) || (this.bidvalue==this.maxOpenBidValue)){
        //console.log("(this.bidvalue<this.maxOpenBidValue) || (this.bidvalue==this.maxOpenBidValue) clicked");
        this.flashMessagesService.show('Bidding Value must be greater than Last bid value', { cssClass: 'alert-danger', timeout: 3000 });
        this.disableConfirm=true;
      }
      else if((this.bidvalue==0)||(this.bidvalue<0)){
        this.flashMessagesService.show('Bidding Value must be greater than 0', { cssClass: 'alert-danger', timeout: 3000 });
        this.disableConfirm=true;
      }
    }

    getBidListing(){

      this.lotcodeText;
      this.sellerText;
      this.villageText;
      this.apmclocation;
      this.agentSelection;
      this.commoditySelection;

    }


    searchLotcode(query:string){
      //console.log("lotcode search",query);
      //this.filteredData=this.data;
      if(query) {
        this.filteredData = _.filter(this.mainContent, (a)=>a.abcLotID.indexOf(query)>=0);
      } else {
        this.filteredData = this.mainContent  ;
      }

    }
    searchSeller(query:string){
      //console.log("Seller search",query);
      //console.log("Seller upper",this.sellerUpper);
      this.sellerUpper=query.toUpperCase();
      if(this.sellerUpper) {
        this.filteredData = _.filter(this.mainContent, (a)=>a.sellerName.indexOf(this.sellerUpper)>=0);
      } else {
        this.filteredData = this.mainContent  ;
      }
    }
    searchVillage(query:string){
      //console.log("Village search",query);
      this.villageUpper=query;
      if(this.villageUpper) {
        this.filteredData = _.filter(this.mainContent, (a)=>a.villageName.indexOf(this.villageUpper)>=0);
      } else {
        this.filteredData = this.mainContent  ;
      }
    }

    onApmcSelection(val){
      //console.log("Selected Location value:",val);
      this.apmclocation=val;
      if(val!="all") {
        this.filteredData = _.filter(this.mainContent, (a)=>a.adoumOprName.indexOf(val)>=0);
      }
      if(this.apmclocation==="all"){
        this.filteredData = this.mainContent  ;
      }

    }


    onAgentSelection(val){
      //console.log("Selected Agent value:",val);
      this.agentSelection=val;
      if(val!="all") {
        this.filteredData = _.filter(this.mainContent, (a)=>a.CA.indexOf(val)>=0);
      }
      if(this.agentSelection==="all"){
        //console.log("agent selection filter called all",this.filteredData)
        this.filteredData = this.mainContent  ;
      }
    }

    onCommoditySelection(val){
      //console.log("Selected commodity value:",val);
      this.commoditySelection=val;
      if(val!="all") {
        this.filteredData = _.filter(this.mainContent, (a)=>a.gmpmgProdName.indexOf(val)>=0);
      }
      if(this.commoditySelection==="all"){
        this.filteredData = this.mainContent  ;
      }
    }

  }
