import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewbidlistingComponent } from './newbidlisting.component';

describe('NewbidlistingComponent', () => {
  let component: NewbidlistingComponent;
  let fixture: ComponentFixture<NewbidlistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewbidlistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewbidlistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
