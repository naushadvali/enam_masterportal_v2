import { Component ,Attribute,OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ClockService } from '../services/clock.service';
import { Router } from '@angular/router';
import {SelectModule} from 'ng2-select';
 import { LocalStorageModule } from 'angular-2-local-storage';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DOCUMENT } from '@angular/platform-browser';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';

@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})
  export class NavbarComponent implements OnInit {
  /*CurrentDate = Date.now();*/
  time: Date;
  state=[];
  items=[];
  adoumOprID:any;
  adoumOprName:StringConstructor;
  adoumOrgID:any;
  data={};
  listData=[];
  apmclocation:string;
  agent:string;
  commodity:string;
  lotcode:string;
  seller:string;
  village:string;
  location:string;
  location1:string;
  userEmailID:any;
  userName:any;
  userPhoneNo:any;
  email:string;
  phone:any;
  pwd:any;
  cpwd:any;
  locationList:any;
  typeMLocation:string;
  chkDisable:boolean=true;
  userType:string;
  emailId:any;
  logoutError:any;
  public my_class1='overlay';
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  constructor(
    private clockService: ClockService,
    public authservice:AuthService,
    private router:Router,
    private flashMessagesService: FlashMessagesService,
    private idle: Idle,
    private keepalive: Keepalive
  ) {
        this.location=localStorage.getItem('apmcSlocation');
        this.typeMLocation=localStorage.getItem('locationTypeM');
        this.userType=localStorage.getItem('userType');


  }

  onLogoutClick(){
    this.logoutError=localStorage.getItem('logoutError');
    if(this.logoutError==0){
        //console.log("Connection error in logout");
        this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
      }
    this.authservice.loggedOut().subscribe(
      ret=>{
        //let $ret=ret;
        //console.log("Logout response from server: ",ret);
        localStorage.clear();
        this.router.navigate(['/login']);
      }
      , (err) => {
            if (err === 'Unauthorized')
            {
              localStorage.clear();
              this.router.navigateByUrl('/login');
        }
      }
    );

  }

  getDark(){
      //console.log("Dark theme called");
      document.getElementById('chngTheme').setAttribute('href', 'assets/custom/css/theme_dark.css');
  }

  getLight(){
      //console.log("light theme called");
      document.getElementById('chngTheme').setAttribute('href', 'assets/custom/css/AdminLTE.min.css');
  }

  ngOnInit() {
    if((this.authservice.result) != undefined){

    }
    else{
      //console.log("Result in navbar is undefined");

    }


    this.clockService.getClock().subscribe(time => this.time = time);
    //this.location=(this.authservice.locationName);
    this.location=localStorage.getItem('location');
    this.typeMLocation=localStorage.getItem('locationTypeM');
    //console.log("Loc in Nav",this.location);
    this.userName=localStorage.getItem('userName');
    this.userPhoneNo=localStorage.getItem('userPhoneNo');
    this.userType=localStorage.getItem('userType');
    this.typeMLocation=localStorage.getItem('locationTypeM');
    this.emailId=localStorage.getItem('userEmailID');


  }

  validateform(){
    //console.log("Email id",this.userEmailID);
    this.pwd='';
    this.cpwd='';
    const sendPreference=
      {
        password:this.pwd,
        cPassWord:this.cpwd,
        contactNo:this.userPhoneNo,
        emailID:this.emailId,
        name:this.userName
      }
      //console.log("data are: ",this.pwd,this.cpwd,this.userPhoneNo,this.userEmailID,this.userName);
      if(this.pwd=='' && this.cpwd==''){
        this.flashMessagesService.show('fields can not be blank', { cssClass: 'alert-Success', timeout: 3000 });
      }
      else{
        this.authservice.getPasswordChange(sendPreference).subscribe(
            $ret=>{
              //console.log("Edit password reply from server",$ret);
              if($ret.status==1){

                this.flashMessagesService.show('Your Preferences was updated', { cssClass: 'alert-Success', timeout: 3000 });

              }else{
                this.flashMessagesService.show('Your Preferences was not updated', { cssClass: 'alert-Success', timeout: 3000 });
              }

            }
            , (err) => {
                        if (err === 'Unauthorized')
                        {
                          localStorage.clear();
                          this.router.navigateByUrl('/login');

                    }
                  }

          )
      }
      //this.flashMessagesService.show('Successfully Changed', { cssClass: 'alert-Success', timeout: 3000 });
  }
  pop_show(){
    this.my_class1='overlay1';
  }
  pop_hide(){
    this.my_class1='overlay';
  }

}
