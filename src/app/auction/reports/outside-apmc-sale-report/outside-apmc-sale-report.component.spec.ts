import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsideApmcSaleReportComponent } from './outside-apmc-sale-report.component';

describe('OutsideApmcSaleReportComponent', () => {
  let component: OutsideApmcSaleReportComponent;
  let fixture: ComponentFixture<OutsideApmcSaleReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideApmcSaleReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideApmcSaleReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
