import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotProgressDetailsReportComponent } from './lot-progress-details-report.component';

describe('LotProgressDetailsReportComponent', () => {
  let component: LotProgressDetailsReportComponent;
  let fixture: ComponentFixture<LotProgressDetailsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotProgressDetailsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotProgressDetailsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
