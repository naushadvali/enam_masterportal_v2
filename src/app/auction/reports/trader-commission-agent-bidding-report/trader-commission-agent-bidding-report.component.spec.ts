import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderCommissionAgentBiddingReportComponent } from './trader-commission-agent-bidding-report.component';

describe('TraderCommissionAgentBiddingReportComponent', () => {
  let component: TraderCommissionAgentBiddingReportComponent;
  let fixture: ComponentFixture<TraderCommissionAgentBiddingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraderCommissionAgentBiddingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderCommissionAgentBiddingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
