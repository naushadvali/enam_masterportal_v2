import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatewiseCommoditySaleReportComponent } from './statewise-commodity-sale-report.component';

describe('StatewiseCommoditySaleReportComponent', () => {
  let component: StatewiseCommoditySaleReportComponent;
  let fixture: ComponentFixture<StatewiseCommoditySaleReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatewiseCommoditySaleReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatewiseCommoditySaleReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
