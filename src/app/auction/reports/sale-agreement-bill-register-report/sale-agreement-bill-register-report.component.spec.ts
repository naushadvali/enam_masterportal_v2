import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleAgreementBillRegisterReportComponent } from './sale-agreement-bill-register-report.component';

describe('SaleAgreementBillRegisterReportComponent', () => {
  let component: SaleAgreementBillRegisterReportComponent;
  let fixture: ComponentFixture<SaleAgreementBillRegisterReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleAgreementBillRegisterReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleAgreementBillRegisterReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
