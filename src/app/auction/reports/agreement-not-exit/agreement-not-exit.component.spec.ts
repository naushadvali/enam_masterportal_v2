import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgreementNotExitComponent } from './agreement-not-exit.component';

describe('AgreementNotExitComponent', () => {
  let component: AgreementNotExitComponent;
  let fixture: ComponentFixture<AgreementNotExitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgreementNotExitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgreementNotExitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
