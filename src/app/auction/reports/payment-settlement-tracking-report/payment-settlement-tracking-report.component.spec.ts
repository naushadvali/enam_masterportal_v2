import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSettlementTrackingReportComponent } from './payment-settlement-tracking-report.component';

describe('PaymentSettlementTrackingReportComponent', () => {
  let component: PaymentSettlementTrackingReportComponent;
  let fixture: ComponentFixture<PaymentSettlementTrackingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentSettlementTrackingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSettlementTrackingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
