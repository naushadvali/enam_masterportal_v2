import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandiwiseTradeSummaryComponent } from './mandiwise-trade-summary.component';

describe('MandiwiseTradeSummaryComponent', () => {
  let component: MandiwiseTradeSummaryComponent;
  let fixture: ComponentFixture<MandiwiseTradeSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandiwiseTradeSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandiwiseTradeSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
