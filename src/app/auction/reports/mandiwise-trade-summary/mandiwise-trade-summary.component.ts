import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';

@Component({
  selector: 'app-mandiwise-trade-summary',
  templateUrl: './mandiwise-trade-summary.component.html',
  styleUrls: ['./mandiwise-trade-summary.component.css']
})
export class MandiwiseTradeSummaryComponent implements OnInit {

  public my_class1='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor() { }

  ngOnInit() {
  }

  div_show1(){
        this.my_class1='overlay1';
      }
  div_hide1(){
        this.my_class1='overlay';
      }
}
