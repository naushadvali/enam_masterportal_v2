import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPerformanceReportComponent } from './daily-performance-report.component';

describe('DailyPerformanceReportComponent', () => {
  let component: DailyPerformanceReportComponent;
  let fixture: ComponentFixture<DailyPerformanceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyPerformanceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyPerformanceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
