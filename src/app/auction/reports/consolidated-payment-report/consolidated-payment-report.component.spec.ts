import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidatedPaymentReportComponent } from './consolidated-payment-report.component';

describe('ConsolidatedPaymentReportComponent', () => {
  let component: ConsolidatedPaymentReportComponent;
  let fixture: ComponentFixture<ConsolidatedPaymentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidatedPaymentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidatedPaymentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
