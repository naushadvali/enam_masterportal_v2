import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';


@Component({
  selector: 'app-proforma',
  templateUrl: './proforma.component.html',
  styleUrls: ['./proforma.component.css']
})
export class ProformaComponent implements OnInit {
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor() { }

  ngOnInit() {
  }

}
