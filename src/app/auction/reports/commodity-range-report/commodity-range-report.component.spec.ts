import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityRangeReportComponent } from './commodity-range-report.component';

describe('CommodityRangeReportComponent', () => {
  let component: CommodityRangeReportComponent;
  let fixture: ComponentFixture<CommodityRangeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityRangeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityRangeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
