import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';

@Component({
  selector: 'app-commodity-range-report',
  templateUrl: './commodity-range-report.component.html',
  styleUrls: ['./commodity-range-report.component.css']
})
export class CommodityRangeReportComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor() { }

  ngOnInit() {
  }


  div_show1(){
        this.my_class1='overlay1';
      }
  div_hide1(){
        this.my_class1='overlay';
      }
  div_show2(){
        this.my_class2='overlay1';
      }
  div_hide2(){
        this.my_class2='overlay';
      }
}
