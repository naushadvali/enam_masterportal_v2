import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleAgreementTypewiseComponent } from './sale-agreement-typewise.component';

describe('SaleAgreementTypewiseComponent', () => {
  let component: SaleAgreementTypewiseComponent;
  let fixture: ComponentFixture<SaleAgreementTypewiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleAgreementTypewiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleAgreementTypewiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
