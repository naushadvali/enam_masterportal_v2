import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-sale-agreement-typewise',
  templateUrl: './sale-agreement-typewise.component.html',
  styleUrls: ['./sale-agreement-typewise.component.css']
})
export class SaleAgreementTypewiseComponent implements OnInit {
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor(
   private _dataTable:DataTableModule,
   private authservice:AuthService,
   private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

}
