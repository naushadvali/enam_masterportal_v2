import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyScReportComponent } from './weekly-sc-report.component';

describe('WeeklyScReportComponent', () => {
  let component: WeeklyScReportComponent;
  let fixture: ComponentFixture<WeeklyScReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyScReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyScReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
