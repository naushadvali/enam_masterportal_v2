import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleAgreementReportPageComponent } from './sale-agreement-report-page.component';

describe('SaleAgreementReportPageComponent', () => {
  let component: SaleAgreementReportPageComponent;
  let fixture: ComponentFixture<SaleAgreementReportPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleAgreementReportPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleAgreementReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
