import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftwiseBidOpeningWinnerlistComponent } from './shiftwise-bid-opening-winnerlist.component';

describe('ShiftwiseBidOpeningWinnerlistComponent', () => {
  let component: ShiftwiseBidOpeningWinnerlistComponent;
  let fixture: ComponentFixture<ShiftwiseBidOpeningWinnerlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftwiseBidOpeningWinnerlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftwiseBidOpeningWinnerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
