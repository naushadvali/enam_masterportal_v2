import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngateNotAgreementComponent } from './ingate-not-agreement.component';

describe('IngateNotAgreementComponent', () => {
  let component: IngateNotAgreementComponent;
  let fixture: ComponentFixture<IngateNotAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngateNotAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngateNotAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
