import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandiwiseAgreementSummaryReportComponent } from './mandiwise-agreement-summary-report.component';

describe('MandiwiseAgreementSummaryReportComponent', () => {
  let component: MandiwiseAgreementSummaryReportComponent;
  let fixture: ComponentFixture<MandiwiseAgreementSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandiwiseAgreementSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandiwiseAgreementSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
