import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-bid-creation-report',
  templateUrl: './bid-creation-report.component.html',
  styleUrls: ['./bid-creation-report.component.css']
})
export class BidCreationReportComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }


  div_show1(){
      this.my_class1='overlay1';
    }
div_hide1(){
      this.my_class1='overlay';
    }
div_show2(){
  this.my_class2='overlay1';
}
div_hide2(){
      this.my_class2='overlay';
    }
div_show3(){
  this.my_class3='overlay1';
  }
div_hide3(){
      this.my_class3='overlay';
    }
div_show4(){
  this.my_class4='overlay1';
}
div_hide4(){
      this.my_class4='overlay';
    }

}
