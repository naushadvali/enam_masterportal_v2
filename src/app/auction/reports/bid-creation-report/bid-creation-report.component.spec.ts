import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidCreationReportComponent } from './bid-creation-report.component';

describe('BidCreationReportComponent', () => {
  let component: BidCreationReportComponent;
  let fixture: ComponentFixture<BidCreationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidCreationReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidCreationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
