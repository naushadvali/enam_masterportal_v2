import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityWiseAssayingComponent } from './commodity-wise-assaying.component';

describe('CommodityWiseAssayingComponent', () => {
  let component: CommodityWiseAssayingComponent;
  let fixture: ComponentFixture<CommodityWiseAssayingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityWiseAssayingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityWiseAssayingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
