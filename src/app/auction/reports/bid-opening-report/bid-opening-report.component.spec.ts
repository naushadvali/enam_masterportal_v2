import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidOpeningReportComponent } from './bid-opening-report.component';

describe('BidOpeningReportComponent', () => {
  let component: BidOpeningReportComponent;
  let fixture: ComponentFixture<BidOpeningReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidOpeningReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidOpeningReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
