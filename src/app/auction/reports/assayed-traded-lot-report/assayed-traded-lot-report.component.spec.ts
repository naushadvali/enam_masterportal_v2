import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssayedTradedLotReportComponent } from './assayed-traded-lot-report.component';

describe('AssayedTradedLotReportComponent', () => {
  let component: AssayedTradedLotReportComponent;
  let fixture: ComponentFixture<AssayedTradedLotReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssayedTradedLotReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssayedTradedLotReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
