import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsideApmcSaleRegisterComponent } from './outside-apmc-sale-register.component';

describe('OutsideApmcSaleRegisterComponent', () => {
  let component: OutsideApmcSaleRegisterComponent;
  let fixture: ComponentFixture<OutsideApmcSaleRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideApmcSaleRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideApmcSaleRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
