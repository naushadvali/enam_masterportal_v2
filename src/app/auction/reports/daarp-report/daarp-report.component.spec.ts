import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaarpReportComponent } from './daarp-report.component';

describe('DaarpReportComponent', () => {
  let component: DaarpReportComponent;
  let fixture: ComponentFixture<DaarpReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaarpReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaarpReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
