import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saleagreement-salebill',
  templateUrl: './saleagreement-salebill.component.html',
  styleUrls: ['./saleagreement-salebill.component.css']
})
export class SaleagreementSalebillComponent implements OnInit {
  public my_class1='overlay';
  public my_class2='overlay';
  constructor() { }

  ngOnInit() {
  }
  
  div_show1(){
        this.my_class1='overlay1';
      }
  div_hide1(){
        this.my_class1='overlay';
      }
  div_show2(){
        this.my_class2='overlay1';
  }
  div_hide2(){
        this.my_class2='overlay';
      }
}
