import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleagreementSalebillComponent } from './saleagreement-salebill.component';

describe('SaleagreementSalebillComponent', () => {
  let component: SaleagreementSalebillComponent;
  let fixture: ComponentFixture<SaleagreementSalebillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleagreementSalebillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleagreementSalebillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
