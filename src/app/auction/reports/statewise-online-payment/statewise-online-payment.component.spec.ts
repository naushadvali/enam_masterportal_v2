import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatewiseOnlinePaymentComponent } from './statewise-online-payment.component';

describe('StatewiseOnlinePaymentComponent', () => {
  let component: StatewiseOnlinePaymentComponent;
  let fixture: ComponentFixture<StatewiseOnlinePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatewiseOnlinePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatewiseOnlinePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
