import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionAgentLotdetailsAftrWeighmentComponent } from './commission-agent-lotdetails-aftr-weighment.component';

describe('CommissionAgentLotdetailsAftrWeighmentComponent', () => {
  let component: CommissionAgentLotdetailsAftrWeighmentComponent;
  let fixture: ComponentFixture<CommissionAgentLotdetailsAftrWeighmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionAgentLotdetailsAftrWeighmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionAgentLotdetailsAftrWeighmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
