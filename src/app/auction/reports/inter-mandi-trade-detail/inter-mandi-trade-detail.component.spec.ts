import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterMandiTradeDetailComponent } from './inter-mandi-trade-detail.component';

describe('InterMandiTradeDetailComponent', () => {
  let component: InterMandiTradeDetailComponent;
  let fixture: ComponentFixture<InterMandiTradeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterMandiTradeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterMandiTradeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
