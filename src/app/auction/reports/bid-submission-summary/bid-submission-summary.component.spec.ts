import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidSubmissionSummaryComponent } from './bid-submission-summary.component';

describe('BidSubmissionSummaryComponent', () => {
  let component: BidSubmissionSummaryComponent;
  let fixture: ComponentFixture<BidSubmissionSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidSubmissionSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidSubmissionSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
