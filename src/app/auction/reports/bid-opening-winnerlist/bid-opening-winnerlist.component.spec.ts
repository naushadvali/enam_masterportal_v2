import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidOpeningWinnerlistComponent } from './bid-opening-winnerlist.component';

describe('BidOpeningWinnerlistComponent', () => {
  let component: BidOpeningWinnerlistComponent;
  let fixture: ComponentFixture<BidOpeningWinnerlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidOpeningWinnerlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidOpeningWinnerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
