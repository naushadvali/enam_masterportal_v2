import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandiwiseTradedCommodityReportComponent } from './mandiwise-traded-commodity-report.component';

describe('MandiwiseTradedCommodityReportComponent', () => {
  let component: MandiwiseTradedCommodityReportComponent;
  let fixture: ComponentFixture<MandiwiseTradedCommodityReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandiwiseTradedCommodityReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandiwiseTradedCommodityReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
