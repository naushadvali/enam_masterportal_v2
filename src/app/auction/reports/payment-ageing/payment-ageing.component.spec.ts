import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAgeingComponent } from './payment-ageing.component';

describe('PaymentAgeingComponent', () => {
  let component: PaymentAgeingComponent;
  let fixture: ComponentFixture<PaymentAgeingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentAgeingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAgeingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
