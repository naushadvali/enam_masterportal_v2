import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidSubmissionReportComponent } from './bid-submission-report.component';

describe('BidSubmissionReportComponent', () => {
  let component: BidSubmissionReportComponent;
  let fixture: ComponentFixture<BidSubmissionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidSubmissionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidSubmissionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
