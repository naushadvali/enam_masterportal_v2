import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityTradeInfoComponent } from './commodity-trade-info.component';

describe('CommodityTradeInfoComponent', () => {
  let component: CommodityTradeInfoComponent;
  let fixture: ComponentFixture<CommodityTradeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityTradeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityTradeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
