import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionAgentwiseSoldunsoldLotReportComponent } from './commission-agentwise-soldunsold-lot-report.component';

describe('CommissionAgentwiseSoldunsoldLotReportComponent', () => {
  let component: CommissionAgentwiseSoldunsoldLotReportComponent;
  let fixture: ComponentFixture<CommissionAgentwiseSoldunsoldLotReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionAgentwiseSoldunsoldLotReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionAgentwiseSoldunsoldLotReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
