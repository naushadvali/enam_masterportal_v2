import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotTrackerDetailReportComponent } from './lot-tracker-detail-report.component';

describe('LotTrackerDetailReportComponent', () => {
  let component: LotTrackerDetailReportComponent;
  let fixture: ComponentFixture<LotTrackerDetailReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotTrackerDetailReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotTrackerDetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
