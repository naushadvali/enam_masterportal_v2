import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSettlementDetailsComponent } from './payment-settlement-details.component';

describe('PaymentSettlementDetailsComponent', () => {
  let component: PaymentSettlementDetailsComponent;
  let fixture: ComponentFixture<PaymentSettlementDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentSettlementDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSettlementDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
