import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsideApmcSaleAgrmntReceiptComponent } from './outside-apmc-sale-agrmnt-receipt.component';

describe('OutsideApmcSaleAgrmntReceiptComponent', () => {
  let component: OutsideApmcSaleAgrmntReceiptComponent;
  let fixture: ComponentFixture<OutsideApmcSaleAgrmntReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideApmcSaleAgrmntReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideApmcSaleAgrmntReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
