import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-e-payment-report',
  templateUrl: './e-payment-report.component.html',
  styleUrls: ['./e-payment-report.component.css']
})
export class EPaymentReportComponent implements OnInit {
  public my_class1='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  div_show1(){
        this.my_class1='overlay1';
      }
  div_hide1(){
        this.my_class1='overlay';
      }
}
