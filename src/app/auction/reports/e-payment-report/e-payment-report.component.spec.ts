import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPaymentReportComponent } from './e-payment-report.component';

describe('EPaymentReportComponent', () => {
  let component: EPaymentReportComponent;
  let fixture: ComponentFixture<EPaymentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPaymentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPaymentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
