import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatewiseFinalReportComponent } from './datewise-final-report.component';

describe('DatewiseFinalReportComponent', () => {
  let component: DatewiseFinalReportComponent;
  let fixture: ComponentFixture<DatewiseFinalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatewiseFinalReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatewiseFinalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
