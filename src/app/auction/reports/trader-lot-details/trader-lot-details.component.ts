import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-trader-lot-details',
  templateUrl: './trader-lot-details.component.html',
  styleUrls: ['./trader-lot-details.component.css']
})
export class TraderLotDetailsComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  dateInsert:any;
  dateInsert1:any;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

  div_show1(){
        this.my_class1='overlay1';
      }
  div_hide1(){
        this.my_class1='overlay';
      }
  div_show2(){
  this.my_class2='overlay1';
}
  div_hide2(){
        this.my_class2='overlay';
      }
  div_show3(){
    this.my_class3='overlay1';
  }
  div_hide3(){
        this.my_class3='overlay';
      }
}
