import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderLotDetailsComponent } from './trader-lot-details.component';

describe('TraderLotDetailsComponent', () => {
  let component: TraderLotDetailsComponent;
  let fixture: ComponentFixture<TraderLotDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraderLotDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderLotDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
