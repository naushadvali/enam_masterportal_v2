import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamStatewiseTradingComponent } from './nam-statewise-trading.component';

describe('NamStatewiseTradingComponent', () => {
  let component: NamStatewiseTradingComponent;
  let fixture: ComponentFixture<NamStatewiseTradingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamStatewiseTradingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamStatewiseTradingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
