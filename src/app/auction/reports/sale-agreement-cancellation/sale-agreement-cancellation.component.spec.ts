import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleAgreementCancellationComponent } from './sale-agreement-cancellation.component';

describe('SaleAgreementCancellationComponent', () => {
  let component: SaleAgreementCancellationComponent;
  let fixture: ComponentFixture<SaleAgreementCancellationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleAgreementCancellationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleAgreementCancellationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
