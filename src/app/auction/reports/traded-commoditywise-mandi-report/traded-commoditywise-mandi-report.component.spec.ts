import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradedCommoditywiseMandiReportComponent } from './traded-commoditywise-mandi-report.component';

describe('TradedCommoditywiseMandiReportComponent', () => {
  let component: TradedCommoditywiseMandiReportComponent;
  let fixture: ComponentFixture<TradedCommoditywiseMandiReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradedCommoditywiseMandiReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradedCommoditywiseMandiReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
