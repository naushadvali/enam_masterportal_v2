import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalTradeComponent } from './digital-trade.component';

describe('DigitalTradeComponent', () => {
  let component: DigitalTradeComponent;
  let fixture: ComponentFixture<DigitalTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
