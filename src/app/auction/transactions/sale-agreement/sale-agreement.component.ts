import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-sale-agreement',
  templateUrl: './sale-agreement.component.html',
  styleUrls: ['./sale-agreement.component.css']
})
export class SaleAgreementComponent implements OnInit {
  public loading = false;
  public my_class1 ='overlay';public my_class2 ='overlay';public my_class3 ='overlay';public my_class4 ='overlay';public my_class5 ='overlay';public my_class6 ='overlay';public my_class7 ='overlay';public my_class8 ='overlay';public my_class9 ='overlay';
  public my_class10 ='overlay';  public my_class11 ='overlay';public my_class12 ='overlay';public my_class13 ='overlay';public my_class14 ='overlay';public my_class15 ='overlay';public my_class17 ='overlay';public my_class18 ='overlay';
  public my_class16 ='overlay';
  public shown1:any=true;
  public shown2:any=false;
  public shown3:any=true;
  public shown4:any=false;
  public shown5:any=true;
  public shown6:any=false;
  public shown7:any=true;
  public shown8:any=false;
  dateInsert:any;
  dateInsert1:any;
  isOneToOne:boolean;aggrementType:string;mspSaleBill:string;mspSaleBillChecked:boolean;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router
  ) { }

  ngOnInit() {
    this.isOneToOne = true;
    this.aggrementType = '0';
    this.genSaleBill = 'N';
    this.mspSaleBill = 'N';
    this.selFTType = "F";
    this.genSaleBillChecked =false;
    this.mspSaleBillChecked =false;
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };
  errorPopupShow(){
        this.my_class6='overlay1';
      }
  onAdvance1(){
    this.shown1=false;
    this.shown2=true;
  }
  onBasic1(){
    this.shown1=true;
    this.shown2=false;
  }
  onAdvance2(){
    this.shown3=false;
    this.shown4=true;
  }
  onBasic2(){
    this.shown3=true;
    this.shown4=false;
  }
  onAdvance3(){
    this.shown5=false;
    this.shown6=true;
  }
  onBasic3(){
    this.shown5=true;
    this.shown6=false;
  }
  onAdvance4(){
    this.shown7=false;
    this.shown8=true;
  }
  onBasic4(){
    this.shown7=true;
    this.shown8=false;
  }

// Show Only Trader Popup
  div_show1(){
        this.my_class13='overlay1';
        this.getTraderList();
    }
// Hide Only Trader Popup
  hideTraderPopUP(){
        this.my_class13='overlay';
        this.selOnlyTraderIndex = -1;
        this.matchTrader = "";
    }
  div_hide1(){
        this.matchFarmer ="";
        this.my_class1='overlay';
        this.selTraderIndex = -1;
      }
  // Open Farmer or trader poup here
  div_show2(){
    if(this.selFTType == 'F'){
    this.my_class2='overlay1';
    }
    else{
    this.my_class1='overlay1';
    }
    this.getDataForSOrT();
  }
  div_hide2(){
        this.my_class2='overlay';
        this.selFarmerIndex = -1;
        this.matchFarmer ="";
      }
  // Open Lot Code Popup
  div_show3(){
      //if(this.finalFTVal.length != 0 ){
        this.my_class3='overlay1';
        this.getLotCodeData();
    //}else{
        //this.my_class16 = 'overlay1';
       //setTimeout(()=>{
      //this.my_class16 = 'overlay';
      //this.onReset();
      //  },2000);
    //  }
  }
  div_hide3(){
        this.matchLotCode ="";
        this.selLotIndex = -1;
        this.my_class3='overlay';
      }
  // Open Fee Category Popup
  div_show4(){
        this.my_class4='overlay1';
        this.feeTypes();
      }
  // Hide Fee Category Popup
  div_hide4(){
        this.my_class4='overlay';
        this.selectedFee = -1;
        this.feeTypeVal = [];
        this.filtFee = "";
      }

// Click one to one
onOneToOne(){
  this.isOneToOne = true;
  this.aggrementType = '0';
  this.onReset();

}
genSaleBillChecked:boolean;
chooseAgrType(agrVal){
  if(agrVal == 'E'){
    this.genSaleBillChecked = false;
    this.mspSaleBillChecked = false;
    this.genSaleBill ="N";
    this.mspSaleBill = "N";
    this.onETender();
  }
  else if(agrVal == 'O'){
    this.genSaleBill ="N";
    this.mspSaleBill = "N";
    this.genSaleBillChecked = false;
    this.mspSaleBillChecked = false;
    this.onOneToOne();
  }

}
// Open eTender Popup List Here
onETender(){
  this.onReset();
  this.my_class5 = 'overlay1';
  this.isOneToOne = false;
  this.aggrementType = '2';
  this.getETenderList();
}
// Hide eTender Popup here
hideETenderPopup(){
  this.my_class5 = 'overlay';
  this.selectedTender = -1;
}

// Fetch all eTender
eTenderList = [];
getETenderList(){
  this.loading = true;
  this.authservice.getETenders().subscribe(eTenderList=>{
    if(eTenderList.listData != undefined){
      this.eTenderList = eTenderList.listData;
    }
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
);
}

// select one tender
selectedTender:number;tenderVal=[]
onSelTender(post,index){
  this.selectedTender=index;
  this.tenderVal = post;
}

// on selct tender ok
selTenderVal =[];tehsilDesc:string;tenderId:string;selTenderOK=[];
sellerName:any;traderName:any;firmName:any;lotCode:any;commodityDesc:any;approxWtInKg:any;rate:any;netWtInQt:any;
actualWtInKg:any;tPackType:any;noOfPack:any;netAmount:any;
onSelectTenderOK(){
  this.selTenderVal = this.tenderVal;
  this.my_class5='overlay';
  this.selectedTender= -1;
  console.log("selTenderVal",this.selTenderVal)
  this.loading = true;
  this.authservice.onSelTenderOK(this.selTenderVal).subscribe($selTenderOK=>{
    this.loading = false;
    if($selTenderOK.data != undefined){
      this.selTenderOK = $selTenderOK.data;
      this.sellerName =  this.selTenderOK["tSellerName"] ;
      console.log("sellerName",this.sellerName)
      this.traderName =  this.selTenderOK["lcamFullNameTrader"] ;
      this.firmName =  this.selTenderOK["firmName"] ;
      this.lotCode =  this.selTenderOK["acuhLotId"] ;
      this.commodityDesc =  this.selTenderOK["tProductName"] ;
      this.approxWtInKg =  this.selTenderOK["auchApproWtkg"] ;
      this.rate =  this.selTenderOK["auchRate"] ;
      this.netWtInQt =  this.selTenderOK["auchNetWtqtl"] ;
      this.actualWtInKg =  this.selTenderOK["auchActualWtkg"] ;
      this.tPackType =  this.selTenderOK["tPackType"] ;
      this.noOfPack =  this.selTenderOK["auchNoOfPack"] ;
      this.netAmount =  this.selTenderOK["auchNetAmt"] ;
    }
    console.log("res after ok",this.selTenderOK )
  }, (err) => {
            if (err === 'Unauthorized')
            {  this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
                  }
              }  );
}

onReset(){
  this.sellerName = this.traderName =  this.firmName =  this.lotCode = this.commodityDesc = this.approxWtInKg =
  this.rate = this.netWtInQt = this.actualWtInKg = this.tPackType = this.noOfPack = this.netAmount =  this.feeDesc = this.grossPay = this.netPay = this.amountInWord = "" ;
  this.selFeeOK = this.selTenderOK = this.selFeeTypeVal = this.finalFTVal = this.traderVal =[];
  this.rowData = [];
  this.successMsg="";  this.saveRes =[];
  this.genSaleBillChecked = false;this.mspSaleBillChecked = false;
  this.filtFee = ""; this.matchFarmer =""; this.matchTrader = "";  this.matchLotCode ="";
}

feeTypeList = [];filteredFeeTypeList = [];
feeTypes(){
  this.loading = true;
  this.authservice.getFeeTypes().subscribe($feeTypes=>{
    if($feeTypes.listData != undefined){
      this.feeTypeList = $feeTypes.listData;
      this.filteredFeeTypeList = $feeTypes.listData;
    }
    this.loading = false;
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);

        }
      }
);
}

// Filter Fee Type
filtFee:string;
onFilterFee(){
  this.filtFee = this.filtFee.toUpperCase();
  if(this.filtFee) {
    this.filteredFeeTypeList = _.filter(this.feeTypeList, (a)=>a.fchCatgDesc.indexOf(this.filtFee)>=0);
  } else {
    this.filteredFeeTypeList = this.feeTypeList;
  }
}

// select one fee type
selectedFee:number;feeTypeVal=[]
onSelFeeType(post,index){
  this.selectedFee=index;
  this.feeTypeVal = post;
}

// On selct fee type OK
selFeeTypeVal=[];selFeeOK=[];grossPay:string;netPay:string;feeDesc:any;rowData=[];amountInWord:any;
onSelectFeeTypeOK(){
  this.filtFee = "";
  this.selFeeTypeVal = this.feeTypeVal;
  this.my_class4='overlay';
  this.selectedFee= -1;
  console.log("selFeeTypeVal",this.selFeeTypeVal)
  this.feeDesc = this.selFeeTypeVal["fchCatgDesc"];
  this.my_class4 = "overlay";
  if(this.aggrementType == "0"){
    this.selTenderOK=[];

    this.selTenderOK["REFDOCID"]= "";
    this.selTenderOK["acuhLotId"]= this.lotCode;
    this.selTenderOK["auchActualWtkg"]= this.actualWtInKg;
    this.selTenderOK["auchAgenId"]= this.traderVal["lcam_trn_id"];
    this.selTenderOK["auchApproWtkg"]= this.approxWtInKg;
    this.selTenderOK["auchImpType"]= this.selFTType;
    this.selTenderOK["auchNetAmt"]= this.netAmount;
    this.selTenderOK["auchNetWtqtl"]= this.netWtInQt;
    this.selTenderOK["auchNoOfPack"]= this.noOfPack;
    this.selTenderOK["auchPackType"]= this.lotCodeOKData["auchPackType"];
    this.selTenderOK["auchRate"]= this.rate;
    this.selTenderOK["firmName"]= this.firmName;
    this.selTenderOK["lcamFullNameTrader"]= this.traderName;
    this.selTenderOK["tPackType"]= this.tPackType;
    this.selTenderOK["tProductName"]= this.commodityDesc;
    this.selTenderOK["tSellerName"]= this.sellerName;
    this.selTenderOK["t_TraderNameEtrade"]= this.traderName;
    if(this.selFTType == "T"){
      this.selTenderOK["auchTraderId"]=this.finalFTVal["lcam_trn_id"]
    }
    else{
      this.selTenderOK["auchFarmerId"] = this.finalFTVal["farmRegIId"]
    }
    this.selTenderOK = Object.assign({}, this.selTenderOK);
    console.log("selTenderOK",Object.assign({}, this.selTenderOK),this.selTenderOK)
}
  var postJson ={
    "saleAggrFee" : this.selFeeTypeVal,
    "salAggreovo" : this.selTenderOK
  }
console.log("postJson",postJson)
this.loading = true;
  this.authservice.onSelFeeTypeOK(postJson).subscribe($selFeeOK=>{
    this.loading = false;
    if($selFeeOK.data != undefined){
      this.selFeeOK = $selFeeOK.data;
      this.grossPay = this.selFeeOK["auchAmtToPay"];
      this.netPay = this.selFeeOK["auchCalcAmount"];
      this.amountInWord = this.selFeeOK["amountInWord"];
      this.rowData = this.selFeeOK["feeCatMap"];
    }
    console.log("res after fee ok",this.selFeeOK )
  }, (err) => {
            if (err === 'Unauthorized')
            {  this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
                  }
              }  );

}

// On Generate Sale Bill
genSaleBill:string;
OnGenerateSaleBill(checked){
  console.log("bill",checked)
  if(checked == true){
    this.genSaleBill = 'Y';
  }
  else{
    this.genSaleBill = 'N';
  }
}
// On Generate msp Sale Bill
onMspSaleBill(checked){
  if(checked == true){
    this.mspSaleBill = 'Y';
  }
  else{
    this.mspSaleBill = 'N';
  }
  console.log("bill",checked)
}

// On Save Sale Agreement
saveRes:any;successMsg:string;ifAgentIdPresent:boolean;
saveSaleAgreement(){
  console.log("this.selTenderOK",this.selTenderOK)
  console.log("this.finalFTVal",this.finalFTVal)
  console.log("this.finalLotCode",this.finalLotCode)
  console.log("this.selFeeTypeVal",this.selFeeTypeVal)
  console.log("this.traderVal",this.traderVal)
  if(this.selTenderOK.length != 0){
    if(this.selFeeTypeVal.length != 0){
      if(this.selFeeOK.length != 0){
        this.ifAgentIdPresent = true;
        for(var i=0;i<this.selFeeOK["feeCatMap"].length ;i++){
          if(this.selFeeOK["feeCatMap"][i].AccdAgentId == undefined){
            this.ifAgentIdPresent = false;
            break;
          }
        }
        console.log("ifAgentIdPresent",this.ifAgentIdPresent)
        if(this.ifAgentIdPresent == false){
          this.my_class12 = 'overlay1';
          setTimeout(()=>{
            this.my_class12 = 'overlay';
          },2000);
        }
        else{
          this.selTenderOK["aggrementType"] = this.aggrementType;
            var postJson ={
              "saleAgreementFeeOkBtn" :this.selFeeOK,
              "saleAgreementEOVO" : this.selTenderOK,
              "saleAgrFeeCatVO" : this.selFeeTypeVal,
              "genrateBill":  this.genSaleBill ,
              "msp": this.mspSaleBill
            }
            console.log("post",postJson)
          this.loading = true;
          this.authservice.saveSaleAgreement(postJson).subscribe($saveSaleAgr=>{
            this.loading = false;
            this.saveRes = $saveSaleAgr;
            this.successMsg = $saveSaleAgr.message;
            console.log("this.saveRes",this.saveRes)
            this.my_class7 = 'overlay1';
            setTimeout(()=>{
              this.my_class7 = 'overlay';
              this.onReset();
            },2000);
          }, (err) => {
                    if (err === 'Unauthorized')
                    {  this.errorPopupShow();
                            setTimeout(()=>{
                              this.my_class4 = 'overlay';
                              localStorage.clear();
                              this.router.navigateByUrl('/login');
                            },3000);
                          }
                          else
                          { this.my_class7 = 'overlay1';
                          setTimeout(()=>{
                            this.my_class7 = 'overlay';
                          },2000);
                                }
                      }  );
        }
      }
      else{
          this.my_class11 = 'overlay1';
          setTimeout(()=>{
            this.my_class11 = 'overlay';
          },2000);
      }
    }
    else{
        this.my_class10 = 'overlay1';
        setTimeout(()=>{
          this.my_class10 = 'overlay';
        },2000);
    }
  }
  else{
    if(this.aggrementType == '0'){
      if(this.finalFTVal.length != 0){
        if(this.traderVal.length != 0){
          if(this.finalLotCode.length != 0){
            if(this.netAmount != undefined && this.netAmount != ""){
              if(this.selFeeTypeVal.length == 0){
                this.my_class11= 'overlay1';
                setTimeout(()=>{
                  this.my_class11 = 'overlay';
                },2000);
              }
            }
            else{
              this.my_class18= 'overlay1';
              setTimeout(()=>{
                this.my_class18 = 'overlay';
              },2000);
            }
          }
          else{
            this.my_class15= 'overlay1';
            setTimeout(()=>{
              this.my_class15 = 'overlay';
            },2000);
          }
        }
        else{
          this.my_class14= 'overlay1';
          setTimeout(()=>{
            this.my_class14 = 'overlay';
          },2000);
        }
      }
      else{
        this.my_class17 = 'overlay1';
        setTimeout(()=>{
          this.my_class17 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class9 = 'overlay1';
      setTimeout(()=>{
        this.my_class9 = 'overlay';
      },2000);
    }
  }
}


// Open Popup for party names
partyNameList=[];filteredPartyNames=[];popupDesc:string;partyType:string;
openPartyNamePopup(post,index){
  this.my_class8 = 'overlay1';
  this.partyType = post.AucdPartyType;
  console.log("pst",post,index,this.partyType)
  this.selPartyIndex = index;
  // if(post.AucdPartyType == 'C'){
  //   this.popupDesc ="APMC"
  // }
  this.loading = true;
  this.authservice.getPartyNames(this.partyType).subscribe($partyNames=>{
    if($partyNames.data.mapList != undefined){
      this.partyNameList = $partyNames.data.mapList;
      this.filteredPartyNames = $partyNames.data.mapList;
    }
    this.loading = false;
    console.log($partyNames.data)
  }, (err) => {
            if (err === 'Unauthorized')
            {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
);
}
// Hide Popup for party Names
hidePartyNamePopup(){
  this.my_class8 = 'overlay';
  this.selectedParty = -1;
}

// Select one party Name
partyVal=[];selectedParty:number;selPartyIndex:number;
onSelPartyNames(post,index){
  this.selectedParty = index;
  this.partyVal = post;
}

// On Party Names Ok
selPartyNameVal=[];
onSelPartyNameOK(){
  this.my_class8 = 'overlay';
  this.selPartyNameVal =this.partyVal;
  console.log("selPartyNameVal",this.selPartyIndex,this.selFeeOK,this.selFeeOK["feeCatMap"][0])
  console.log("selPartyIndex",this.selPartyIndex)
  if(this.partyType == "C"){
    this.selFeeOK["feeCatMap"][this.selPartyIndex].AccdAgentId = this.selPartyNameVal["ADOUM_OPRCD"];
    this.rowData[this.selPartyIndex].t_partyname = this.selPartyNameVal["ADOUM_OPRNAME"];
  }
  else{
    this.selFeeOK["feeCatMap"][this.selPartyIndex].AccdAgentId = this.selPartyNameVal["lcam_trn_id"];
    this.rowData[this.selPartyIndex].t_partyname = this.selPartyNameVal["lcam_full_name"];
  }
  this.selectedParty = -1;
  console.log("selFeeOK",this.selFeeOK)
}


/// One To One
// Choose Farmer Or Trader
selFTType:string;
chooseFarmerTrader(val){
  this.finalFTVal =[];this.selFarmerTraderVal =[];this.sellerName="";
  if(val == 'F'){
  this.selFTType = "F";
  }
  else{
    this.selFTType = "T";
  }
  console.log("selFTType",this.selFTType)
  console.log("finalFTVal",this.finalFTVal)
}

// Get Data For seller or trader
farmerTraderData =[];
filtFarmerTraderData =[];
getDataForSOrT(){
 if(this.selFTType == 'F'){
   this.getFarmerList();
 }
 else{
   this.getTraderList();
 }

}

// Get Farmer List
getFarmerList(){
  this.loading = true;
  this.authservice.selectSellerName().subscribe($data=>{
    this.farmerTraderData=$data.listData;
    this.filtFarmerTraderData=this.farmerTraderData;
    this.loading = false;
  }, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class6= 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);

      }
    }
)
}
// Get Trader List
getTraderList(){
  this.loading = true;
  this.authservice.getTraderList().subscribe($allTraderData=>{
    console.log("Trader List data: ",$allTraderData.listData);
    this.farmerTraderData=$allTraderData.listData;
    this.filtFarmerTraderData=this.farmerTraderData;
    this.loading = false;
  }, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class6 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);

      }
    }
);
}

//Filter farmer fields
matchFarmer:string;
searchFarmerName(){
  this.matchFarmer= this.matchFarmer.toUpperCase();
  if(this.matchFarmer) {
    this.filtFarmerTraderData = _.filter(this.farmerTraderData, (a)=>a.frmFullName.indexOf(this.matchFarmer)>=0);
  } else {
    this.filtFarmerTraderData = this.farmerTraderData  ;
  }
}

// sel farmer
selFarmerTraderVal:any;selFarmerIndex:number;
onFarmerSelect(post,index){
  this.selFarmerTraderVal = post;
  this.selFarmerIndex = index;
  console.log("selected row farmer : ",this.selFarmerTraderVal);
}
// sel trader
selTraderIndex:number;
onTraderSelect(post,index){
  this.selFarmerTraderVal = post;
  this.selTraderIndex=index;
  console.log("selected row trader : ",this.selFarmerTraderVal);
}

// On Farmer Select OK
finalFTVal=[];
onSelFarmerOK(){
  this.matchFarmer ="";
  this.finalFTVal = this.selFarmerTraderVal;
  this.my_class2 = "overlay";
  console.log("onSelFarmerOK",this.finalFTVal)
  this.selFarmerIndex = -1;
  this.sellerName = this.finalFTVal["frmFullName"];
  this.finalLotCode =[];
  this.lotCode = this.commodityDesc = this.approxWtInKg = this.actualWtInKg = this.netWtInQt = this.tPackType = this.noOfPack = "";
}
// On Trader Select OK
onSelTraderOK(){
  this.matchFarmer ="";
  this.finalFTVal = this.selFarmerTraderVal;
  this.my_class1 = "overlay";
  console.log("onSelTraderOK",this.finalFTVal)
  this.selTraderIndex = -1;
  this.sellerName = this.finalFTVal["lcam_full_name"];
}

// sel trader only
selOnlyTraderIndex:number;
selTraderVal =[];
onOnlyTraderSelect(post,index){
  this.selTraderVal = post;
  this.selOnlyTraderIndex=index;
  console.log("selected row trader : ",this.selTraderVal);
}
// On Only Trader Select OK
traderVal=[]
onTraderOK(){
  this.matchTrader = "";
  this.traderVal = this.selTraderVal;
  this.my_class13 = "overlay";
  console.log("onOnlySelTraderOK",this.traderVal)
  this.selOnlyTraderIndex = -1;
  this.traderName = this.traderVal["lcam_full_name"];
  this.firmName = this.traderVal["lcam_agen_comp_name"];
}

// Open Lot code popup
lotCodeData =[];
filteredLotCodeData =[];
getLotCodeData(){
  var postJson = {
    "auchAgenId": this.traderVal["lcam_trn_id"],
    "auchImpType": this.selFTType,
    "firmName": this.firmName,
    "lcamFullNameTrader": this.traderName,
    "tSellerName": this.sellerName,
    "t_TraderNameEtrade": this.traderName
  }

  if(this.selFTType == "T"){
    postJson["auchTraderId"]=this.finalFTVal["lcam_trn_id"]
  }
  else{
    postJson["auchFarmerId"] = this.finalFTVal["farmRegIId"]
  }
  console.log("post",postJson)
  this.loading = true;
  this.authservice.fetchLotCodeData(postJson).subscribe(lotCodeData=>{
    console.log("Trader List data: ",lotCodeData.listData);
    this.loading = false;
    this.lotCodeData = lotCodeData.listData;
    this.filteredLotCodeData=this.lotCodeData;
  }, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class6 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
      }
    }
  );
}

// Select one lot here
selLotVal =[];selLotIndex:number;
onLotSelect(post,index){
  this.selLotIndex = index;
  this.selLotVal = post;
}
// On LotCode Select Ok
finalLotCode=[];lotCodeOKData=[];
onSelLotCodeOK(){
  this.matchLotCode ="";
  this.selLotIndex = -1;
  this.finalLotCode = this.selLotVal;
  this.lotCode = this.selLotVal["iledLotId"]
  this.my_class3 = "overlay";
  console.log("finalLotCode",this.finalLotCode)
  this.loading = true;
  this.authservice.saveLotCodeDataOK(this.finalLotCode).subscribe(lotCodeDataOK=>{
    this.loading = false;
    if(lotCodeDataOK.data != undefined){
      this.lotCodeOKData = lotCodeDataOK.data;
      console.log("lotCodeOKData",this.lotCodeOKData)
      this.commodityDesc = this.lotCodeOKData["tProductName"];
      this.approxWtInKg = this.lotCodeOKData["auchApproWtkg"];
      this.actualWtInKg = this.lotCodeOKData["auchActualWtkg"];
      this.tPackType = this.lotCodeOKData["tPackType"];
      this.noOfPack = this.lotCodeOKData["auchNoOfPack"];
      this.netWtInQt = this.lotCodeOKData["auchNetWtqtl"];
    }
  }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class6 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
  );
}

// Calculate Net amount
calcNetAmount(){
  console.log("this.rate",this.rate)
  this.netAmount = Number(this.rate) * Number(this.netWtInQt);
}

// Filter by Trader Name
matchTrader:string;
searchTraderName(){
  this.matchTrader= this.matchTrader.toUpperCase();
  if(this.matchTrader) {
    this.filtFarmerTraderData = _.filter(this.farmerTraderData, (a)=>a.lcam_full_name.indexOf(this.matchTrader)>=0);
  } else {
    this.filtFarmerTraderData = this.farmerTraderData  ;
  }
}
// Filter by Lot Code
matchLotCode:string;
searchLotCode(){
  if(this.matchLotCode) {
    this.filteredLotCodeData = _.filter(this.lotCodeData, (a)=>a.iledLotCode.indexOf(this.matchLotCode)>=0);
  } else {
    this.filteredLotCodeData = this.lotCodeData  ;
  }
}



}
