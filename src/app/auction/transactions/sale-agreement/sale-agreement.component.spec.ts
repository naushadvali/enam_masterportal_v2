import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleAgreementComponent } from './sale-agreement.component';

describe('SaleAgreementComponent', () => {
  let component: SaleAgreementComponent;
  let fixture: ComponentFixture<SaleAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
