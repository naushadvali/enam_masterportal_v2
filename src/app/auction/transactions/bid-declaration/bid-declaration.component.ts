import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';


@Component({
  selector: 'app-bid-declaration',
  templateUrl: './bid-declaration.component.html',
  styleUrls: ['./bid-declaration.component.css'],
  providers: [DatePipe]
})
export class BidDeclarationComponent implements OnInit {
  public loading = false;
  public my_class1='overlay';public my_class2='overlay';public my_class3='overlay';public my_class4='overlay';
  public my_class5='overlay';  public my_class6='overlay';public my_class7='overlay';public my_class8='overlay';
  public my_class9='overlay';public my_class10='overlay';public my_class11='overlay';public my_class12='overlay';public my_class13='overlay';public my_class14='overlay';public my_class15='overlay';public my_class16='overlay';public my_class17='overlay';public my_class18='overlay';public my_class19='overlay';
  defSel:boolean;

  dateInsert:any;dateInsert1:any;dateInsert2:any;fromDate:any;toDate:any;today:any;defaultDate:any;nextDate:any;
  extendedDate:any;
  date: Date = new Date();
      settings = {
          bigBanner: true,
          timePicker: true,
          format: 'dd/MM/yyyy HH:mm:ss a',
          defaultOpen: false
      }
  reportType:string;bidRep:string;printFromDate:any;printToDate:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
    private datepipe:DatePipe
  ) { }

  ngOnInit() {
    this.today = new Date();
  //  this.fromDate = this.today;
    // this.toDate = this.today;
    this.defaultDate = this.datepipe.transform((this.today), 'dd/MM/yyyy HH:mm:ss')
    var nextDay = new Date(this.today);
    nextDay.setDate(this.today.getDate()+1);
    this.nextDate = this.datepipe.transform((nextDay), 'dd/MM/yyyy')
    // For Print Report
    this.reportType="pdf";
    this.bidRep="O";
    this.activeCreate = false;

    var dd = this.today.getDate();
    var mm = this.today.getMonth()+1; //January is 0!
    var yyyy = this.today.getFullYear();
    this.fromDate =
    { date: { year: this.today.getFullYear(), month: mm, day: dd } };
    this.toDate =
    { date: { year: this.today.getFullYear(), month: mm, day: dd } };
    this.getOpenBidLists();
  }
  errorPopupShow(){
    this.my_class1='overlay1';
  }
  printReport(){
  //  console.log("  this.bidRep & reportType", this.bidRep,this.reportType)
    if(this.printFromDate != undefined && this.printFromDate != "" && this.printToDate != undefined && this.printToDate != ""){
      var currentDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd') + ' 00:00:00'
      var toDate = this.datepipe.transform(new Date(this.printToDate.jsdate), 'yyyy-MM-dd') + ' 00:00:00'
      var fromDate = this.datepipe.transform(new Date(this.printFromDate.jsdate), 'yyyy-MM-dd') + ' 00:00:00'

      console.log("fromDate",fromDate)
      console.log("toDate",toDate)

      if(fromDate <= toDate ){
        console.log("errorPopupShow")
        if(toDate <= currentDate ){
          var dateObj = {"fromDate": this.printFromDate.formatted, "toDate": this.printToDate.formatted}

          if(this.reportType == 'pdf'){
            this.loading = true;
            this.authservice.printBidDeclarationReportPDF(this.bidRep,dateObj).subscribe(
              (res) => {
                  this.loading = false;
                  this.my_class13='overlay';
                  // this.onReset();
                  var fileURL = URL.createObjectURL(res);
                  window.open(fileURL);
                }
                , (err) => {
                            if (err === 'Unauthorized')
                            {
                              this.errorPopupShow();
                                    setTimeout(()=>{
                                      this.my_class1 = 'overlay';
                                      localStorage.clear();
                                      this.router.navigateByUrl('/login');
                                    },3000);
                        }
                      }
            )
          }
          if(this.reportType == 'rtf'){
            this.loading = true;
            this.authservice.printBidDeclarationReportRTF(this.bidRep,dateObj).subscribe(
              (res) => {
                  this.loading = false;
                  this.my_class13='overlay';
                  // this.onReset();
                  var fileURL = URL.createObjectURL(res);
                  window.open(fileURL);
                }
                , (err) => {
                            if (err === 'Unauthorized')
                            {
                              this.errorPopupShow();
                                    setTimeout(()=>{
                                      this.my_class1 = 'overlay';
                                      localStorage.clear();
                                      this.router.navigateByUrl('/login');
                                    },3000);
                        }
                      }
            )
          }
          if(this.reportType == 'xls'){
            this.loading = true;
            this.authservice.printBidDeclarationReportXLS(this.bidRep,dateObj).subscribe(
              (res) => {
                  this.loading = false;
                  this.my_class13='overlay';
                  // this.onReset();
                  var fileURL = URL.createObjectURL(res);
                  window.open(fileURL);
                }
                , (err) => {
                            if (err === 'Unauthorized')
                            {
                              this.errorPopupShow();
                                    setTimeout(()=>{
                                      this.my_class1 = 'overlay';
                                      localStorage.clear();
                                      this.router.navigateByUrl('/login');
                                    },3000);
                                  }
                                }  )
                              }
        }
        else{
          this.my_class15 = 'overlay1';
          setTimeout(()=>{
            this.my_class15 = 'overlay';
          },2000);
        }
      }
      else{
        console.log("not here")
        this.my_class16 = 'overlay1';
        setTimeout(()=>{
          this.my_class16 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class14 = 'overlay1';
      setTimeout(()=>{
        this.my_class14 = 'overlay';
      },2000);
    }

  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  div_show(){
      this.my_class13='overlay1';
    }
  div_hide(){
      this.my_class13='overlay';
      this.bidRep ="O";this.reportType = "pdf";this.printFromDate = this.printToDate = "";
    }

openBidLists=[];
filteredOpenBidLists=[];
openBidChecked:boolean;
getOpenBidLists(){
  this.filteredOpenBidLists =[];
  console.log("this.fromDate",this.fromDate)
  console.log("this.toDate",this.toDate)
  if(this.fromDate.jsdate == undefined){
    var dd = this.fromDate.date.day;
    var mm = this.fromDate.date.month;
    if(dd<10){
      dd = "0"+dd;
    }
    if(mm<10){
      mm = "0"+mm;
    }
     var fromDate = (mm+"/"+dd+"/"+this.fromDate.date.year);
  }
  if(this.toDate.jsdate == undefined){
    var dd = this.toDate.date.day;
    var mm = this.toDate.date.month;
    if(dd<10){
      dd = "0"+dd;
    }
    if(mm<10){
      mm = "0"+mm;
    }
    var toDate = (mm+"/"+dd+"/"+this.toDate.date.year);
  }
  if(this.fromDate.jsdate != undefined ){
      fromDate = this.datepipe.transform((this.fromDate.jsdate), 'MM/dd/yyyy');
  }
  if(this.toDate.jsdate != undefined){
      // toDate = this.toDate.formatted;
      toDate = this.datepipe.transform((this.toDate.jsdate), 'MM/dd/yyyy');
  }
  // var fromDate = this.datepipe.transform((this.fromDate), 'dd/MM/yyyy')
  // var toDate = this.datepipe.transform((this.toDate), 'dd/MM/yyyy')

  var time1 = new Date(fromDate).getTime();
  var time2 = new Date(toDate).getTime();
  var time3 = new Date().getTime();
  console.log("fromDate",fromDate)
  console.log("toDate",toDate)
  var timeDiff0 = (time1 - time3);
  var diffDays0 = Math.ceil(timeDiff0 / (1000 * 3600 * 24));
  console.log("diffDays0",diffDays0)

    if(diffDays0 <= 0){
      var timeDiff1 = (time2 - time3);
      var diffDays1 = Math.ceil(timeDiff1 / (1000 * 3600 * 24));
        if(diffDays1 <=0){
          var timeDiff2 = (time1 - time2);
          var diffDays2 = Math.ceil(timeDiff2 / (1000 * 3600 * 24));
          if(diffDays2 <=0){
            var timeDiff3 = Math.abs(time1 - time2);
            var diffDays3 = Math.ceil(timeDiff3 / (1000 * 3600 * 24));
            console.log("diffDays",diffDays3)

            if(diffDays3 > 4){
              this.flashMessagesService.show('Maximum Date Difference Of 3 Days Is Allowed Between From And To Dates', { cssClass: 'alert-danger', timeout: 3000 });
            }
            else{
              var postToDate = this.datepipe.transform((toDate), 'dd/MM/yyyy');
              var postFromDate = this.datepipe.transform((fromDate), 'dd/MM/yyyy');
              console.log("postToDate",postToDate)
              console.log("postFromDate",postFromDate)

                var postJson ={
                  "fromDate": postFromDate,
                  "toDate": postToDate
                }
                this.loading = true;
                this.authservice.getOpenBidListing(postJson).subscribe($data=>{
                  console.log("open bid listing search: ",$data);
                  console.log("open bid listing search: ",$data.listData);
                  if($data.listData != undefined){
                      this.openBidLists = $data.listData ;
                      this.filteredOpenBidLists = $data.listData ;
                      this.activeCreate = true;
                  }else{
                      this.activeCreate = false;
                  }
                  this.loading = false;
                }, (err) => {
                            if (err === 'Unauthorized')
                            {
                              this.errorPopupShow();
                                    setTimeout(()=>{
                                      this.my_class1 = 'overlay';
                                      localStorage.clear();
                                      this.router.navigateByUrl('/login');
                                    },3000);
                            }
                      });
            }
          }
          else{
            this.flashMessagesService.show('From Date Cannot be After To Date', { cssClass: 'alert-danger', timeout: 3000 });
          }
        }
        else{
          this.flashMessagesService.show('To Date Cannot be After Current Date', { cssClass: 'alert-danger', timeout: 3000 });
        }
    }
    else{
    this.flashMessagesService.show('From Date Cannot be After Current Date', { cssClass: 'alert-danger', timeout: 3000 });
    }
}


createBidList=[];
isPresent:boolean;
activeCreate:boolean;
onCheckBid(post,isChecked,index){
console.log(post,isChecked,index,  (<HTMLInputElement>document.getElementById("checkId_"+index)).checked)

  this.isPresent = false;
  this.activeCreate = false;
  // For Single
  // if(isChecked == true){
  //   this.activeCreate = true;
  //   this.createBidList = post;
  // }
  // else{
  //   this.activeCreate = false;
  //   this.createBidList = [];
  // }
  // For Multiple
  // if(isChecked == true){
  //   for(var i = 0; i < this.createBidList.length; i++) {
  //     if (this.createBidList[i].abcLotId == post.abcLotId) {
  //       this.isPresent = true;
  //       break;
  //     }
  //   }
  //   if(this.isPresent == false){
  //     console.log("here",this.createBidList,this.isPresent)
  //    this.createBidList.push(post);
  //   }
  // }
  // else{
  //   for(var i = 0; i < this.createBidList.length; i++) {
  //     if (this.createBidList[i].abcLotId == post.abcLotId) {
  //       this.createBidList.splice(i,1);
  //       break;
  //     }
  //   }
  // }
  if(isChecked == true){
    post.checked = true;
  }
  else{
    post.checked = false;
  }
  if( this.createBidList.length != 0){
    this.activeCreate = true;
  }
  else{
    this.activeCreate = false;
  }

  console.log(this.createBidList)
  console.log("filteredOpenBidLists",this.filteredOpenBidLists)

}


// Open Publish Results Popup
openBidAction = [];
filteredopenBidAction = [];
withBidPresentList=[];
openPubResultPopUp(){
  this.createBidList =[];
  for(var i=0;i<this.filteredOpenBidLists.length;i++){
    if(this.filteredOpenBidLists[i].checked == true){
      this.createBidList.push(this.filteredOpenBidLists[i]);
    }
  }
  console.log("createBidList",this.createBidList)

  // For Multiple
  if(this.createBidList.length > 1){
    for(var i=0;i<this.createBidList.length;i++){
      if(Number(this.createBidList[i]["abcMinBuyers"]) <= Number(this.createBidList[i]["totbidders"])){
        this.withBidPresentList.push(this.createBidList[i]);
      }
    }

    if(this.withBidPresentList.length !=0){
      console.log("withBidPresentList",this.withBidPresentList)
      this.loading = true;
      this.authservice.saveBidActionMulti(this.withBidPresentList).subscribe($data=>{
      this.loading = false;
      this.my_class11 = 'overlay1';
      setTimeout(()=>{
        this.my_class11 = 'overlay';
      },2000);
      this.openBidAction = [];
      this.createBidList = [];
      this.withBidPresentList = [];
      this.getOpenBidLists();
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
                  else{
                    this.my_class12 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class12 = 'overlay';
                    },2000);
                  }
            });
    }
    else{
      this.my_class10 = 'overlay1';
      setTimeout(()=>{
        this.my_class10 = 'overlay';
      },2000);
    }

  }
  // For Single
  if(this.createBidList.length == 1){
    var createBidListObj = this.createBidList[0];
    if(Number(createBidListObj["abcMinBuyers"]) <= Number(createBidListObj["totbidders"])){
      console.log("withBidPresentList single",createBidListObj)
      this.my_class2 = 'overlay1';
      this.loading = true;
      this.authservice.getOpenBidAction(createBidListObj).subscribe($data=>{
        if($data.listData != undefined){
        this.openBidAction = $data.listData ;
        this.filteredopenBidAction = $data.listData ;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
            });
    }
    else{
      this.my_class4 = 'overlay1';
      setTimeout(()=>{
        this.my_class4 = 'overlay';
      },2000);
    }
  }

  if(this.createBidList.length == 0){
    this.my_class19 = 'overlay1';
    setTimeout(()=>{
      this.my_class19 = 'overlay';
    },2000);
  }

}


// Hide Publish Results Popup
div_hide_pubPopUp(){
  this.my_class2 = 'overlay';
  // this.openBidAction = [];
  // this.createBidList = [];
}

// Save Open Bid
saveMsg:string;
onSaveOpenBid(){
  var postJson= {
    "bidOpenActionList" : this.openBidAction,
    "bidOpening" : this.createBidList[0]
  }
  console.log("pst",postJson)
  this.loading = true;
  this.authservice.saveBidAction(postJson).subscribe($data=>{
    this.loading = false;
    this.saveMsg = $data.message ;
    this.my_class3 = 'overlay1';
    setTimeout(()=>{
      this.my_class3 = 'overlay';
      this.my_class2 = "overlay";
    },2000);
    this.openBidAction = [];
    this.createBidList = [];
    this.getOpenBidLists();
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}


// Open Extend Bid Popup
openExtendBidPopUp(){
  this.createBidList =[];
  for(var i=0;i<this.filteredOpenBidLists.length;i++){
    if(this.filteredOpenBidLists[i].checked == true){
      this.createBidList.push(this.filteredOpenBidLists[i]);
    }
  }
  console.log("createBidList",this.createBidList)
    if(this.createBidList.length == 1){
      this.my_class5 = 'overlay1';
      // this.createBidList = [];
    }
    else{
      this.my_class17= 'overlay1';
      setTimeout(()=>{
        this.my_class17 = 'overlay';
      },2000);
    }
}
// Hide Extended Bid Popup
div_hide_exPopUp(){
  this.my_class5 = 'overlay';
  this.extendedDate = new Date()
}


// Extend Bid here
nextDay:any;singleCreate =[];
onSaveExtendedBid(){
  if(this.extendedDate == undefined){
    this.extendedDate = new Date();
  }
  var defaultDate = this.datepipe.transform((this.today), 'dd/MM/yyyy HH:mm:ss')
  var exDate = this.datepipe.transform((this.extendedDate), 'dd/MM/yyyy HH:mm:ss')
  var exTime = new Date(this.extendedDate).getTime();
  var defTime = new Date(this.today).getTime();
  this.nextDay = new Date();
  this.nextDay.setDate(this.today.getDate()+1);
  var nextTime = new Date(this.nextDay).getTime();

  if((Number(exTime) > Number(defTime)) && (Number(exTime) < Number(nextTime))){
    if(this.createBidList.length >1){
      this.singleCreate = this.createBidList[this.createBidList.length -1]
    }
    else{
      this.singleCreate = this.createBidList[0];
    }
    var postJson ={
      "bidOpening": this.singleCreate,
      "dateVo": {
        "fromDateTime" : exDate
      }
    }
    console.log("here",postJson)
    this.loading = true;
    this.authservice.extendBid(postJson).subscribe($data=>{
      this.loading = false;
      if($data.status == '1'){
        this.my_class7 = 'overlay1';
        setTimeout(()=>{
          this.my_class7 = 'overlay';
          this.my_class5 = "overlay";
        },2000);
        this.createBidList = [];
        this.getOpenBidLists();
      }
    }, (err) => {
                if (err === 'Unauthorized')
                {
                  this.errorPopupShow();
                        setTimeout(()=>{
                          this.my_class1 = 'overlay';
                          localStorage.clear();
                          this.router.navigateByUrl('/login');
                        },3000);
                }
                else{
                  this.my_class12 = 'overlay1';
                  setTimeout(()=>{
                    this.my_class12 = 'overlay';
                  },2000);
                }
          });
  }
  else{
    this.my_class6= 'overlay1';
    setTimeout(()=>{
      this.my_class6 = 'overlay';
    },2000);
  }

}

// Open Exit Bid Popup
openExitBidPopup(){
  this.createBidList =[];
  for(var i=0;i<this.filteredOpenBidLists.length;i++){
    if(this.filteredOpenBidLists[i].checked == true){
      this.createBidList.push(this.filteredOpenBidLists[i]);
    }
  }
  if(this.createBidList.length == 1){
    this.my_class8 = 'overlay1';
  }
  else{
    this.my_class18= 'overlay1';
    setTimeout(()=>{
      this.my_class18 = 'overlay';
    },2000);
  }
}

// Save Exit Bid Here
onExitBid(){
  this.loading = true;
  this.authservice.exitBid(this.createBidList[0]).subscribe($data=>{
    this.loading = false;
    if($data.status == '1'){
      this.my_class9 = 'overlay1';
      setTimeout(()=>{
        this.my_class9 = 'overlay';
        this.my_class8 = "overlay";
      },2000);
      this.getOpenBidLists();
    }
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}

// Hide Exit Bid Popup
closeExitBidPopup(){
  this.my_class8 = 'overlay';
  this.createBidList = [];
}

// Filter By Commdity Name
matchCommodityName:string;
onCommodity(){
  this.matchCommodityName=this.matchCommodityName.toUpperCase();
  var filteredData = _.filter(this.openBidLists, (a)=>a.gmpmgProdName.indexOf(this.matchCommodityName)>=0);
  this.filteredOpenBidLists = filteredData;
}

// Filter By lotcode
matchLotCode:string;
onLotCode(){
  var filteredData = _.filter(this.openBidLists, (a)=>a.iledLotCode.indexOf(this.matchLotCode)>=0);
  this.filteredOpenBidLists = filteredData;
}
// Filter By bid status
onBidStatus(value){
  console.log("value",value)
  var filteredData = _.filter(this.openBidLists, (a)=>a.bidderstatus.indexOf(value)>=0);
  this.filteredOpenBidLists = filteredData;
}

// Reset Page
onReset(){
  this.defSel = true;
  this.openBidLists =[];
  this.filteredOpenBidLists =[];
  this.fromDate = this.today;
  this.toDate = this.today;
  this.matchCommodityName ="";
  this.matchLotCode ="";
  this.bidRep ="O";this.reportType = "pdf";this.printFromDate = this.printToDate = "";
}
// Select All
onSelectAll(){
  for(var i=0;i<this.filteredOpenBidLists.length;i++){
    // (<HTMLInputElement>document.getElementById("checkId_"+i)).checked = true;
    this.filteredOpenBidLists[i].checked = true;
    this.createBidList = this.filteredOpenBidLists;
  }
    //this.activeCreate = true;
    console.log("this.filteredOpenBidLists",this.filteredOpenBidLists)
    console.log("this.createBidList",this.createBidList)
}
// Select All
onDeSelectAll(){
  for(var i=0;i<this.openBidLists.length;i++){
    // (<HTMLInputElement>document.getElementById("checkId_"+i)).checked = false;
    this.filteredOpenBidLists[i].checked = false;
    this.createBidList = [];
  }
  console.log("this.filteredOpenBidLists",this.filteredOpenBidLists)
  console.log("this.createBidList",this.createBidList)
}

// Winner Wise
chooseWinnerWise(){
  this.bidRep = "O";
}
// CA Wise
chooseCAWise(){
  this.bidRep = "C";
}
// Trader Wise
chooseTraderWise(){
  this.bidRep = "T";
}
// Choose Report Type
chooseReportType(reportType){
  this.reportType = reportType;
  console.log("reportType",this.reportType)
}
}
