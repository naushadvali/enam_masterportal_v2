import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidDeclarationComponent } from './bid-declaration.component';

describe('BidDeclarationComponent', () => {
  let component: BidDeclarationComponent;
  let fixture: ComponentFixture<BidDeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidDeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidDeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
