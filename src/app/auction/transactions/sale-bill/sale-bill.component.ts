import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-sale-bill',
  templateUrl: './sale-bill.component.html',
  styleUrls: ['./sale-bill.component.css']
})
export class SaleBillComponent implements OnInit {
public my_class1 ='overlay';public my_class2 ='overlay';public my_class3 ='overlay';
public loading = false;
constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router
  ) { }
  dateInsert:any;
  agrDate:any;
  saleBillDate:any;
  ngOnInit() {
  }
  errorPopupShow(){
        this.my_class1 = 'overlay1';
      }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  // Fetch all invoices
  invoiceList = [];filteredInvoiceList=[];
  getInvoiceList(){
    if(this.agrDate != undefined && this.agrDate != ""){
      var dateObj ={
        "fromDate": this.agrDate.formatted
      }
      this.loading = true;
      this.authservice.getInvoiceList(dateObj).subscribe(invoiceList=>{
        this.invoiceList = [];this.filteredInvoiceList=[];
        if(invoiceList.listData != undefined){
          this.invoiceList = invoiceList.listData;
          this.filteredInvoiceList = invoiceList.listData;
        }
        this.loading = false;
      }, (err) => {
                if (err === 'Unauthorized')
                {
                        this.errorPopupShow();
                        setTimeout(()=>{
                          this.my_class1 = 'overlay';
                          localStorage.clear();
                          this.router.navigateByUrl('/login');
                        },3000);
            }
          }  );
    }
    else{
      this.my_class3 = 'overlay1';
      setTimeout(()=>{
        this.my_class3 = 'overlay';
      },2000);    }
  }


// On Invoice Details popup
invoiceDetails=[];selInvoice=[];
onInvoiceDetails(post){
this.selInvoice = post;
this.my_class2 = 'overlay1';
this.loading = true;
this.authservice.showInvoiceDetails(this.selInvoice).subscribe(invoiceDetails=>{
  this.loading = false;
  if(invoiceDetails.listData != undefined){
    this.invoiceDetails = invoiceDetails.listData;
  }
}, (err) => {
          if (err === 'Unauthorized')
          {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class1 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
      }
    }  );
}

// Hide Invoice Details popup
hideInvoiceDetailsPoup(){
  this.invoiceDetails =[];
  this.my_class2 = 'overlay';
}

// Filter Agreement Number
filtAgr:string;
onFilterAgrNo(){
  if(this.filtAgr) {
    this.filteredInvoiceList = _.filter(this.invoiceList, (a)=>a.auchTranid.indexOf(this.filtAgr)>=0);
  } else {
    this.filteredInvoiceList = this.invoiceList;
  }
}

// Filter Lot Code
filtLot:string;
onFilterLot(){
  if(this.filtLot) {
    this.filteredInvoiceList = _.filter(this.invoiceList, (a)=>a.iledLotCode.indexOf(this.filtLot)>=0);
  } else {
    this.filteredInvoiceList = this.invoiceList;
  }
}


// reset first page
onReset(){
  this.filtAgr = this.filtLot = this.agrDate  = "";
  this.filteredInvoiceList = this.invoiceList = [];
  this.filtAgr2 = this.filtLot2 = this.saleBillDate  = "";
  this.filteredPrintInvoiceList = this.printInvoiceList = [];
}

// Fetch all print invoices
printInvoiceList = [];filteredPrintInvoiceList=[];
getPrintInvoiceList(){
  if(this.saleBillDate != undefined && this.saleBillDate != ""){
    var dateObj ={
      "fromDate": this.saleBillDate.formatted
    }
    this.loading = true;
    this.authservice.getPrintInvoiceList(dateObj).subscribe(invoiceList=>{
      this.loading = false;
      this.printInvoiceList = [];this.filteredPrintInvoiceList=[];
      if(invoiceList.listData != undefined){
        this.printInvoiceList = invoiceList.listData;
        this.filteredPrintInvoiceList = invoiceList.listData;
      }
    }, (err) => {
              if (err === 'Unauthorized')
              {
                      this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
          }
        }  );
  }
  else{
    this.my_class3 = 'overlay1';
    setTimeout(()=>{
      this.my_class3 = 'overlay';
    },2000);
  }

}

// Filter Agreement Number For Tab2
filtAgr2:string;
onFilterAgrNoTab2(){
  if(this.filtAgr2) {
    this.filteredPrintInvoiceList = _.filter(this.printInvoiceList, (a)=>a.auchTranid.indexOf(this.filtAgr2)>=0);
  } else {
    this.filteredPrintInvoiceList = this.printInvoiceList;
  }
}

// Filter Lot Code [(ngModel)]="filtLot" (input)="onFilterLot()"
filtLot2:string;
onFilterLotTab2(){
  if(this.filtLot2) {
    this.filteredPrintInvoiceList = _.filter(this.printInvoiceList, (a)=>a.iledLotCode.indexOf(this.filtLot2)>=0);
  } else {
    this.filteredPrintInvoiceList = this.printInvoiceList;
  }
}

// Print invoice for tab2
printInvoicetab2(postJson){
  this.loading = true;
  this.authservice.printInvoiceTab2Pdf(postJson).subscribe(
    (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
        this.loading = false;
      }
      , (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
              }
            }
  )
}
// Print invoice for tab1
printInvoicetab1(postJson){
  this.loading = true;
  this.authservice.printInvoiceTab1Pdf(postJson).subscribe(
    (res) => {
      this.onReset();
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
        this.loading = false;
      }
      , (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
              }
            }
  )
}

}
