import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidRejectionComponent } from './bid-rejection.component';

describe('BidRejectionComponent', () => {
  let component: BidRejectionComponent;
  let fixture: ComponentFixture<BidRejectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidRejectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidRejectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
