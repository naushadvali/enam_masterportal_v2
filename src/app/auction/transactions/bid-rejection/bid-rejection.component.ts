import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-bid-rejection',
  templateUrl: './bid-rejection.component.html',
  styleUrls: ['./bid-rejection.component.css'],
  providers: [DatePipe]
})
export class BidRejectionComponent implements OnInit {
dateInsert:any;
declarationDate:any;
public my_class1='overlay';
public my_class2='overlay';
public loading = false;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router :Router,
    private datepipe:DatePipe
  ) { }

  activeSelect:boolean;
  activeDeSelect:boolean;

  ngOnInit() {
    this.activeSelect = false;
    this.activeDeSelect = false;
  }
  errorPopupShow(){
    this.my_class1='overlay1';
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

// Search Declared Bid
onBidRejSearch(){
  this.activeCreate = false;
  this.toRejBidLists =[];
  this.filteredBidLists =[];
  if(this.declarationDate.jsdate == undefined){
  var dd = this.declarationDate.date.day;
  var mm = this.declarationDate.date.month;
  if(dd<10){
    dd = "0"+dd;
  }
  if(mm<10){
    mm = "0"+mm;
  }
  var filtFromDate = (mm+"/"+dd+"/"+this.declarationDate.date.year);
  this.declarationDate.formatted = filtFromDate;
  }

  console.log("declarationDate",this.declarationDate)

  var postJson ={
    "fromDate": this.declarationDate.formatted
  }
  this.loading = true;
  this.authservice.searchDeclaredBid(postJson).subscribe($data=>{
    if($data.listData != undefined){
    this.toRejBidLists = $data.listData ;
    this.filteredBidLists = $data.listData ;
    this.activeSelect = true;
  } else {
    this.activeSelect = false;
  }
  this.loading = false;
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}


createBidList=[];
isPresent:boolean;
activeCreate:boolean;
onCheckBid(post,isChecked,index){
console.log(post,isChecked,index,  (<HTMLInputElement>document.getElementById("checkId_"+index)).checked)

  this.isPresent = false;
  this.activeCreate = false;

  // For Multiple
  if(isChecked == true){
    for(var i = 0; i < this.createBidList.length; i++) {
      if (this.createBidList[i].aboLotId == post.aboLotId) {
        this.isPresent = true;
        break;
      }
    }
    if(this.isPresent == false){
      console.log("here")
     this.createBidList.push(post);
    }
  }
  else{
    for(var i = 0; i < this.createBidList.length; i++) {
      if (this.createBidList[i].aboLotId == post.aboLotId) {
        this.createBidList.splice(i,1);
        break;
      }
    }
  }
  if( this.createBidList.length != 0){
    this.activeDeSelect = true;
    this.activeCreate = true;
  } else{
    this.activeDeSelect = false;
    this.activeCreate = false;
  }

  console.log(this.createBidList)

}

// Save Reject Bid
onSaveRejectedBid(){
  console.log("createBidList",this.createBidList)

  // this.my_class2 = 'overlay1';
  // setTimeout(()=>{
  //   this.my_class2 = 'overlay';
  // },2000);
  this.loading = true;
  this.authservice.saveRejectedBid(this.createBidList).subscribe($data=>{
    this.loading = false;
    if($data.status == '1'){
      this.my_class2 = 'overlay1';
      setTimeout(()=>{
        this.my_class2 = 'overlay';
      },2000);
      this.onBidRejSearch();
    }
  }, (err) => {
              if (err === 'Unauthorized')
              {
                this.errorPopupShow();
                      setTimeout(()=>{
                        this.my_class1 = 'overlay';
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                      },3000);
              }
        });
}


// Filter By Commdity Name
matchCommodityName:string;
toRejBidLists =[];
filteredBidLists =[];
onCommodity(){
  this.matchCommodityName=this.matchCommodityName.toUpperCase();
  var filteredData = _.filter(this.toRejBidLists, (a)=>a.gmpmgProdName.indexOf(this.matchCommodityName)>=0);
  this.filteredBidLists = filteredData;
}

// Filter By lotcode
matchLotCode:string;
onLotCode(){
  var filteredData = _.filter(this.toRejBidLists, (a)=>a.iledLotCode.indexOf(this.matchLotCode)>=0);
  this.filteredBidLists = filteredData;
}


// Select All
onSelectAll(){
  this.createBidList = [];
  for(var i=0;i<this.toRejBidLists.length;i++){
    (<HTMLInputElement>document.getElementById("checkId_"+i)).checked =true;
      this.createBidList.push(this.toRejBidLists[i])
  }
  console.log(this.createBidList)
  if(this.createBidList.length!=0){
     this.activeCreate = true;
     this.activeDeSelect = true;
     //this.activeSelect = false;
  }
}

// Deselect All
onDeSelectAll(){
  this.createBidList = [];
  for(var i=0;i<this.toRejBidLists.length;i++){
    (<HTMLInputElement>document.getElementById("checkId_"+i)).checked =false;
  }
  console.log(this.createBidList)
  this.activeCreate = false;
  this.activeDeSelect = false;
  //this.activeSelect = true;
}

// Reset Rejected Bid
today:any;
onResetBidCreation(){
  this.createBidList = [];
  this.filteredBidLists = [];
  this.toRejBidLists = [];
  this.today = new Date();
  var dd = this.today.getDate();
  var mm = this.today.getMonth()+1; //January is 0!
  var yyyy = this.today.getFullYear();
  this.declarationDate =
  { date: { year: this.today.getFullYear(), month: mm, day: dd } };
  this.matchLotCode = "";
  this.matchCommodityName = "";
}

}
