import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import * as _ from 'lodash';


@Component({
  selector: 'app-bid-creation',
  templateUrl: './bid-creation.component.html',
  styleUrls: ['./bid-creation.component.css'],
  providers: [DatePipe]
})
export class BidCreationComponent implements OnInit {
  public loading = false;
  fromDate:any;
  toDate:any;
  defFromDate:any;
  defToDate:any;
  today:any;
  dateInsert:any;
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';
  public my_class9='overlay';
  bidType:string;
  matchLotCode:string;
  matchCommodityName:string;
  matchAgentName:string;
  matchSellerName:any;
  matchTraderName:any;
  matchCompanyName:any;

  date: Date = new Date();
      settings = {
          bigBanner: true,
          timePicker: true,
          format: 'dd/MM/yyyy HH:mm:ss',
          defaultOpen: false
      }
  viewBidDate:any;
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private datepipe:DatePipe,
    private router:Router
  ) { }

  activeSelect:boolean;
  activeDeSelect:boolean;

  ngOnInit() {
    this.today = new Date();
    var dd = this.today.getDate();
    var mm = this.today.getMonth()+1; //January is 0!
    var yyyy = this.today.getFullYear();
    this.fromDate =
    { date: { year: this.today.getFullYear(), month: mm, day: dd } };
    this.toDate =
    { date: { year: this.today.getFullYear(), month: mm, day: dd } };
    this.bidType = "O";
    this.defFromDate = this.fromDate;
    this.defToDate = this.toDate;
    this.activeSelect = false;
    this.activeDeSelect = false;
    this.viewBidDate = this.defFromDate ;
    this.onViewBidSearchByDate();
  }
  errorPopupShow(){
    this.my_class1='overlay1';
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  bidCreationList=[];
  filtererdBidCreationList=[];
  onBidListSearch(){
    this.bidCreationList = [];
    this.filtererdBidCreationList = [];
    console.log("from",this.fromDate);
    console.log("to",this.toDate);
    if(this.fromDate != undefined && this.toDate != undefined && this.fromDate != "" && this.toDate != "" ){
      var toDate,fromDate,filtFromDate,filtToDate,currentDate;
      if(this.fromDate.jsdate == undefined){
        var dd = this.fromDate.date.day;
        var mm = this.fromDate.date.month;
        if(dd<10){
          dd = "0"+dd;
        }
        if(mm<10){
          mm = "0"+mm;
        }
         filtFromDate = (mm+"/"+dd+"/"+this.fromDate.date.year);
         fromDate = (this.fromDate.date.year+'-'+mm+'-'+dd) + ' 00:00:00';
        console.log("here",fromDate)
      }
      if(this.toDate.jsdate == undefined){
        var dd = this.toDate.date.day;
        var mm = this.toDate.date.month;
        if(dd<10){
          dd = "0"+dd;
        }
        if(mm<10){
          mm = "0"+mm;
        }
         toDate = (this.toDate.date.year+'-'+mm+'-'+dd) + ' 00:00:00';
         filtToDate = (mm+"/"+dd+"/"+this.toDate.date.year);
        console.log("here to",toDate)
      }
      currentDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd') + ' 00:00:00'

      if(this.fromDate.jsdate != undefined ){
         fromDate = this.datepipe.transform(new Date(this.fromDate.jsdate), 'yyyy-MM-dd') + ' 00:00:00'
      }
      if(this.toDate.jsdate != undefined){
         toDate = this.datepipe.transform(new Date(this.toDate.jsdate), 'yyyy-MM-dd') + ' 00:00:00'
      }

      console.log("dates",fromDate,toDate)
      if(fromDate <= toDate){
        if(toDate <= currentDate ){
          if(this.fromDate.jsdate == undefined || this.toDate.jsdate == undefined){
            var timeDiff = Math.abs(new Date(filtFromDate).getTime() - new Date(filtToDate).getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          }
          else{
            var timeDiff = Math.abs(this.fromDate.jsdate.getTime() - this.toDate.jsdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          }

             if(diffDays > 4){
               this.flashMessagesService.show('Maximum Date Difference Of 3 Days Is Allowed Between From And To Dates', { cssClass: 'alert-danger', timeout: 3000 });
             }
             else{
               let postJson= {
                 "fromDate":"",
                 "toDate": ""
               }
               var postFromDate,postToDate;
                  if(this.fromDate.formatted == undefined){
                    postFromDate = this.getConvertedDate(this.fromDate);
                  }
                  else{
                    postFromDate = this.fromDate.formatted;
                  }
                  if(this.toDate.formatted == undefined){
                    postToDate = this.getConvertedDate(this.toDate);
                  }
                  else{
                    postToDate = this.toDate.formatted;
                  }
                  postJson= {
                   "fromDate": postFromDate,
                   "toDate": postToDate
                 }
               console.log("postJson",postJson)
               this.loading = true;
               this.authservice.searchBidCreationList(postJson).subscribe($data=>{
                 console.log("bid listing search: ",$data);
                 if($data.listData != undefined){
                   this.bidCreationList = $data.listData;
                   this.filtererdBidCreationList = $data.listData;
                   this.activeSelect = true;
                 } else {
                   this.activeSelect = false;
                   this.activeDeSelect = false;
                 }
                 this.loading = false;
               }, (err) => {
                           if (err === 'Unauthorized')
                           {
                             this.errorPopupShow();
                                   setTimeout(()=>{
                                     this.my_class1 = 'overlay';
                                     localStorage.clear();
                                     this.router.navigateByUrl('/login');
                                   },3000);
                           }
                     });

             }
        }
        else{
        //  this.flashMessagesService.show('To-Date Should Be On Or Before Current Date', { cssClass: 'alert-danger', timeout: 3000 });
        this.my_class5 = 'overlay1';
        setTimeout(()=>{
          this.my_class5 = 'overlay';
        },2000);
        }
      }
      else{
        //this.flashMessagesService.show('From-Date Should Be On Or Before To-Date', { cssClass: 'alert-danger', timeout: 3000 });
        this.my_class6 = 'overlay1';
        setTimeout(()=>{
          this.my_class6 = 'overlay';
        },2000);
      }
    }
    else{
    //  this.flashMessagesService.show('Dates Are Mandatory To Search', { cssClass: 'alert-danger', timeout: 3000 });
    this.my_class7 = 'overlay1';
    setTimeout(()=>{
      this.my_class7 = 'overlay';
    },2000);
    }
  }


getConvertedDate(inDate){
  var dd = inDate.date.day;
  var mm = inDate.date.month;
  if(dd<10){
    dd = "0"+dd;
  }
  if(mm<10){
    mm = "0"+mm;
  }
  var outDate= (dd+"/"+ mm+"/"+inDate.date.year);
  return outDate;
}


createBidList=[];
isPresent:boolean;
activeCreate:boolean;
onCheckBid(post,isChecked,index){
    console.log(post,isChecked,index,  (<HTMLInputElement>document.getElementById("checkId_"+index)).checked
)

  this.isPresent = false;

  if(isChecked == true){
    for(var i = 0; i < this.createBidList.length; i++) {
      if (this.createBidList[i].iledLotId == post.iledLotId) {
        this.isPresent = true;
        break;
      }
    }
    if(this.isPresent == false){
      console.log("here")
     this.createBidList.push(post);
    }
  }
  else{
    for(var i = 0; i < this.createBidList.length; i++) {
      if (this.createBidList[i].iledLotId == post.iledLotId) {
        this.createBidList.splice(i,1);
        break;
      }
    }
  }
  if( this.createBidList.length != 0){
    this.activeCreate = true;
    this.activeSelect = true;
    this.activeDeSelect = true;
  }else{
    this.activeDeSelect = false;
    this.activeCreate = false;
  }
  console.log(this.createBidList)

}


onCreateBids(){
  if(this.createBidList.length != 0){
    this.my_class2 = "overlay1";
  }else{
      this.my_class4 = 'overlay1';
      setTimeout(()=>{
        this.my_class4 = 'overlay';
      },2000);
  }
  this.activeCreate = false;
}

autoAssignChecked:boolean;
allowMultiChecked:boolean;
div_hide_bid(){
  this.my_class2 = "overlay";
  this.startTime = new Date();
  this.closingTime = new Date();
  this.declarationTime = new Date();
  this.autoAssignWinner = "";
  this.minBuyers = "";
  this.allowMultiBid = "";
  this.minSellingPrice = "";
  this.autoAssignChecked = false;
  this.allowMultiChecked = false;
}

minBuyers:string;
minSellingPrice:string;
startTime:any;
closingTime:any;
declarationTime:any;
invalidMoment:any;
autoAssignWinner:any;allowMultiBid:any;

createBidData(){
  console.log("here",this.startTime)
  console.log("createBidList",this.createBidList)
  if(this.startTime == "" || this.startTime == undefined){
    this.startTime = new Date();
  }
  if(this.closingTime == "" || this.closingTime == undefined){
    this.closingTime = new Date();
  }
  if(this.declarationTime == "" || this.declarationTime == undefined){
    this.declarationTime = new Date();
  }

  if((this.minBuyers == "" && this.minBuyers == undefined) || (this.startTime == "" && this.startTime == undefined) ||
  (this.closingTime == "" && this.closingTime == undefined) || (this.declarationTime == "" && this.declarationTime == undefined)){
    this.flashMessagesService.show('Please Enter All The Mandatory Fields', { cssClass: 'alert-danger', timeout: 3000 });
  }
  else{
    if(this.minBuyers != "" && this.minBuyers != undefined){
      if(this.startTime != "" && this.startTime != undefined){
        if(this.closingTime != "" && this.closingTime != undefined){
          if(this.declarationTime != "" && this.declarationTime != undefined){


            var date1 = new Date(this.startTime);
            var date2 = new Date(this.closingTime);
            var date3 = new Date(this.declarationTime);
            var today = new Date();

            var timeDiff0 = (date1.getTime() - today.getTime());
            var diffDays0 = Math.ceil(timeDiff0 / (1000 * 3600 * 24));
            if(diffDays0 >=0){
              var timeDiff1 = (date2.getTime() - date1.getTime());
              var diffDays1 = Math.ceil(timeDiff1 / (1000 * 3600 * 24));

              if(diffDays1 >0){
                var timeDiff2 = (date3.getTime() - date2.getTime());
                var diffDays2 = Math.ceil(timeDiff2 / (1000 * 3600 * 24));

                if(diffDays2 >0){
                  if(this.autoAssignChecked == true){
                  this.autoAssignWinner = "Y";
                  }
                  else{
                    this.autoAssignWinner = "N";
                  }

                  if(this.allowMultiChecked == true){
                  this.allowMultiBid = "Y";
                  }
                  else{
                    this.allowMultiBid = "N";
                  }

                  var startTime = this.datepipe.transform((this.startTime), 'dd/MM/yyyy HH:mm:ss');
                  var closingTime = this.datepipe.transform((this.closingTime), 'dd/MM/yyyy HH:mm:ss')
                  var declarationTime = this.datepipe.transform((this.declarationTime), 'dd/MM/yyyy HH:mm:ss')

                  for(var i=0;i<this.createBidList.length;i++){
                    this.createBidList[i].startDateTime = startTime;
                    this.createBidList[i].endDateTime = closingTime;
                    this.createBidList[i].bidOpenDateTime = declarationTime;
                    this.createBidList[i].gmpmBidType = this.bidType;
                    this.createBidList[i].gmpmAutoAssinWiner = this.autoAssignWinner;
                    this.createBidList[i].gmpmMinBuyers = this.minBuyers;
                    this.createBidList[i].gmpmSubMultBid = this.allowMultiBid;
                    this.createBidList[i].gmpmMspBidRate = this.minSellingPrice;
                    this.createBidList[i].gmpmAllowPartial = "N";  //may Change in future
                  }

                  console.log("this.createBidList",this.createBidList)

                this.loading = true;
                this.authservice.createBid(this.createBidList).subscribe($data=>{
                  console.log("saved ",$data);
                  this.loading = false;
                  if($data.status == '1'){
                    this.my_class3 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class3 = 'overlay';
                      this.my_class2 = "overlay";
                    },2000);
                    this.createBidList =[];this.filtererdBidCreationList =[];
                    this.bidCreationList=[];
                    this.startTime = new Date();
                    this.closingTime = new Date();
                    this.declarationTime = new Date();
                    this.autoAssignWinner = "";
                    this.minBuyers = "";
                    this.allowMultiBid = "";
                    this.minSellingPrice = "";
                    this.autoAssignChecked = false;
                    this.allowMultiChecked = false;
                    this.onBidListSearch();
                  }
                }, (err) => {
                            if (err === 'Unauthorized')
                            {
                              this.errorPopupShow();
                                    setTimeout(()=>{
                                      this.my_class1 = 'overlay';
                                      localStorage.clear();
                                      this.router.navigateByUrl('/login');
                                    },3000);
                            }
                            else{
                              this.my_class8 = 'overlay1';
                              setTimeout(()=>{
                                this.my_class8 = 'overlay';
                              },2000);
                            }
                      });
                }
                else{
                  this.flashMessagesService.show('Bid Declarartion Time Should Be Greater Than Bid Closing Time', { cssClass: 'alert-danger', timeout: 3000 });
                }
              }
              else{
                this.flashMessagesService.show('Bid Closing Time Should Be Greater Than Bid Start Time', { cssClass: 'alert-danger', timeout: 3000 });
              }
            }
            else{
              this.flashMessagesService.show('Bid Start Time Should Be Greater Or Equal To Current Time', { cssClass: 'alert-danger', timeout: 3000 });
            }
          }
          else{
            this.flashMessagesService.show('Please Enter Bid Open Date', { cssClass: 'alert-danger', timeout: 3000 });

          }
        }
        else{
          this.flashMessagesService.show('Please Enter Bid End Date', { cssClass: 'alert-danger', timeout: 3000 });

        }

      }
      else{
        this.flashMessagesService.show('Please Enter Bid Start Date', { cssClass: 'alert-danger', timeout: 3000 });
      }

    }
    else{
      this.flashMessagesService.show('Please Enter Minimum Number Of Buyers', { cssClass: 'alert-danger', timeout: 3000 });
    }
  }
}

onOpenBidType(){
  this.bidType = "O";
  console.log("this.bidType open",this.bidType)

}
onCloseBidType(){
  this.bidType = "C";
  console.log("this.bidType close",this.bidType)
}

// Filter Data
onLotCode(){
  var filteredData = _.filter(this.bidCreationList, (a)=>a.iledLotCode.indexOf(this.matchLotCode)>=0);
  this.filtererdBidCreationList = filteredData;
}
onCommodity(){
  var filteredData = _.filter(this.bidCreationList, (a)=>a.gmpmgProdName.indexOf(this.matchCommodityName)>=0);
  this.filtererdBidCreationList = filteredData;
}

onAgentName(){
  if(this.matchAgentName != ""){
    var filteredData = _.filter(
        this.bidCreationList, (a)=>{
          if(a.lcam_full_name != undefined){
            a.lcam_full_name.indexOf(this.matchAgentName)>=0
          }
        });
    this.filtererdBidCreationList = filteredData;
  }
  else{
    this.filtererdBidCreationList = this.bidCreationList;
  }
}

onCompanyName(){
  if(this.matchCompanyName != ""){
    var filteredData = _.filter(
        this.bidCreationList, (a)=>{
          if(a.lcamAgenCompName != undefined){
            a.lcamAgenCompName.indexOf(this.matchCompanyName)>=0
          }
        });
    this.filtererdBidCreationList = filteredData;
  }
  else{
    this.filtererdBidCreationList = this.bidCreationList;
  }
}

onSeller(){
// console.log("sellerName",this.matchSellerName);
// var filteredData = _.filter(this.bidCreationList, (a)=>a.frmFullName.indexOf(this.matchSellerName)>=0);
// this.filtererdBidCreationList = filteredData;

if(this.matchSellerName != ""){
  var filteredData = _.filter(
      this.bidCreationList, (a)=>{
        if(a.frmFullName != undefined){
          a.frmFullName.indexOf(this.matchSellerName)>=0
        }
      });
this.filtererdBidCreationList = filteredData;
}
else{
  this.filtererdBidCreationList = this.bidCreationList;
}
}

onTrader(){
// var filteredData = _.filter(this.bidCreationList, (a)=>a.lcamFullName.indexOf(this.matchTraderName)>=0);
// this.filtererdBidCreationList = filteredData;

if(this.matchTraderName != ""){
  var filteredData = _.filter(
      this.bidCreationList, (a)=>{
        if(a.lcamFullName != undefined){
          a.lcamFullName.indexOf(this.matchTraderName)>=0
        }
      });
this.filtererdBidCreationList = filteredData;
}
else{
  this.filtererdBidCreationList = this.bidCreationList;
}
}


onSelectAll(){
  this.createBidList = [];
  for(var i=0;i<this.bidCreationList.length;i++){
    (<HTMLInputElement>document.getElementById("checkId_"+i)).checked =true;
      this.createBidList.push(this.bidCreationList[i])
  }
  console.log(this.createBidList)
  if(this.createBidList.length!=0){
     this.activeCreate = true;
     this.activeDeSelect = true;
  }
}
onDeSelectAll(){
  this.createBidList = [];
  for(var i=0;i<this.bidCreationList.length;i++){
    (<HTMLInputElement>document.getElementById("checkId_"+i)).checked =false;
  }
  console.log(this.createBidList)
  this.activeCreate = false;
  this.activeDeSelect = false;
}

onResetBidCreation(){
  this.createBidList = [];
  this.filtererdBidCreationList = [];
  this.bidCreationList = [];
  this.toDate = this.defToDate;
  this.fromDate = this.defFromDate;
  this.matchLotCode = "";
  this.matchCommodityName = "";
  this.matchAgentName = "";
  this.matchSellerName = "";
  this.matchTraderName = "";
  this.matchCompanyName = "";
  this.activeSelect = false;
  this.activeDeSelect = false;
}

viewBidList =[];filteredViewBidList= [];
onViewBidSearchByDate(){
  var filtViewDate;var dateObj;
  console.log("viewBidDate",this.viewBidDate)
  var dd = this.viewBidDate.date.day;
  var mm = this.viewBidDate.date.month;
  if(dd<10){
    dd = "0"+dd;
  }
  if(mm<10){
    mm = "0"+mm;
  }
  filtViewDate = (mm+"/"+dd+"/"+this.viewBidDate.date.year);
  console.log("here to",filtViewDate)
  dateObj = {"fromDate": filtViewDate}

  if(this.viewBidDate != undefined && this.viewBidDate != "" && this.viewBidDate != null){
    this.loading = true;
    this.authservice.viewBidList(dateObj).subscribe($data=>{
      console.log("bid view search: ",$data);
      this.viewBidList =[];
      this.filteredViewBidList =[];
      if($data.listData != undefined){
        this.viewBidList = $data.listData;
        this.filteredViewBidList = $data.listData;
      }
      this.loading = false;
    }, (err) => {
                if (err === 'Unauthorized')
                {
                  this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class1 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
                }
          });
  }
  else{
    this.my_class9 = 'overlay1';
    setTimeout(()=>{
      this.my_class9 = 'overlay';
    },2000);
  }
}


// view bid search
viewLotCode:string;
onViewBidSearch(){
  var filtViewDate;var dateObj;
  console.log("viewLotCode",this.viewLotCode)
  if(this.viewLotCode != undefined && this.viewLotCode != ""){
    if(this.viewBidDate == null){
      this.viewBidDate = "";
      dateObj = {"lotId": this.viewLotCode.toString()}
    }
    else{
      if(this.viewBidDate.jsdate == undefined ){
        var dd = this.viewBidDate.date.day;
        var mm = this.viewBidDate.date.month;
        if(dd<10){
          dd = "0"+dd;
        }
        if(mm<10){
          mm = "0"+mm;
        }
        filtViewDate = (mm+"/"+dd+"/"+this.viewBidDate.date.year);
        console.log("here to",filtViewDate)
        dateObj = {"fromDate": filtViewDate,"lotId": this.viewLotCode.toString()}
      }
      else{
        dateObj = {"fromDate":this.viewBidDate.formatted,"lotId": this.viewLotCode.toString()}
      }
    }
    this.loading = true;
    if(this.viewBidDate != undefined && this.viewBidDate != "" && this.viewBidDate != null){
      this.authservice.viewBidList(dateObj).subscribe($data=>{
        console.log("bid view search: ",$data);
        this.viewBidList =[];
        this.filteredViewBidList =[];
        if($data.listData != undefined){
          this.viewBidList = $data.listData;
          this.filteredViewBidList = $data.listData;
        }
        this.loading = false;
      }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                          setTimeout(()=>{
                            this.my_class1 = 'overlay';
                            localStorage.clear();
                            this.router.navigateByUrl('/login');
                          },3000);
                  }
            });
    }
    else{
      this.my_class9 = 'overlay1';
      setTimeout(()=>{
        this.my_class9 = 'overlay';
      },2000);
    }
  }
  else{
      this.my_class9 = 'overlay1';
      setTimeout(()=>{
        this.my_class9 = 'overlay';
      },2000);
  }
}

//reset view bid
onResetViewBid(){
  this.viewBidDate = this.defFromDate;
  this.viewLotCode = "";
  this.filteredViewBidList =[];
  this.viewBidList = [];
}
}
