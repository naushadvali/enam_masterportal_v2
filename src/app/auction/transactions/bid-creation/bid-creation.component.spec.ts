import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidCreationComponent } from './bid-creation.component';

describe('BidCreationComponent', () => {
  let component: BidCreationComponent;
  let fixture: ComponentFixture<BidCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
