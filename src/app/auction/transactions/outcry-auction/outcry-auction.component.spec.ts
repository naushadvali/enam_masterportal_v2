import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutcryAuctionComponent } from './outcry-auction.component';

describe('OutcryAuctionComponent', () => {
  let component: OutcryAuctionComponent;
  let fixture: ComponentFixture<OutcryAuctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutcryAuctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutcryAuctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
