import { Component,OnInit ,ViewChild, Compiler} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
 import { LocalStorageModule } from 'angular-2-local-storage';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { ValidateService } from '../services/validate.service';
import { RoleAccessService } from '../services/role-access.service';
import { FormsModule, ReactiveFormsModule,FormBuilder, Validators ,NG_VALIDATORS} from '@angular/forms';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs/Rx';


@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']

})
export class LoginComponent implements OnInit {
  userid:String;
  password:String;
  token1:any;
  err:string;
  userForm: any;
  createdInOprID:string;
  loginID:string;
  oprID:string;
  orgID:string;
  userEmailID:any;
  userName:string;
  userPhoneNo:string;
  userRefType:string;
  userType:string;
  locationList:any;
  //globalNLocation=[];
  location:string;
  userTypeSlocation=[];
  apmcSlocation:string;
  apmcSOprid=[];
  adoumOprID:string;
  userDefLnf:any;
  userRefVal:any;
  userGlobal:any;
  userMenuType:any;
  userID:any;
  globalNLocation:any;
  text:any;
  possible:any;
  captchaValue:any;
  emailId:any;
  public loading = false;
  loginError:any;
  english:any;
  hindi:any;
  gujurati:any;
  telegu:any;
  locAndOprId=[];


  constructor(
    private authservice:AuthService,
    private router:Router,
    private validateservice:ValidateService,
    private roleAccessService:RoleAccessService,
    private flashMessagesService: FlashMessagesService,
    public _compiler: Compiler
  ){}

  ngOnInit(){
      this.english=environment.eng;
      this.hindi=environment.hi;
      this.gujurati=environment.gu;
      this.telegu=environment.te;


      if(this.authservice.loggedIn()){
        this.router.navigate(['/dashboard']);

      }
      else if(!this.authservice.loggedIn()){
        this.router.navigate(['/login']);
      }
      this._compiler.clearCache();


      // this.text = "";
      // this.possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
      //
      // for (var i = 0; i < 5; i++){
      //   this.text += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
      // }
      // Observable.interval(1000 * 60).subscribe(x => {
      //    this.text = "";
      //    for (var i = 0; i < 5; i++)
      //     this.text += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
      //
      //
      //    });

}

  // checkCaptcha(){
  //     //console.log("hi");
  //     if(this.captchaValue!=this.text){
  //       this.flashMessagesService.show('Captcha mismatch', { cssClass: 'alert-danger', timeout: 2000 });
  //     }
  // }


  roleList =[];
  onLoginSubmit(){
    //console.log(this.userid);
    //console.log(this.password);
    //this.router.navigate(['/dashboard']);
    const user={
      loginID:this.userid,
      password:this.password
    };

     if(this.userid == undefined || this.userid == undefined || this.userid == ''){
      //this.err="All fields are required!!";
      this.flashMessagesService.show('Invalid userid', { cssClass: 'alert-danger', timeout: 3000 });

    }else if(this.password == undefined || this.password == undefined || this.password == ''){
      //this.err="All fields are required!!";
      this.flashMessagesService.show('Invalid password', { cssClass: 'alert-danger', timeout: 3000 });
    }else{

      //console.log("server error msg in login ts",this.authservice.errorStatus);

      // this.loginError=localStorage.getItem('loginError');
      // if(this.loginError==0){
      //   this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
      // }
      //this.loading=true;
      this.authservice.loginUser(user).subscribe(
          $ret=>{
            //console.log("response from server ",$ret);

            //console.log("Status:",$ret.status);
            if($ret.status==1){

              this.router.navigate(['/dashboard']);

              this.token1=$ret.message;
              //console.log("this is $ret.message token: ",$ret.message);
              this.createdInOprID=$ret.data.createdInOprID;
              this.loginID=$ret.data.loginID;
              this.oprID=$ret.data.oprID;
              this.orgID=$ret.data.orgID;
              this.userEmailID=$ret.data.userEmailID;
              this.userName=$ret.data.userName;
              this.userPhoneNo=$ret.data.userPhoneNo;
              this.userRefType=$ret.data.userRefType;
              this.userType=$ret.data.userType;
              this.locationList=$ret.data.locationList;
              this.userDefLnf=$ret.data.userDefLnf;
              this.userRefVal=$ret.data.userRefVal;
              this.userGlobal=$ret.data.userGlobal;
              this.userMenuType=$ret.data.userMenuType;
              this.userID=$ret.data.userID;
              // this.emailId=$ret.userEmailID;
              this.location=this.locationList.adoumOprName;
              this.adoumOprID=this.locationList.adoumOprID;
              //this.loading=false;
              //console.log("Location  in login ts : ",this.location,this.locationList[0]);
              //console.log("Location  in login ts : ",this.locationList[0].adoumOprName);
              // For Role List
              this.roleList=$ret.data.roleList;
            }else{
              // this.router.navigate(['/dashboard']);
              this.flashMessagesService.show('Invalid Login Credentials', { cssClass: 'alert-danger', timeout: 3000 });
              this.router.navigate(['/login']);
              //this.loading=false;
            }
            // this.text = "";
            // this.possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            //
            // for (var i = 0; i < 5; i++){
            //   this.text += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
            //   console.log("Random number is :",this.text);
            // }

              const token1=$ret.message;
            //console.log("Token loaded in Login TS",$ret.message);
            //console.log("this is the createdInOprId: ",$ret.data.createdInOprID);

            //
          //  if(this.userType=='M'){
            //  for(let i=0;i<=this.locationList.length;i++){


            //  }
            //}else{
            if(this.roleList != undefined){
              localStorage.setItem('roleList',JSON.stringify(this.roleList));
              this.roleAccessService.getAdminRoleAccess();
              this.roleAccessService.getEntryExitRoleAccess();
              this.roleAccessService.getLotOpRoleAccess();
              this.roleAccessService.getAuctionRoleAccess();
            }
            if(this.locationList != undefined){
              for(let i=0;i<=this.locationList.length-1;i++){

                 this.location=this.locationList[i].adoumOprName;
                 this.apmcSOprid=this.locationList[i].adoumOprID;
                 this.adoumOprID=this.locationList[i].adoumOprID;
                 this.locAndOprId.push(this.locationList[i]);
                 //console.log("Concatenated: ",this.locAndOprId);
                 if(this.oprID==this.locationList[i].adoumOprID){
                    this.apmcSlocation=this.locationList[i].adoumOprName;

                 }
              }
            }


            //}

            //localStorage.setItem('userDefLnf',this.userDefLnf);
            localStorage.setItem('loginID',this.loginID);
            localStorage.setItem('oprID',this.oprID);
            localStorage.setItem('orgID',this.orgID);
            localStorage.setItem('location',this.location);
            localStorage.setItem('userName',this.userName);
            localStorage.setItem('userPhoneNo',this.userPhoneNo);
            //localStorage.setItem('userRefType',this.userRefType);
            localStorage.setItem('userType',this.userType);
            //localStorage.setItem('apmcSlocation',this.apmcSlocation);
            // localStorage.setItem('arrayLocation',JSON.stringify(this.locAndOprId));
            // localStorage.setItem('userRefVal',this.userRefVal);
            localStorage.setItem('adoumOprID',this.adoumOprID);
            //localStorage.setItem('userGlobal',this.userGlobal);
            //localStorage.setItem('userMenuType',this.userMenuType);
            localStorage.setItem('userID',this.userID);
            localStorage.setItem('userEmailID',this.userEmailID);
            if(this.authservice.loggedIn()){
              this.router.navigate(['/dashboard']);
            }
             if($ret.status==1){
               //console.log("this is $ret.message: ",$ret.message);
               this.authservice.storeUserData(token1,{});

             }
            else{
              this.router.navigate(['/login']);
              this.flashMessagesService.show('Invalid Login Credentials', { cssClass: 'alert-danger', timeout: 3000 });
            }

            }
        )}
      };



}
