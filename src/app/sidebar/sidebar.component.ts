import { Component ,OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { RoleAccessService } from '../services/role-access.service';
@Component({
    moduleId: module.id,
    selector: 'sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['sidebar.component.css']
})
export class SidebarComponent implements OnInit{
  constructor(public authservice:AuthService,public roleAccess:RoleAccessService){
  //console.log("In side bar");
  }

  ngOnInit(){

  }

}
