import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import * as FileSaver from 'file-saver';
import { ResponseContentType } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  user:any;
  sendPreference:any;
  authUrl:any;
  token1:string;
  dateInsert:string;
  //lotcode:String;
  invoiceno:string;
  loginUrl:any;
  logoutUrl:any;
  location:any;
  bidauthUrl:any;
  getpdf:any;
  apmclocation:string;
  agent:string;
  commodity:string;
  lotcode:string;
  seller:string;
  village:string;
  createdInOprID:string;
  loginID:string;
  oprID:string;
  orgID:string;
  userEmailID:string;
  userName:string;
  userPhoneNo:string;
  userRefType:string;
  addprefer:string;
  bidnow:string;
  result:any;
  locationName:any;
  userFullName:any;
  testUrl:any;
  text:any;
  possible:any;
  getTable=[];


  constructor(private http:Http) {
    //this.token="435453735ybdgjfa";

    // this.authUrl="http://staging.techl33t.com:8000/enam/api/view/";
    // this.loginUrl="http://staging.techl33t.com:8000/enam/api/login";
    // this.logoutUrl="http://staging.techl33t.com:8000/enam/api/logout";
    // this.bidauthUrl="http://staging.techl33t.com:8000/enam/api/view/newBidList/";
    // this.getpdf="http://staging.techl33t.com:8000/enam/api/view/pdf";


    // this.testUrl="http://192.168.0.125:8080/enam/api/view/";
    // this.authUrl="http://staging.techl33t.com:8181/enam/api/view/";
    // this.loginUrl="http://staging.techl33t.com:8181/enam/api/login";
    // this.logoutUrl="http://staging.techl33t.com:8181/enam/api/logout";
    // this.bidauthUrl="http://staging.techl33t.com:8181/enam/api/view/newBidList/";
    // this.getpdf="http://staging.techl33t.com:8181/enam/api/view/pdf";
    // this.addprefer="http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity";
    // this.bidnow="http://staging.techl33t.com:8181/enam/api/view/bidNow";



    // this.authUrl="http://10.247.33.159:8080/enam/api/view/";
    // this.loginUrl="http://10.247.33.159:8080/enam/api/login";
    // this.logoutUrl="http://10.247.33.159:8080/enam/api/logout";
    // this.bidauthUrl="http://10.247.33.159:8080/enam/api/view/newBidList/";
    // this.getpdf="http://10.247.33.159:8080/enam/api/view/pdf";
    // this.addprefer="http://10.247.33.159:8080/enam/api/view/onAddPrefferedCommodity";
    // this.bidnow="http://10.247.33.159:8080/enam/api/view/bidNow";
  }

  //PENDING LIST
  /*getPendingList(dateInsert,lotcode,invoiceno){
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token);
  headers.append('Content-type','application/json');
  return this.http.get(this.authUrl+'pendingList/'+dateInsert+'&'+lotcode+'&'+invoiceno,{headers:headers})
  .map(res=>res.json())
  .catch((error:any) => Observable.throw('Server error'));
}*/

//login

    loginUser(user){
      let headers = new Headers();
      headers.append('Content-type','application/json');
      return this.http.post(environment.loginUrl,user,{headers:headers})
        .map(res=>res.json())
        .catch((error:any) => {
    		localStorage.setItem('loginError',error.status);
    		return Observable.throw('Server error')});
      //this.result=ret;
      //console.log("Result from service",JSON.parse(this.result._body).data.locationList[0].adoumOprName);
      //this.locationName=JSON.parse(this.result._body).data.locationList[0].adoumOprName;
      //this.userFullName=JSON.parse(this.result._body).data.userName;
    //  console.log("Full Name from service",JSON.parse(this.result._body).data.userName);

    }

    storeUserData(token1, user){
      //console.log("storageData in server",token1);
      this.text = "";
      this.possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

      for (var i = 0; i < 10; i++){
        this.text += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
        //console.log("Random number is :",this.text);
      }
      localStorage.setItem('Rx8CdJBiVK',token1);
      localStorage.setItem('user',JSON.stringify(user));

       this.token1 = token1;
       //console.log("this.token1 in server",this.token1);
      // this.user = user;
    }

    loadToken(){
      //const token1 = sessionStorage.getItem(this.text);
      //console.log("token in load token: ",token1);
      this.token1 = localStorage.getItem('Rx8CdJBiVK');
      //console.log("token in load token: ",this.token1);
    }
    getStorageData(key){
      //console.log("key from server",sessionStorage.getItem(key));
      return localStorage.getItem(key);
    }
    // getAuthUser(){
    //   const user = sessionStorage.getItem('user');
    //   this.user = JSON.parse(user);
    //   return this.user;
    // }
    getAuthUser(){
       if(this.loggedIn()){
         let user = localStorage.getItem("user");
         this.user = JSON.parse(user);
         return this.user;
       }
       else{
         return null;
       }
     }

    loggedIn() {
      if (localStorage.getItem('Rx8CdJBiVK') === null) {
        return false;
      }else{

        return true;
      }
      //return tokenNotExpired('token');
    }
    //LOGOUT
    loggedOut(){
      //sessionStorage.clear();
      this.loadToken();
      //console.log("local storage token in logged out: ",localStorage.getItem('token1'));
      //console.log("This token: ",this.token1);
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.logoutUrl,{headers:headers})
      .map(res=>{res.json()})
      //.catch((error:any) => Observable.throw('Server error'));
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //PASSWORD CHANGE
    getPasswordChange(sendPreference){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.authUrl+"preferenceSave/",sendPreference,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }




    //PENDING LIST
    getPendingList(fromDate,lotcode,invoiceno){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.authUrl+"pendingList/"+lotcode+"&"+invoiceno,{"fromDate":fromDate},{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //COMMODITY LIST
    getCommodityList(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'commodityList',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });

    }
    //PERMIT REQUESTED
    getPermitReq(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'getPermits',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }


    //PERMIT VIEW
    // getPermitView(){
    //   this.loadToken();
    //   let headers = new Headers();
    //   headers.append('Authorization','Bearer '+this.token);
    //   headers.append('Content-type','application/json');
    //   return this.http.get(this.authUrl+'permitsView',{headers:headers})
    //   .map(res=>res.json())
    //   .catch((error:any) => Observable.throw('Server error'));
    // }
    //TRADE VIEW
    getTradeView(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'tradeView',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //SECURITY DEPOSIT
    getSecurityDeposit(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'securityDeposits',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //OUTCRY
    getOutcry(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'outcry',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //BANK DETAILS
    getBankDetails(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'bankDetails',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //COMMODITY BID LISTING
    getCommodityBidListing(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'commodityBidListing',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //BID REPORT
    getBidreport(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'reports',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //GET LOCATION
    getLocation(){
      this.loadToken();
      let headers = new Headers();
      //console.log("Token from Get Location",this.token);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+'stateList',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    postLocation(location){
      this.loadToken();
      //console.log("Location from Auth Service: ",location);
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.authUrl+'apmcList',location,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    getNewBidListing(apmcOprId){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.bidauthUrl+apmcOprId,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    loadApmcData(apmcdata){
      this.loadToken();
      localStorage.setItem('apmcdata',apmcdata);
      //console.log("storage APMC data from Authservice: ",apmcdata);
    }

    getApmcData(){
      const gotApmcData=('apmcdata');
    }

    getChallanPdf(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/pdf');
      return this.http.get(environment.getpdf,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    sendPrefData(sendPreference){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.loginUrl,sendPreference,{headers:headers}).map(ret=>ret.json());
    }
    addPreferredData(addPrefered){
      this.loadToken();
      let headers = new Headers();
      //console.log("Added for sending data on click add prefred ",addPrefered);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.addprefer,addPrefered,{headers:headers}).map(ret=>ret.json());
    }

    // Get All Agents using APMC List Id
    getAgentList(apmcOprId){
      this.loadToken();
      let headers = new Headers();
      //console.log("Agent from Service",apmcOprId);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      //console.log("Check url",this.authUrl+"agentList/"+apmcOprId);
      return this.http.get(environment.authUrl+"agentList/"+apmcOprId,{headers:headers})

      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //Commodity bidlisting commodity details
    getCommodityListAgent(agentId){
      this.loadToken();
      let headers = new Headers();
      //console.log("Agent from Service",agentId);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      //console.log("Check url",environment.authUrl+"productList/"+agentId);
      return this.http.get(environment.authUrl+"productList/"+agentId,{headers:headers})

      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //Bid now button Newbidlisting
    bidNow(bidNow){
      this.loadToken();
      let headers = new Headers();
      //console.log("bid now details from service ",bidNow);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.bidnow,bidNow,{headers:headers}).map(ret=>ret.json());
    }

    //Bidhistory
    getBidHistory(date){
      this.loadToken();
      //console.log("Bidistory from service ",date);
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.authUrl+"myBidHistory/",date,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    getCustomerdetails(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+"traderInfo",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    accessCode:any;encRequest:any;acc:any;
    getCcaRequest(ccaRequest){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      //console.log("cca request format in server: ",ccaRequest);
      return this.http.post(environment.authUrl+"encCcaReq",ccaRequest,{headers:headers})
      .map(res=>{
        res.json();
        this.acc=res;
        this.accessCode=JSON.parse(this.acc).data.accessCode;
        this.encRequest=JSON.parse(this.acc).data.ccaRequest;
      })
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get Certificate
    getCertificate(abcOprID,abcLotID){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.authUrl+"assayingResult/"+abcOprID+'&'+abcLotID,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //get Test Result PDF
    getTradeResult(abcLotID,abcOprID){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/pdf');
      return this.http.get(environment.authUrl+"assayingResultPdf/"+abcLotID+'&'+abcOprID,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get online receipt PDF
    getOnlinereceipt(receiptJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/pdf');
      return this.http.get(environment.authUrl+"paidReportPdf/"+receiptJson,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }


    //get settlement PDF
    getSettlementresult(settlementJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/pdf');
      return this.http.get(environment.authUrl+"settlementReportPdf/"+settlementJson,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    getReport(date){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.authUrl+"getReports/",date,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    // Entryexit > Transaction >Sellername
    selectSellerName(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"farmerList/",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //get all State Name
    getAllStateName(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"stateList/"+"null",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //getting statename perticular by id
    getStateName(id){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"stateList/"+id,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //getting district perticular by id
    getDistrictName(id,did){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"districtList/"+id+"/"+did,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //getting tehsil perticular by id
    getTehsilName(did,tid){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"tahsilList/"+did+"/"+tid,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get all district
    getAllDistrictName(id){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"districtList/"+id+"/"+null,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get all tehsil
    getAllTehsilName(did){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"tahsilList/"+did+"/"+null,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //get all Village/city
    getAllVillageName(did,tid){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"cityList/"+did+"/"+tid+"/"+null,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get all Commodity List
    getAllCommodityList(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"commodityList/",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get all Bag Type
    getBagType(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"bagsTypes/",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get Apprxqty of bags
    getApprxQty(apprxQtl){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"approxQty/",apprxQtl,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //get vehicle type list
    getVehicleTypeList(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"vehicleTypeList/",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get CA list
    getCAList(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"caList/",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }


    //getSaveResult
    getSaveResult(savedData){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"recordSaveLotEntry/",savedData,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //get Traderlist
    getTraderList(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(environment.admin+"traderList/",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //getCreate Farmer Registrtion
    getCreateFarmer(farmerData){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"farmerRegistration/",farmerData,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //lot code search in lot operation>transaction>weighment
    searchWlotCode(lotDates){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"searchLotWithDate/",lotDates,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }
    //save weighment
    saveWeighment(weighData){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"saveWeighment/",weighData,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //Split merge of Lot Management
    LMSplitMerge(dateData,lotType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"listSplitMerge/"+lotType,dateData,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }


    //Sample Creation
    sampleCreationSearch(DateInsert){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(environment.admin+"listSampleCreation/",DateInsert,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    getData(){

        return[
          {
            role:"admin"
          },
          {
            role:"entry"
          },
          {
            role:"ee"
          },
          {
            role:"stateadmin"
          },
          {
            role:"globaladmin"
          },
          {
            role:"apmcadmin"
          },
          {
            role:"adminnam"
          }
        ]
    }



// By Najma
lotCodeSearchForAssaying(dateData){
  console.log(dateData);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"assayingList/",dateData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Commodity Name
commodityForAssaying(json){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"assayingCommodityDetail/",json,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Assaying Lot Paremeter List
parameterListForAssaying(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"assayingParameterList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Assaying Sample Id
sampleIdForAssaying(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"assayingSampleId/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Save Assaying
saveForAssaying(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveAssaying/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Save Sample Data
saveForSample(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveSampleRecord/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Search Pending Lots
searchPendingLots(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"tradeApprovalList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

saveApprovalTrades(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveTradeApproval/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Gate Info
getGateInfo(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"vechileGateType/",{headers:headers})

  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Yard Info
getYardInfo(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"vechileYardType/",{headers:headers})

  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Registered Vehicles
getRegVehicles(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"registredVehicleList/",{headers:headers})

  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}


//Search Bids
searchBidCreationList(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidCreationList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Create Bids
createBid(postJson){
  console.log(postJson);
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveBidCreation/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Bid Declaration --Get Open Bid Listing
getOpenBidListing(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidOpeningList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Declaration -- Open Bid Action
getOpenBidAction(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidOpenAction/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Declaration -- Save Bid Action
saveBidAction(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidOpenSave/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Declaration -- Save Bid Action For Multiple
saveBidActionMulti(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidOpenSaveMultiple/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Declaration -- Extend Bid
extendBid(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidExtend/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Declaration -- Close Bid
exitBid(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidClosed/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Rejection -- Search Bid
searchDeclaredBid(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidRejectionList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Bid Rejection -- Save Rejected Bid
saveRejectedBid(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"bidRejectionSave/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//Save Vehicle Entry
saveVehicleEntry(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveVechileGateEntry/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
//Save Vehicle Registration
saveVehicleRegistration(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveVechileRegistration/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//get all eTenders
getETenders(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"saleAgreementEtenderList/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//on Select Tender OK
onSelTenderOK(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saleAgreementEtenderPopupOkBtn/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//get all fee types
getFeeTypes(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"saleAgreementFeeCatList/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

//on Select Fee Type OK
onSelFeeTypeOK(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saleAgreementFeeCatOkBtn/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Save Sale Agreement
saveSaleAgreement(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saleAggrementSave/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Get Party Names Sale Agreement
getPartyNames(partyId){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"searchPartyName/"+partyId,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Fetch Lot Code Data
fetchLotCodeData(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"onetoOneLotCodeFetch/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// On Lot Code Data Ok
saveLotCodeDataOK(savedData){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"onetoOneLotCodeReturn/",savedData,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}


// Get Invoice Lists For Sale Bill
getInvoiceList(date){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"invoiceList/",date,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Show Invoice Details For Sale Bill
showInvoiceDetails(post){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"invoiceListDtl/",post,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Print Invoice Lists For Sale Bill
getPrintInvoiceList(date){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"printInvoviceList/",date,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Print Invoice For Tab2
printInvoiceTab2Pdf(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"printInvovice/pdf",postJson,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/pdf' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Print Invoice For Tab1
printInvoiceTab1Pdf(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"genrateInvovice/",postJson,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/pdf' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Sale Bill Lists For Sale Bill cancellation
getSaleBillList(date){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saleBillCancellation/",date,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Save Sale Bill cancellation
saveSaleBillCancellation(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"updateSaleBillCancellation/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get Sale Agreement Lists For Sale Agreement cancellation
getSaleAgrList(date){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"agreementCancellation/",date,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Save Sale Bill cancellation
saveSaleAgrCancellation(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"updateAgreementCancellation/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Show Previusly Created Sample Data
getPreviousSampleDetails(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"prevSampleCreationdtl/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Update Previusly Created Sample Data
updatePreviousSampleDetails(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"updateSampleRecord/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Print Bid Decalration Report in pdf
printBidDeclarationReportPDF(bidRep,dateObj){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"printReportBidDeclration/"+bidRep+"/"+"pdf"+"/",dateObj,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/pdf' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Print Bid Decalration Report in rtf
printBidDeclarationReportRTF(bidRep,dateObj){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"printReportBidDeclration/"+bidRep+"/"+"rtf"+"/",dateObj,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/rtf' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Print Bid Decalration Report in xls
printBidDeclarationReportXLS(bidRep,dateObj){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"printReportBidDeclration/"+bidRep+"/"+"xls"+"/",dateObj,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/xls' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Get lot code details for Vehice wise
getLotVehicleWise(dateObj){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"searchLotWithDateForVechWise/",dateObj,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Save Wighment Vehicle wise
saveWeighmentVehicleWise(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveWeighmentVechWise/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Populate seller list for farmer
getSellerForFarmer(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"gateExitPopulateFarmer/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch lot Details for farmer
fetchLotDetailsForFarmer(post){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"gateExitLotDetail/",post,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Populate seller list for trader
getSellerForTrader(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"gateExitPopulateTrader/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Populate seller list for lot wise farmer
getLotForFarmer(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"gateExitLotCodeFarmerWise/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Populate UOM Conversion list for lot wise farmer
getUOMList(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"uomConvList/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}


// Save gate exit Details
saveGateExit(post){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveGateExit/",post,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}


// Populate trader list for post trade exit
getTraderForPostTrade(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.admin+"traderListPostTradeExit/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Fetch lot Details for trader post exit
fetchLotDetailsForTraderPost(post){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"gatePostTradeExitLotDetail/",post,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch bids in create bid
viewBidList(dateObj){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"viewBidList/",dateObj,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch bids in create bid
updateAgent(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"updateAgent/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// save split for lot
saveSplitData(post,splitNo){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"recordSaveLotEntrySplitMerge/"+splitNo,post,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/pdf' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// save merge for lot
saveMergeData(post){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saveMergeLotEntry/",post,{headers:headers,responseType: ResponseContentType.Blob})
  .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/pdf' })
        })
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}

// Fetch Arrival Summary
fetchArrivalSummary(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"arrivalSummary/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch Apmc Wise Working status
fetchApmcWiseWorkingStatus(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"apmcWiseWorkingAsNow/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch Lot Tracker
fetchLotTracker(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"lotTrackerList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch Sale Summary
fetchSaleSummary(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"saleSummary/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch Apmc list
fetchApmcList(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"apmcList/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch Commodity List for state admin
fetchCommodityListForStateAdmin(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"commodityBidname/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}
// Fetch all commodity bid listing
searchAllCommodityBidListing(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.admin+"otherCommodityBidListing/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }

    });
}


// Master Portal Starts here

// save organistion Master
saveOrganisationMaster(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.master+"saveOrganizationMaster/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }
    });
}
// view organistion Master
viewOrganisationMaster(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.master+"viewOrganizationMaster/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }
    });
}

// Get Main Operating Unit List
getMainOperatingUnit(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.master+"mainOprUnitDropDown/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }
    });
}


// save opr unit Master
saveOprUnitMaster(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(environment.master+"saveOperatingUnit/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }
    });
}

// view organistion Master
viewOprUnitMaster(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(environment.master+"mainOprUnitView/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
        if (e.status === 401) {
            return Observable.throw('Unauthorized');
        }
    });
}


}
