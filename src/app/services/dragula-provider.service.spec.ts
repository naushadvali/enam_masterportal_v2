import { TestBed, inject } from '@angular/core/testing';

import { DragulaProviderService } from './dragula-provider.service';

describe('DragulaProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DragulaProviderService]
    });
  });

  it('should be created', inject([DragulaProviderService], (service: DragulaProviderService) => {
    expect(service).toBeTruthy();
  }));
});
