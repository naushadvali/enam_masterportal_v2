import { Injectable } from '@angular/core';

@Injectable()
export class RoleAccessService {
  roleList =[];
  // admin present
  adminPresent:boolean;adminMasterPresent:boolean;adminReportPresent:boolean;adminTranPresent:boolean;
  // admin master
  showUserMasterAPMC:boolean;showSystemParameter:boolean;showOrgMaster:boolean;showOprUnits:boolean;showUserGlobal:boolean;showConfDocScheme:boolean;showCustMenu:boolean;showGenMaster:boolean;showMasterSetup:boolean;showSysParamLegal:boolean;showSysParamOpr:boolean;
  showDefaultSites:boolean;showMetaData:boolean;showUserPwdChange:boolean;showUserParameterMaster:boolean;showRightMaster:boolean;showRoleMaster:boolean;showOprUnitSetUp:boolean;showStateMaster:boolean;showAdminSetup:boolean;showUserMasterState:boolean;showLoginCounter:boolean;showPortalNews:boolean;showUserActReport:boolean;showFlexMaster:boolean;
  // admin report
  showMandiDetailsReport:boolean;showUserLogHistoryReport:boolean;showMasterSetupReport:boolean;
  constructor() {
    // Get roles for the user
    this.roleList=JSON.parse(localStorage.getItem('roleList'));
    //check admin present
    this.adminPresent = false;
    this.adminMasterPresent = false;
    this.adminReportPresent = false;
    this.adminTranPresent = false;
    //admin master
    this.showUserMasterAPMC = false;
    this.showSystemParameter = false;
    this.showOrgMaster = false;
    this.showOprUnits = false;
    this.showUserGlobal = false;
    this.showConfDocScheme = false;
    this.showCustMenu = false;
    this.showGenMaster = false;
    this.showMasterSetup = false;
    this.showSysParamLegal = false;
    this.showSysParamOpr = false;
    this.showDefaultSites = false;
    this.showMetaData = false;
    this.showUserPwdChange = false;
    this.showUserParameterMaster = false;
    this.showRightMaster = false;
    this.showRoleMaster = false;
    this.showOprUnitSetUp = false;
    this.showStateMaster = false;
    this.showAdminSetup = false;
    this.showUserMasterState = false;
    this.showLoginCounter = false;
    this.showPortalNews = false;
    this.showUserActReport = false;
    // admin report
    this.showMandiDetailsReport = false;
    this.showUserLogHistoryReport = false;
    this.showMasterSetupReport = false;
    //admin transaction
    this.showFlexMaster = false;
    this.getAdminRoleAccess();

    // entry exit
    this.entryExitPresent = false;
    this.entryExitMasterPresent = false;
    this.entryExitTranPresent = false;
    this.entryExitReportPresent = false;
    // entry exit master
    this.showProductTypeMaster = false;
    this.showUnitMeasurement = false;
    this.showVehicleRate = false;
    this.showGateRate = false;
    this.showBagType = false;
    this.showMarketType = false;
    this.showProductMaster = false;
    this.showAssayingParameter = false;
    this.showCommodityMaster = false;
    this.showCommodityGroup = false;
    this.showAgentMaster = false;
    this.showFeeComponent = false;
    this.showFeeCategory = false;
    this.showAgentMasterGovt = false;
    this.showUnitConversion = false;
    this.showFeeComponentAPMC = false;
    this.showSubYard = false;
    this.showStackHolder = false;
    this.showWareHouse = false;
    this.showFarmerReg = false;
    // entry exit transaction
    this.showExitVehReg = false;
    this.showExitVehGateEntry = false;
    this.showExitGateEntry = false;
    this.showExitLotEntry = false;
    this.showExitgate = false;
    this.showAgrCancellation = false;
    this.showGateEntryNew = false;
    this.showAgentTraderNoti = false;
    this.showAgentTraderExcel = false;
    this.showGateEntryNewGJ = false;
    this.showGateEntryResp = false;
    this.showGateEntryCache = false;
    this.showGateEntryFull = false;
    this.showDemoGateExit = false;
    this.getEntryExitRoleAccess();

    //lot operations
    this.lotOpPresent = false;
    this.lotOpTranPresent = false;
    this.lotOpReportPresent = false;
    // lot operations report
    this.showSampleAssayingResult = false;
    this.showLotSampleDetail = false;
    this.showLotTracker = false;
    this.showLotSummaryReport = false;
    this.showAssayingReport = false;
    this.showLotAssayingSummaryReport = false;
    // lot operations transaction
    this.showWeighment = false;
    this.showSampleCreation = false;
    this.showAssaying = false;
    this.showLotMng = false;
    this.showApprovalForTrade = false;
    this.getLotOpRoleAccess();

    // auction
    this.auctionPresent = false;
    this.auctionTranPresent = false;
    this.auctionMasterPresent = false;
    this.auctionReportPresent = false;
    // auction transaction
    this.showOutSideMandy = false;
    this.showSaleAgr = false;
    this.showOutCry = false;
    this.showBidCreation = false;
    this.showBidDeclr = false;
    this.showGenInvoice = false;
    this.showSettlement = false;
    this.showSettlementNew = false;
    this.showSettlementPaymentOn = false;
    this.showLotHold = false;
    this.showBidRej = false;
    // auction master
    this.showSaleAgrForMaster = false;
    // auction report
    this.showBidOpeningReport = false;
    this.showBidSubSummary = false;
    this.showSaleInvoiceReport = false;
    this.showAgrNotExit = false;
    this.showIngateNotAgr = false;
    this.showSettleReport = false;
    this.showBidOpeningWinner = false;
    this.showSaleSummaryReport = false;
    this.showFeeComponentReport = false;
    this.showTradersLotDetail = false;
    this.showCommAgent = false;
    this.showTrCommBiddingReport = false;
    this.showTrDeclrReport = false;
    this.showShiftBidOpeningReport = false;
    this.showDateFinalReport = false;
    this.showNamReviewReport = false;
    this.showLotTrackerReport = false;
    this.showNamTradingReport = false;
    this.showDAARPReport = false;
    this.showSaleAgrTypeReport = false;
    this.showStateWiseCommSaleReport = false;
    this.showLotProgressDetailsReport = false;
    this.showDailyPerfReport = false;
    this.showPaymentReport = false;
    this.showCommWiseSoldReport = false;
    this.showManiWiseCommReport = false;
    this.showTradedCommWiseMandiReport = false;
    this.showManiWiseAgrReport = false;
    this.showConPaymentReport = false;
    this.showWeeklySCReport = false;
    this.showCommTradeInfoReport = false;
    this.showEPaymentReport = false;
    this.showAssayedTradedLotReport = false;
    this.showAuditReport = false;
    this.showSallAgrBillRegReport = false;
    this.showCashlessTranReport = false;
    this.showCommRangeReport = false;
    this.showPaymentAgingReport = false;
    this.showOnTradingReport = false;
    this.showCommWiseAssayingReport = false;
    this.showSaleAgrBillTempReport = false;
    this.showAvgBidReceivedReport = false;
    this.showCommWiseNationalTradeReport = false;
    this.showInterMandiTradeReport = false;
    this.showMandiWiseTradeSumReport = false;
    this.showMandiWiseENamReport = false;
    this.showPaymentSetTrackingReport = false;
    this.showMarketFeeBreakupgReport = false;
    this.showPerformaReport = false;
    this.showDigTradeReport = false;
    this.showCommWiseMonthlyReport = false;
    this.showMandiWiseENamProcessReport = false;
    this.getAuctionRoleAccess();
  }

    getAdminRoleAccess(){
      this.roleList=JSON.parse(localStorage.getItem('roleList'));
      if(this.roleList != null){
        // check admin present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '1'){
            this.adminPresent = true;
            break;
          }
        }
        // check admin master present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M'){
            this.adminMasterPresent = true;
            break;
          }
        }
        // check admin transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'T'){
            this.adminTranPresent = true;
            break;
          }
        }
        // check admin transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'R'){
            this.adminReportPresent = true;
            break;
          }
        }

        for(var i=0;i<this.roleList.length;i++){
          if(this.adminPresent == true){
            // Admin Master
            if(this.adminMasterPresent == true){
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1032'){
                this.showUserMasterAPMC = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1009'){
                this.showSystemParameter = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1001'){
                this.showOrgMaster = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1002'){
                this.showOprUnits = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1003'){
                this.showUserGlobal = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1004'){
                this.showConfDocScheme = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1005'){
                this.showCustMenu = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1006'){
                this.showGenMaster = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1007'){
                this.showMasterSetup = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1008'){
                this.showSysParamLegal = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1009'){
                this.showSysParamOpr = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='10100'){
                this.showDefaultSites = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1011'){
                this.showMetaData = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='10130'){
                this.showUserPwdChange = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='10140'){
                this.showUserParameterMaster= true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1018'){
                this.showRoleMaster = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='10190'){
                this.showOprUnitSetUp = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='10210'){
                this.showStateMaster = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='10250'){
                this.showAdminSetup = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1031'){
                this.showUserMasterState = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1031'){
                this.showUserMasterState = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='2097'){
                this.showLoginCounter = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='3052'){
                this.showPortalNews = true;
              }
            }



            // Admin Reports
            if(this.adminReportPresent == true){
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3037'){
                this.showMandiDetailsReport = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2093'){
                this.showUserLogHistoryReport = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2091'){
                this.showMasterSetupReport = true;
              }
              if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2065'){
                this.showUserActReport = true;
              }
            }

            // Admin transaction
            if(this.adminTranPresent == true){
            if(this.roleList[i].moduleId == '1' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='10120'){
              this.showFlexMaster = true;
              }
            }
          }

        }
      }

    }

    entryExitPresent:boolean;entryExitMasterPresent:boolean;entryExitTranPresent:boolean;entryExitReportPresent:boolean;
    // master
    showProductTypeMaster:boolean; showUnitMeasurement:boolean; showVehicleRate:boolean; showGateRate:boolean; showBagType:boolean; showMarketType:boolean; showProductMaster:boolean; showAssayingParameter:boolean; showCommodityMaster:boolean; showCommodityGroup:boolean;  showFarmerReg:boolean; showAgentMaster:boolean; showFeeComponent:boolean; showFeeCategory:boolean; showAgentMasterGovt:boolean; showUnitConversion:boolean;  showFeeComponentAPMC:boolean; showSubYard:boolean;  showStackHolder:boolean; showWareHouse:boolean;
    //Reports
    showInGateReceipt:boolean; showInGateRegister:boolean; showVehRegReceipt:boolean; showInGateCollection:boolean; showInGateEntryReceipt:boolean; showWeighBridgeReport:boolean;
    showWeighBridgeReceiptReport:boolean;  showExitGateRegReport:boolean; showExitGateReceiptReport:boolean; showStockSummaryReport:boolean;  showTraderStockSummaryReport:boolean;  showCommAgStockSummaryReport:boolean; showGateReceiptSample:boolean; showVehRateReport:boolean; showBagReport:boolean;  showCommListingReport:boolean;  showFarmerRegReport:boolean; showAssayingParReport:boolean; showAgTrRegReport:boolean; showTraderPurchaseReport:boolean;  showArrivalSumReport:boolean; showArrivalDWReport:boolean;  showCommAgReport:boolean; showFarmerDetailReport:boolean; showComAgPayableReport:boolean;  showComModelPriceReport:boolean; showNamReportTS:boolean; showExcelReport:boolean; showUPDailyReport:boolean;  showCommGroupVarietyReport:boolean; showExitGateRegDemoReport:boolean;
    //transaction
    showExitVehReg:boolean; showExitVehGateEntry:boolean; showExitGateEntry:boolean; showExitLotEntry:boolean; showExitgate:boolean; showAgrCancellation:boolean; showGateEntryNew:boolean;
    showAgentTraderNoti:boolean;  showAgentTraderExcel:boolean; showGateEntryNewGJ :boolean;showGateEntryResp:boolean; showGateEntryCache:boolean; showGateEntryFull:boolean;  showDemoGateExit:boolean;
    getEntryExitRoleAccess(){
      this.roleList=JSON.parse(localStorage.getItem('roleList'));
      if(this.roleList != null){
        // check entry exit present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '2'){
            this.entryExitPresent = true;
            break;
          }
        }
        // check entry exit master present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M'){
            this.entryExitMasterPresent = true;
            break;
          }
        }
        // check entry exit transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T'){
            this.entryExitTranPresent = true;
            break;
          }
        }
        // check entry exit transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R'){
            this.entryExitReportPresent = true;
            break;
          }
        }

        for(var i=0;i<this.roleList.length;i++){
          if(this.entryExitPresent == true){
            // check entry master present
            if(this.entryExitMasterPresent == true){
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1020'){
                this.showProductTypeMaster = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1022'){
                this.showUnitMeasurement = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1024'){
                this.showVehicleRate = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1028'){
                this.showGateRate = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1069'){
                this.showBagType = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1070'){
                this.showMarketType = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1080'){
                this.showProductMaster = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1084'){
                this.showAssayingParameter = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1085'){
                this.showCommodityMaster = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1086'){
                this.showCommodityGroup = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1088'){
                this.showFarmerReg = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1089'){
                this.showAgentMaster = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1090'){
                this.showFeeComponent = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1092'){
                this.showFeeCategory = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='109300001'){
                this.showAgentMasterGovt = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1094'){
                this.showUnitConversion = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='2003'){
                this.showFeeComponentAPMC = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='3055'){
                this.showSubYard = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='3090'){
                this.showStackHolder = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='1025'){
                this.showWareHouse = true;
              }
            }

            //check gate entry report
            if(this.entryExitReportPresent == true){
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1037'){
                this.showInGateReceipt = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1038'){
                this.showInGateRegister = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1039'){
                this.showVehRegReceipt = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1040'){
                this.showInGateCollection = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1044'){
                this.showInGateEntryReceipt = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1047'){
                this.showWeighBridgeReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1054'){
                this.showWeighBridgeReceiptReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1056'){
                this.showExitGateRegReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1057'){
                this.showExitGateReceiptReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='106300000'){
                this.showStockSummaryReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='109100000'){
                this.showTraderStockSummaryReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='109300000'){
                this.showCommAgStockSummaryReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1099'){
                this.showGateReceiptSample = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2007'){
                this.showVehRateReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2009'){
                this.showBagReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2010'){
                this.showCommListingReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2011'){
                this.showFarmerRegReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2012'){
                this.showAssayingParReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2013'){
                this.showAgTrRegReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2017'){
                this.showTraderPurchaseReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2018'){
                this.showArrivalSumReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2050'){
                this.showArrivalDWReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2051'){
                this.showCommAgReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2052'){
                this.showFarmerDetailReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2062'){
                this.showComAgPayableReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2063'){
                this.showComModelPriceReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2092'){
                this.showNamReportTS = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3009'){
                this.showExcelReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3014'){
                this.showUPDailyReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3044'){
                this.showCommGroupVarietyReport = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3061'){
                this.showExitGateRegDemoReport = true;
              }
            }
            // entry transaction
            if(this.entryExitTranPresent == true){
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1023'){
                this.showExitVehReg = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1026'){
                this.showExitVehGateEntry = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1027'){
                this.showExitGateEntry = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1029'){
                this.showExitLotEntry = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1042'){
                this.showExitgate = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1046'){
                this.showAgrCancellation = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='2023'){
                this.showGateEntryNew = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='2069'){
                this.showAgentTraderNoti = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='2079'){
                this.showAgentTraderExcel = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='3000'){
                this.showGateEntryNewGJ = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='3002'){
                this.showGateEntryResp = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='30130000'){
                this.showGateEntryCache = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='3063'){
                this.showGateEntryFull = true;
              }
              if(this.roleList[i].moduleId == '2' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1043'){
                this.showDemoGateExit = true;
              }
            }
          }
        }
      }
    }

    lotOpPresent:boolean;lotOpTranPresent:boolean;lotOpReportPresent:boolean;
    //lot op Reports
    showSampleAssayingResult:boolean; showLotSampleDetail:boolean; showLotTracker:boolean; showLotSummaryReport:boolean; showAssayingReport:boolean; showLotAssayingSummaryReport:boolean;
    // lot op transaction
    showWeighment:boolean; showSampleCreation:boolean; showAssaying:boolean; showLotMng:boolean; showApprovalForTrade:boolean;
    getLotOpRoleAccess(){
      this.roleList=JSON.parse(localStorage.getItem('roleList'));
      if(this.roleList != null){
        // check lot op present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '3'){
            this.lotOpPresent = true;
            break;
          }
        }
        // check  lot op transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'T'){
            this.lotOpTranPresent = true;
            break;
          }
        }
        // check  lot op transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R'){
            this.lotOpReportPresent = true;
            break;
          }
        }
        for(var i=0;i<this.roleList.length;i++){
          // lot op report
          if(this.lotOpReportPresent == true){
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1049'){
              this.showSampleAssayingResult = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1052'){
              this.showLotSampleDetail = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1067'){
              this.showLotTracker = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2020'){
              this.showLotSummaryReport = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2021'){
              this.showAssayingReport = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3058'){
              this.showLotAssayingSummaryReport = true;
            }
          }
          // lot op transaction
          if(this.lotOpTranPresent == true){
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1030'){
              this.showWeighment = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1033'){
              this.showSampleCreation = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1034'){
              this.showAssaying = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1035'){
              this.showLotMng = true;
            }
            if(this.roleList[i].moduleId == '3' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1036'){
              this.showApprovalForTrade = true;
            }
          }
        }
      }
    }

    auctionPresent:boolean;auctionTranPresent:boolean;auctionMasterPresent:boolean;auctionReportPresent:boolean;
    // transaction
    showOutSideMandy:boolean; showSaleAgr:boolean; showOutCry:boolean;  showBidCreation:boolean;  showBidDeclr:boolean; showGenInvoice:boolean;  showSettlement:boolean;  showSettlementNew:boolean;showSettlementPaymentOn:boolean; showBidRej:boolean; showLotHold:boolean;
    //master
    showSaleAgrForMaster:boolean;
    //report
    showSaleAgrReport:boolean; showOutsideAPMCSaleReport:boolean; showOutsideAPMCSaleAgrReport :boolean;showSaleAgrCancel:boolean; showBidCreationReport:boolean; showBidSubmissionReport:boolean; showOutsideAPMCSaleRegReport:boolean;
    showBidOpeningReport:boolean; showBidSubSummary:boolean; showSaleInvoiceReport:boolean; showAgrNotExit:boolean; showIngateNotAgr:boolean; showSettleReport:boolean; showBidOpeningWinner:boolean; showSaleSummaryReport :boolean;showFeeComponentReport:boolean; showTradersLotDetail:boolean; showCommAgent:boolean; showTrCommBiddingReport:boolean; showTrDeclrReport:boolean; showShiftBidOpeningReport :boolean;showDateFinalReport:boolean; showNamReviewReport :boolean;
    showLotTrackerReport:boolean; showNamTradingReport :boolean;showDAARPReport :boolean;showSaleAgrTypeReport:boolean; showStateWiseCommSaleReport:boolean; showLotProgressDetailsReport:boolean; showDailyPerfReport:boolean; showPaymentReport:boolean; showCommWiseSoldReport:boolean; showTradedCommWiseMandiReport:boolean; showManiWiseCommReport :boolean;showManiWiseAgrReport:boolean; showConPaymentReport:boolean; showWeeklySCReport:boolean;
    showCommTradeInfoReport :boolean;showEPaymentReport:boolean; showAssayedTradedLotReport:boolean; showAuditReport:boolean; showSallAgrBillRegReport:boolean; showCashlessTranReport :boolean;showCommRangeReport :boolean;
    showPaymentAgingReport:boolean; showOnTradingReport:boolean; showCommWiseAssayingReport:boolean; 	showSaleAgrBillTempReport:boolean; showAvgBidReceivedReport :boolean;showCommWiseNationalTradeReport :boolean;
    showInterMandiTradeReport:boolean; showMandiWiseTradeSumReport:boolean; showMandiWiseENamReport:boolean; showPaymentSetTrackingReport:boolean; showMarketFeeBreakupgReport:boolean; showPerformaReport:boolean; showDigTradeReport:boolean; showCommWiseMonthlyReport:boolean; showMandiWiseENamProcessReport:boolean;
    getAuctionRoleAccess(){
      this.roleList=JSON.parse(localStorage.getItem('roleList'));
      if(this.roleList != null){
        // check auction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '4'){
            this.auctionPresent = true;
            break;
          }
        }
        // check auction master present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'M'){
            this.auctionMasterPresent = true;
            break;
          }
        }
        // check auction transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T'){
            this.auctionTranPresent = true;
            break;
          }
        }
        // check auction transaction present
        for(var i=0;i<this.roleList.length;i++){
          if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R'){
            this.auctionReportPresent = true;
            break;
          }
        }
        for(var i=0;i<this.roleList.length;i++){
          if(this.auctionPresent == true){
            if(this.auctionTranPresent == true){
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1041000'){
                this.showOutSideMandy = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1045'){
                this.showSaleAgr = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1058'){
                this.showOutCry = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1061'){
                this.showBidCreation = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1062'){
                this.showBidDeclr = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1083'){
                this.showGenInvoice = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='1087000'){
                this.showSettlement = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='2005000'){
                this.showSettlementNew = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='2019000'){
                this.showSettlementPaymentOn = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='2064'){
                this.showBidRej = true;
              }
              if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'T' && this.roleList[i].adrmRightId =='3048'){
                this.showLotHold = true;
              }
            }
          }
          if(this.auctionMasterPresent == true){
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'M' && this.roleList[i].adrmRightId =='3047'){
              this.showSaleAgrForMaster = true;
            }
          }
          if(this.auctionReportPresent == true){
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1050'){
              this.showSaleAgrReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1051'){
              this.showOutsideAPMCSaleReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1059'){
              this.showOutsideAPMCSaleAgrReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1060'){
              this.showSaleAgrCancel = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1064'){
              this.showBidCreationReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1065'){
              this.showBidSubmissionReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1066'){
              this.showOutsideAPMCSaleRegReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1068'){
              this.showBidOpeningReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1081'){
              this.showBidSubSummary = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1082'){
              this.showSaleInvoiceReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1095'){
              this.showAgrNotExit = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1096'){
              this.showIngateNotAgr = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='1098'){
              this.showSettleReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2006'){
              this.showBidOpeningWinner = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2015'){
              this.showSaleSummaryReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2016'){
              this.showFeeComponentReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2025'){
              this.showTradersLotDetail = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2026'){
              this.showCommAgent = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2027'){
              this.showTrCommBiddingReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2028'){
              this.showTrDeclrReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2029'){
              this.showShiftBidOpeningReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2053'){
              this.showDateFinalReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2066'){
              this.showNamReviewReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='2096'){
              this.showLotTrackerReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3001'){
              this.showNamTradingReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3003'){
              this.showDAARPReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3007'){
              this.showSaleAgrTypeReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3008'){
              this.showStateWiseCommSaleReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3016'){
              this.showLotProgressDetailsReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3017'){
              this.showDailyPerfReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3026'){
              this.showPaymentReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3030'){
              this.showCommWiseSoldReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3031'){
              this.showTradedCommWiseMandiReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3032'){
              this.showManiWiseCommReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3033'){
              this.showManiWiseAgrReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3035'){
              this.showConPaymentReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3036'){
              this.showWeeklySCReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3039'){
              this.showCommTradeInfoReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3040'){
              this.showEPaymentReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3041'){
              this.showAssayedTradedLotReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3042'){
              this.showAuditReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3043'){
              this.showSallAgrBillRegReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3045'){
              this.showCashlessTranReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3046'){
              this.showCommRangeReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3049'){
              this.showPaymentAgingReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3050'){
              this.showOnTradingReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3051'){
              this.showCommWiseAssayingReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3053'){
              this.showSaleAgrBillTempReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3056'){
              this.showAvgBidReceivedReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3057'){
              this.showCommWiseNationalTradeReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3059'){
              this.showInterMandiTradeReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3060'){
              this.showMandiWiseTradeSumReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3080'){
              this.showMandiWiseENamReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3038'){
              this.showPaymentSetTrackingReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3065'){
              this.showMarketFeeBreakupgReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3066'){
              this.showPerformaReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3067'){
              this.showDigTradeReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3068'){
              this.showCommWiseMonthlyReport = true;
            }
            if(this.roleList[i].moduleId == '4' && this.roleList[i].adrmType == 'R' && this.roleList[i].adrmRightId =='3081'){
              this.showMandiWiseENamProcessReport = true;
            }
          }

        }
      }
    }
}
