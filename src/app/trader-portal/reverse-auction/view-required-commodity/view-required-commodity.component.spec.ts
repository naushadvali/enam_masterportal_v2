import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRequiredCommodityComponent } from './view-required-commodity.component';

describe('ViewRequiredCommodityComponent', () => {
  let component: ViewRequiredCommodityComponent;
  let fixture: ComponentFixture<ViewRequiredCommodityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRequiredCommodityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRequiredCommodityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
