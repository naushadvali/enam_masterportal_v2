import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitCommodityRequirmentComponent } from './submit-commodity-requirment.component';

describe('SubmitCommodityRequirmentComponent', () => {
  let component: SubmitCommodityRequirmentComponent;
  let fixture: ComponentFixture<SubmitCommodityRequirmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitCommodityRequirmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitCommodityRequirmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
