import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusOfGrievanceComponent } from './status-of-grievance.component';

describe('StatusOfGrievanceComponent', () => {
  let component: StatusOfGrievanceComponent;
  let fixture: ComponentFixture<StatusOfGrievanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusOfGrievanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusOfGrievanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
