import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrievanceEntryComponent } from './grievance-entry.component';

describe('GrievanceEntryComponent', () => {
  let component: GrievanceEntryComponent;
  let fixture: ComponentFixture<GrievanceEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrievanceEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrievanceEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
