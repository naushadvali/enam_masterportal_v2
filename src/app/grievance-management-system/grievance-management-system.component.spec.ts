import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrievanceManagementSystemComponent } from './grievance-management-system.component';

describe('GrievanceManagementSystemComponent', () => {
  let component: GrievanceManagementSystemComponent;
  let fixture: ComponentFixture<GrievanceManagementSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrievanceManagementSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrievanceManagementSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
