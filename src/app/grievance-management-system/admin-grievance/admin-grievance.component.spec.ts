import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGrievanceComponent } from './admin-grievance.component';

describe('AdminGrievanceComponent', () => {
  let component: AdminGrievanceComponent;
  let fixture: ComponentFixture<AdminGrievanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGrievanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGrievanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
