import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApmcOthersGrievanceComponent } from './apmc-others-grievance.component';

describe('ApmcOthersGrievanceComponent', () => {
  let component: ApmcOthersGrievanceComponent;
  let fixture: ComponentFixture<ApmcOthersGrievanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApmcOthersGrievanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApmcOthersGrievanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
