import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apmc-others-grievance',
  templateUrl: './apmc-others-grievance.component.html',
  styleUrls: ['./apmc-others-grievance.component.css']
})
export class ApmcOthersGrievanceComponent implements OnInit {
  disableDropdown:boolean=true;
  checkBox:boolean=false;
  constructor() { }

  ngOnInit() {
  }
  checkDropDown(chkData){
    console.log("checkbox event data: ",chkData);
    if(chkData==false){

      this.disableDropdown=true;
      //console.log("checkbox: ",this.checkBox+" dropdown: ",this.disableDropdown);
    }

    else if(chkData==true){
      this.disableDropdown=false;
      //console.log("else checkbox: ",this.checkBox+" else dropdown: ",this.disableDropdown);
    }
  }

}
