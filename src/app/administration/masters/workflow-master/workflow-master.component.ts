import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DragulaModule } from 'ng2-dragula';
import { DragulaProviderService } from '../../../services/dragula-provider.service';

@Component({
  selector: 'app-workflow-master',
  templateUrl: './workflow-master.component.html',
  styleUrls: ['./workflow-master.component.css'],
  providers:[ DragulaProviderService ]
})
export class WorkflowMasterComponent implements OnInit {

  public many: Array<string> = ['Gate Entry', 'Weighment', 'Lot Management', 'Sample Creation','Assaying','Approval of Trade','Bid Creation','Trading','Bid Declaration','Sale Agreement','Gate Exit'];
  public many2: Array<string> = [];



  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private dragulaService: DragulaProviderService
  ) {
      // console.log("many",this.many);
      // console.log("many2",this.many2);

    dragulaService.dropModel.subscribe((value) => {
     this.onDropModel(value.slice(1));

    });
    dragulaService.removeModel.subscribe((value) => {
     this.onRemoveModel(value.slice(1));

    });

  }

  ngOnInit() {

  }
  

  private onDropModel(args:any):void {
    let [el, target, source] = args;
    console.log(args);
    console.log("el",el);
    console.log("target",target);
    console.log("source",source);
  }

  private onRemoveModel(args:any):void {
    let [el, source] = args;
    //console.log('onRemoveModel:');
    console.log("removal el",el);
    console.log("removal source",source);
  }

  workflowData(){
    console.log("many",this.many);
    console.log("many2",this.many2);
  }

}
