import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-operating-unit',
  templateUrl: './operating-unit.component.html',
  styleUrls: ['./operating-unit.component.css']
})
export class OperatingUnitComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';
  public my_class9='overlay';
  public my_class10='overlay';
  public my_class11='overlay';

  public shown1:any=false;
  public shown2:any=true;
  public shown3:any=false;
  public shown4:any=true;
  public shown5:any=false;
  public shown6:any=true;
  public loading = false;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router
  ) { }

  ngOnInit() {
    this.oprUnitType = "A";
  }

  onAdvance(){
    this.shown1=true;
    this.shown2=false;
  }
  onBasic(){
    this.shown1=false;
    this.shown2=true;
  }
  stateAdv(){
    this.shown3=true;
    this.shown4=false;
  }
  stateBas(){
    this.shown3=false;
    this.shown4=true;
  }
  districtAdv(){
    this.shown5=true;
    this.shown6=false;
  }
  districtBas(){
    this.shown5=false;
    this.shown6=true;
  }
  errorPopupShow(){
    this.my_class3 = 'overlay1';
  }

  // Open Main Operating Unit Popup here
  mainOprUnitList =[];filtMainOprUnitList =[];
  div_show1(){
      this.loading = true;
      this.authservice.getMainOperatingUnit().subscribe(fetchOprUnitList=>{
        console.log("fetchOprUnitList: ",fetchOprUnitList.listData);
        this.mainOprUnitList = fetchOprUnitList.listData;
        this.filtMainOprUnitList = fetchOprUnitList.listData;
        this.loading = false;
        this.my_class1='overlay1';
      }, (err) => {
        if (err === 'Unauthorized')
        {
          this.errorPopupShow();
          setTimeout(()=>{
          this.my_class4 = 'overlay';
          localStorage.clear();
          this.router.navigateByUrl('/login');
        },3000);
       }
        else{
          this.my_class5 = 'overlay1';
          setTimeout(()=>{
            this.my_class5 = 'overlay';
          },2000);
        }
      }
    );
  }

  div_hide1(){
        this.my_class1='overlay';
        this.selectedOprUnit = -1;
        this.selOprUnitVal =[];
      }
  // Open State Popup
  stateList = [];filtStateList =[];
  div_show2(){
      this.loading = true;
      this.authservice.getAllStateName().subscribe(fetchStateList=>{
        console.log("fetchStateList: ",fetchStateList.listData);
        this.stateList = fetchStateList.listData;
        this.filtStateList = fetchStateList.listData;
        this.loading = false;
        this.my_class2 = 'overlay1';
      }, (err) => {
        if (err === 'Unauthorized')
        {
          this.errorPopupShow();
          setTimeout(()=>{
          this.my_class4 = 'overlay';
          localStorage.clear();
          this.router.navigateByUrl('/login');
        },3000);
       }
        else{
          this.my_class5 = 'overlay1';
          setTimeout(()=>{
            this.my_class5 = 'overlay';
          },2000);
        }
      }
    );
    }
  div_hide2(){
        this.my_class2='overlay';
        this.selectedState = -1;
        this.selStateVal =[];
      }
  // Ftech District List Here
  districtList =[];filtDistList=[];
  div_show3(){
      if(this.selStateId != undefined && this.selStateId !=""){
      this.loading = true;
      this.authservice.getAllDistrictName(this.selStateId).subscribe(fetchDistList=>{
        console.log("fetchStateList: ",fetchDistList.listData);
        this.districtList = fetchDistList.listData;
        this.filtDistList = fetchDistList.listData;
        this.loading = false;
        this.my_class3 = 'overlay1';
      }, (err) => {
        if (err === 'Unauthorized')
        {
          this.errorPopupShow();
          setTimeout(()=>{
          this.my_class4 = 'overlay';
          localStorage.clear();
          this.router.navigateByUrl('/login');
        },3000);
       }
        else{
          this.my_class5 = 'overlay1';
          setTimeout(()=>{
            this.my_class5 = 'overlay';
          },2000);
        }
      }
    );
  }
  else{
    this.my_class6 = 'overlay1';
    setTimeout(()=>{
      this.my_class6 = 'overlay';
    },2000);
  }
}
  div_hide3(){
        this.my_class3='overlay';
        this.selectedDist = -1;
        this.selDistVal =[];
      }

// On Select Opr Unit
selOprUnitVal=[];selectedOprUnit:number;
selOprUnit(onOprUnitSelectVal,index){
  this.selOprUnitVal = onOprUnitSelectVal;
  this.selectedOprUnit = index;
}

// On Select Opr Unit OK
selOprUnitValOK =[];
oprUnitCode:string;nameOfPremises:string;villageName:string;phNo:string;mainOprUnit:string;bankAccount:string;secDepo:string;
stateName:string;oprUnitName:string;areaName:string;pinCode:string;emailId:string;bankName:string;ifscCode:string;oprUnitType:string;
districtName:string;
panNo:string;vat:string;tdsCircle:string;profTax:string;serviceTax:string;tan:string;tcanNo:string;tdcanNo:string;placeName:string;
ssiRegNo:string;
personName:string;pFlatNo:string;pAreaName:string;pPinCode:string;adminName:string;otherName:string;designation:string;pNameOfPremi:string;pVillage:string;pEmailId:string;pAdminContact:string;pOtherContact:string;pPhoneNum:string;pRoad:string;pState:string;pAdminEmail:string;pOtherEmail:string;
onSelOprUnitOK(){
  this.selOprUnitValOK = this.selOprUnitVal;
  this.mainOprUnit = this.selOprUnitValOK["adoumOprname"];
  console.log("selOprUnitValOK",this.selOprUnitValOK);
  this.selectedOprUnit = -1;
  this.selOprUnitVal =[];
  this.my_class1='overlay';
}

// On Select State
selStateVal=[];selectedState:number;
selState(onStateSelectVal,index){
  this.selStateVal = onStateSelectVal;
  this.selectedState = index;
}

// On Select State OK
selStateValOK =[];selStateId:string;
onSelStateOK(){
  this.selStateValOK = this.selStateVal;
  this.stateName = this.selStateValOK["gmsmStateDesc"];
  this.selStateId = this.selStateValOK["gmsmStateId"];
  console.log("selStateValOK",this.selStateValOK);
  this.selectedState = -1;
  this.selStateVal =[];
  this.my_class2='overlay';
}

// On Select District
selDistVal=[];selectedDist:number;
selDist(onDistSelectVal,index){
  this.selDistVal = onDistSelectVal;
  this.selectedDist = index;
}

// On Select Dist OK
selDistValOK =[];
onSelDistOK(){
  this.selDistValOK = this.selDistVal;
  this.districtName = this.selDistValOK["gmdmDistDesc"];
  console.log("selDistValOK",this.selDistValOK);
  this.selectedDist = -1;
  this.selDistVal =[];
  this.my_class3='overlay';
}

// On Change Opr Unit type
onOpUnitType(unitType){
  this.oprUnitType = unitType;
}

// Save Opr Unit Master
postSaveRes:any;saveMsg:string;
onSaveOprUnitMaster(){
  if(this.oprUnitCode !=undefined && this.oprUnitCode != ""){
    if(this.oprUnitName !=undefined && this.oprUnitName != ""){
      if(this.selOprUnitValOK.length != 0){
        console.log("selOprUnitValOK",this.selOprUnitValOK,this.selOprUnitValOK.length)
        if(this.selStateValOK.length == 0){
          this.selStateValOK = undefined
        }
        if(this.selDistValOK.length == 0){
          this.selDistValOK = undefined
        }
        var saveObj = {"mainOprUnitViewVO": {"adoumOprname": this.oprUnitName,"adoumAdd1": this.nameOfPremises,"adoumAdd2": this.areaName,"adoumCity": this.villageName,
    		"adoumPin": this.pinCode,"adoumPhno": this.phNo,"adoumEmail": this.emailId,"adoumPanno": this.panNo,"adoumTanno": this.tan,
    		"adoumCstbst": "hsuus87","adoumVat": this.vat,"adoumCfield1": this.bankName,"adoumCfield2": this.bankAccount,"adoumCfield3": this.ifscCode,"adoumOprcd": this.oprUnitCode,"adoumSsi": this.ssiRegNo,"adoumPtaxno": this.profTax,"adoumProad": this.pRoad,"adoumPpre": this.pNameOfPremi,"adoumPername": this.personName,"adoumPdesg": this.designation,"adoumPpin": this.pPinCode,"adoumTcan": this.tcanNo,"adoumTdscircle": this.tdsCircle,"adoumParea": this.pAreaName,"adoumTdcan": this.tdcanNo,"adoumPlace": this.placeName,"adoumPstate": this.pState,"adoumPcity": this.pVillage,"adoumPphno": this.pPhoneNum,"adoumPemail": this.pEmailId,"adoumPflatno": this.pFlatNo,"adoumCode": this.oprUnitCode,"adoumStaxno": this.serviceTax,"adoumOuType": this.oprUnitType,"adoumOtherMail": this.pOtherEmail,"adoumOtherName": this.otherName,"adoumOtherPhno": this.pOtherContact,"adoumAdminMail": this.pAdminEmail,"adoumAdminName": this.adminName,"adoumAdminPhno": this.pAdminContact,
    		"adoumSdPer": this.serviceTax,"adoumPaymentEnabled": this.adoumPaymentEnabled
    	 },
    	"mainOprUnitVO": this.selOprUnitValOK,
    	"stateTypeVo": this.selStateValOK,
    	"districtListVo": this.selDistValOK
      }
      console.log("saveObj",saveObj)
      this.authservice.saveOprUnitMaster(saveObj).subscribe(postSave=>{
        console.log("After save ",postSave);
        this.postSaveRes = postSave;
        this.loading = false;
        this.saveMsg = postSave.message;
        this.my_class10 = 'overlay1';
        if(postSave.status == 1){
          this.onResetOprUnitMaster();
        }
      }, (err) => {
        if (err === 'Unauthorized')
        {
          this.errorPopupShow();
          setTimeout(()=>{
          this.my_class4 = 'overlay';
          localStorage.clear();
          this.router.navigateByUrl('/login');
        },3000);
       }
        else{
          this.my_class5 = 'overlay1';
          setTimeout(()=>{
            this.my_class5 = 'overlay';
          },2000);
        }
          }
    );



      }
      else{
        this.my_class9 = 'overlay1';
        setTimeout(()=>{
          this.my_class9 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class8 = 'overlay1';
      setTimeout(()=>{
        this.my_class8 = 'overlay';
      },2000);
    }
  }
  else{
    this.my_class7 = 'overlay1';
    setTimeout(()=>{
      this.my_class7 = 'overlay';
    },2000);
  }
}

// On Allow Payment
adoumPaymentEnabled:string;
onAllowPayment(checked){
  this.adoumPaymentEnabled = checked;
}


// Reset opr unit Master
onResetOprUnitMaster(){
  this.oprUnitCode="";
  this.nameOfPremises="";
  this.villageName="";
  this.phNo="";
  this.mainOprUnit="";
  this.bankAccount="";
  this.secDepo="";
  this.stateName="";
  this.oprUnitName="";
  this.areaName="";
  this.pinCode="";
  this.emailId="";
  this.bankName="";
  this.ifscCode="";
  this.oprUnitType="";
  this.districtName="";
  this.panNo="";
  this.vat="";
  this.tdsCircle="";
  this.profTax="";
  this.serviceTax="";
  this.tan="";
  this.tcanNo="";
  this.tdcanNo="";
  this.placeName="";
  this.ssiRegNo="";
  this.personName="";
  this.pFlatNo="";
  this.pAreaName="";
  this.pPinCode="";
  this.adminName="";
  this.otherName="";
  this.designation="";
  this.pNameOfPremi="";
  this.pVillage="";
  this.pEmailId="";
  this.pAdminContact="";
  this.pOtherContact="";
  this.pPhoneNum="";
  this.pRoad="";
  this.pState="";
  this.pAdminEmail="";
  this.pOtherEmail="";
}

// Hide Popup After Save Org Master
showOnSaveSuccess:boolean;
hideMessagePopUp(){
  this.my_class10 = 'overlay';
  this.showOnSaveSuccess = true;
}

// View Opr Unit Master
viewOprUnitList = [];filteredOprUnitList =[];
onViewOprUnitMaster(){
  console.log("here")
  this.loading = true;
  this.authservice.viewOprUnitMaster().subscribe(viewOprUnitList=>{
    this.viewOprUnitList = viewOprUnitList.listData;
    this.filteredOprUnitList = viewOprUnitList.listData;
    this.loading = false;
    this.my_class11 = 'overlay1';
  }, (err) => {
    if (err === 'Unauthorized')
    {
      this.errorPopupShow();
      setTimeout(()=>{
      this.my_class4 = 'overlay';
      localStorage.clear();
      this.router.navigateByUrl('/login');
    },3000);
   }
    else{
      this.my_class5 = 'overlay1';
      setTimeout(()=>{
        this.my_class5 = 'overlay';
      },2000);
    }
  }
);
}

// On Select Opr Unit
onSelOprUnit(post){
  console.log("post",post)
  this.matchCity = "";
  this.matchOprUNitName = "";
  this.matchOprUnitCode = "";
  this.my_class11 = "overlay";
  this.oprUnitCode = post.adoumCode;
  this.nameOfPremises = post.adoumAdd1;
  this.villageName = post.adoumCity;
  this.phNo = post.adoumPhno;
  this.mainOprUnit = post.adoumOprname1;
  this.bankAccount = post.adoumCfield2;
  this.secDepo = post.adoumSdPer;
  this.stateName = post.gmsmStateDesc;
  this.oprUnitName=post.adoumOprname;
  this.areaName=post.adoumAdd2;
  this.pinCode=post.adoumPin;
  this.emailId=post.adoumEmail;
  this.bankName=post.adoumCfield1;
  this.ifscCode=post.adoumCfield3;
  this.oprUnitType=post.adoumOuType;
  this.districtName=post.gmdmDistDesc;
  this.panNo=post.adoumPanno;
  this.vat=post.adoumVat;
  this.tdsCircle=post.adoumTdscircle;
  this.profTax=post.adoumPtaxno;
  this.serviceTax=post.adoumStaxno;
  this.tan=post.adoumTanno;
  this.tcanNo=post.adoumTcan;
  this.tdcanNo=post.adoumTdcan;
  this.placeName=post.adoumPlace;
  this.ssiRegNo=post.adoumSsi;
  this.personName=post.adoumPername;
  this.pFlatNo=post.adoumPflatno;
  this.pAreaName=post.adoumParea;
  this.pPinCode=post.adoumPpin;
  this.adminName=post.adoumAdminName;
  this.otherName=post.adoumOtherName;
  this.designation=post.adoumPdesg;
  this.pNameOfPremi=post.adoumPpre;
  this.pVillage=post.adoumPcity;
  this.pEmailId=post.adoumPemail;
  this.pAdminContact=post.adoumAdminPhno;
  this.pOtherContact=post.adoumOtherPhno;
  this.pPhoneNum=post.adoumPphno;
  this.pRoad=post.adoumProad;
  this.pState=post.adoumPstate;
  this.pAdminEmail=post.adoumAdminMail;
  this.pOtherEmail=post.adoumOtherMail;
}

// Filter Opr Unit Code
matchOprUnitCode:string;
filterOprUnitCode(){
  if(this.matchOprUnitCode) {
    this.filteredOprUnitList = _.filter(this.viewOprUnitList, (a)=>a.adoumCode.toUpperCase().indexOf(this.matchOprUnitCode.toUpperCase())>=0);
  } else {
    this.filteredOprUnitList = this.viewOprUnitList  ;
  }
}

// Filter Opr Unit Name
matchOprUNitName:string;
filterOprUnitName(){
  if(this.matchOprUNitName) {
    this.filteredOprUnitList = _.filter(this.viewOprUnitList, (a)=>a.adoumOprname.toUpperCase().indexOf(this.matchOprUNitName.toUpperCase())>=0);
  } else {
    this.filteredOprUnitList = this.viewOprUnitList  ;
  }
}

// Filter City
matchCity:string;
filterCity(){
  var filtList=[];
  for(var i=0;i<this.viewOprUnitList.length;i++){
    if(this.viewOprUnitList[i].adoumCity !=undefined){
      filtList.push(this.viewOprUnitList[i]);
    }
  }
  if(this.matchCity != undefined && this.matchCity != ""){
    this.filteredOprUnitList = _.filter(filtList , (a)=>a.adoumCity.toUpperCase().indexOf(this.matchCity.toUpperCase())>=0);
  }
  else{
  this.filteredOprUnitList  = this.viewOprUnitList;
  }
}

// Hide View popup
hideViewPopup(){
  this.my_class11 = 'overlay';
  this.matchCity = "";
  this.matchOprUNitName = "";
  this.matchOprUnitCode = "";
}
}
