import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatingUnitComponent } from './operating-unit.component';

describe('OperatingUnitComponent', () => {
  let component: OperatingUnitComponent;
  let fixture: ComponentFixture<OperatingUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatingUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatingUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
