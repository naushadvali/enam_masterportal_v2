import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-organisation-master',
  templateUrl: './organisation-master.component.html',
  styleUrls: ['./organisation-master.component.css']
})
export class OrganisationMasterComponent implements OnInit {
  public my_class1 = 'overlay';public my_class2 = 'overlay';public my_class3 = 'overlay';public my_class4 = 'overlay';
  public my_class5 = 'overlay';public my_class6 = 'overlay';
  public loading = false;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router
  ) { }

  ngOnInit() {
  }


  // View Organisation Master
  viewOrgMstList = [];filteredOrgMstList =[];
  onViewOrgMaster(){
    console.log("here")
    this.loading = true;
    this.authservice.viewOrganisationMaster().subscribe(viewOrgMstList=>{
      console.log("All trader name: ",viewOrgMstList.listData);
      this.viewOrgMstList = viewOrgMstList.listData;
      this.filteredOrgMstList = viewOrgMstList.listData;
      this.loading = false;
      this.my_class6 = 'overlay1';
    }, (err) => {
      if (err === 'Unauthorized')
      {
        this.errorPopupShow();
        setTimeout(()=>{
        this.my_class3 = 'overlay';
        localStorage.clear();
        this.router.navigateByUrl('/login');
      },3000);
     }
      else{
        this.my_class4 = 'overlay1';
        setTimeout(()=>{
          this.my_class4 = 'overlay';
        },2000);
      }
    }
  );
  }

  // Hide View popup
  hideViewPopup(){
    this.my_class6 = 'overlay';
    this.matchOrgName = "";
    this.matchCityName = "";
    this.matchCode = "";
  }

  errorPopupShow(){
        this.my_class3 = 'overlay1';
      }

  // Save Oraganisation Master
  orgCode:string;nameOfRoad:string;cityName:string;emailId:string;orgName:string;areaName:string;pinCode:string;phNumber:string;
  panNo:string;cstNo:string;tdCanNo:string;tdsCircle:string;placeName:string;tanNo:string;vatNo:string;tcanNo:string;accountGroupNo:string;estCodeNo:string;
  personalName:string;flatNo:string;areaNo:string;perPinCode:string;adminName:string;otherName:string;designation:string;nameOfPremises:string;villName:string;perEmailId:string;adminContact:string;otherContact:string;perPhoneNumber:string;roadName:string;stateName:string;adminEmail:string;otherEmail:string;
  saveMsg:string;postSaveRes:any;
  onSaveOrgMaster(){
    console.log("orgCode",this.orgCode)
    console.log("orgName",this.orgName)
    if(this.orgCode != undefined && this.orgCode != ""){
      if(this.orgName != undefined && this.orgName != ""){
        if(this.pinCode != undefined && this.pinCode != ""){

        }
        else{

        }
        console.log("here")
        var postObj = { "adomOrgname": this.orgName,"adomCity": this.cityName,"adomPin": this.pinCode.toString(),"adomPhno": this.phNumber.toString(),"adomEmail": this.emailId,"adomAdd1": this.nameOfRoad,"adomAdd2": this.areaName,
        "adomPanno": this.panNo,"adomTan": this.tanNo,"adomCstbst": this.cstNo,"adomVat": this.vatNo,"adomOrgcd": this.orgCode,"adomTdcan": this.tdCanNo,"adomTcan": this.tcanNo,"adomTdscircle": this.tdsCircle,
        "adomPlace": this.placeName,
        "adomPername": this.personalName,"adomPdesg": this.designation,"adomPpre": this.nameOfPremises,"adomProad": this.roadName,
        "adomParea": this.areaNo,"adomPcity": this.villName,"adomPstate":this.stateName,"adomPpin": this.perPinCode,"adomPphno": this.perPhoneNumber,"adomPemail": this.perEmailId,"adomPflatno": this.flatNo,"adomEstbCd": this.estCodeNo,"adomAcGrpNo": this.accountGroupNo,"adomAdminPhno": this.adminContact,"adomOtherPhno": this.otherContact,"adomAdminMail": this.adminEmail,"adomAdminName": this.adminName,"adomOherMail":this.otherEmail,"adomOtherName": this.otherName
        }

        console.log("postObj",postObj)
        this.loading = true;
        this.authservice.saveOrganisationMaster(postObj).subscribe(postSave=>{
          console.log("After save ",postSave);
          this.postSaveRes = postSave;
          this.loading = false;
          this.saveMsg = postSave.message;
          this.my_class5 = 'overlay1';
          if(postSave.status == 1){
            this.onResetOrgMaster();
          }
        }, (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                    setTimeout(()=>{
                    this.my_class3 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
                 }
                  else{
                    this.my_class4 = 'overlay1';
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                    },2000);
                  }
            }
      );
      }
      else{
        this.my_class2 = 'overlay1';
        setTimeout(()=>{
          this.my_class2 = 'overlay';
        },2000);
      }
    }
    else{
      this.my_class1 = 'overlay1';
      setTimeout(()=>{
        this.my_class1 = 'overlay';
      },2000);
    }
  }

// Hide Popup After Save Org Master
showOnSaveSuccess:boolean;
hideMessagePopUp(){
  this.my_class5 = 'overlay';
  this.showOnSaveSuccess = true;
}

// On Select Org Id
onSelOrgId(post){
 this.matchOrgName = "";
 this.matchCityName = "";
 this.matchCode = "";
 this.my_class6 = 'overlay';
 this.orgCode = post.adomOrgcd;
 this.orgName = post.adomOrgname;
 this.nameOfRoad = post.adomAdd1;
 this.cityName = post.adomCity;
 this.emailId = post.adomEmail;
 this.areaName = post.adomAdd2;
 this.pinCode = post.adomPin;
 this.phNumber = post.adomPhno;
 this.panNo = post.adomPanno;
 this.cstNo = post.adomCstbst;
 this.tdCanNo = post.adomTdcan;
 this.tdsCircle = post.adomTdscircle;
 this.placeName = post.adomPlace;
 this.tanNo = post.adomTan;
 this.vatNo = post.adomVat;
 this.tcanNo = post.adomTcan;
 this.accountGroupNo = post.adomAcGrpNo;
 this.estCodeNo = post.adomEstbCd;
 this.personalName = post.adomPername;
 this.flatNo = post.adomPflatno;
 this.areaNo = post.adomParea;
 this.perPinCode = post.adomPpin;
 this.adminName = post.adomAdminName;
 this.otherName = post.adomOtherName;
 this.designation = post.adomPdesg;
 this.nameOfPremises = post.adomPpre;
 this.villName = post.adomPcity;
 this.perEmailId = post.adomPemail;
 this.adminContact = post.adomAdminPhno;
 this.otherContact = post.adomOtherPhno;
 this.perPhoneNumber = post.adomPphno;
 this.roadName = post.adomProad;
 this.stateName = post.adomPstate;
 this.adminEmail = post.adomAdminMail;
 this.otherEmail = post.adomOtherMail;
}

// on reset page
onResetOrgMaster(){
  this.orgCode = "";
  this.orgName = "";
  this.nameOfRoad = "";
  this.cityName = "";
  this.emailId = "";
  this.areaName = "";
  this.pinCode = "";
  this.phNumber = "";
  this.panNo = "";
  this.cstNo = "";
  this.tdCanNo = "";
  this.tdsCircle = "";
  this.placeName = "";
  this.tanNo = "";
  this.vatNo = "";
  this.tcanNo = "";
  this.accountGroupNo = "";
  this.estCodeNo = "";
  this.personalName = "";
  this.flatNo = "";
  this.areaNo = "";
  this.perPinCode = "";
  this.adminName = "";
  this.otherName = "";
  this.designation = "";
  this.nameOfPremises = "";
  this.villName = "";
  this.perEmailId = "";
  this.adminContact = "";
  this.otherContact = "";
  this.perPhoneNumber = "";
  this.roadName = "";
  this.stateName = "";
  this.adminEmail = "";;
  this.otherEmail = "";;
}


// Filter Org Name
matchOrgName:string;
filterOrgName(){
  if(this.matchOrgName) {
    this.filteredOrgMstList = _.filter(this.viewOrgMstList, (a)=>a.adomOrgname.toUpperCase().indexOf(this.matchOrgName.toUpperCase())>=0);
  } else {
    this.filteredOrgMstList = this.viewOrgMstList  ;
  }
}

// Filter City Name
matchCityName:string;
filterCityName(){
  var filtList=[];
  for(var i=0;i<this.viewOrgMstList.length;i++){
    if(this.viewOrgMstList[i].adomCity !=undefined){
      filtList.push(this.viewOrgMstList[i]);
    }
  }
  if(this.matchCityName != undefined && this.matchCityName != ""){
    this.filteredOrgMstList = _.filter(filtList , (a)=>a.adomCity.toUpperCase().indexOf(this.matchCityName.toUpperCase())>=0);
  }
  else{
  this.filteredOrgMstList  = this.viewOrgMstList;
  }
}

// Filter Org Code
matchCode:string;
filterCode(){
  if(this.matchCode) {
    this.filteredOrgMstList = _.filter(this.viewOrgMstList, (a)=>a.adomOrgcd.toUpperCase().indexOf(this.matchCode.toUpperCase())>=0);
  } else {
    this.filteredOrgMstList = this.viewOrgMstList  ;
  }
}



}
