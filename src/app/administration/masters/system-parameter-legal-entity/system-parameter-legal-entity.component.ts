import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-system-parameter-legal-entity',
  templateUrl: './system-parameter-legal-entity.component.html',
  styleUrls: ['./system-parameter-legal-entity.component.css']
})
export class SystemParameterLegalEntityComponent implements OnInit {
  public shown1:any=true;
  public shown3:any=false;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  advanceBtn(){
    this.shown1=false;
    this.shown3=true;
  }
  basicBtn(){
    this.shown3=false;
    this.shown1=true;
  }

}
