import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemParameterLegalEntityComponent } from './system-parameter-legal-entity.component';

describe('SystemParameterLegalEntityComponent', () => {
  let component: SystemParameterLegalEntityComponent;
  let fixture: ComponentFixture<SystemParameterLegalEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemParameterLegalEntityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemParameterLegalEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
