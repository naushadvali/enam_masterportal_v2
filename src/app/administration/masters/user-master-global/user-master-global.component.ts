import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-user-master-global',
  templateUrl: './user-master-global.component.html',
  styleUrls: ['./user-master-global.component.css']
})
export class UserMasterGlobalComponent implements OnInit {
  tableData=[];
   selectedRow : any;
  // setClickedRow : Function;
  toSaveData=[];
  toSavetable=[];
  flag:any;
  rightFullTable=[];
  rightSelectedRow:any;
  leftCurrentIndex:any;
  leftCurrentPost:any;
  copy1=[];
  newLeftPushedData=[];
  y=[];
  copyOfRight=[];
  public my_class1='overlay';
  reportFromDate:string;
  reportToDate:string;
  dateInsert:any;
  dateChange:any;
  monthChnage:any;
  dateChange1:any;
  monthChnage1:any;
  fromDate:string;
  toDate:string;

  displayname:any;
  loginId:any;
  phone:any;
  password:any;
  email:any;
  operatingUnit:any;
  menu:any;
  userType:any;
  code:any;
  user:any;

  OUnit:any;
  OMenu:any;
  OUserType:any;
  OCode:any;
  OUser:any;
  dateInsert1:any;
  dateInsert2:any;


constructor(
  private _dataTable:DataTableModule,
  private authservice:AuthService,
  private flashMessagesService: FlashMessagesService,
) {
}

    leftArray =[];
    rightArray =[];
    selRightRole = [];
    selLeftRole = [];
    ngOnInit() {
      this.tableData=this.authservice.getData();
      this.copy1=this.tableData;

      this.leftArray = this.authservice.getData();
      this.rightArray = [];
      this.disableRightArrow = true;
      this.disableLeftArrow = true;
    }
    public myDatePickerOptions: IMyDpOptions = {
      dateFormat: 'dd/mm/yyyy',
      editableDateField:false,
      openSelectorOnInputClick:true,
      inline:false,
    };
    // setClickedRow(post:any,index){
    //
    //   this.selectedRow=index;
    //   //console.log("Post is",index,post)
    //   //this.toSaveData.push(post);
    //   //console.log("Table data are in setclick",this.copy1);
    //   this.leftCurrentPost=post;
    //   this.selRole=post;
    //   console.log("sel row",this.selRole)
    // }

    pushedData(){
      this.flag=1;
      //console.log("Table data are on click",this.copy1);
      console.log("Left current role",this.leftCurrentPost);

      for(var i=0;i<this.copy1.length;i++){
        if(this.copy1[i].role==this.leftCurrentPost.role){
            this.copy1.splice(i,1);
            console.log("remaining data in copy1",this.copy1);
            break;
        }

      }
      this.toSavetable.push(this.leftCurrentPost);
      console.log("saved right data",this.toSavetable);
      this.y=this.toSavetable;

      console.log("Flag 1 called");
    }
    pushAlltoRight(){
      this.flag=2;
      console.log("Flag 2 called");
      for(var i=0;i<this.copy1.length;i++){

          this.y.push(this.copy1[i]);
          console.log("value in Y",this.y);
        }
      this.rightFullTable=this.y;
      for(var i=0;i<this.copy1.length;i++){
            this.copy1.splice(i,this.copy1.length);
            break;
        }
      console.log("right table data",this.rightFullTable);
      this.copyOfRight=this.rightFullTable;

    }
    getClickedRow(post:any,index){
      //console.log("x value",index,post);
      this.selectedRow=index;
      this.rightSelectedRow=post;
      console.log("The right data selected to push",this.rightSelectedRow);
    }
    pushLeft(){
      this.flag=3;
      // console.log("1",this.copyOfRight);
      // console.log("2",this.rightFullTable);

      // for(var i=0;i<this.copyOfRight.length;i++){
      //     for(var j=0;j<this.rightFullTable.length;j++){
      //         if(this.rightSelectedRow.role==this.rightFullTable[j].role){
      //               this.copyOfRight.splice(i,1);
      //               console.log("remaining data in rightFullTable",this.copyOfRight);
      //               break;
      //         }
      //     }
      //   this.rightFullTable.push(this.copyOfRight);
      //
      // }
      // for(var i=0;i<this.copyOfRight.length;i++){
      //   if(this.rightSelectedRow.role==this.copyOfRight[i].role){
      //                 this.copyOfRight.splice(i,1);
      //                 this.newLeftPushedData.push(this.copyOfRight[i]);
      //                 console.log("remaining data in rightFullTable",this.copyOfRight,this.newLeftPushedData);
      //                 break;
      //           }
      // }



      console.log("pushed left is",this.newLeftPushedData);
      console.log("Flag 3 called");
    }

    // Dropdown selections
    onSelectOperatingUnit(oprUnit)
    {
      this.operatingUnit=oprUnit;
      this.OUnit=this.operatingUnit;
      console.log("operating unit",this.operatingUnit);
    }
    OnSelectMenu(menu){
      this.menu=menu;
      this.OMenu=menu;
       console.log("Menu selected",menu);
    }
    OnSelectUserType(userType){
      this.userType=userType;
      this.OUserType=this.userType;
      console.log("Usertype",userType);
    }
    onSelectCode(code){
      this.code=code;
      this.OCode=this.code;
      console.log("selected code is",code);
    }
    onSelectUser(user){
      this.user=user;
      this.OUser=this.user;
      console.log("selected user",user);
    }


    // Popup show hide feature
    div_show1(){
        this.my_class1='overlay1';
      }
    closeButton1(){
      this.my_class1='overlay';
    }

    // On click Save button
    savingData(){

                //for from date
                const reportFromDate={
                  day:this.reportFromDate["date"]["day"],
                  month:this.reportFromDate["date"]["month"],
                  year:this.reportFromDate["date"]["year"]
                }



            	  this.dateChange=this.reportFromDate["date"]["day"];
            	  for(var i=1;i<=9;i++){
            		  if(this.dateChange==i){
            			  let zero=0;
            			  let x:any;
            			  let y:any;

            			  x=zero.toString()+(this.dateChange).toString();
                    this.dateChange=x;
            		  }
            	  }
          	  this.monthChnage=this.reportFromDate["date"]["month"];

          	  for(var j=1;j<=9;j++){
          		  if(this.monthChnage==j){
          			  let zero1=0;
          			  let m:any;

          			  m=zero1.toString()+(this.monthChnage).toString();

          			  this.monthChnage=m;
          		  }
          	  }

                this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];


                //For to date
                const reportToDate={
                  day:this.reportToDate["date"]["day"],
                  month:this.reportToDate["date"]["month"],
                  year:this.reportToDate["date"]["year"]
                }


            	  this.dateChange1=this.reportToDate["date"]["day"];
            	  for(var i=1;i<=9;i++){
            		  if(this.dateChange1==i){
            			  let zero=0;
            			  let x:any;
            			  let y:any;

            			  x=zero.toString()+(this.dateChange1).toString();
                    this.dateChange1=x;
            		  }
            	  }
          	  this.monthChnage1=this.reportToDate["date"]["month"];

          	  for(var j=1;j<=9;j++){
          		  if(this.monthChnage1==j){
          			  let zero1=0;
          			  let m:any;


          			  m=zero1.toString()+(this.monthChnage1).toString();

          			  this.monthChnage1=m;
          		  }
          	  }

                this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];
                //console.log("from date is",this.fromDate);
                this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];
                //console.log("to date is",this.toDate);


                const dateJson={
                  fromDate:this.fromDate,
                  toDate:this.toDate
                }

            // getall datas
            this.displayname;
            this.loginId=this.authservice.getStorageData('loginID');
            this.phone;
            this.password;
            this.email;
            console.log("on click operating unit value  ",this.operatingUnit);
            this.menu;
            this.userType;
            this.code;
            this.user;
            this.fromDate;
            this.toDate;
            //console.log(this.displayname,this.loginId,this.phone,this.password,this.email,this.operatingUnit,this.menu,this.userType,this.code,this.user,this.fromDate,this.toDate);

    }

    // On click right arrow
    disableRightArrow:boolean;
    onClickRightArrow(){
      if(this.selRightRole.length != 0){
        this.disableRightArrow = false;
        for(var i=0;i<this.leftArray.length;i++){
          if(this.leftArray[i].role==this.selRightRole["role"]){
              this.leftArray.splice(i,1);
              break;
          }
        }
        this.rightArray.push(this.selRightRole);
      }
      else{
        this.disableRightArrow = true;
      }
      this.selectedRightRow = -1;
      this.selRightRole = [];
    }

    // On click right arrow all
    onClickRightArrowAll(){
      this.rightArray = this.authservice.getData();
      this.leftArray = [];
    }
    // On click left arrow all
    onClickLeftArrowAll(){
      this.rightArray = [];
      this.leftArray = this.authservice.getData();
    }
    // On click right arrow
    disableLeftArrow:boolean;
    onClickLeftArrow(){
      if(this.selLeftRole.length != 0){
        this.disableLeftArrow = false;
        for(var i=0;i<this.rightArray.length;i++){
          if(this.rightArray[i].role==this.selLeftRole["role"]){
              this.rightArray.splice(i,1);
              break;
          }
        }
        this.leftArray.push(this.selLeftRole);
      }
      else{
        this.disableLeftArrow = true;
      }
      this.selectedLeftRow = -1;
      this.selLeftRole = [];
    }
    // click right row
    selectedRightRow:number;
    setRightClickedRow(post,index){
      this.selRightRole = post;
      this.selectedRightRow = index;
      this.disableRightArrow = false;
    }
    // click left row
    selectedLeftRow:number;
    setLeftClickedRow(post,index){
      this.selLeftRole = post;
      this.selectedLeftRow = index;
      this.disableLeftArrow = false;
    }
}
