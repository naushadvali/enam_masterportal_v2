import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMasterGlobalComponent } from './user-master-global.component';

describe('UserMasterGlobalComponent', () => {
  let component: UserMasterGlobalComponent;
  let fixture: ComponentFixture<UserMasterGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMasterGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMasterGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
