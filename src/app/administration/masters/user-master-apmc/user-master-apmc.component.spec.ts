import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMasterApmcComponent } from './user-master-apmc.component';

describe('UserMasterApmcComponent', () => {
  let component: UserMasterApmcComponent;
  let fixture: ComponentFixture<UserMasterApmcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMasterApmcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMasterApmcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
