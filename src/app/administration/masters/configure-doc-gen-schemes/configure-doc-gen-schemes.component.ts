import { Component, OnInit } from '@angular/core';

import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-configure-doc-gen-schemes',
  templateUrl: './configure-doc-gen-schemes.component.html',
  styleUrls: ['./configure-doc-gen-schemes.component.css']
})
export class ConfigureDocGenSchemesComponent implements OnInit {

  public my_class1='overlay';
  public shown1:any=false;
  public shown2:any=true;
  dateInsert:any;

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false,
  };

  docTypeAdv(){
    this.shown1=true;
    this.shown2=false;
  }

  docTypeBas(){
    this.shown1=false;
    this.shown2=true;
  }

  div_show1(){
        this.my_class1='overlay1';
      }
  div_hide1(){
        this.my_class1='overlay';
      }

}
