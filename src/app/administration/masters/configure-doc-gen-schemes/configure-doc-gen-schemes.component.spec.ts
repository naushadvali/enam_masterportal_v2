import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureDocGenSchemesComponent } from './configure-doc-gen-schemes.component';

describe('ConfigureDocGenSchemesComponent', () => {
  let component: ConfigureDocGenSchemesComponent;
  let fixture: ComponentFixture<ConfigureDocGenSchemesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureDocGenSchemesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureDocGenSchemesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
