import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMasterStateComponent } from './user-master-state.component';

describe('UserMasterStateComponent', () => {
  let component: UserMasterStateComponent;
  let fixture: ComponentFixture<UserMasterStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMasterStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMasterStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
