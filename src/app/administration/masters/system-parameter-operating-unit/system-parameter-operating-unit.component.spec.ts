import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemParameterOperatingUnitComponent } from './system-parameter-operating-unit.component';

describe('SystemParameterOperatingUnitComponent', () => {
  let component: SystemParameterOperatingUnitComponent;
  let fixture: ComponentFixture<SystemParameterOperatingUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemParameterOperatingUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemParameterOperatingUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
