import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-system-parameter-operating-unit',
  templateUrl: './system-parameter-operating-unit.component.html',
  styleUrls: ['./system-parameter-operating-unit.component.css']
})
export class SystemParameterOperatingUnitComponent implements OnInit {
  public shown1:any=true;
  public shown3:any=false;
  public shown4:any=true;
  public shown5:any=false;
  public shown6:any=true;
  public shown7:any=false;
  public my_class1='overlay';
  public my_class2='overlay';

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }
  advanceBtn(){
    this.shown1=false;
    this.shown3=true;
  }
  basicBtn(){
    this.shown3=false;
    this.shown1=true;
  }
  ParamAdv(){
    this.shown5=true;
    this.shown4=false;
  }

  ParamBas(){
    this.shown5=false;
    this.shown4=true;
  }

  ModAdv(){
    this.shown7=true;
    this.shown6=false;
  }

  ModBas(){
    this.shown7=false;
    this.shown6=true;
  }


  div_show1(){
      this.my_class1='overlay1';
    }
  div_hide1(){
      this.my_class1='overlay';
      }
  div_show2(){
      this.my_class2='overlay1';
    }
  div_hide2(){
      this.my_class2='overlay';
      }
}
