import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterSetupReportComponent } from './master-setup-report.component';

describe('MasterSetupReportComponent', () => {
  let component: MasterSetupReportComponent;
  let fixture: ComponentFixture<MasterSetupReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterSetupReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterSetupReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
