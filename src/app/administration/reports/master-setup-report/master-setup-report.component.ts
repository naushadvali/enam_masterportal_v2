import { Component, OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../../../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-master-setup-report',
  templateUrl: './master-setup-report.component.html',
  styleUrls: ['./master-setup-report.component.css']
})
export class MasterSetupReportComponent implements OnInit {

  public my_class1='overlay';
  public my_class2='overlay';

  constructor(
   private _dataTable:DataTableModule,
   private authservice:AuthService,
   private flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit() {
  }

  div_show1(){
      this.my_class1='overlay1';
    }
div_hide1(){
      this.my_class1='overlay';
    }
    div_show2(){
        this.my_class2='overlay1';
      }
  div_hide2(){
        this.my_class2='overlay';
      }
}
