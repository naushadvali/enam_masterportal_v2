import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApmcBankdetailsReportComponent } from './apmc-bankdetails-report.component';

describe('ApmcBankdetailsReportComponent', () => {
  let component: ApmcBankdetailsReportComponent;
  let fixture: ComponentFixture<ApmcBankdetailsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApmcBankdetailsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApmcBankdetailsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
