import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({

  selector: 'bankdetails',
  templateUrl: 'bankdetails.component.html',
  styleUrls: ['bankdetails.component.css']
})
export class BankdetailsComponent {

  bankData:any=[];
  bankdetailsError:any;
  constructor(private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,)
  {
    //BANK DETAILS
    // this.bankdetailsError=sessionStorage.getItem('bankdetailsError');
    // if(this.bankdetailsError==0){
    //     this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
    //   }
    this.authservice.getBankDetails().subscribe
    (
      $data=>{
        this.bankData=$data.data;
        //console.log("bankData comes here: ",this.bankData);
      }
      );
    }

  }
