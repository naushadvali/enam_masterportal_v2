// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  // admin:"http://192.168.0.133:8080/enam/api/admin/",
  // testUrl:"http://staging.techl33t.com:8181/enam/api/view/",
  // authUrl:"http://staging.techl33t.com:8181/enam/api/view/",
  // loginUrl:"http://192.168.0.133:8080/enam/api/login",
  // logoutUrl:"http://train.enam.gov.in/enam/api/logout",
  // bidauthUrl:"http://staging.techl33t.com:8181/enam/api/view/newBidList/",
  // getpdf:"http://staging.techl33t.com:8181/enam/api/view/pdf",
  // addprefer:"http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity",
  // bidnow:"http://staging.techl33t.com:8181/enam/api/view/bidNow",
  // eng:"http://staging.techl33t.com/enam/dist/",
  // hi:"http://staging.techl33t.com/enam/dist/hi",
  // gu:"http://staging.techl33t.com/enam/dist/gu",
  // te:"http://staging.techl33t.com/enam/dist/te"
  admin:"http://train.enam.gov.in/enam_admin/api/admin/",
  testUrl:"http://train.enam.gov.in/enam_admin/api/view/",
  authUrl:"http://train.enam.gov.in/enam_admin/api/view/",
  loginUrl:"http://train.enam.gov.in/enam_admin/api/login",
  logoutUrl:"http://train.enam.gov.in/enam_admin/api/logout",
  bidauthUrl:"http://train.enam.gov.in/enam_admin/api/view/newBidList/",
  getpdf:"http://train.enam.gov.in/enam_admin/api/view/pdf",
  addprefer:"http://train.enam.gov.in/enam_admin/api/view/onAddPrefferedCommodity",
  bidnow:"http://train.enam.gov.in/enam_admin/api/view/bidNow",
  eng:"http://train.enam.gov.in/admin/",
  hi:"http://train.enam.gov.in/admin/hi",
  gu:"http://train.enam.gov.in/admin/gu",
  te:"http://train.enam.gov.in/admin/te"
};

// export const environment = {
//   production: true,
//   testUrl:"http://192.168.0.108:8181/enam/api/view/",
//   authUrl:"http://192.168.0.108:8181/enam/api/view/",
//   loginUrl:"http://192.168.0.108:8181/enam/api/login",
//   logoutUrl:"http://192.168.0.108:8181/enam/api/logout",
//   bidauthUrl:"http://192.168.0.108:8181/enam/api/view/newBidList/",
//   getpdf:"http://192.168.0.108:8181/enam/api/view/pdf",
//   addprefer:"http://192.168.0.108:8181/enam/api/view/onAddPrefferedCommodity",
//   bidnow:"http://192.168.0.108:8181/enam/api/view/bidNow",
//   eng:"http://192.168.0.108/enam/dist/",
//   hi:"http://192.168.0.108/enam/dist/hi",
//   gu:"http://192.168.0.108/enam/dist/gu",
//   te:"http://192.168.0.108/enam/dist/te"
// };
// export const environment = {
//   production: true,
//   admin:"http://train.enam.gov.in/enam/api/view/admin/",
//   testUrl:"http://train.enam.gov.in/enam/api/view/",
//   authUrl:"http://train.enam.gov.in/enam/api/view/",
//   loginUrl:"http://train.enam.gov.in/enam/api/login",
//   logoutUrl:"http://train.enam.gov.in/enam/api/logout",
//   bidauthUrl:"http://train.enam.gov.in/enam/api/view/newBidList/",
//   getpdf:"http://train.enam.gov.in/enam/api/view/pdf",
//   addprefer:"http://train.enam.gov.in/enam/api/view/onAddPrefferedCommodity",
//   bidnow:"http://train.enam.gov.in/enam/api/view/bidNow",
//   eng:"http://train.enam.gov.in/enam/dist/",
//   hi:"http://train.enam.gov.in/enam/dist/hi",
//   gu:"http://train.enam.gov.in/enam/dist/gu",
//   te:"http://train.enam.gov.in/enam/dist/te"
// };
