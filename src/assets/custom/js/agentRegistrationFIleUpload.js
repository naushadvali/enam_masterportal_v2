function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image')
                    .attr('src', e.target.result)
                    .width(330)
                    .height(227);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
