import { EnamMasterPortalPage } from './app.po';

describe('enam-master-portal App', () => {
  let page: EnamMasterPortalPage;

  beforeEach(() => {
    page = new EnamMasterPortalPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
